import axios, { AxiosRequestConfig, AxiosError } from 'axios'
import { ApiKeyName } from './constants'
import { SayHey } from './types'

function checkApiKey(config: AxiosRequestConfig) {
  if (!config.headers[ApiKeyName]) {
    throw new SayHey.Error(
      'Missing api key in request header',
      SayHey.ErrorType.InternalErrorType.AuthMissing
    )
  }
  return config
}

function checkaccessToken(config: AxiosRequestConfig) {
  if (!config.headers.Authorization) {
    throw new SayHey.Error(
      'Missing auth token in request header',
      SayHey.ErrorType.InternalErrorType.AuthMissing
    )
  }
  return config
}

function requestFailed(error: AxiosError) {
  const { response } = error
  const err = new SayHey.Error(response?.data.message, response?.data.type)
  err.setStatusCode(response?.status)
  err.setErrorActions(response?.data.actions || {})
  throw err
}

export default {
  createWithoutAuth: (config: { baseURL: string; apiKey: string }) => {
    const { baseURL, apiKey } = config
    const instance = axios.create({
      baseURL,
      headers: {
        [ApiKeyName]: apiKey
      }
    })
    instance.interceptors.request.use(checkApiKey)
    instance.interceptors.response.use(undefined, requestFailed)
    return instance
  },
  createWithAuth: (config: { baseURL: string; apiKey: string; accessToken: string }) => {
    const { baseURL, accessToken, apiKey } = config
    const instance = axios.create({
      baseURL,
      headers: {
        [ApiKeyName]: apiKey,
        Authorization: `Bearer ${accessToken}`
      }
    })
    instance.interceptors.request.use(checkApiKey)
    instance.interceptors.request.use(checkaccessToken)
    instance.interceptors.response.use(undefined, requestFailed)
    return instance
  }
}
