import { Routes, ApiKeyName, API_VERSION, MESSAGE_READ_UPDATE_INTERVAL } from './constants'
import httpClient from './axios'
import StaticAxios, { AxiosInstance, AxiosPromise } from 'axios'
import {
  ConversationDomainEntity,
  MessageDomainEntity,
  AuthResponse,
  MessageType,
  HttpMethod,
  MessageDTO,
  SayHey,
  ConversationDTO,
  GalleryPayload,
  MediaDomainEntity,
  ConversableUsersBatchResult,
  ConversableUsersBatchDTO,
  ReachableUsersQuery,
  ConversationUserQuery,
  ConversationUserQueryType,
  ConversationReachableUsersQuery,
  ConversationUsersBatchDTO,
  Publisher,
  SendMessageParams,
  ConversationUsersBatch,
  ReactionType,
  ConversationReactionsDomainEntity,
  UserSelfDTO,
  UserSelfDomainEntity,
  DefaultGroupImageUrlSet,
  FileUpload,
  ConversationUsersBasicDTO,
  ReactedUserDTO
} from './types'
import { ok, err, Result } from 'neverthrow'
import {
  ConversationMapper,
  MessageMapper,
  UserMapper,
  MediaMapper,
  ReactionMapper
} from './mapper'
import decode from 'jwt-decode'
import PublisherKitClient from 'php-sayhey-publisher-kit-client'
import { isUrl, mimeTypes } from './helper'
export { isSingleEmoji, parseText } from './helper'

export * from './types'

export default class ChatKit {
  static accessToken: string

  static publisherToken: string

  static refreshToken: string

  static instanceIdPushNotification: string = ''

  static readonly apiVersion: string = API_VERSION

  static httpClient: AxiosInstance

  static appId: string

  static apiKey: string

  static serverUrl: string

  static authDetailChangeListener: (res: AuthResponse) => Promise<void> | void

  static eventListener: (event: SayHey.Event) => void

  static sessionExpiredListener?: () => void

  private static _initialized: boolean = false

  private static _subscribed: boolean = false

  public static isUserAuthenticated: boolean = false

  private static readMessageMap: { [conversationId: string]: string } = {}

  static readMessageSequenceMap: { [conversationId: string]: number } = {}

  private static isSendingReadReceipt: boolean = false

  private static timerHandle: any = null

  private static exchangeTokenPromise: Promise<Result<boolean, SayHey.Error>> | null = null

  private static disableAutoRefreshToken?: boolean

  private static disableUserDisabledHandler?: boolean

  private static disableUserDeletedHandler?: boolean

  private static loginId: number = 0

  private constructor() {}

  /**
   * Helper methods
   */

  /**
   *
   * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
   */
  private static async guard<T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> {
    if (!ChatKit._initialized) {
      return err(
        new SayHey.Error(
          'ChatKit has not been initialized!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    if (!ChatKit._subscribed) {
      return err(
        new SayHey.Error(
          'ChatKit events have not been subscribed to!',
          SayHey.ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    try {
      const res = await cb()
      if (res instanceof SayHey.Error) {
        return err(res)
      }
      return ok(res)
    } catch (e) {
      if (e instanceof SayHey.Error) {
        return err(e)
      }
      return err(new SayHey.Error('Unknown error', SayHey.ErrorType.InternalErrorType.Unknown))
    }
  }

  public static async exchangeTokenOnce(): Promise<Result<boolean, SayHey.Error>> {
    if (ChatKit.exchangeTokenPromise) {
      return ChatKit.exchangeTokenPromise
    }
    ChatKit.exchangeTokenPromise = ChatKit.exchangeToken()
    const response = await ChatKit.exchangeTokenPromise
    ChatKit.exchangeTokenPromise = null
    return response
  }

  public static isSocketConnected(): boolean {
    return PublisherKitClient.isConnected()
  }

  /**
   *
   * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
   */
  private static async guardWithAuth<T>(
    cb: (...args: any[]) => Promise<T | SayHey.Error>
  ): Promise<Result<T, SayHey.Error>> {
    // Capturing current login id
    const currentLoginId = ChatKit.loginId
    const res = await ChatKit.guard(cb)
    if (res.isErr()) {
      if (
        !ChatKit.disableAutoRefreshToken &&
        res.error.type === SayHey.ErrorType.AuthErrorType.TokenExpiredType
      ) {
        // If token expires check if token has been exchanged since this call started
        if (currentLoginId === ChatKit.loginId) {
          const response = await ChatKit.exchangeTokenOnce()
          if (response.isOk()) {
            return ChatKit.guard(cb)
          }
          ChatKit.signOut()
        } else {
          // If token has been exchanged, use the new credentials to make the request again
          return ChatKit.guard(cb)
        }
      }
    }
    return res
  }

  /**
   *
   * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
   * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
   * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
   */
  private static async updateAuthDetails(
    accessToken: string,
    publisherToken: string,
    refreshToken: string
  ) {
    ChatKit.accessToken = accessToken
    ChatKit.publisherToken = publisherToken
    ChatKit.refreshToken = refreshToken
    const id = accessToken ? decode<any>(accessToken).id : ''
    if (publisherToken && accessToken && refreshToken) {
      ChatKit.loginId += 1
      ChatKit.httpClient = httpClient.createWithAuth({
        apiKey: ChatKit.apiKey,
        baseURL: `${ChatKit.serverUrl}/${ChatKit.apiVersion}/apps/${ChatKit.appId}`,
        accessToken: ChatKit.accessToken
      })
      PublisherKitClient.updateAccessParams({
        token: publisherToken,
        userId: id
      })
      if (!PublisherKitClient.isConnected()) {
        PublisherKitClient.connect()
      }
    }
    await ChatKit.authDetailChangeListener({
      userId: id,
      accessToken: accessToken,
      publisherToken: publisherToken,
      refreshToken: refreshToken
    })
  }

  /**
   *
   * @param urlPath - url path returned from SayHey
   */
  public static getMediaSourceDetails(urlPath: string) {
    return {
      uri: `${ChatKit.serverUrl}${urlPath}`,
      method: 'GET',
      headers: {
        [ApiKeyName]: ChatKit.apiKey,
        Authorization: `Bearer ${ChatKit.accessToken}`,
        Accept: 'image/*, video/*, audio/*'
      }
    }
  }

  public static async getMediaDirectUrl(
    params:
      | {
          type: 'image'
          urlPath: string
          size: 'tiny' | 'small' | 'medium' | 'large' | 'xlarge'
        }
      | {
          type: 'other'
          urlPath: string
        }
  ): Promise<Result<string, SayHey.Error>> {
    async function getMediaDirectUrl() {
      const queryString: any = {
        noRedirect: true
      }
      if (params.type === 'image') {
        queryString.size = params.size
      }
      const { data } = await ChatKit.httpClient({
        url: `${ChatKit.serverUrl}${params.urlPath}`,
        method: 'GET',
        params: queryString
      })
      return data.url as string
    }
    return ChatKit.guardWithAuth(getMediaDirectUrl)
  }

  private static async uploadFile(
    fileUri: string | Blob,
    fileName: string,
    progress?: (percentage: number) => void,
    thumbnailId?: string
  ): Promise<Result<string, SayHey.Error>> {
    async function uploadFile() {
      const {
        UploadFile: { Endpoint, Method }
      } = Routes
      const formData = new FormData()
      if (fileUri instanceof Blob) {
        formData.append('file', fileUri, fileName)
        if (thumbnailId) {
          formData.append('thumbnailId', thumbnailId)
        }
      } else {
        const extension = fileName.split('.').pop()
        if (extension) {
          formData.append('file', {
            uri: fileUri,
            name: fileName,
            type: mimeTypes[extension.toLowerCase()]
          } as any)
          if (thumbnailId) {
            formData.append('thumbnailId', thumbnailId)
          }
        } else {
          return new SayHey.Error('Invalid file')
        }
      }
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: formData,
        onUploadProgress: progress
          ? (event: ProgressEvent) => {
              const { loaded, total } = event
              progress((loaded / total) * 0.95)
            }
          : undefined
      })
      progress?.(1)
      return data.id
    }
    return ChatKit.guardWithAuth(uploadFile)
  }

  public static async uploadImage(
    fileUri: string | Blob,
    fileName: string
  ): Promise<Result<string, SayHey.Error>> {
    const extension = fileName
      .split('.')
      .pop()
      ?.toLowerCase()
    const isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image'
    if (isSupportedImage) {
      return ChatKit.uploadFile(fileUri, fileName)
    }
    return err(
      new SayHey.Error('Please select a valid image!', SayHey.ErrorType.InternalErrorType.BadData)
    )
  }

  /**
   * Setup methods
   */

  public static initialize(params: {
    sayheyAppId: string
    sayheyApiKey: string
    publisherAppId: string
    publisherApiKey: string
    sayheyUrl: string
    publisherUrl: string
    messageUpdateInterval?: number
    disableAutoRefreshToken?: boolean
    disableUserDisabledHandler?: boolean
    disableUserDeletedHandler?: boolean
  }) {
    const {
      sayheyAppId,
      sayheyApiKey,
      publisherApiKey,
      publisherAppId,
      sayheyUrl,
      publisherUrl,
      messageUpdateInterval,
      disableAutoRefreshToken,
      disableUserDeletedHandler,
      disableUserDisabledHandler
    } = params
    ChatKit.serverUrl = sayheyUrl
    const baseURL = `${ChatKit.serverUrl}/${ChatKit.apiVersion}/apps/${sayheyAppId}`
    ChatKit.apiKey = sayheyApiKey
    ChatKit.appId = sayheyAppId
    ChatKit.disableAutoRefreshToken = disableAutoRefreshToken
    ChatKit.disableUserDisabledHandler = disableUserDisabledHandler
    ChatKit.disableUserDeletedHandler = disableUserDeletedHandler
    ChatKit.httpClient = httpClient.createWithoutAuth({
      apiKey: ChatKit.apiKey,
      baseURL
    })
    PublisherKitClient.initialize({
      appId: publisherAppId,
      clientApiKey: publisherApiKey,
      publisherUrl: publisherUrl
    })
    if (ChatKit.timerHandle !== null) {
      clearInterval(ChatKit.timerHandle)
    }
    ChatKit.timerHandle = setInterval(() => {
      ChatKit.isSendingReadReceipt = true
      const copy = { ...ChatKit.readMessageMap }
      ChatKit.readMessageMap = {}
      ChatKit.isSendingReadReceipt = false
      for (const conversationId in copy) {
        const messageId = copy[conversationId]
        ChatKit.markMessageRead(messageId)
      }
    }, messageUpdateInterval || MESSAGE_READ_UPDATE_INTERVAL)
    ChatKit._initialized = true
  }

  public static addListeners = (listeners: {
    onAuthDetailChange: (res: AuthResponse) => Promise<void> | void
    onEvent: (event: SayHey.Event) => void
    onSocketConnect?: () => void
    onSocketDisconnect?: (reason: string) => void
    onSocketError?: (error: Error) => void
    onSocketReconnect?: (attemptNumber: number) => void
    onSessionExpired?: () => void
  }): Result<boolean, SayHey.Error> => {
    if (ChatKit._initialized) {
      const {
        onSocketConnect,
        onSocketDisconnect,
        onSocketError,
        onSocketReconnect,
        onAuthDetailChange,
        onEvent,
        onSessionExpired
      } = listeners
      ChatKit._subscribed = true
      ChatKit.authDetailChangeListener = onAuthDetailChange
      ChatKit.eventListener = onEvent
      ChatKit.sessionExpiredListener = onSessionExpired
      PublisherKitClient.onMessage(ChatKit.onMessage as any)
      onSocketConnect && PublisherKitClient.onConnect(onSocketConnect)
      onSocketDisconnect && PublisherKitClient.onDisconnect(onSocketDisconnect)
      onSocketError && PublisherKitClient.onError(onSocketError)
      onSocketReconnect && PublisherKitClient.onReconnect(onSocketReconnect)
      return ok(true)
    }
    return err(
      new SayHey.Error(
        'ChatKit has not been initialized!',
        SayHey.ErrorType.InternalErrorType.Uninitialized
      )
    )
  }

  public static async disconnectPublisher() {
    PublisherKitClient.disconnect()
  }

  public static async reconnectPublisher() {
    PublisherKitClient.connect()
  }

  /**
   * Token APIs
   */

  /**
   *
   * @param email - user email for SayHey
   * @param password - user password for SayHey
   *
   */
  public static async signIn(
    email: string,
    password: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function signInCallback() {
      const {
        SignIn: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email,
          password
        }
      })
      const { accessToken: accessToken, refreshToken, publisherToken } = data
      await ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)
      ChatKit.isUserAuthenticated = true
      return true
    }
    return ChatKit.guard(signInCallback)
  }

  public static async signInWithToken(
    accessToken: string,
    publisherToken: string,
    refreshToken: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function signInWithTokenCallback() {
      await ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)
      ChatKit.isUserAuthenticated = true
      return true
    }
    return ChatKit.guard(signInWithTokenCallback)
  }

  public static async signUp(
    email: string,
    password: string,
    firstName: string,
    lastName: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function signUp() {
      const {
        SignUp: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          email,
          password,
          firstName,
          lastName
        }
      })
      const { accessToken: accessToken, refreshToken, publisherToken } = data
      await ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)
      ChatKit.isUserAuthenticated = true
      return true
    }
    return ChatKit.guard(signUp)
  }

  public static async signOut() {
    PublisherKitClient.disconnect()
    ChatKit.isUserAuthenticated = false
    ChatKit.loginId = 0
    const unregisteringPushNotification = async () => {
      const result = await ChatKit.unregisterForPushNotification()
      if (
        result.isOk() ||
        (result.isErr() && result.error.statusCode && result.error.statusCode < 500)
      ) {
        return
      }
      setTimeout(unregisteringPushNotification, 3000)
    }
    await unregisteringPushNotification()
    await ChatKit.updateAuthDetails('', '', '')
  }

  public static async exchangeToken(): Promise<Result<boolean, SayHey.Error>> {
    async function exchangeToken() {
      try {
        const { data } = await StaticAxios({
          method: HttpMethod.Post,
          url: `${ChatKit.serverUrl}/${ChatKit.apiVersion}/tokens/exchange`,
          data: {
            refreshToken: ChatKit.refreshToken
          }
        })
        const { refreshToken, accessToken, publisherToken } = data
        if (refreshToken && accessToken) {
          await ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)
          ChatKit.isUserAuthenticated = true
        } else {
          await ChatKit.updateAuthDetails('', '', '')
          ChatKit.sessionExpiredListener?.()
          ChatKit.isUserAuthenticated = false
        }
      } catch (e) {
        await ChatKit.updateAuthDetails('', '', '')
        ChatKit.sessionExpiredListener?.()
        ChatKit.isUserAuthenticated = false
      }
      return ChatKit.isUserAuthenticated
    }
    return ChatKit.guard(exchangeToken)
  }

  public static async changePassword(
    currentPassword: string,
    newPassword: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function changePassword() {
      const {
        ChangePassword: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          currentPassword,
          newPassword
        }
      })
      return true
    }
    return ChatKit.guardWithAuth(changePassword)
  }

  /**
   * User APIs
   */

  public static async getReachableUsers({
    searchString,
    limit,
    nextCursor,
    includeRefIdInSearch
  }: ReachableUsersQuery = {}): Promise<Result<ConversableUsersBatchResult, SayHey.Error>> {
    async function getReachableUsers() {
      const {
        GetReachableUsers: { Endpoint, Method }
      } = Routes
      let url = Endpoint
      let params: any = {
        search: searchString || '',
        includeRefIdInSearch: includeRefIdInSearch || false
      }
      if (limit) {
        params.limit = limit
      }
      if (nextCursor) {
        params.offset = nextCursor
      }
      const { data } = await ChatKit.httpClient({
        url,
        method: Method,
        params
      })
      return {
        ...(data as ConversableUsersBatchDTO),
        results: UserMapper.toDomainEntities((data as ConversableUsersBatchDTO).results)
      }
    }
    return ChatKit.guardWithAuth(getReachableUsers)
  }

  public static async updateUserProfileImage(
    fileUri: string,
    fileName: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function updateUserProfileImage() {
      const fileIdResponse = await ChatKit.uploadFile(fileUri, fileName)
      if (fileIdResponse.isOk()) {
        const {
          UpdateUserProfileImage: { Endpoint, Method }
        } = Routes
        await ChatKit.httpClient({
          url: Endpoint,
          method: Method,
          data: {
            profileImageId: fileIdResponse.value
          }
        })
        return true
      }
      return false
    }
    return ChatKit.guardWithAuth(updateUserProfileImage)
  }

  public static async updateUserProfileInfo(options: {
    firstName?: string
    lastName?: string
    email?: string
  }): Promise<Result<boolean, SayHey.Error>> {
    async function updateUserProfileInfo() {
      const {
        UpdateUserProfileInfo: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: options
      })
      return true
    }
    return ChatKit.guardWithAuth(updateUserProfileInfo)
  }

  public static async getUserDetails(): Promise<Result<UserSelfDomainEntity, SayHey.Error>> {
    async function getUserDetails() {
      const {
        GetUserProfile: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return UserMapper.toSelfDomainEntity(data as UserSelfDTO)
    }
    return ChatKit.guardWithAuth(getUserDetails)
  }

  public static async muteAllNotifications(): Promise<Result<boolean, SayHey.Error>> {
    async function muteAllNotifications() {
      const {
        MuteAllNotifications: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(muteAllNotifications)
  }

  public static async unmuteAllNotifications(): Promise<Result<boolean, SayHey.Error>> {
    async function unmuteAllNotifications() {
      const {
        UnmuteAllNotifications: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(unmuteAllNotifications)
  }

  /**
   * Conversation APIs
   */

  public static async getConversations(): Promise<
    Result<ConversationDomainEntity[], SayHey.Error>
  > {
    async function getConversations() {
      const {
        GetConversations: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return ConversationMapper.toDomainEntities(data)
    }
    return ChatKit.guardWithAuth(getConversations)
  }

  public static async getConversationDetails(
    conversationId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> {
    async function getConversationDetails() {
      const {
        GetConversationDetails: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })

      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return ChatKit.guardWithAuth(getConversationDetails)
  }

  public static async checkForExistingConversation(
    withUserId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> {
    async function checkForExistingConversation() {
      const {
        GetExistingConversationWithUser: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(withUserId),
        method: Method
      })
      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return ChatKit.guardWithAuth(checkForExistingConversation)
  }

  public static async createIndividualConversation(
    withUserId: string
  ): Promise<Result<ConversationDomainEntity, SayHey.Error>> {
    async function createIndividualConversation() {
      const {
        CreateIndividualConversation: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          memberId: withUserId
        }
      })
      return ConversationMapper.toDomainEntity(data as ConversationDTO)
    }
    return ChatKit.guardWithAuth(createIndividualConversation)
  }

  public static async createGroupConversation(params: {
    groupName: string
    file: FileUpload
    userIds: string[]
  }): Promise<Result<ConversationDomainEntity, SayHey.Error>> {
    async function createGroupConversation() {
      const {
        CreateGroupConversation: { Endpoint, Method }
      } = Routes
      const { file, groupName, userIds } = params
      const fileIdResponse = await ChatKit.uploadFile(file.path, file.fileName)
      if (fileIdResponse.isOk()) {
        const { data } = await ChatKit.httpClient({
          url: Endpoint,
          method: Method,
          data: {
            groupName,
            groupImageId: fileIdResponse.value,
            memberIds: userIds
          }
        })
        return ConversationMapper.toDomainEntity(data as ConversationDTO)
      }
      return fileIdResponse.error
    }
    return ChatKit.guardWithAuth(createGroupConversation)
  }

  public static async muteConversation(
    conversationId: string
  ): Promise<Result<string, SayHey.Error>> {
    async function muteConversation() {
      const {
        MuteConversation: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return data.muteUntil as string
    }
    return ChatKit.guardWithAuth(muteConversation)
  }

  public static async unmuteConversation(
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function unmuteConversation() {
      const {
        UnmuteConversation: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(unmuteConversation)
  }

  public static async pinConversation(
    conversationId: string
  ): Promise<Result<string, SayHey.Error>> {
    async function pinConversation() {
      const {
        PinConversation: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return data.pinnedAt as string
    }
    return ChatKit.guardWithAuth(pinConversation)
  }

  public static async unpinConversation(
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function unpinConversation() {
      const {
        UnpinConversation: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(unpinConversation)
  }

  public static async deleteConversation(
    conversationId: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function deleteConversation() {
      const {
        DeleteConversation: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(deleteConversation)
  }

  public static async deleteAllConversations(): Promise<Result<boolean, SayHey.Error>> {
    async function deleteAllConversations() {
      const {
        DeleteAllConversations: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return true
    }
    return ChatKit.guardWithAuth(deleteAllConversations)
  }

  public static async updateGroupImage(
    conversationId: string,
    file: FileUpload
  ): Promise<Result<boolean, SayHey.Error>> {
    async function updateGroupImage() {
      const fileIdResponse = await ChatKit.uploadFile(file.path, file.fileName)
      if (fileIdResponse.isOk()) {
        const {
          UpdateGroupImage: { Endpoint, Method }
        } = Routes

        await ChatKit.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          data: {
            groupImageId: fileIdResponse.value
          }
        })
        return true
      }
      return false
    }
    return ChatKit.guardWithAuth(updateGroupImage)
  }

  public static async updateGroupInfo(
    conversationId: string,
    groupName: string
  ): Promise<Result<boolean, SayHey.Error>> {
    async function updateGroupInfo() {
      const {
        UpdateGroupInfo: { Endpoint, Method }
      } = Routes

      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          groupName
        }
      })
      return true
    }
    return ChatKit.guardWithAuth(updateGroupInfo)
  }

  public static async getConversationUsers({
    conversationId,
    searchString,
    limit,
    nextCursor,
    type,
    includeRefIdInSearch
  }: ConversationUserQuery): Promise<Result<ConversationUsersBatch, SayHey.Error>> {
    async function getConversationUsers() {
      const {
        GetConversationUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type: type || ConversationUserQueryType.Current,
          offset: nextCursor,
          limit,
          search: searchString,
          includeRefIdInSearch: includeRefIdInSearch || false
        }
      })
      return {
        ...(data as ConversationUsersBatchDTO),
        results: UserMapper.toConversableUserDomainEntities(
          (data as ConversationUsersBatchDTO).results
        )
      }
    }
    return ChatKit.guardWithAuth(getConversationUsers)
  }

  public static async getConversationReachableUsers({
    conversationId,
    searchString,
    limit,
    nextCursor,
    includeRefIdInSearch
  }: ConversationReachableUsersQuery): Promise<Result<ConversableUsersBatchResult, SayHey.Error>> {
    async function getConversationReachableUsers() {
      const {
        GetConversationReachableUsers: { Endpoint, Method }
      } = Routes
      let url = Endpoint(conversationId)
      const { data } = await ChatKit.httpClient({
        url,
        method: Method,
        params: {
          limit,
          search: searchString,
          offset: nextCursor,
          includeRefIdInSearch: includeRefIdInSearch || false
        }
      })
      return {
        ...(data as ConversableUsersBatchDTO),
        results: UserMapper.toDomainEntities((data as ConversableUsersBatchDTO).results)
      }
    }
    return ChatKit.guardWithAuth(getConversationReachableUsers)
  }

  public static async getConversationBasicUsers(params: {
    conversationId: string
    type?: 'all' | 'current' | 'default'
  }): Promise<Result<ConversationUsersBasicDTO[], SayHey.Error>> {
    const { conversationId, type = 'current' } = params
    return ChatKit.guard(async function getConversationUsers() {
      const {
        GetConversationBasicUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          type
        }
      })
      return data as ConversationUsersBasicDTO[]
    })
  }

  public static async addUsersToConversation(
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> {
    async function addUsersToConversation() {
      const {
        AddUsersToConversation: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    }
    return ChatKit.guardWithAuth(addUsersToConversation)
  }

  public static async removeUsersFromConversation(
    conversationId: string,
    userIds: string[],
    force?: boolean
  ): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        RemoveUsersFromConversation: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          memberIds: userIds
        },
        params: {
          force
        }
      })
      return true
    })
  }

  public static async makeConversationOwner(
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        MakeConversationOwner: { Endpoint, Method }
      } = Routes

      await ChatKit.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })
  }

  public static async removeConversationOwner(
    conversationId: string,
    userId: string
  ): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        RemoveConversationOwner: { Endpoint, Method }
      } = Routes

      await ChatKit.httpClient({
        url: Endpoint(conversationId, userId),
        method: Method
      })
      return true
    })
  }

  /**
   * @deprecated The method should not be used
   */
  public static async getUserConversationReactions(
    conversationId: string
  ): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        GetUserConversationReactions: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method
      })
      return ReactionMapper.toDomianEntity(data)
    })
  }

  public static async getUserConversationMessagesReactions(
    conversationId: string,
    messageIds: string[]
  ): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        GetUserConversationMessagesReactions: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: {
          messageIds
        }
      })
      return ReactionMapper.toDomianEntity(data)
    })
  }

  public static async getGroupDefaultImageSet(): Promise<
    Result<DefaultGroupImageUrlSet[], SayHey.Error>
  > {
    return ChatKit.guardWithAuth(async () => {
      const {
        GetGroupDefaultImageSet: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint,
        method: Method
      })
      return data as DefaultGroupImageUrlSet[]
    })
  }

  /**
   * Message APIs
   */

  public static async getMessages(
    conversationId: string,
    pivotSequence?: number,
    after?: boolean
  ): Promise<Result<MessageDomainEntity[], SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        GetMessages: { Endpoint, Method }
      } = Routes
      if (!after) {
        const { data } = await ChatKit.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          params: {
            beforeSequence: pivotSequence,
            limit: 20
          }
        })
        return MessageMapper.toDomainEntities(data as MessageDTO[])
      }
      if (!pivotSequence) {
        return []
      }

      let lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER
      let messageListIds: Set<string> = new Set()
      let messageList: MessageDTO[] = []
      while (lastMessageSequenceInBatch > pivotSequence + 1) {
        const { data } = await ChatKit.httpClient({
          url: Endpoint(conversationId),
          method: Method,
          params: {
            limit: 20,
            beforeSequence: lastMessageSequenceInBatch
          }
        })
        let incomingMessageList: MessageDTO[] = data
        if (incomingMessageList.length === 0) {
          break
        }

        for (let m of incomingMessageList) {
          lastMessageSequenceInBatch = m.sequence
          if (lastMessageSequenceInBatch <= pivotSequence) {
            break
          }

          if (!messageListIds.has(m.id)) {
            messageListIds.add(m.id)
            messageList.push(m)
          }
        }
      }

      return MessageMapper.toDomainEntities(messageList)
    })
  }

  private static onMessage(data: Publisher.Event) {
    switch (data.event) {
      case Publisher.EventType.NewMessage: {
        ChatKit.eventListener({
          event: data.event,
          payload: {
            message: MessageMapper.toDomainEntity(data.payload.message),
            clientRefId: data.payload.clientRefId
          }
        })
        break
      }

      case Publisher.EventType.ConversationMuted: {
        const {
          conversationId,
          conversationMuteDto: { muteUntil }
        } = data.payload
        ChatKit.eventListener({
          event: data.event,
          payload: {
            conversationId,
            muteUntil: new Date(muteUntil)
          }
        })
        break
      }

      case Publisher.EventType.ConversationPinned: {
        const {
          conversationId,
          conversationPinDto: { pinnedAt }
        } = data.payload
        ChatKit.eventListener({
          event: data.event,
          payload: {
            conversationId,
            pinnedAt: new Date(pinnedAt)
          }
        })
        break
      }

      case Publisher.EventType.ConversationHideAndClear: {
        const {
          conversationId,
          conversationUserHideClearDto: { startAfter, visible }
        } = data.payload
        ChatKit.eventListener({
          event: data.event,
          payload: {
            conversationId,
            visible,
            startAfter: new Date(startAfter)
          }
        })
        break
      }

      case Publisher.EventType.ConversationHideAndClearAll: {
        const {
          conversationUserHideClearDto: { startAfter, visible }
        } = data.payload
        ChatKit.eventListener({
          event: data.event,
          payload: {
            visible,
            startAfter: new Date(startAfter)
          }
        })
        break
      }

      case Publisher.EventType.ConversationGroupInfoChanged: {
        ChatKit.eventListener({
          event: data.event,
          payload: {
            ...data.payload,
            groupPicture: data.payload.groupPicture
              ? MediaMapper.toDomainEntity(data.payload.groupPicture)
              : null
          }
        })
        break
      }

      case Publisher.EventType.ConversationCreated:
      case Publisher.EventType.ConversationUnpinned:
      case Publisher.EventType.ConversationUnmuted:
      case Publisher.EventType.UserInfoChanged:
      case Publisher.EventType.ConversationGroupOwnerAdded:
      case Publisher.EventType.ConversationGroupOwnerRemoved:
      case Publisher.EventType.ConversationGroupMemberAdded:
      case Publisher.EventType.ConversationGroupMemberRemoved:
      case Publisher.EventType.UserReactedToMessage:
      case Publisher.EventType.UserUnReactedToMessage:
      case Publisher.EventType.MessageReactionsUpdate:
      case Publisher.EventType.MessageDeleted:
      case Publisher.EventType.UserEnabled: {
        ChatKit.eventListener(data)
        break
      }
      case Publisher.EventType.UserDisabled: {
        ChatKit.eventListener(data)
        if (!ChatKit.disableUserDisabledHandler) {
          ChatKit.signOut()
        }
        break
      }
      case Publisher.EventType.UserDeleted: {
        ChatKit.eventListener(data)
        if (!ChatKit.disableUserDeletedHandler) {
          ChatKit.signOut()
        }
        break
      }
    }
  }

  public static async sendMessage(
    params: SendMessageParams
  ): Promise<Result<MessageDomainEntity, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const { conversationId, clientRefId } = params
      const {
        SendMessage: { Endpoint, Method }
      } = Routes
      let payload: any
      switch (params.type) {
        case MessageType.Text:
        case MessageType.Emoji: {
          payload = {
            type: params.type,
            text: params.text.trim()
          }
          break
        }
        case MessageType.Gif: {
          if (!isUrl(params.url)) {
            return new SayHey.Error(
              'Invalid URL!',
              SayHey.ErrorType.InternalErrorType.MissingParameter
            )
          }
          payload = {
            type: MessageType.Gif,
            refUrl: params.url,
            text: params.text?.trim()
          }
          break
        }
        case MessageType.Audio:
        case MessageType.Image:
        case MessageType.Document: {
          const { file, progress } = params
          const subProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.9)
              }
            : undefined
          const fileIdResponse = await ChatKit.uploadFile(file.uri, file.fileName, subProgress)
          if (fileIdResponse.isErr()) {
            return fileIdResponse.error
          }
          payload = {
            type: file.type,
            text: file.text?.trim(),
            fileId: fileIdResponse.value
          }
          break
        }
        case MessageType.Video: {
          const { file, progress } = params
          const subProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.4)
              }
            : undefined
          const { uri, fileName, thumbnail } = file
          let thumbnailId = ''
          if (thumbnail) {
            const thumbnailIdResponse = await ChatKit.uploadFile(
              thumbnail.uri,
              thumbnail.name,
              subProgress
            )
            if (thumbnailIdResponse.isErr()) {
              return thumbnailIdResponse.error
            }
            thumbnailId = thumbnailIdResponse.value
          }
          const videoUploadProgress = progress
            ? (percentage: number) => {
                progress(percentage * 0.6 + 0.4)
              }
            : undefined
          const fileIdResponse = await ChatKit.uploadFile(
            uri,
            fileName,
            videoUploadProgress,
            thumbnailId
          )
          if (fileIdResponse.isErr()) {
            return fileIdResponse.error
          }
          payload = {
            type: file.type,
            text: file.text?.trim(),
            fileId: fileIdResponse.value
          }
        }
      }
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        data: payload,
        params: {
          clientRefId
        }
      })
      return MessageMapper.toDomainEntity(data as MessageDTO)
    })
  }

  public static async getGalleryMessages(
    conversationId: string,
    beforeSequence?: number,
    limit?: number
  ): Promise<Result<GalleryPayload, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        GetMessages: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(conversationId),
        method: Method,
        params: {
          galleryOnly: true,
          limit,
          beforeSequence
        }
      })
      const messages = MessageMapper.toDomainEntities(data as MessageDTO[])
      const mediaList = messages.map(message => message.file) as MediaDomainEntity[]
      return {
        nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
        messageList: messages
      }
    })
  }

  public static async setMessageReaction(
    messageId: string,
    type: ReactionType
  ): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        SetMessageReaction: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(messageId),
        method: Method,
        data: {
          type: type
        }
      })
      return true
    })
  }

  public static async removeMessageReaction(
    messageId: string,
    type: ReactionType
  ): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        RemoveMessageReaction: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(messageId),
        method: Method,
        data: {
          type: type
        }
      })
      return true
    })
  }

  public static readMessage(params: {
    conversationId: string
    messageId: string
    sequence: number
    immediateUpdate?: boolean
  }) {
    const { conversationId, messageId, sequence, immediateUpdate } = params
    const mark = () => {
      if (
        Number.isInteger(ChatKit.readMessageSequenceMap[conversationId]) &&
        ChatKit.readMessageSequenceMap[conversationId] >= sequence
      ) {
        return
      }
      if (immediateUpdate) {
        ChatKit.markMessageRead(messageId).then(res => {
          if (res.isErr()) {
            ChatKit.readMessageMap[conversationId] = messageId
          }
        })
      } else {
        ChatKit.readMessageMap[conversationId] = messageId
      }
      ChatKit.readMessageSequenceMap[conversationId] = sequence
    }
    if (!ChatKit.isSendingReadReceipt) {
      mark()
    } else {
      setTimeout(mark, 500)
    }
  }

  private static async markMessageRead(messageId: string): Promise<Result<boolean, SayHey.Error>> {
    return ChatKit.guardWithAuth(async () => {
      const {
        MarkMessageAsRead: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(messageId),
        method: Method
      })
      return true
    })
  }

  /** Push Notification APIs */
  public static async registerForPushNotification(
    instanceId: string,
    token: string
  ): Promise<Result<boolean, SayHey.Error>> {
    if (!instanceId || !token) {
      return err(new SayHey.Error('Invalid instance id or token string!'))
    }
    async function registerForPushNotification() {
      const {
        RegisterPushNotification: { Endpoint, Method }
      } = Routes
      ChatKit.instanceIdPushNotification = instanceId
      await ChatKit.httpClient({
        url: Endpoint,
        method: Method,
        data: {
          instanceId,
          token
        }
      })
      return true
    }
    return ChatKit.guardWithAuth(registerForPushNotification)
  }

  public static async unregisterForPushNotification(): Promise<Result<boolean, SayHey.Error>> {
    if (!ChatKit.instanceIdPushNotification) {
      return ok(true)
    }
    async function unregisterForPushNotification() {
      const {
        UnregisterPushNotification: { Endpoint, Method }
      } = Routes
      await ChatKit.httpClient({
        url: Endpoint(ChatKit.instanceIdPushNotification),
        method: Method
      })
      ChatKit.instanceIdPushNotification = ''
      return true
    }
    return ChatKit.guard(unregisterForPushNotification)
  }

  public static async getReactedUsers({
    messageId,
    type,
    limit,
    afterSequence
  }: {
    messageId: string
    type?: ReactionType
    limit: number
    afterSequence: number | null
  }): Promise<Result<ReactedUserDTO[], SayHey.Error>> {
    async function getReactedUsers() {
      const {
        GetReactedUsers: { Endpoint, Method }
      } = Routes
      const { data } = await ChatKit.httpClient({
        url: Endpoint(messageId),
        method: Method,
        params: {
          limit,
          afterSequence,
          type: type || ''
        }
      })
      return data
    }
    return ChatKit.guardWithAuth(getReactedUsers)
  }
}
