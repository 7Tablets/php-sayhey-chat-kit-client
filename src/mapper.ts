import {
  ConversationDTO,
  ConversationDomainEntity,
  MessageDTO,
  MessageDomainEntity,
  ConversationType,
  UserDTO,
  UserDomainEntity,
  MediaDTO,
  MediaDomainEntity,
  SystemMessageDTO,
  SystemMessageDomainEntity,
  ConversationUserDomainEntity,
  UserOfConversationDTO,
  ConversationReactionsDomainEntity,
  ConversationReactionsDTO,
  UserSelfDTO,
  UserSelfDomainEntity
} from './types'
import { parseText } from './helper'

export class ConversationMapper {
  static toDomainEntity(conversation: ConversationDTO): ConversationDomainEntity {
    const {
      id,
      lastReadMessageId,
      lastMessage,
      muteUntil,
      pinnedAt,
      badge,
      type,
      createdAt,
      updatedAt,
      isOwner,
      startAfter,
      visible,
      endBefore,
      deletedFromConversationAt,
      serverCreated,
      serverManaged
    } = conversation
    const common = {
      id,
      lastReadMessageId,
      lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
      muteUntil: muteUntil ? new Date(muteUntil) : null,
      pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
      createdAt: new Date(createdAt),
      updatedAt: new Date(updatedAt),
      startAfter: startAfter ? new Date(startAfter) : null,
      endBefore: endBefore ? new Date(endBefore) : null,
      deletedFromConversationAt: deletedFromConversationAt
        ? new Date(deletedFromConversationAt)
        : null,
      badge: badge ? badge : 0,
      type,
      isOwner,
      visible,
      serverCreated,
      serverManaged
    }
    if (conversation.type === ConversationType.Group) {
      const { group } = conversation
      return {
        ...common,
        type: ConversationType.Group,
        group: {
          ...group,
          picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null
        }
      }
    }
    const { user } = conversation
    return {
      ...common,
      type: ConversationType.Individual,
      user: {
        ...user,
        picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null
      }
    }
  }

  static toDomainEntities(conversations: ConversationDTO[]): ConversationDomainEntity[] {
    return conversations.map(conversation => ConversationMapper.toDomainEntity(conversation))
  }
}

export class MessageMapper {
  static toDomainEntity(message: MessageDTO): MessageDomainEntity {
    const {
      id,
      conversationId,
      sender,
      type,
      text,
      sequence,
      systemMessage,
      file,
      createdAt,
      reactions,
      refUrl,
      deletedAt
    } = message
    const parsedResult = parseText(text)
    return {
      id,
      conversationId,
      sender,
      type,
      text,
      deletedAt: deletedAt ? new Date(deletedAt) : null,
      sequence,
      systemMessage: systemMessage ? SystemMessageMapper.toDomainEntity(systemMessage) : null,
      createdAt: new Date(createdAt),
      reactions,
      sent: true,
      parsedText: parsedResult ? parsedResult.parsedText : null,
      file: file ? MediaMapper.toDomainEntity(file) : null,
      refUrl,
      previewUrl: parsedResult?.previewUrl
    } as MessageDomainEntity
  }

  static toDomainEntities(messages: MessageDTO[]): MessageDomainEntity[] {
    return messages.map(value => MessageMapper.toDomainEntity(value))
  }
}

export class UserMapper {
  static toDomainEntity(user: UserDTO): UserDomainEntity {
    const { id, firstName, lastName, picture, email, refId, disabled, deletedAt } = user
    return {
      id,
      firstName,
      lastName,
      fullName: `${firstName} ${lastName}`,
      email,
      refId,
      username: email,
      disabled,
      deletedAt: deletedAt ? new Date(deletedAt) : null,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null
    }
  }

  static toConversableUserDomainEntity(user: UserOfConversationDTO): ConversationUserDomainEntity {
    const {
      id,
      firstName,
      lastName,
      picture,
      email,
      isOwner,
      deletedFromConversationAt,
      refId,
      disabled,
      deletedAt
    } = user
    return {
      id,
      firstName,
      lastName,
      username: email,
      fullName: `${firstName} ${lastName}`,
      email,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      isOwner,
      deletedFromConversationAt,
      refId,
      disabled,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }

  static toDomainEntities(users: UserDTO[]): UserDomainEntity[] {
    return users.map(UserMapper.toDomainEntity)
  }

  static toConversableUserDomainEntities(
    users: UserOfConversationDTO[]
  ): ConversationUserDomainEntity[] {
    return users.map(UserMapper.toConversableUserDomainEntity)
  }
  static toSelfDomainEntity(user: UserSelfDTO): UserSelfDomainEntity {
    const {
      id,
      firstName,
      lastName,
      picture,
      email,
      muteAllUntil,
      refId,
      disabled,
      deletedAt
    } = user
    return {
      id,
      firstName,
      lastName,
      username: email,
      fullName: `${firstName} ${lastName}`,
      email,
      refId,
      picture: picture ? MediaMapper.toDomainEntity(picture) : null,
      muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null,
      disabled,
      deletedAt: deletedAt ? new Date(deletedAt) : null
    }
  }
}

export class MediaMapper {
  static toDomainEntity(media: MediaDTO): MediaDomainEntity {
    const {
      urlPath,
      fileSize,
      fileType,
      extension,
      filename,
      mimeType,
      encoding,
      imageInfo,
      videoInfo
    } = media
    return {
      urlPath,
      fileSize,
      fileType,
      extension,
      filename,
      mimeType,
      encoding,
      imageInfo,
      videoInfo: !videoInfo
        ? null
        : {
            duration: videoInfo.duration,
            thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
          }
    }
  }
}

export class SystemMessageMapper {
  static toDomainEntity(systemMessage: SystemMessageDTO): SystemMessageDomainEntity {
    const { systemMessageType, id, refValue, actorCount, initiator, actors, text } = systemMessage
    return {
      systemMessageType,
      id,
      refValue,
      actorCount,
      initiator,
      actors,
      text
    }
  }
}

export class ReactionMapper {
  static toDomianEntity(reactions: ConversationReactionsDTO[]): ConversationReactionsDomainEntity {
    const conversationReactions: ConversationReactionsDomainEntity = {}
    reactions.forEach(item => {
      const messageId = item.messageId
      const reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type]

      if (!conversationReactions[messageId]) {
        conversationReactions[messageId] = []
      }
      const currentReactionTypes = conversationReactions[messageId] || []
      const reactionSet = new Set([...currentReactionTypes, ...reactionTypesArray])

      conversationReactions[messageId] = Array.from(reactionSet)
    })
    return conversationReactions
  }
}
