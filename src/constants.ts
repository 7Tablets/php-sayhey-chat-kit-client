import { HttpMethod } from './types'

export const API_VERSION = 'v1'

export const ApiKeyName = 'x-sayhey-client-api-key'

export const MESSAGE_READ_UPDATE_INTERVAL = 5000

export const Routes = {
  SignIn: {
    Endpoint: '/users/sign-in',
    Method: HttpMethod.Post
  },
  SignUp: {
    Endpoint: '/users/sign-up',
    Method: HttpMethod.Post
  },
  ChangePassword: {
    Endpoint: '/users/change-password',
    Method: HttpMethod.Post
  },
  GetReachableUsers: {
    Endpoint: '/users/conversable',
    Method: HttpMethod.Get
  },
  GetExistingConversationWithUser: {
    Endpoint: (userId: string) => `/users/${userId}/conversations/individual`,
    Method: HttpMethod.Get
  },
  GetConversations: {
    Endpoint: '/conversations',
    Method: HttpMethod.Get
  },
  GetConversationDetails: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}`,
    Method: HttpMethod.Get
  },
  CreateIndividualConversation: {
    Endpoint: '/conversations/individual',
    Method: HttpMethod.Post
  },
  CreateGroupConversation: {
    Endpoint: '/conversations/group',
    Method: HttpMethod.Post
  },
  GetMessages: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/messages`,
    Method: HttpMethod.Get
  },
  SendMessage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/messages`,
    Method: HttpMethod.Post
  },
  MuteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/mute`,
    Method: HttpMethod.Put
  },
  UnmuteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/mute`,
    Method: HttpMethod.Delete
  },
  PinConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/pin`,
    Method: HttpMethod.Put
  },
  UnpinConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/pin`,
    Method: HttpMethod.Delete
  },
  DeleteConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/hide-clear`,
    Method: HttpMethod.Put
  },
  DeleteAllConversations: {
    Endpoint: `/conversations/hide-clear`,
    Method: HttpMethod.Put
  },
  UploadFile: {
    Endpoint: '/files',
    Method: HttpMethod.Post
  },
  UpdateGroupImage: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-image`,
    Method: HttpMethod.Put
  },
  UpdateGroupInfo: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/group-info`,
    Method: HttpMethod.Put
  },
  UpdateUserProfileImage: {
    Endpoint: '/users/profile-image',
    Method: HttpMethod.Put
  },
  UpdateUserProfileInfo: {
    Endpoint: '/users/profile-info',
    Method: HttpMethod.Put
  },
  GetConversationUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/users`,
    Method: HttpMethod.Get
  },
  GetConversationReachableUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/conversable-users`,
    Method: HttpMethod.Get
  },
  AddUsersToConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/users`,
    Method: HttpMethod.Post
  },
  RemoveUsersFromConversation: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/delete-users`,
    Method: HttpMethod.Post
  },
  MakeConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `/conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Put
  },
  RemoveConversationOwner: {
    Endpoint: (conversationId: string, userId: string) =>
      `/conversations/${conversationId}/owners/${userId}`,
    Method: HttpMethod.Delete
  },
  GetUserConversationReactions: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/reactions`,
    Method: HttpMethod.Get
  },
  GetUserConversationMessagesReactions: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/get-messages-reactions`,
    Method: HttpMethod.Post
  },
  SetMessageReaction: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reactions/add`,
    Method: HttpMethod.Put
  },
  RemoveMessageReaction: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reactions/remove`,
    Method: HttpMethod.Put
  },
  MuteAllNotifications: {
    Endpoint: '/users/mute-all',
    Method: HttpMethod.Put
  },
  UnmuteAllNotifications: {
    Endpoint: '/users/mute-all',
    Method: HttpMethod.Delete
  },
  GetUserProfile: {
    Endpoint: '/users/profile-info',
    Method: HttpMethod.Get
  },
  GetGroupDefaultImageSet: {
    Endpoint: '/default-group-images',
    Method: HttpMethod.Get
  },
  MarkMessageAsRead: {
    Endpoint: (messageId: string) => `/read-messages/${messageId}`,
    Method: HttpMethod.Put
  },
  GetConversationBasicUsers: {
    Endpoint: (conversationId: string) => `/conversations/${conversationId}/basic-users`,
    Method: HttpMethod.Get
  },
  RegisterPushNotification: {
    Endpoint: '/device-tokens',
    Method: HttpMethod.Post
  },
  UnregisterPushNotification: {
    Endpoint: (instanceId: string) => `/device-tokens/${instanceId}`,
    Method: HttpMethod.Delete
  },
  GetReactedUsers: {
    Endpoint: (messageId: string) => `/messages/${messageId}/reacted-users`,
    Method: HttpMethod.Get
  }
}
