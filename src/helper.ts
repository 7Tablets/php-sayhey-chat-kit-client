import { length } from 'stringz'
import { HighLightType, ParsedLine } from './types'

const emojiRegex = /\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]/
export const isSingleEmoji = (text: string) => length(text) === 1 && text.match(emojiRegex)

export const textHighlightRegex = /(\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\})|((([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*)|(((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$))|(\S+@\S+\.\S+)/gi

const urlRegex = /(([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*/
// New phone regex (?:^|\s{1})((\+?1\s?)?((?:\([2-9]{1}\d{2}\))|(?:[2-9]{1}\d{2}))[\s\-]?(\d{3})[\s\-]?(\d{4}))(?:\s|$)
const phoneRegex = /((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$)/
const emailRegex = /\S+@\S+\.\S+/i
const userIdRegex = /\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\}/i
export const isUrl = (url: string) => !!urlRegex.test(url)
export const isPhone = (phone: string) => !!phoneRegex.test(phone)
export const isEmail = (email: string) => !!emailRegex.test(email)
export const isUser = (userId: string) => !!userIdRegex.test(userId)

export const parseText = (
  text: string | null
): { parsedText: ParsedLine[]; previewUrl: string | null } | null => {
  if (!text) {
    return null
  }
  const result: ParsedLine[] = []
  const trimedText = text.trim()
  let url: string | null = null
  trimedText.split('\n').forEach((line, index) => {
    result.push([])
    if (line.trim()) {
      let lastIndex = 0
      let match: RegExpExecArray | null
      while ((match = textHighlightRegex.exec(line))) {
        if (isUrl(match[0])) {
          if (match.index > lastIndex) {
            const currentText = line.substring(lastIndex, match.index)
            if (currentText.length > 1) {
              result[index].push({ type: HighLightType.Text, value: currentText })
            }
          }
          if (!url) {
            url = match[0].replace(match[4], match[4].toLowerCase())
          }
          result[index].push({ type: HighLightType.Url, value: match[0] })
          lastIndex = textHighlightRegex.lastIndex
        } else if (isUser(match[0])) {
          if (match.index > lastIndex) {
            const currentText = line.substring(lastIndex, match.index)
            if (currentText.length > 1) {
              result[index].push({ type: HighLightType.Text, value: currentText })
            }
          }
          result[index].push({ type: HighLightType.UserId, value: match[2] })
          lastIndex = textHighlightRegex.lastIndex
        } else if (isPhone(match[0])) {
          const startIndex = match[9] ? match.index + 1 : match.index
          if (startIndex > lastIndex) {
            const currentText = line.substring(lastIndex, startIndex)
            if (currentText.length >= 1) {
              result[index].push({ type: HighLightType.Text, value: currentText })
            }
          }
          result[index].push({ type: HighLightType.Phone, value: match[10] })
          lastIndex = match[15] ? textHighlightRegex.lastIndex - 1 : textHighlightRegex.lastIndex
        } else {
          if (match.index > lastIndex) {
            const currentText = line.substring(lastIndex, match.index)
            if (currentText.length > 1) {
              result[index].push({ type: HighLightType.Text, value: currentText })
            }
          }
          result[index].push({ type: HighLightType.Email, value: match[0] })
          lastIndex = textHighlightRegex.lastIndex
        }
      }
      if (lastIndex <= line.length - 1) {
        result[index].push({ type: HighLightType.Text, value: line.substring(lastIndex) })
      }
    } else {
      result[index].push({ type: HighLightType.Text, value: '\n' })
    }
  })

  return {
    parsedText: result,
    previewUrl: url
  }
}

export const mimeTypes: { [extension: string]: string } = {
  '3gp': 'video/3gpp',
  a: 'application/octet-stream',
  ai: 'application/postscript',
  aif: 'audio/x-aiff',
  aiff: 'audio/x-aiff',
  asc: 'application/pgp-signature',
  asf: 'video/x-ms-asf',
  asm: 'text/x-asm',
  asx: 'video/x-ms-asf',
  atom: 'application/atom+xml',
  au: 'audio/basic',
  avi: 'video/x-msvideo',
  bat: 'application/x-msdownload',
  bin: 'application/octet-stream',
  bmp: 'image/bmp',
  bz2: 'application/x-bzip2',
  c: 'text/x-csrc',
  cab: 'application/vnd.ms-cab-compressed',
  can: 'application/candor',
  cc: 'text/x-c++src',
  chm: 'application/vnd.ms-htmlhelp',
  class: 'application/octet-stream',
  com: 'application/x-msdownload',
  conf: 'text/plain',
  cpp: 'text/x-c',
  crt: 'application/x-x509-ca-cert',
  css: 'text/css',
  csv: 'text/csv',
  cxx: 'text/x-c',
  deb: 'application/x-debian-package',
  der: 'application/x-x509-ca-cert',
  diff: 'text/x-diff',
  djv: 'image/vnd.djvu',
  djvu: 'image/vnd.djvu',
  dll: 'application/x-msdownload',
  dmg: 'application/octet-stream',
  doc: 'application/msword',
  dot: 'application/msword',
  dtd: 'application/xml-dtd',
  dvi: 'application/x-dvi',
  ear: 'application/java-archive',
  eml: 'message/rfc822',
  eps: 'application/postscript',
  exe: 'application/x-msdownload',
  f: 'text/x-fortran',
  f77: 'text/x-fortran',
  f90: 'text/x-fortran',
  flv: 'video/x-flv',
  for: 'text/x-fortran',
  gem: 'application/octet-stream',
  gemspec: 'text/x-script.ruby',
  gif: 'image/gif',
  gyp: 'text/x-script.python',
  gypi: 'text/x-script.python',
  gz: 'application/x-gzip',
  h: 'text/x-chdr',
  hh: 'text/x-c++hdr',
  heic: 'image/heic',
  heif: 'image/heif',
  hevc: 'video/mp4',
  htm: 'text/html',
  html: 'text/html',
  ico: 'image/vnd.microsoft.icon',
  ics: 'text/calendar',
  ifb: 'text/calendar',
  iso: 'application/octet-stream',
  jpeg: 'image/jpeg',
  jpg: 'image/jpeg',
  js: 'application/javascript',
  json: 'application/json',
  less: 'text/css',
  log: 'text/plain',
  lua: 'text/x-script.lua',
  luac: 'application/x-bytecode.lua',
  m3u: 'audio/x-mpegurl',
  m4v: 'video/mp4',
  man: 'text/troff',
  manifest: 'text/cache-manifest',
  markdown: 'text/x-markdown',
  mathml: 'application/mathml+xml',
  mbox: 'application/mbox',
  mdoc: 'text/troff',
  md: 'text/x-markdown',
  me: 'text/troff',
  mid: 'audio/midi',
  midi: 'audio/midi',
  mime: 'message/rfc822',
  mml: 'application/mathml+xml',
  mng: 'video/x-mng',
  mov: 'video/quicktime',
  mp3: 'audio/mpeg',
  mp4: 'video/mp4',
  mp4v: 'video/mp4',
  mpeg: 'video/mpeg',
  mpg: 'video/mpeg',
  ms: 'text/troff',
  msi: 'application/x-msdownload',
  odp: 'application/vnd.oasis.opendocument.presentation',
  ods: 'application/vnd.oasis.opendocument.spreadsheet',
  odt: 'application/vnd.oasis.opendocument.text',
  ogg: 'application/ogg',
  p: 'text/x-pascal',
  pas: 'text/x-pascal',
  pbm: 'image/x-portable-bitmap',
  pdf: 'application/pdf',
  pem: 'application/x-x509-ca-cert',
  pgm: 'image/x-portable-graymap',
  pgp: 'application/pgp-encrypted',
  pkg: 'application/octet-stream',
  png: 'image/png',
  pnm: 'image/x-portable-anymap',
  ppm: 'image/x-portable-pixmap',
  pps: 'application/vnd.ms-powerpoint',
  ppt: 'application/vnd.ms-powerpoint',
  ps: 'application/postscript',
  psd: 'image/vnd.adobe.photoshop',
  qt: 'video/quicktime',
  ra: 'audio/x-pn-realaudio',
  ram: 'audio/x-pn-realaudio',
  rar: 'application/x-rar-compressed',
  rdf: 'application/rdf+xml',
  roff: 'text/troff',
  rss: 'application/rss+xml',
  rtf: 'application/rtf',
  s: 'text/x-asm',
  sgm: 'text/sgml',
  sgml: 'text/sgml',
  sh: 'application/x-sh',
  sig: 'application/pgp-signature',
  snd: 'audio/basic',
  so: 'application/octet-stream',
  svg: 'image/svg+xml',
  svgz: 'image/svg+xml',
  swf: 'application/x-shockwave-flash',
  t: 'text/troff',
  tar: 'application/x-tar',
  tbz: 'application/x-bzip-compressed-tar',
  tci: 'application/x-topcloud',
  tcl: 'application/x-tcl',
  tex: 'application/x-tex',
  texi: 'application/x-texinfo',
  texinfo: 'application/x-texinfo',
  text: 'text/plain',
  tif: 'image/tiff',
  tiff: 'image/tiff',
  torrent: 'application/x-bittorrent',
  tr: 'text/troff',
  ttf: 'application/x-font-ttf',
  txt: 'text/plain',
  vcf: 'text/x-vcard',
  vcs: 'text/x-vcalendar',
  war: 'application/java-archive',
  wav: 'audio/x-wav',
  webm: 'video/webm',
  wma: 'audio/x-ms-wma',
  wmv: 'video/x-ms-wmv',
  wmx: 'video/x-ms-wmx',
  wrl: 'model/vrml',
  wsdl: 'application/wsdl+xml',
  xbm: 'image/x-xbitmap',
  xhtml: 'application/xhtml+xml',
  xls: 'application/vnd.ms-excel',
  xml: 'application/xml',
  zip: 'application/zip'
}
