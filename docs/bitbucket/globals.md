[php-sayhey-chat-kit-client](globals.md)

# php-sayhey-chat-kit-client

## Index

### Modules

* [Publisher](modules/publisher.md)
* [SayHey](modules/sayhey.md)

### Enumerations

* [ConversationType](enums/conversationtype.md)
* [ConversationUserQueryType](enums/conversationuserquerytype.md)
* [FileType](enums/filetype.md)
* [HighLightType](enums/highlighttype.md)
* [HttpMethod](enums/httpmethod.md)
* [ImageSizes](enums/imagesizes.md)
* [MessageType](enums/messagetype.md)
* [ReactionType](enums/reactiontype.md)
* [SystemMessageType](enums/systemmessagetype.md)

### Classes

* [ChatKit](classes/chatkit.md)
* [ConversationMapper](classes/conversationmapper.md)
* [MediaMapper](classes/mediamapper.md)
* [MessageMapper](classes/messagemapper.md)
* [ReactionMapper](classes/reactionmapper.md)
* [ReactionsObject](classes/reactionsobject.md)
* [SystemMessageMapper](classes/systemmessagemapper.md)
* [UserMapper](classes/usermapper.md)

### Interfaces

* [AdditionalUserOfConversation](interfaces/additionaluserofconversation.md)
* [AuthDTO](interfaces/authdto.md)
* [AuthResponse](interfaces/authresponse.md)
* [BaseConversation](interfaces/baseconversation.md)
* [BaseConversationDTO](interfaces/baseconversationdto.md)
* [BaseConversationDomainEntity](interfaces/baseconversationdomainentity.md)
* [BaseMedia](interfaces/basemedia.md)
* [BaseMessage](interfaces/basemessage.md)
* [BaseUser](interfaces/baseuser.md)
* [ConversableUsersBatchDTO](interfaces/conversableusersbatchdto.md)
* [ConversableUsersBatchMeta](interfaces/conversableusersbatchmeta.md)
* [ConversableUsersBatchResult](interfaces/conversableusersbatchresult.md)
* [ConversationConversableUsersBatch](interfaces/conversationconversableusersbatch.md)
* [ConversationConversableUsersBatchDTO](interfaces/conversationconversableusersbatchdto.md)
* [ConversationConversableUsersBatchMeta](interfaces/conversationconversableusersbatchmeta.md)
* [ConversationInfoDTO](interfaces/conversationinfodto.md)
* [ConversationInfoDomainEntity](interfaces/conversationinfodomainentity.md)
* [ConversationReachableUsersQuery](interfaces/conversationreachableusersquery.md)
* [ConversationReactionsDTO](interfaces/conversationreactionsdto.md)
* [ConversationUserDomainEntity](interfaces/conversationuserdomainentity.md)
* [ConversationUserQuery](interfaces/conversationuserquery.md)
* [ConversationUsersBasicDTO](interfaces/conversationusersbasicdto.md)
* [ConversationUsersBatch](interfaces/conversationusersbatch.md)
* [ConversationUsersBatchDTO](interfaces/conversationusersbatchdto.md)
* [ConversationUsersBatchMeta](interfaces/conversationusersbatchmeta.md)
* [DefaultGroupImageUrlSet](interfaces/defaultgroupimageurlset.md)
* [GalleryPayload](interfaces/gallerypayload.md)
* [GroupConversationDTO](interfaces/groupconversationdto.md)
* [GroupConversationDomainEntity](interfaces/groupconversationdomainentity.md)
* [ImageInfoMedia](interfaces/imageinfomedia.md)
* [IndividualConversationDTO](interfaces/individualconversationdto.md)
* [IndividualConversationDomainEntity](interfaces/individualconversationdomainentity.md)
* [MediaDTO](interfaces/mediadto.md)
* [MediaDomainEntity](interfaces/mediadomainentity.md)
* [MessageDTO](interfaces/messagedto.md)
* [MessageDomainEntity](interfaces/messagedomainentity.md)
* [ReachableUsersQuery](interfaces/reachableusersquery.md)
* [ReactedUserDTO](interfaces/reacteduserdto.md)
* [Reaction](interfaces/reaction.md)
* [SearchPaginateResponse](interfaces/searchpaginateresponse.md)
* [SendEmojiMessageParams](interfaces/sendemojimessageparams.md)
* [SendFileMessageParams](interfaces/sendfilemessageparams.md)
* [SendGifMessageParams](interfaces/sendgifmessageparams.md)
* [SendMessageParamsBase](interfaces/sendmessageparamsbase.md)
* [SendTextMessageParams](interfaces/sendtextmessageparams.md)
* [SubImageMinimal](interfaces/subimageminimal.md)
* [SystemMessageBase](interfaces/systemmessagebase.md)
* [SystemMessageDTO](interfaces/systemmessagedto.md)
* [SystemMessageDomainEntity](interfaces/systemmessagedomainentity.md)
* [UserConversationInfoDTO](interfaces/userconversationinfodto.md)
* [UserConversationInfoDomainEntity](interfaces/userconversationinfodomainentity.md)
* [UserDTO](interfaces/userdto.md)
* [UserDomainEntity](interfaces/userdomainentity.md)
* [UserOfConversationDTO](interfaces/userofconversationdto.md)
* [UserSearchPaginateResponse](interfaces/usersearchpaginateresponse.md)
* [UserSelfDTO](interfaces/userselfdto.md)
* [UserSelfDomainEntity](interfaces/userselfdomainentity.md)
* [UserSimpleInfo](interfaces/usersimpleinfo.md)

### Type aliases

* [ConversationDTO](globals.md#markdown-header-conversationdto)
* [ConversationDomainEntity](globals.md#markdown-header-conversationdomainentity)
* [ConversationReactionsDomainEntity](globals.md#markdown-header-conversationreactionsdomainentity)
* [FileUpload](globals.md#markdown-header-fileupload)
* [FileWithText](globals.md#markdown-header-filewithtext)
* [ParsedLine](globals.md#markdown-header-parsedline)
* [SendMessageParams](globals.md#markdown-header-sendmessageparams)

### Variables

* [API_VERSION](globals.md#markdown-header-const-api_version)
* [ApiKeyName](globals.md#markdown-header-const-apikeyname)
* [MESSAGE_READ_UPDATE_INTERVAL](globals.md#markdown-header-const-message_read_update_interval)
* [emailRegex](globals.md#markdown-header-const-emailregex)
* [emojiRegex](globals.md#markdown-header-const-emojiregex)
* [phoneRegex](globals.md#markdown-header-const-phoneregex)
* [textHighlightRegex](globals.md#markdown-header-const-texthighlightregex)
* [urlRegex](globals.md#markdown-header-const-urlregex)
* [userIdRegex](globals.md#markdown-header-const-useridregex)

### Functions

* [checkApiKey](globals.md#markdown-header-checkapikey)
* [checkaccessToken](globals.md#markdown-header-checkaccesstoken)
* [isEmail](globals.md#markdown-header-const-isemail)
* [isPhone](globals.md#markdown-header-const-isphone)
* [isSingleEmoji](globals.md#markdown-header-const-issingleemoji)
* [isUrl](globals.md#markdown-header-const-isurl)
* [isUser](globals.md#markdown-header-const-isuser)
* [parseText](globals.md#markdown-header-const-parsetext)
* [requestFailed](globals.md#markdown-header-requestfailed)

### Object literals

* [Routes](globals.md#markdown-header-const-routes)
* [mimeTypes](globals.md#markdown-header-const-mimetypes)

## Type aliases

###  ConversationDTO

Ƭ **ConversationDTO**: *[IndividualConversationDTO](interfaces/individualconversationdto.md) | [GroupConversationDTO](interfaces/groupconversationdto.md)*

Defined in types.ts:248

___

###  ConversationDomainEntity

Ƭ **ConversationDomainEntity**: *[GroupConversationDomainEntity](interfaces/groupconversationdomainentity.md) | [IndividualConversationDomainEntity](interfaces/individualconversationdomainentity.md)*

Defined in types.ts:272

___

###  ConversationReactionsDomainEntity

Ƭ **ConversationReactionsDomainEntity**: *Partial‹object›*

Defined in types.ts:1184

___

###  FileUpload

Ƭ **FileUpload**: *object*

Defined in types.ts:318

#### Type declaration:

* **fileName**: *string*

* **path**: *string | Blob*

___

###  FileWithText

Ƭ **FileWithText**: *object*

Defined in types.ts:323

#### Type declaration:

* **fileName**: *string*

* **text**? : *undefined | string*

* **thumbnail**? : *undefined | object*

* **type**: *[MessageType](enums/messagetype.md)*

* **uri**: *string | Blob*

___

###  ParsedLine

Ƭ **ParsedLine**: *object[]*

Defined in types.ts:172

___

###  SendMessageParams

Ƭ **SendMessageParams**: *[SendTextMessageParams](interfaces/sendtextmessageparams.md) | [SendEmojiMessageParams](interfaces/sendemojimessageparams.md) | [SendGifMessageParams](interfaces/sendgifmessageparams.md) | [SendFileMessageParams](interfaces/sendfilemessageparams.md)*

Defined in types.ts:1173

## Variables

### `Const` API_VERSION

• **API_VERSION**: *"v1"* = "v1"

Defined in constants.ts:3

___

### `Const` ApiKeyName

• **ApiKeyName**: *"x-sayhey-client-api-key"* = "x-sayhey-client-api-key"

Defined in constants.ts:5

___

### `Const` MESSAGE_READ_UPDATE_INTERVAL

• **MESSAGE_READ_UPDATE_INTERVAL**: *5000* = 5000

Defined in constants.ts:7

___

### `Const` emailRegex

• **emailRegex**: *RegExp‹›* = /\S+@\S+\.\S+/i

Defined in helper.ts:12

___

### `Const` emojiRegex

• **emojiRegex**: *RegExp‹›* = /\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]/

Defined in helper.ts:4

___

### `Const` phoneRegex

• **phoneRegex**: *RegExp‹›* = /((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$)/

Defined in helper.ts:11

___

### `Const` textHighlightRegex

• **textHighlightRegex**: *RegExp‹›* = /(\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\})|((([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*)|(((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$))|(\S+@\S+\.\S+)/gi

Defined in helper.ts:7

___

### `Const` urlRegex

• **urlRegex**: *RegExp‹›* = /(([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*/

Defined in helper.ts:9

___

### `Const` userIdRegex

• **userIdRegex**: *RegExp‹›* = /\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\}/i

Defined in helper.ts:13

## Functions

###  checkApiKey

▸ **checkApiKey**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:5

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

###  checkaccessToken

▸ **checkaccessToken**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:15

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

### `Const` isEmail

▸ **isEmail**(`email`: string): *boolean*

Defined in helper.ts:16

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |

**Returns:** *boolean*

___

### `Const` isPhone

▸ **isPhone**(`phone`: string): *boolean*

Defined in helper.ts:15

**Parameters:**

Name | Type |
------ | ------ |
`phone` | string |

**Returns:** *boolean*

___

### `Const` isSingleEmoji

▸ **isSingleEmoji**(`text`: string): *null | false | RegExpMatchArray‹›*

Defined in helper.ts:5

**Parameters:**

Name | Type |
------ | ------ |
`text` | string |

**Returns:** *null | false | RegExpMatchArray‹›*

___

### `Const` isUrl

▸ **isUrl**(`url`: string): *boolean*

Defined in helper.ts:14

**Parameters:**

Name | Type |
------ | ------ |
`url` | string |

**Returns:** *boolean*

___

### `Const` isUser

▸ **isUser**(`userId`: string): *boolean*

Defined in helper.ts:17

**Parameters:**

Name | Type |
------ | ------ |
`userId` | string |

**Returns:** *boolean*

___

### `Const` parseText

▸ **parseText**(`text`: string | null): *object | null*

Defined in helper.ts:19

**Parameters:**

Name | Type |
------ | ------ |
`text` | string &#124; null |

**Returns:** *object | null*

___

###  requestFailed

▸ **requestFailed**(`error`: AxiosError): *void*

Defined in axios.ts:25

**Parameters:**

Name | Type |
------ | ------ |
`error` | AxiosError |

**Returns:** *void*

## Object literals

### `Const` Routes

### ▪ **Routes**: *object*

Defined in constants.ts:9

▪ **AddUsersToConversation**: *object*

Defined in constants.ts:106

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **ChangePassword**: *object*

Defined in constants.ts:18

* **Endpoint**: *string* = "/users/change-password"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateGroupConversation**: *object*

Defined in constants.ts:42

* **Endpoint**: *string* = "/conversations/group"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **CreateIndividualConversation**: *object*

Defined in constants.ts:38

* **Endpoint**: *string* = "/conversations/individual"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **DeleteAllConversations**: *object*

Defined in constants.ts:74

* **Endpoint**: *string* = `/conversations/hide-clear`

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **DeleteConversation**: *object*

Defined in constants.ts:70

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationBasicUsers**: *object*

Defined in constants.ts:160

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationDetails**: *object*

Defined in constants.ts:34

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationReachableUsers**: *object*

Defined in constants.ts:102

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversationUsers**: *object*

Defined in constants.ts:98

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetConversations**: *object*

Defined in constants.ts:30

* **Endpoint**: *string* = "/conversations"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetExistingConversationWithUser**: *object*

Defined in constants.ts:26

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`userId`: string): *string*

▪ **GetGroupDefaultImageSet**: *object*

Defined in constants.ts:152

* **Endpoint**: *string* = "/default-group-images"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetMessages**: *object*

Defined in constants.ts:46

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetReachableUsers**: *object*

Defined in constants.ts:22

* **Endpoint**: *string* = "/users/conversable"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **GetReactedUsers**: *object*

Defined in constants.ts:172

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`messageId`: string): *string*

▪ **GetUserConversationMessagesReactions**: *object*

Defined in constants.ts:128

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **GetUserConversationReactions**: *object*

Defined in constants.ts:124

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

* **Endpoint**(`conversationId`: string): *string*

▪ **GetUserProfile**: *object*

Defined in constants.ts:148

* **Endpoint**: *string* = "/users/profile-info"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Get

▪ **MakeConversationOwner**: *object*

Defined in constants.ts:114

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **MarkMessageAsRead**: *object*

Defined in constants.ts:156

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **MuteAllNotifications**: *object*

Defined in constants.ts:140

* **Endpoint**: *string* = "/users/mute-all"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **MuteConversation**: *object*

Defined in constants.ts:54

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **PinConversation**: *object*

Defined in constants.ts:62

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **RegisterPushNotification**: *object*

Defined in constants.ts:164

* **Endpoint**: *string* = "/device-tokens"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **RemoveConversationOwner**: *object*

Defined in constants.ts:119

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string, `userId`: string): *string*

▪ **RemoveMessageReaction**: *object*

Defined in constants.ts:136

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **RemoveUsersFromConversation**: *object*

Defined in constants.ts:110

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **SendMessage**: *object*

Defined in constants.ts:50

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

* **Endpoint**(`conversationId`: string): *string*

▪ **SetMessageReaction**: *object*

Defined in constants.ts:132

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`messageId`: string): *string*

▪ **SignIn**: *object*

Defined in constants.ts:10

* **Endpoint**: *string* = "/users/sign-in"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SignUp**: *object*

Defined in constants.ts:14

* **Endpoint**: *string* = "/users/sign-up"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **UnmuteAllNotifications**: *object*

Defined in constants.ts:144

* **Endpoint**: *string* = "/users/mute-all"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

▪ **UnmuteConversation**: *object*

Defined in constants.ts:58

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **UnpinConversation**: *object*

Defined in constants.ts:66

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`conversationId`: string): *string*

▪ **UnregisterPushNotification**: *object*

Defined in constants.ts:168

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`instanceId`: string): *string*

▪ **UpdateGroupImage**: *object*

Defined in constants.ts:82

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateGroupInfo**: *object*

Defined in constants.ts:86

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

* **Endpoint**(`conversationId`: string): *string*

▪ **UpdateUserProfileImage**: *object*

Defined in constants.ts:90

* **Endpoint**: *string* = "/users/profile-image"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **UpdateUserProfileInfo**: *object*

Defined in constants.ts:94

* **Endpoint**: *string* = "/users/profile-info"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Put

▪ **UploadFile**: *object*

Defined in constants.ts:78

* **Endpoint**: *string* = "/files"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

___

### `Const` mimeTypes

### ▪ **mimeTypes**: *object*

Defined in helper.ts:90

###  3gp

• **3gp**: *string* = "video/3gpp"

Defined in helper.ts:91

###  a

• **a**: *string* = "application/octet-stream"

Defined in helper.ts:92

###  ai

• **ai**: *string* = "application/postscript"

Defined in helper.ts:93

###  aif

• **aif**: *string* = "audio/x-aiff"

Defined in helper.ts:94

###  aiff

• **aiff**: *string* = "audio/x-aiff"

Defined in helper.ts:95

###  asc

• **asc**: *string* = "application/pgp-signature"

Defined in helper.ts:96

###  asf

• **asf**: *string* = "video/x-ms-asf"

Defined in helper.ts:97

###  asm

• **asm**: *string* = "text/x-asm"

Defined in helper.ts:98

###  asx

• **asx**: *string* = "video/x-ms-asf"

Defined in helper.ts:99

###  atom

• **atom**: *string* = "application/atom+xml"

Defined in helper.ts:100

###  au

• **au**: *string* = "audio/basic"

Defined in helper.ts:101

###  avi

• **avi**: *string* = "video/x-msvideo"

Defined in helper.ts:102

###  bat

• **bat**: *string* = "application/x-msdownload"

Defined in helper.ts:103

###  bin

• **bin**: *string* = "application/octet-stream"

Defined in helper.ts:104

###  bmp

• **bmp**: *string* = "image/bmp"

Defined in helper.ts:105

###  bz2

• **bz2**: *string* = "application/x-bzip2"

Defined in helper.ts:106

###  c

• **c**: *string* = "text/x-csrc"

Defined in helper.ts:107

###  cab

• **cab**: *string* = "application/vnd.ms-cab-compressed"

Defined in helper.ts:108

###  can

• **can**: *string* = "application/candor"

Defined in helper.ts:109

###  cc

• **cc**: *string* = "text/x-c++src"

Defined in helper.ts:110

###  chm

• **chm**: *string* = "application/vnd.ms-htmlhelp"

Defined in helper.ts:111

###  class

• **class**: *string* = "application/octet-stream"

Defined in helper.ts:112

###  com

• **com**: *string* = "application/x-msdownload"

Defined in helper.ts:113

###  conf

• **conf**: *string* = "text/plain"

Defined in helper.ts:114

###  cpp

• **cpp**: *string* = "text/x-c"

Defined in helper.ts:115

###  crt

• **crt**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:116

###  css

• **css**: *string* = "text/css"

Defined in helper.ts:117

###  csv

• **csv**: *string* = "text/csv"

Defined in helper.ts:118

###  cxx

• **cxx**: *string* = "text/x-c"

Defined in helper.ts:119

###  deb

• **deb**: *string* = "application/x-debian-package"

Defined in helper.ts:120

###  der

• **der**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:121

###  diff

• **diff**: *string* = "text/x-diff"

Defined in helper.ts:122

###  djv

• **djv**: *string* = "image/vnd.djvu"

Defined in helper.ts:123

###  djvu

• **djvu**: *string* = "image/vnd.djvu"

Defined in helper.ts:124

###  dll

• **dll**: *string* = "application/x-msdownload"

Defined in helper.ts:125

###  dmg

• **dmg**: *string* = "application/octet-stream"

Defined in helper.ts:126

###  doc

• **doc**: *string* = "application/msword"

Defined in helper.ts:127

###  dot

• **dot**: *string* = "application/msword"

Defined in helper.ts:128

###  dtd

• **dtd**: *string* = "application/xml-dtd"

Defined in helper.ts:129

###  dvi

• **dvi**: *string* = "application/x-dvi"

Defined in helper.ts:130

###  ear

• **ear**: *string* = "application/java-archive"

Defined in helper.ts:131

###  eml

• **eml**: *string* = "message/rfc822"

Defined in helper.ts:132

###  eps

• **eps**: *string* = "application/postscript"

Defined in helper.ts:133

###  exe

• **exe**: *string* = "application/x-msdownload"

Defined in helper.ts:134

###  f

• **f**: *string* = "text/x-fortran"

Defined in helper.ts:135

###  f77

• **f77**: *string* = "text/x-fortran"

Defined in helper.ts:136

###  f90

• **f90**: *string* = "text/x-fortran"

Defined in helper.ts:137

###  flv

• **flv**: *string* = "video/x-flv"

Defined in helper.ts:138

###  for

• **for**: *string* = "text/x-fortran"

Defined in helper.ts:139

###  gem

• **gem**: *string* = "application/octet-stream"

Defined in helper.ts:140

###  gemspec

• **gemspec**: *string* = "text/x-script.ruby"

Defined in helper.ts:141

###  gif

• **gif**: *string* = "image/gif"

Defined in helper.ts:142

###  gyp

• **gyp**: *string* = "text/x-script.python"

Defined in helper.ts:143

###  gypi

• **gypi**: *string* = "text/x-script.python"

Defined in helper.ts:144

###  gz

• **gz**: *string* = "application/x-gzip"

Defined in helper.ts:145

###  h

• **h**: *string* = "text/x-chdr"

Defined in helper.ts:146

###  heic

• **heic**: *string* = "image/heic"

Defined in helper.ts:148

###  heif

• **heif**: *string* = "image/heif"

Defined in helper.ts:149

###  hevc

• **hevc**: *string* = "video/mp4"

Defined in helper.ts:150

###  hh

• **hh**: *string* = "text/x-c++hdr"

Defined in helper.ts:147

###  htm

• **htm**: *string* = "text/html"

Defined in helper.ts:151

###  html

• **html**: *string* = "text/html"

Defined in helper.ts:152

###  ico

• **ico**: *string* = "image/vnd.microsoft.icon"

Defined in helper.ts:153

###  ics

• **ics**: *string* = "text/calendar"

Defined in helper.ts:154

###  ifb

• **ifb**: *string* = "text/calendar"

Defined in helper.ts:155

###  iso

• **iso**: *string* = "application/octet-stream"

Defined in helper.ts:156

###  jpeg

• **jpeg**: *string* = "image/jpeg"

Defined in helper.ts:157

###  jpg

• **jpg**: *string* = "image/jpeg"

Defined in helper.ts:158

###  js

• **js**: *string* = "application/javascript"

Defined in helper.ts:159

###  json

• **json**: *string* = "application/json"

Defined in helper.ts:160

###  less

• **less**: *string* = "text/css"

Defined in helper.ts:161

###  log

• **log**: *string* = "text/plain"

Defined in helper.ts:162

###  lua

• **lua**: *string* = "text/x-script.lua"

Defined in helper.ts:163

###  luac

• **luac**: *string* = "application/x-bytecode.lua"

Defined in helper.ts:164

###  m3u

• **m3u**: *string* = "audio/x-mpegurl"

Defined in helper.ts:165

###  m4v

• **m4v**: *string* = "video/mp4"

Defined in helper.ts:166

###  man

• **man**: *string* = "text/troff"

Defined in helper.ts:167

###  manifest

• **manifest**: *string* = "text/cache-manifest"

Defined in helper.ts:168

###  markdown

• **markdown**: *string* = "text/x-markdown"

Defined in helper.ts:169

###  mathml

• **mathml**: *string* = "application/mathml+xml"

Defined in helper.ts:170

###  mbox

• **mbox**: *string* = "application/mbox"

Defined in helper.ts:171

###  md

• **md**: *string* = "text/x-markdown"

Defined in helper.ts:173

###  mdoc

• **mdoc**: *string* = "text/troff"

Defined in helper.ts:172

###  me

• **me**: *string* = "text/troff"

Defined in helper.ts:174

###  mid

• **mid**: *string* = "audio/midi"

Defined in helper.ts:175

###  midi

• **midi**: *string* = "audio/midi"

Defined in helper.ts:176

###  mime

• **mime**: *string* = "message/rfc822"

Defined in helper.ts:177

###  mml

• **mml**: *string* = "application/mathml+xml"

Defined in helper.ts:178

###  mng

• **mng**: *string* = "video/x-mng"

Defined in helper.ts:179

###  mov

• **mov**: *string* = "video/quicktime"

Defined in helper.ts:180

###  mp3

• **mp3**: *string* = "audio/mpeg"

Defined in helper.ts:181

###  mp4

• **mp4**: *string* = "video/mp4"

Defined in helper.ts:182

###  mp4v

• **mp4v**: *string* = "video/mp4"

Defined in helper.ts:183

###  mpeg

• **mpeg**: *string* = "video/mpeg"

Defined in helper.ts:184

###  mpg

• **mpg**: *string* = "video/mpeg"

Defined in helper.ts:185

###  ms

• **ms**: *string* = "text/troff"

Defined in helper.ts:186

###  msi

• **msi**: *string* = "application/x-msdownload"

Defined in helper.ts:187

###  odp

• **odp**: *string* = "application/vnd.oasis.opendocument.presentation"

Defined in helper.ts:188

###  ods

• **ods**: *string* = "application/vnd.oasis.opendocument.spreadsheet"

Defined in helper.ts:189

###  odt

• **odt**: *string* = "application/vnd.oasis.opendocument.text"

Defined in helper.ts:190

###  ogg

• **ogg**: *string* = "application/ogg"

Defined in helper.ts:191

###  p

• **p**: *string* = "text/x-pascal"

Defined in helper.ts:192

###  pas

• **pas**: *string* = "text/x-pascal"

Defined in helper.ts:193

###  pbm

• **pbm**: *string* = "image/x-portable-bitmap"

Defined in helper.ts:194

###  pdf

• **pdf**: *string* = "application/pdf"

Defined in helper.ts:195

###  pem

• **pem**: *string* = "application/x-x509-ca-cert"

Defined in helper.ts:196

###  pgm

• **pgm**: *string* = "image/x-portable-graymap"

Defined in helper.ts:197

###  pgp

• **pgp**: *string* = "application/pgp-encrypted"

Defined in helper.ts:198

###  pkg

• **pkg**: *string* = "application/octet-stream"

Defined in helper.ts:199

###  png

• **png**: *string* = "image/png"

Defined in helper.ts:200

###  pnm

• **pnm**: *string* = "image/x-portable-anymap"

Defined in helper.ts:201

###  ppm

• **ppm**: *string* = "image/x-portable-pixmap"

Defined in helper.ts:202

###  pps

• **pps**: *string* = "application/vnd.ms-powerpoint"

Defined in helper.ts:203

###  ppt

• **ppt**: *string* = "application/vnd.ms-powerpoint"

Defined in helper.ts:204

###  ps

• **ps**: *string* = "application/postscript"

Defined in helper.ts:205

###  psd

• **psd**: *string* = "image/vnd.adobe.photoshop"

Defined in helper.ts:206

###  qt

• **qt**: *string* = "video/quicktime"

Defined in helper.ts:207

###  ra

• **ra**: *string* = "audio/x-pn-realaudio"

Defined in helper.ts:208

###  ram

• **ram**: *string* = "audio/x-pn-realaudio"

Defined in helper.ts:209

###  rar

• **rar**: *string* = "application/x-rar-compressed"

Defined in helper.ts:210

###  rdf

• **rdf**: *string* = "application/rdf+xml"

Defined in helper.ts:211

###  roff

• **roff**: *string* = "text/troff"

Defined in helper.ts:212

###  rss

• **rss**: *string* = "application/rss+xml"

Defined in helper.ts:213

###  rtf

• **rtf**: *string* = "application/rtf"

Defined in helper.ts:214

###  s

• **s**: *string* = "text/x-asm"

Defined in helper.ts:215

###  sgm

• **sgm**: *string* = "text/sgml"

Defined in helper.ts:216

###  sgml

• **sgml**: *string* = "text/sgml"

Defined in helper.ts:217

###  sh

• **sh**: *string* = "application/x-sh"

Defined in helper.ts:218

###  sig

• **sig**: *string* = "application/pgp-signature"

Defined in helper.ts:219

###  snd

• **snd**: *string* = "audio/basic"

Defined in helper.ts:220

###  so

• **so**: *string* = "application/octet-stream"

Defined in helper.ts:221

###  svg

• **svg**: *string* = "image/svg+xml"

Defined in helper.ts:222

###  svgz

• **svgz**: *string* = "image/svg+xml"

Defined in helper.ts:223

###  swf

• **swf**: *string* = "application/x-shockwave-flash"

Defined in helper.ts:224

###  t

• **t**: *string* = "text/troff"

Defined in helper.ts:225

###  tar

• **tar**: *string* = "application/x-tar"

Defined in helper.ts:226

###  tbz

• **tbz**: *string* = "application/x-bzip-compressed-tar"

Defined in helper.ts:227

###  tci

• **tci**: *string* = "application/x-topcloud"

Defined in helper.ts:228

###  tcl

• **tcl**: *string* = "application/x-tcl"

Defined in helper.ts:229

###  tex

• **tex**: *string* = "application/x-tex"

Defined in helper.ts:230

###  texi

• **texi**: *string* = "application/x-texinfo"

Defined in helper.ts:231

###  texinfo

• **texinfo**: *string* = "application/x-texinfo"

Defined in helper.ts:232

###  text

• **text**: *string* = "text/plain"

Defined in helper.ts:233

###  tif

• **tif**: *string* = "image/tiff"

Defined in helper.ts:234

###  tiff

• **tiff**: *string* = "image/tiff"

Defined in helper.ts:235

###  torrent

• **torrent**: *string* = "application/x-bittorrent"

Defined in helper.ts:236

###  tr

• **tr**: *string* = "text/troff"

Defined in helper.ts:237

###  ttf

• **ttf**: *string* = "application/x-font-ttf"

Defined in helper.ts:238

###  txt

• **txt**: *string* = "text/plain"

Defined in helper.ts:239

###  vcf

• **vcf**: *string* = "text/x-vcard"

Defined in helper.ts:240

###  vcs

• **vcs**: *string* = "text/x-vcalendar"

Defined in helper.ts:241

###  war

• **war**: *string* = "application/java-archive"

Defined in helper.ts:242

###  wav

• **wav**: *string* = "audio/x-wav"

Defined in helper.ts:243

###  webm

• **webm**: *string* = "video/webm"

Defined in helper.ts:244

###  wma

• **wma**: *string* = "audio/x-ms-wma"

Defined in helper.ts:245

###  wmv

• **wmv**: *string* = "video/x-ms-wmv"

Defined in helper.ts:246

###  wmx

• **wmx**: *string* = "video/x-ms-wmx"

Defined in helper.ts:247

###  wrl

• **wrl**: *string* = "model/vrml"

Defined in helper.ts:248

###  wsdl

• **wsdl**: *string* = "application/wsdl+xml"

Defined in helper.ts:249

###  xbm

• **xbm**: *string* = "image/x-xbitmap"

Defined in helper.ts:250

###  xhtml

• **xhtml**: *string* = "application/xhtml+xml"

Defined in helper.ts:251

###  xls

• **xls**: *string* = "application/vnd.ms-excel"

Defined in helper.ts:252

###  xml

• **xml**: *string* = "application/xml"

Defined in helper.ts:253

###  zip

• **zip**: *string* = "application/zip"

Defined in helper.ts:254
