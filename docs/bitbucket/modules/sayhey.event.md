[php-sayhey-chat-kit-client](../globals.md) › [SayHey](sayhey.md) › [Event](sayhey.event.md)

# Module: Event

## Index

### Interfaces

* [AllConversationsDeleted](../interfaces/sayhey.event.allconversationsdeleted.md)
* [ConversationCreated](../interfaces/sayhey.event.conversationcreated.md)
* [ConversationGroupInfoChanged](../interfaces/sayhey.event.conversationgroupinfochanged.md)
* [ConversationGroupMemberAdded](../interfaces/sayhey.event.conversationgroupmemberadded.md)
* [ConversationGroupMemberRemoved](../interfaces/sayhey.event.conversationgroupmemberremoved.md)
* [ConversationGroupOwnerAdded](../interfaces/sayhey.event.conversationgroupowneradded.md)
* [ConversationGroupOwnerRemoved](../interfaces/sayhey.event.conversationgroupownerremoved.md)
* [ConversationHideAndClear](../interfaces/sayhey.event.conversationhideandclear.md)
* [ConversationMuted](../interfaces/sayhey.event.conversationmuted.md)
* [ConversationPinned](../interfaces/sayhey.event.conversationpinned.md)
* [ConversationUnmuted](../interfaces/sayhey.event.conversationunmuted.md)
* [ConversationUnpinned](../interfaces/sayhey.event.conversationunpinned.md)
* [MessageDeleted](../interfaces/sayhey.event.messagedeleted.md)
* [MessageReactionsUpdate](../interfaces/sayhey.event.messagereactionsupdate.md)
* [NewMessage](../interfaces/sayhey.event.newmessage.md)
* [UserDeleted](../interfaces/sayhey.event.userdeleted.md)
* [UserDisabled](../interfaces/sayhey.event.userdisabled.md)
* [UserEnabled](../interfaces/sayhey.event.userenabled.md)
* [UserInfoChanged](../interfaces/sayhey.event.userinfochanged.md)
* [UserReactedToMessage](../interfaces/sayhey.event.userreactedtomessage.md)
* [UserUnReactedToMessage](../interfaces/sayhey.event.userunreactedtomessage.md)

### Type aliases

* [AllEvents](sayhey.event.md#markdown-header-allevents)

## Type aliases

###  AllEvents

Ƭ **AllEvents**: *[AllConversationsDeleted](../interfaces/sayhey.event.allconversationsdeleted.md) | [ConversationCreated](../interfaces/publisher.event.conversationcreated.md) | [NewMessage](../interfaces/publisher.event.newmessage.md) | [ConversationMuted](../interfaces/publisher.event.conversationmuted.md) | [ConversationUnmuted](../interfaces/publisher.event.conversationunmuted.md) | [ConversationUnpinned](../interfaces/publisher.event.conversationunpinned.md) | [ConversationPinned](../interfaces/publisher.event.conversationpinned.md) | [ConversationHideAndClear](../interfaces/publisher.event.conversationhideandclear.md) | [ConversationGroupInfoChanged](../interfaces/publisher.event.conversationgroupinfochanged.md) | [UserInfoChanged](../interfaces/publisher.event.userinfochanged.md) | [ConversationGroupOwnerAdded](../interfaces/publisher.event.conversationgroupowneradded.md) | [ConversationGroupOwnerRemoved](../interfaces/publisher.event.conversationgroupownerremoved.md) | [ConversationGroupMemberRemoved](../interfaces/publisher.event.conversationgroupmemberremoved.md) | [ConversationGroupMemberAdded](../interfaces/publisher.event.conversationgroupmemberadded.md) | [UserReactedToMessage](../interfaces/publisher.event.userreactedtomessage.md) | [UserUnReactedToMessage](../interfaces/publisher.event.userunreactedtomessage.md) | [MessageReactionsUpdate](../interfaces/publisher.event.messagereactionsupdate.md) | [UserDisabled](../interfaces/publisher.event.userdisabled.md) | [UserEnabled](../interfaces/publisher.event.userenabled.md) | [MessageDeleted](../interfaces/publisher.event.messagedeleted.md) | [UserDeleted](../interfaces/publisher.event.userdeleted.md)*

Defined in types.ts:943
