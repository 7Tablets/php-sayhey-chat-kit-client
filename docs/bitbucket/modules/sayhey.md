[php-sayhey-chat-kit-client](../globals.md) › [SayHey](sayhey.md)

# Module: SayHey

## Index

### Modules

* [ChatKitError](sayhey.chatkiterror.md)
* [Event](sayhey.event.md)
* [Payload](sayhey.payload.md)

### Variables

* [Error](sayhey.md#markdown-header-error)
* [ErrorType](sayhey.md#markdown-header-errortype)
* [EventType](sayhey.md#markdown-header-eventtype)

## Variables

###  Error

• **Error**: *[AppError](../classes/sayhey.chatkiterror.apperror.md)*

Defined in types.ts:1143

___

###  ErrorType

• **ErrorType**: *[ErrorType](sayhey.chatkiterror.errortype.md)*

Defined in types.ts:1139

___

###  EventType

• **EventType**: *[EventType](../enums/publisher.eventtype.md)*

Defined in types.ts:1137
