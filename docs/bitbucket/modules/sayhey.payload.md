[php-sayhey-chat-kit-client](../globals.md) › [SayHey](sayhey.md) › [Payload](sayhey.payload.md)

# Module: Payload

## Index

### Interfaces

* [ConversationCreated](../interfaces/sayhey.payload.conversationcreated.md)
* [ConversationGroupInfoChanged](../interfaces/sayhey.payload.conversationgroupinfochanged.md)
* [ConversationGroupMemberAdded](../interfaces/sayhey.payload.conversationgroupmemberadded.md)
* [ConversationGroupMemberRemoved](../interfaces/sayhey.payload.conversationgroupmemberremoved.md)
* [ConversationGroupOwnerAdded](../interfaces/sayhey.payload.conversationgroupowneradded.md)
* [ConversationGroupOwnerRemoved](../interfaces/sayhey.payload.conversationgroupownerremoved.md)
* [ConversationHideAndClear](../interfaces/sayhey.payload.conversationhideandclear.md)
* [ConversationHideAndClearAll](../interfaces/sayhey.payload.conversationhideandclearall.md)
* [ConversationMuted](../interfaces/sayhey.payload.conversationmuted.md)
* [ConversationPinned](../interfaces/sayhey.payload.conversationpinned.md)
* [ConversationUnmuted](../interfaces/sayhey.payload.conversationunmuted.md)
* [ConversationUnpinned](../interfaces/sayhey.payload.conversationunpinned.md)
* [MessageDeleted](../interfaces/sayhey.payload.messagedeleted.md)
* [MessageReactionsUpdate](../interfaces/sayhey.payload.messagereactionsupdate.md)
* [NewMessage](../interfaces/sayhey.payload.newmessage.md)
* [UserDeleted](../interfaces/sayhey.payload.userdeleted.md)
* [UserDisabled](../interfaces/sayhey.payload.userdisabled.md)
* [UserEnabled](../interfaces/sayhey.payload.userenabled.md)
* [UserInfoChanged](../interfaces/sayhey.payload.userinfochanged.md)
* [UserReactedToMessage](../interfaces/sayhey.payload.userreactedtomessage.md)
* [UserUnReactedToMessage](../interfaces/sayhey.payload.userunreactedtomessage.md)
