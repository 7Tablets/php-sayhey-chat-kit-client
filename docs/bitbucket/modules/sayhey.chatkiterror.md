[php-sayhey-chat-kit-client](../globals.md) › [SayHey](sayhey.md) › [ChatKitError](sayhey.chatkiterror.md)

# Module: ChatKitError

## Index

### Modules

* [ErrorType](sayhey.chatkiterror.errortype.md)

### Enumerations

* [PushNotificationError](../enums/sayhey.chatkiterror.pushnotificationerror.md)

### Classes

* [AppError](../classes/sayhey.chatkiterror.apperror.md)

### Type aliases

* [Type](sayhey.chatkiterror.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[Type](sayhey.chatkiterror.errortype.md#markdown-header-type) | [PushNotificationError](../enums/sayhey.chatkiterror.pushnotificationerror.md)*

Defined in types.ts:1095
