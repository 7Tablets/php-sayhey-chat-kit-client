[php-sayhey-chat-kit-client](../globals.md) › [Publisher](publisher.md)

# Module: Publisher

## Index

### Modules

* [Event](publisher.event.md)
* [Payload](publisher.payload.md)

### Enumerations

* [EventType](../enums/publisher.eventtype.md)
