[php-sayhey-chat-kit-client](../globals.md) › [SayHey](sayhey.md) › [ChatKitError](sayhey.chatkiterror.md) › [ErrorType](sayhey.chatkiterror.errortype.md)

# Module: ErrorType

## Index

### Enumerations

* [AuthErrorType](../enums/sayhey.chatkiterror.errortype.autherrortype.md)
* [ConversationErrorType](../enums/sayhey.chatkiterror.errortype.conversationerrortype.md)
* [DateErrorType](../enums/sayhey.chatkiterror.errortype.dateerrortype.md)
* [FileErrorType](../enums/sayhey.chatkiterror.errortype.fileerrortype.md)
* [GeneralErrorType](../enums/sayhey.chatkiterror.errortype.generalerrortype.md)
* [InternalErrorType](../enums/sayhey.chatkiterror.errortype.internalerrortype.md)
* [MessageErrorType](../enums/sayhey.chatkiterror.errortype.messageerrortype.md)
* [ServerErrorType](../enums/sayhey.chatkiterror.errortype.servererrortype.md)
* [UserErrorType](../enums/sayhey.chatkiterror.errortype.usererrortype.md)

### Type aliases

* [Type](sayhey.chatkiterror.errortype.md#markdown-header-type)

## Type aliases

###  Type

Ƭ **Type**: *[InternalErrorType](../enums/sayhey.chatkiterror.errortype.internalerrortype.md) | [AuthErrorType](../enums/sayhey.chatkiterror.errortype.autherrortype.md) | [FileErrorType](../enums/sayhey.chatkiterror.errortype.fileerrortype.md) | [ServerErrorType](../enums/sayhey.chatkiterror.errortype.servererrortype.md) | [GeneralErrorType](../enums/sayhey.chatkiterror.errortype.generalerrortype.md) | [MessageErrorType](../enums/sayhey.chatkiterror.errortype.messageerrortype.md) | [UserErrorType](../enums/sayhey.chatkiterror.errortype.usererrortype.md) | [ConversationErrorType](../enums/sayhey.chatkiterror.errortype.conversationerrortype.md) | [DateErrorType](../enums/sayhey.chatkiterror.errortype.dateerrortype.md)*

Defined in types.ts:977
