[php-sayhey-chat-kit-client](../globals.md) › [ConversableUsersBatchDTO](conversableusersbatchdto.md)

# Interface: ConversableUsersBatchDTO

## Hierarchy

  ↳ [ConversableUsersBatchMeta](conversableusersbatchmeta.md)

  ↳ **ConversableUsersBatchDTO**

## Index

### Properties

* [completed](conversableusersbatchdto.md#markdown-header-completed)
* [limit](conversableusersbatchdto.md#markdown-header-limit)
* [nextOffset](conversableusersbatchdto.md#markdown-header-nextoffset)
* [offset](conversableusersbatchdto.md#markdown-header-offset)
* [results](conversableusersbatchdto.md#markdown-header-results)
* [search](conversableusersbatchdto.md#markdown-header-search)
* [totalCount](conversableusersbatchdto.md#markdown-header-totalcount)
* [userId](conversableusersbatchdto.md#markdown-header-userid)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserDTO](userdto.md)[]*

Defined in types.ts:348

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17

___

###  userId

• **userId**: *string*

*Inherited from [ConversableUsersBatchMeta](conversableusersbatchmeta.md).[userId](conversableusersbatchmeta.md#markdown-header-userid)*

Defined in types.ts:344
