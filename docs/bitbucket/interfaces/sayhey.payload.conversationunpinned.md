[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationUnpinned](sayhey.payload.conversationunpinned.md)

# Interface: ConversationUnpinned

## Hierarchy

* **ConversationUnpinned**

## Index

### Properties

* [conversationId](sayhey.payload.conversationunpinned.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:744
