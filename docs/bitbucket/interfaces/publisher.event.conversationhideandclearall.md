[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationHideAndClearAll](publisher.event.conversationhideandclearall.md)

# Interface: ConversationHideAndClearAll

## Hierarchy

* EventMessage

  ↳ **ConversationHideAndClearAll**

## Index

### Properties

* [event](publisher.event.conversationhideandclearall.md#markdown-header-event)
* [payload](publisher.event.conversationhideandclearall.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationHideAndClearAll](../enums/publisher.eventtype.md#markdown-header-conversationhideandclearall)*

*Overrides void*

Defined in types.ts:617

___

###  payload

• **payload**: *[ConversationHideAndClearAll](publisher.payload.conversationhideandclearall.md)*

*Overrides void*

Defined in types.ts:618
