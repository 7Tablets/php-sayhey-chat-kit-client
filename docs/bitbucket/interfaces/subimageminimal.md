[php-sayhey-chat-kit-client](../globals.md) › [SubImageMinimal](subimageminimal.md)

# Interface: SubImageMinimal

## Hierarchy

* **SubImageMinimal**

## Index

### Properties

* [height](subimageminimal.md#markdown-header-height)
* [imageSize](subimageminimal.md#markdown-header-imagesize)
* [postfix](subimageminimal.md#markdown-header-postfix)
* [width](subimageminimal.md#markdown-header-width)

## Properties

###  height

• **height**: *number*

Defined in types.ts:60

___

###  imageSize

• **imageSize**: *[ImageSizes](../enums/imagesizes.md)*

Defined in types.ts:58

___

###  postfix

• **postfix**: *string*

Defined in types.ts:61

___

###  width

• **width**: *number*

Defined in types.ts:59
