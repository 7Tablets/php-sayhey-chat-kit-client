[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupOwnerAdded](sayhey.payload.conversationgroupowneradded.md)

# Interface: ConversationGroupOwnerAdded

## Hierarchy

* **ConversationGroupOwnerAdded**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupowneradded.md#markdown-header-conversationid)
* [isOwner](sayhey.payload.conversationgroupowneradded.md#markdown-header-isowner)
* [userId](sayhey.payload.conversationgroupowneradded.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:776

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:778

___

###  userId

• **userId**: *string*

Defined in types.ts:777
