[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationMuted](sayhey.payload.conversationmuted.md)

# Interface: ConversationMuted

## Hierarchy

* **ConversationMuted**

## Index

### Properties

* [conversationId](sayhey.payload.conversationmuted.md#markdown-header-conversationid)
* [muteUntil](sayhey.payload.conversationmuted.md#markdown-header-muteuntil)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:730

___

###  muteUntil

• **muteUntil**: *Date*

Defined in types.ts:731
