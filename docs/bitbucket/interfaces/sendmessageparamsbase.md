[php-sayhey-chat-kit-client](../globals.md) › [SendMessageParamsBase](sendmessageparamsbase.md)

# Interface: SendMessageParamsBase

## Hierarchy

* **SendMessageParamsBase**

  ↳ [SendTextMessageParams](sendtextmessageparams.md)

  ↳ [SendEmojiMessageParams](sendemojimessageparams.md)

  ↳ [SendGifMessageParams](sendgifmessageparams.md)

  ↳ [SendFileMessageParams](sendfilemessageparams.md)

## Index

### Properties

* [clientRefId](sendmessageparamsbase.md#markdown-header-clientrefid)
* [conversationId](sendmessageparamsbase.md#markdown-header-conversationid)

## Properties

###  clientRefId

• **clientRefId**: *string*

Defined in types.ts:1148

___

###  conversationId

• **conversationId**: *string*

Defined in types.ts:1147
