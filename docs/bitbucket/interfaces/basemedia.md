[php-sayhey-chat-kit-client](../globals.md) › [BaseMedia](basemedia.md)

# Interface: BaseMedia

## Hierarchy

* **BaseMedia**

  ↳ [MediaDTO](mediadto.md)

  ↳ [MediaDomainEntity](mediadomainentity.md)

## Index

### Properties

* [encoding](basemedia.md#markdown-header-encoding)
* [extension](basemedia.md#markdown-header-extension)
* [fileSize](basemedia.md#markdown-header-filesize)
* [fileType](basemedia.md#markdown-header-filetype)
* [filename](basemedia.md#markdown-header-filename)
* [imageInfo](basemedia.md#markdown-header-imageinfo)
* [mimeType](basemedia.md#markdown-header-mimetype)
* [urlPath](basemedia.md#markdown-header-urlpath)

## Properties

###  encoding

• **encoding**: *string*

Defined in types.ts:82

___

###  extension

• **extension**: *string*

Defined in types.ts:81

___

###  fileSize

• **fileSize**: *number*

Defined in types.ts:84

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)*

Defined in types.ts:78

___

###  filename

• **filename**: *string*

Defined in types.ts:83

___

###  imageInfo

• **imageInfo**: *[ImageInfoMedia](imageinfomedia.md) | null*

Defined in types.ts:85

___

###  mimeType

• **mimeType**: *string*

Defined in types.ts:80

___

###  urlPath

• **urlPath**: *string*

Defined in types.ts:79
