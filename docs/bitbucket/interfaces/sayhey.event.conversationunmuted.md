[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationUnmuted](sayhey.event.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* **ConversationUnmuted**

## Index

### Properties

* [event](sayhey.event.conversationunmuted.md#markdown-header-event)
* [payload](sayhey.event.conversationunmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationUnmuted*

Defined in types.ts:864

___

###  payload

• **payload**: *[ConversationUnmuted](publisher.payload.conversationunmuted.md)*

Defined in types.ts:865
