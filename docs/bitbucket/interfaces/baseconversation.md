[php-sayhey-chat-kit-client](../globals.md) › [BaseConversation](baseconversation.md)

# Interface: BaseConversation

## Hierarchy

* **BaseConversation**

  ↳ [BaseConversationDTO](baseconversationdto.md)

## Index

### Properties

* [id](baseconversation.md#markdown-header-id)
* [isOwner](baseconversation.md#markdown-header-isowner)
* [lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)
* [serverCreated](baseconversation.md#markdown-header-servercreated)
* [serverManaged](baseconversation.md#markdown-header-servermanaged)
* [type](baseconversation.md#markdown-header-type)
* [visible](baseconversation.md#markdown-header-visible)

## Properties

###  id

• **id**: *string*

Defined in types.ts:195

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:198

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

Defined in types.ts:196

___

###  serverCreated

• **serverCreated**: *boolean*

Defined in types.ts:200

___

###  serverManaged

• **serverManaged**: *boolean*

Defined in types.ts:201

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

Defined in types.ts:197

___

###  visible

• **visible**: *boolean*

Defined in types.ts:199
