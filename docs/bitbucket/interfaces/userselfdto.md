[php-sayhey-chat-kit-client](../globals.md) › [UserSelfDTO](userselfdto.md)

# Interface: UserSelfDTO

## Hierarchy

  ↳ [UserDTO](userdto.md)

  ↳ **UserSelfDTO**

## Index

### Properties

* [deletedAt](userselfdto.md#markdown-header-deletedat)
* [disabled](userselfdto.md#markdown-header-disabled)
* [email](userselfdto.md#markdown-header-email)
* [firstName](userselfdto.md#markdown-header-firstname)
* [id](userselfdto.md#markdown-header-id)
* [lastName](userselfdto.md#markdown-header-lastname)
* [muteAllUntil](userselfdto.md#markdown-header-mutealluntil)
* [picture](userselfdto.md#markdown-header-picture)
* [refId](userselfdto.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *string | null*

*Inherited from [UserDTO](userdto.md).[deletedAt](userdto.md#markdown-header-deletedat)*

Defined in types.ts:301

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  muteAllUntil

• **muteAllUntil**: *string | null*

Defined in types.ts:1189

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserDTO](userdto.md).[picture](userdto.md#markdown-header-picture)*

Defined in types.ts:300

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295
