[php-sayhey-chat-kit-client](../globals.md) › [SystemMessageDomainEntity](systemmessagedomainentity.md)

# Interface: SystemMessageDomainEntity

## Hierarchy

* [SystemMessageBase](systemmessagebase.md)

  ↳ **SystemMessageDomainEntity**

## Index

### Properties

* [actorCount](systemmessagedomainentity.md#markdown-header-actorcount)
* [actors](systemmessagedomainentity.md#markdown-header-actors)
* [id](systemmessagedomainentity.md#markdown-header-id)
* [initiator](systemmessagedomainentity.md#markdown-header-initiator)
* [refValue](systemmessagedomainentity.md#markdown-header-refvalue)
* [systemMessageType](systemmessagedomainentity.md#markdown-header-systemmessagetype)
* [text](systemmessagedomainentity.md#markdown-header-optional-text)

## Properties

###  actorCount

• **actorCount**: *number*

*Inherited from [SystemMessageBase](systemmessagebase.md).[actorCount](systemmessagebase.md#markdown-header-actorcount)*

Defined in types.ts:137

___

###  actors

• **actors**: *[UserSimpleInfo](usersimpleinfo.md)[]*

*Inherited from [SystemMessageBase](systemmessagebase.md).[actors](systemmessagebase.md#markdown-header-actors)*

Defined in types.ts:139

___

###  id

• **id**: *string*

*Inherited from [SystemMessageBase](systemmessagebase.md).[id](systemmessagebase.md#markdown-header-id)*

Defined in types.ts:135

___

###  initiator

• **initiator**: *[UserSimpleInfo](usersimpleinfo.md) | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[initiator](systemmessagebase.md#markdown-header-initiator)*

Defined in types.ts:138

___

###  refValue

• **refValue**: *string | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[refValue](systemmessagebase.md#markdown-header-refvalue)*

Defined in types.ts:136

___

###  systemMessageType

• **systemMessageType**: *[SystemMessageType](../enums/systemmessagetype.md)*

*Inherited from [SystemMessageBase](systemmessagebase.md).[systemMessageType](systemmessagebase.md#markdown-header-systemmessagetype)*

Defined in types.ts:134

___

### `Optional` text

• **text**? : *string | null*

*Inherited from [SystemMessageBase](systemmessagebase.md).[text](systemmessagebase.md#markdown-header-optional-text)*

Defined in types.ts:140
