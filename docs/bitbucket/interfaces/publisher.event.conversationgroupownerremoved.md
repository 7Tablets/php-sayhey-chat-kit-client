[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationGroupOwnerRemoved](publisher.event.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* EventMessage

  ↳ **ConversationGroupOwnerRemoved**

## Index

### Properties

* [event](publisher.event.conversationgroupownerremoved.md#markdown-header-event)
* [payload](publisher.event.conversationgroupownerremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationGroupOwnerRemoved](../enums/publisher.eventtype.md#markdown-header-conversationgroupownerremoved)*

*Overrides void*

Defined in types.ts:637

___

###  payload

• **payload**: *[ConversationGroupOwnerRemoved](publisher.payload.conversationgroupownerremoved.md)*

*Overrides void*

Defined in types.ts:638
