[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserInfoChanged](sayhey.payload.userinfochanged.md)

# Interface: UserInfoChanged

## Hierarchy

* **UserInfoChanged**

## Index

### Properties

* [userEmail](sayhey.payload.userinfochanged.md#markdown-header-useremail)
* [userFirstName](sayhey.payload.userinfochanged.md#markdown-header-userfirstname)
* [userId](sayhey.payload.userinfochanged.md#markdown-header-userid)
* [userLastName](sayhey.payload.userinfochanged.md#markdown-header-userlastname)
* [userMuteAllUntil](sayhey.payload.userinfochanged.md#markdown-header-usermutealluntil)
* [userPicture](sayhey.payload.userinfochanged.md#markdown-header-userpicture)
* [userRefId](sayhey.payload.userinfochanged.md#markdown-header-userrefid)

## Properties

###  userEmail

• **userEmail**: *string*

Defined in types.ts:761

___

###  userFirstName

• **userFirstName**: *string*

Defined in types.ts:763

___

###  userId

• **userId**: *string*

Defined in types.ts:760

___

###  userLastName

• **userLastName**: *string*

Defined in types.ts:764

___

###  userMuteAllUntil

• **userMuteAllUntil**: *Date | null*

Defined in types.ts:766

___

###  userPicture

• **userPicture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:762

___

###  userRefId

• **userRefId**: *string | null*

Defined in types.ts:765
