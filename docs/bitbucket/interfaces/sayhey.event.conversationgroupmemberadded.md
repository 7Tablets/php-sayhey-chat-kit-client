[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupMemberAdded](sayhey.event.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberAdded**

## Index

### Properties

* [event](sayhey.event.conversationgroupmemberadded.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupmemberadded.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupMemberAdded*

*Overrides void*

Defined in types.ts:904

___

###  payload

• **payload**: *[ConversationGroupMemberAdded](publisher.payload.conversationgroupmemberadded.md)*

*Overrides void*

Defined in types.ts:905
