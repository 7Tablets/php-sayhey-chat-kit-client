[php-sayhey-chat-kit-client](../globals.md) › [BaseMessage](basemessage.md)

# Interface: BaseMessage

## Hierarchy

* **BaseMessage**

  ↳ [MessageDTO](messagedto.md)

  ↳ [MessageDomainEntity](messagedomainentity.md)

## Index

### Properties

* [conversationId](basemessage.md#markdown-header-conversationid)
* [id](basemessage.md#markdown-header-id)
* [reactions](basemessage.md#markdown-header-reactions)
* [refUrl](basemessage.md#markdown-header-refurl)
* [sequence](basemessage.md#markdown-header-sequence)
* [text](basemessage.md#markdown-header-text)
* [type](basemessage.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:156

___

###  id

• **id**: *string*

Defined in types.ts:155

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

Defined in types.ts:161

___

###  refUrl

• **refUrl**: *string | null*

Defined in types.ts:159

___

###  sequence

• **sequence**: *number*

Defined in types.ts:160

___

###  text

• **text**: *string | null*

Defined in types.ts:158

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

Defined in types.ts:157
