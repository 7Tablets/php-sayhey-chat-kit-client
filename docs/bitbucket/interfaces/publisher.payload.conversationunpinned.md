[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationUnpinned](publisher.payload.conversationunpinned.md)

# Interface: ConversationUnpinned

## Hierarchy

* **ConversationUnpinned**

## Index

### Properties

* [conversationId](publisher.payload.conversationunpinned.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:464
