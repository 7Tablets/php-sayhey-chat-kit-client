[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserDisabled](sayhey.event.userdisabled.md)

# Interface: UserDisabled

## Hierarchy

* EventMessage

  ↳ **UserDisabled**

## Index

### Properties

* [event](sayhey.event.userdisabled.md#markdown-header-event)
* [payload](sayhey.event.userdisabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserDisabled*

*Overrides void*

Defined in types.ts:924

___

###  payload

• **payload**: *[UserDisabled](publisher.payload.userdisabled.md)*

*Overrides void*

Defined in types.ts:925
