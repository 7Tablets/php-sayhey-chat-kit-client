[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationPinned](sayhey.payload.conversationpinned.md)

# Interface: ConversationPinned

## Hierarchy

* **ConversationPinned**

## Index

### Properties

* [conversationId](sayhey.payload.conversationpinned.md#markdown-header-conversationid)
* [pinnedAt](sayhey.payload.conversationpinned.md#markdown-header-pinnedat)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:735

___

###  pinnedAt

• **pinnedAt**: *Date*

Defined in types.ts:736
