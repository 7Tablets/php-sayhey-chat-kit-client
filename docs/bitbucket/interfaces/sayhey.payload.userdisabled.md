[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserDisabled](sayhey.payload.userdisabled.md)

# Interface: UserDisabled

## Hierarchy

* **UserDisabled**

## Index

### Properties

* [disabled](sayhey.payload.userdisabled.md#markdown-header-disabled)
* [userId](sayhey.payload.userdisabled.md#markdown-header-userid)

## Properties

###  disabled

• **disabled**: *boolean*

Defined in types.ts:817

___

###  userId

• **userId**: *string*

Defined in types.ts:816
