[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationUnmuted](sayhey.payload.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* **ConversationUnmuted**

## Index

### Properties

* [conversationId](sayhey.payload.conversationunmuted.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:740
