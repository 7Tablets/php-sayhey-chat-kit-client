[php-sayhey-chat-kit-client](../globals.md) › [ConversableUsersBatchResult](conversableusersbatchresult.md)

# Interface: ConversableUsersBatchResult

## Hierarchy

  ↳ [ConversableUsersBatchMeta](conversableusersbatchmeta.md)

  ↳ **ConversableUsersBatchResult**

## Index

### Properties

* [completed](conversableusersbatchresult.md#markdown-header-completed)
* [limit](conversableusersbatchresult.md#markdown-header-limit)
* [nextOffset](conversableusersbatchresult.md#markdown-header-nextoffset)
* [offset](conversableusersbatchresult.md#markdown-header-offset)
* [results](conversableusersbatchresult.md#markdown-header-results)
* [search](conversableusersbatchresult.md#markdown-header-search)
* [totalCount](conversableusersbatchresult.md#markdown-header-totalcount)
* [userId](conversableusersbatchresult.md#markdown-header-userid)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserDomainEntity](userdomainentity.md)[]*

Defined in types.ts:352

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17

___

###  userId

• **userId**: *string*

*Inherited from [ConversableUsersBatchMeta](conversableusersbatchmeta.md).[userId](conversableusersbatchmeta.md#markdown-header-userid)*

Defined in types.ts:344
