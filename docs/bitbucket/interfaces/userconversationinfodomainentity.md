[php-sayhey-chat-kit-client](../globals.md) › [UserConversationInfoDomainEntity](userconversationinfodomainentity.md)

# Interface: UserConversationInfoDomainEntity

## Hierarchy

* [ConversationInfoDomainEntity](conversationinfodomainentity.md)

  ↳ **UserConversationInfoDomainEntity**

## Index

### Properties

* [email](userconversationinfodomainentity.md#markdown-header-email)
* [id](userconversationinfodomainentity.md#markdown-header-id)
* [name](userconversationinfodomainentity.md#markdown-header-name)
* [picture](userconversationinfodomainentity.md#markdown-header-picture)
* [refId](userconversationinfodomainentity.md#markdown-header-refid)

## Properties

###  email

• **email**: *string*

Defined in types.ts:222

___

###  id

• **id**: *string*

Defined in types.ts:221

___

###  name

• **name**: *string*

*Inherited from [ConversationInfoDomainEntity](conversationinfodomainentity.md).[name](conversationinfodomainentity.md#markdown-header-name)*

Defined in types.ts:211

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

*Inherited from [ConversationInfoDomainEntity](conversationinfodomainentity.md).[picture](conversationinfodomainentity.md#markdown-header-picture)*

Defined in types.ts:210

___

###  refId

• **refId**: *string | null*

Defined in types.ts:223
