[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserUnReactedToMessage](sayhey.event.userunreactedtomessage.md)

# Interface: UserUnReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserUnReactedToMessage**

## Index

### Properties

* [event](sayhey.event.userunreactedtomessage.md#markdown-header-event)
* [payload](sayhey.event.userunreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserUnReactedToMessage*

*Overrides void*

Defined in types.ts:914

___

###  payload

• **payload**: *[UserUnReactedToMessage](publisher.payload.userunreactedtomessage.md)*

*Overrides void*

Defined in types.ts:915
