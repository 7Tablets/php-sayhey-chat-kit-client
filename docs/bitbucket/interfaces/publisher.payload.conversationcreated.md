[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationCreated](publisher.payload.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* **ConversationCreated**

## Index

### Properties

* [conversationId](publisher.payload.conversationcreated.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:456
