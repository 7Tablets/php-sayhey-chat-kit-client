[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationHideAndClear](sayhey.payload.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* **ConversationHideAndClear**

## Index

### Properties

* [conversationId](sayhey.payload.conversationhideandclear.md#markdown-header-conversationid)
* [startAfter](sayhey.payload.conversationhideandclear.md#markdown-header-startafter)
* [visible](sayhey.payload.conversationhideandclear.md#markdown-header-visible)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:748

___

###  startAfter

• **startAfter**: *Date*

Defined in types.ts:749

___

###  visible

• **visible**: *boolean*

Defined in types.ts:750
