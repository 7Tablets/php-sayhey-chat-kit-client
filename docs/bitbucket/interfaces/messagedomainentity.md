[php-sayhey-chat-kit-client](../globals.md) › [MessageDomainEntity](messagedomainentity.md)

# Interface: MessageDomainEntity

## Hierarchy

* [BaseMessage](basemessage.md)

  ↳ **MessageDomainEntity**

## Index

### Properties

* [conversationId](messagedomainentity.md#markdown-header-conversationid)
* [createdAt](messagedomainentity.md#markdown-header-createdat)
* [deletedAt](messagedomainentity.md#markdown-header-deletedat)
* [file](messagedomainentity.md#markdown-header-file)
* [id](messagedomainentity.md#markdown-header-id)
* [parsedText](messagedomainentity.md#markdown-header-parsedtext)
* [previewUrl](messagedomainentity.md#markdown-header-optional-previewurl)
* [reactions](messagedomainentity.md#markdown-header-reactions)
* [refUrl](messagedomainentity.md#markdown-header-refurl)
* [sender](messagedomainentity.md#markdown-header-sender)
* [sent](messagedomainentity.md#markdown-header-sent)
* [sequence](messagedomainentity.md#markdown-header-sequence)
* [systemMessage](messagedomainentity.md#markdown-header-systemmessage)
* [text](messagedomainentity.md#markdown-header-text)
* [type](messagedomainentity.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [BaseMessage](basemessage.md).[conversationId](basemessage.md#markdown-header-conversationid)*

Defined in types.ts:156

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:177

___

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:178

___

###  file

• **file**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:176

___

###  id

• **id**: *string*

*Inherited from [BaseMessage](basemessage.md).[id](basemessage.md#markdown-header-id)*

Defined in types.ts:155

___

###  parsedText

• **parsedText**: *[ParsedLine](../globals.md#markdown-header-parsedline)[] | null*

Defined in types.ts:180

___

### `Optional` previewUrl

• **previewUrl**? : *string | null*

Defined in types.ts:181

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

*Inherited from [BaseMessage](basemessage.md).[reactions](basemessage.md#markdown-header-reactions)*

Defined in types.ts:161

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[refUrl](basemessage.md#markdown-header-refurl)*

Defined in types.ts:159

___

###  sender

• **sender**: *[UserDomainEntity](userdomainentity.md) | null*

Defined in types.ts:175

___

###  sent

• **sent**: *boolean*

Defined in types.ts:179

___

###  sequence

• **sequence**: *number*

*Inherited from [BaseMessage](basemessage.md).[sequence](basemessage.md#markdown-header-sequence)*

Defined in types.ts:160

___

###  systemMessage

• **systemMessage**: *[SystemMessageDomainEntity](systemmessagedomainentity.md) | null*

Defined in types.ts:182

___

###  text

• **text**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[text](basemessage.md#markdown-header-text)*

Defined in types.ts:158

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [BaseMessage](basemessage.md).[type](basemessage.md#markdown-header-type)*

Defined in types.ts:157
