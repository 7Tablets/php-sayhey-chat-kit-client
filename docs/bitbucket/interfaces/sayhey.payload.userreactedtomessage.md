[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserReactedToMessage](sayhey.payload.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* **UserReactedToMessage**

## Index

### Properties

* [conversationId](sayhey.payload.userreactedtomessage.md#markdown-header-conversationid)
* [messageId](sayhey.payload.userreactedtomessage.md#markdown-header-messageid)
* [type](sayhey.payload.userreactedtomessage.md#markdown-header-type)
* [userId](sayhey.payload.userreactedtomessage.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:796

___

###  messageId

• **messageId**: *string*

Defined in types.ts:797

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:798

___

###  userId

• **userId**: *string*

Defined in types.ts:799
