[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserEnabled](sayhey.payload.userenabled.md)

# Interface: UserEnabled

## Hierarchy

* **UserEnabled**

## Index

### Properties

* [disabled](sayhey.payload.userenabled.md#markdown-header-disabled)
* [userId](sayhey.payload.userenabled.md#markdown-header-userid)

## Properties

###  disabled

• **disabled**: *boolean*

Defined in types.ts:822

___

###  userId

• **userId**: *string*

Defined in types.ts:821
