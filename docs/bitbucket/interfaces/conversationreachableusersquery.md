[php-sayhey-chat-kit-client](../globals.md) › [ConversationReachableUsersQuery](conversationreachableusersquery.md)

# Interface: ConversationReachableUsersQuery

## Hierarchy

* **ConversationReachableUsersQuery**

  ↳ [ConversationUserQuery](conversationuserquery.md)

## Index

### Properties

* [conversationId](conversationreachableusersquery.md#markdown-header-conversationid)
* [includeRefIdInSearch](conversationreachableusersquery.md#markdown-header-optional-includerefidinsearch)
* [limit](conversationreachableusersquery.md#markdown-header-optional-limit)
* [nextCursor](conversationreachableusersquery.md#markdown-header-optional-nextcursor)
* [searchString](conversationreachableusersquery.md#markdown-header-optional-searchstring)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:369

___

### `Optional` includeRefIdInSearch

• **includeRefIdInSearch**? : *undefined | false | true*

Defined in types.ts:373

___

### `Optional` limit

• **limit**? : *undefined | number*

Defined in types.ts:371

___

### `Optional` nextCursor

• **nextCursor**? : *undefined | number*

Defined in types.ts:372

___

### `Optional` searchString

• **searchString**? : *undefined | string*

Defined in types.ts:370
