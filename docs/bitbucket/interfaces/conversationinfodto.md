[php-sayhey-chat-kit-client](../globals.md) › [ConversationInfoDTO](conversationinfodto.md)

# Interface: ConversationInfoDTO

## Hierarchy

* **ConversationInfoDTO**

  ↳ [UserConversationInfoDTO](userconversationinfodto.md)

## Index

### Properties

* [name](conversationinfodto.md#markdown-header-name)
* [picture](conversationinfodto.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

Defined in types.ts:206

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:205
