[php-sayhey-chat-kit-client](../globals.md) › [BaseUser](baseuser.md)

# Interface: BaseUser

User Types

## Hierarchy

* **BaseUser**

  ↳ [UserDTO](userdto.md)

  ↳ [UserDomainEntity](userdomainentity.md)

## Index

### Properties

* [disabled](baseuser.md#markdown-header-disabled)
* [email](baseuser.md#markdown-header-email)
* [firstName](baseuser.md#markdown-header-firstname)
* [id](baseuser.md#markdown-header-id)
* [lastName](baseuser.md#markdown-header-lastname)
* [refId](baseuser.md#markdown-header-refid)

## Properties

###  disabled

• **disabled**: *boolean*

Defined in types.ts:296

___

###  email

• **email**: *string*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

Defined in types.ts:292

___

###  id

• **id**: *string*

Defined in types.ts:291

___

###  lastName

• **lastName**: *string*

Defined in types.ts:293

___

###  refId

• **refId**: *string | null*

Defined in types.ts:295
