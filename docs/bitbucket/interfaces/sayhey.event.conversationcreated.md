[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationCreated](sayhey.event.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* **ConversationCreated**

## Index

### Properties

* [event](sayhey.event.conversationcreated.md#markdown-header-event)
* [payload](sayhey.event.conversationcreated.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationCreated*

Defined in types.ts:844

___

###  payload

• **payload**: *[ConversationCreated](publisher.payload.conversationcreated.md)*

Defined in types.ts:845
