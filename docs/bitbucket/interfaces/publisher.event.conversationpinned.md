[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationPinned](publisher.event.conversationpinned.md)

# Interface: ConversationPinned

## Hierarchy

* EventMessage

  ↳ **ConversationPinned**

## Index

### Properties

* [event](publisher.event.conversationpinned.md#markdown-header-event)
* [payload](publisher.event.conversationpinned.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationPinned](../enums/publisher.eventtype.md#markdown-header-conversationpinned)*

*Overrides void*

Defined in types.ts:607

___

###  payload

• **payload**: *[ConversationPinned](publisher.payload.conversationpinned.md)*

*Overrides void*

Defined in types.ts:608
