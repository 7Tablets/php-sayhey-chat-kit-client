[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserUnReactedToMessage](publisher.payload.userunreactedtomessage.md)

# Interface: UserUnReactedToMessage

## Hierarchy

* **UserUnReactedToMessage**

## Index

### Properties

* [conversationId](publisher.payload.userunreactedtomessage.md#markdown-header-conversationid)
* [messageId](publisher.payload.userunreactedtomessage.md#markdown-header-messageid)
* [type](publisher.payload.userunreactedtomessage.md#markdown-header-optional-type)
* [userId](publisher.payload.userunreactedtomessage.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:546

___

###  messageId

• **messageId**: *string*

Defined in types.ts:547

___

### `Optional` type

• **type**? : *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:549

___

###  userId

• **userId**: *string*

Defined in types.ts:548
