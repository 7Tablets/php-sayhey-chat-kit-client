[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationUnpinned](sayhey.event.conversationunpinned.md)

# Interface: ConversationUnpinned

## Hierarchy

* **ConversationUnpinned**

## Index

### Properties

* [event](sayhey.event.conversationunpinned.md#markdown-header-event)
* [payload](sayhey.event.conversationunpinned.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationUnpinned*

Defined in types.ts:869

___

###  payload

• **payload**: *[ConversationUnpinned](publisher.payload.conversationunpinned.md)*

Defined in types.ts:870
