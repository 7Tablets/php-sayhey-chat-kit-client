[php-sayhey-chat-kit-client](../globals.md) › [GalleryPayload](gallerypayload.md)

# Interface: GalleryPayload

## Hierarchy

* **GalleryPayload**

## Index

### Properties

* [messageList](gallerypayload.md#markdown-header-messagelist)
* [nextCursor](gallerypayload.md#markdown-header-nextcursor)

## Properties

###  messageList

• **messageList**: *[MessageDomainEntity](messagedomainentity.md)[]*

Defined in types.ts:336

___

###  nextCursor

• **nextCursor**: *number*

Defined in types.ts:335
