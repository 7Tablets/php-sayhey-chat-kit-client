[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [MessageReactionsUpdate](publisher.payload.messagereactionsupdate.md)

# Interface: MessageReactionsUpdate

## Hierarchy

* **MessageReactionsUpdate**

## Index

### Properties

* [conversationId](publisher.payload.messagereactionsupdate.md#markdown-header-conversationid)
* [messageId](publisher.payload.messagereactionsupdate.md#markdown-header-messageid)
* [messageReactionsObject](publisher.payload.messagereactionsupdate.md#markdown-header-messagereactionsobject)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:553

___

###  messageId

• **messageId**: *string*

Defined in types.ts:554

___

###  messageReactionsObject

• **messageReactionsObject**: *[ReactionsObject](../classes/reactionsobject.md)*

Defined in types.ts:555
