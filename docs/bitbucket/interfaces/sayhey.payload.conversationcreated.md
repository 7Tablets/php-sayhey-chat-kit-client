[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationCreated](sayhey.payload.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* **ConversationCreated**

## Index

### Properties

* [conversationId](sayhey.payload.conversationcreated.md#markdown-header-conversationid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:721
