[php-sayhey-chat-kit-client](../globals.md) › [UserSimpleInfo](usersimpleinfo.md)

# Interface: UserSimpleInfo

## Hierarchy

* **UserSimpleInfo**

## Index

### Properties

* [firstName](usersimpleinfo.md#markdown-header-firstname)
* [id](usersimpleinfo.md#markdown-header-id)
* [lastName](usersimpleinfo.md#markdown-header-lastname)
* [refId](usersimpleinfo.md#markdown-header-refid)

## Properties

###  firstName

• **firstName**: *string*

Defined in types.ts:148

___

###  id

• **id**: *string*

Defined in types.ts:150

___

###  lastName

• **lastName**: *string*

Defined in types.ts:149

___

###  refId

• **refId**: *string | null*

Defined in types.ts:151
