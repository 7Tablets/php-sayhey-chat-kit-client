[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationMuted](publisher.payload.conversationmuted.md)

# Interface: ConversationMuted

## Hierarchy

* **ConversationMuted**

## Index

### Properties

* [conversationId](publisher.payload.conversationmuted.md#markdown-header-conversationid)
* [conversationMuteDto](publisher.payload.conversationmuted.md#markdown-header-conversationmutedto)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:468

___

###  conversationMuteDto

• **conversationMuteDto**: *object*

Defined in types.ts:469

#### Type declaration:

* **muteUntil**: *string*
