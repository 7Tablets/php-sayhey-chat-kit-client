[php-sayhey-chat-kit-client](../globals.md) › [ConversationConversableUsersBatch](conversationconversableusersbatch.md)

# Interface: ConversationConversableUsersBatch

## Hierarchy

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ **ConversationConversableUsersBatch**

## Index

### Properties

* [completed](conversationconversableusersbatch.md#markdown-header-completed)
* [conversationId](conversationconversableusersbatch.md#markdown-header-conversationid)
* [limit](conversationconversableusersbatch.md#markdown-header-limit)
* [nextOffset](conversationconversableusersbatch.md#markdown-header-nextoffset)
* [offset](conversationconversableusersbatch.md#markdown-header-offset)
* [results](conversationconversableusersbatch.md#markdown-header-results)
* [search](conversationconversableusersbatch.md#markdown-header-search)
* [totalCount](conversationconversableusersbatch.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationConversableUsersBatchDTO](conversationconversableusersbatchdto.md).[conversationId](conversationconversableusersbatchdto.md#markdown-header-conversationid)*

Defined in types.ts:395

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  results

• **results**: *[UserDomainEntity](userdomainentity.md)[]*

Defined in types.ts:391

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
