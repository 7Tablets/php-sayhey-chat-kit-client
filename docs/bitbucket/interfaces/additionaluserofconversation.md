[php-sayhey-chat-kit-client](../globals.md) › [AdditionalUserOfConversation](additionaluserofconversation.md)

# Interface: AdditionalUserOfConversation

## Hierarchy

* **AdditionalUserOfConversation**

  ↳ [UserOfConversationDTO](userofconversationdto.md)

## Index

### Properties

* [deletedFromConversationAt](additionaluserofconversation.md#markdown-header-deletedfromconversationat)
* [isOwner](additionaluserofconversation.md#markdown-header-isowner)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:381

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:382
