[php-sayhey-chat-kit-client](../globals.md) › [ConversationReactionsDTO](conversationreactionsdto.md)

# Interface: ConversationReactionsDTO

## Hierarchy

* **ConversationReactionsDTO**

## Index

### Properties

* [messageId](conversationreactionsdto.md#markdown-header-messageid)
* [type](conversationreactionsdto.md#markdown-header-type)

## Properties

###  messageId

• **messageId**: *string*

Defined in types.ts:1180

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md) | [ReactionType](../enums/reactiontype.md)[]*

Defined in types.ts:1181
