[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [UserUnReactedToMessage](sayhey.payload.userunreactedtomessage.md)

# Interface: UserUnReactedToMessage

## Hierarchy

* **UserUnReactedToMessage**

## Index

### Properties

* [conversationId](sayhey.payload.userunreactedtomessage.md#markdown-header-conversationid)
* [messageId](sayhey.payload.userunreactedtomessage.md#markdown-header-messageid)
* [type](sayhey.payload.userunreactedtomessage.md#markdown-header-optional-type)
* [userId](sayhey.payload.userunreactedtomessage.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:809

___

###  messageId

• **messageId**: *string*

Defined in types.ts:810

___

### `Optional` type

• **type**? : *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:812

___

###  userId

• **userId**: *string*

Defined in types.ts:811
