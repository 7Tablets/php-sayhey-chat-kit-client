[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupOwnerRemoved](sayhey.event.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* **ConversationGroupOwnerRemoved**

## Index

### Properties

* [event](sayhey.event.conversationgroupownerremoved.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupownerremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupOwnerRemoved*

Defined in types.ts:889

___

###  payload

• **payload**: *[ConversationGroupOwnerRemoved](publisher.payload.conversationgroupownerremoved.md)*

Defined in types.ts:890
