[php-sayhey-chat-kit-client](../globals.md) › [ReachableUsersQuery](reachableusersquery.md)

# Interface: ReachableUsersQuery

## Hierarchy

* **ReachableUsersQuery**

## Index

### Properties

* [includeRefIdInSearch](reachableusersquery.md#markdown-header-optional-includerefidinsearch)
* [limit](reachableusersquery.md#markdown-header-optional-limit)
* [nextCursor](reachableusersquery.md#markdown-header-optional-nextcursor)
* [searchString](reachableusersquery.md#markdown-header-optional-searchstring)

## Properties

### `Optional` includeRefIdInSearch

• **includeRefIdInSearch**? : *undefined | false | true*

Defined in types.ts:359

___

### `Optional` limit

• **limit**? : *undefined | number*

Defined in types.ts:357

___

### `Optional` nextCursor

• **nextCursor**? : *undefined | number*

Defined in types.ts:358

___

### `Optional` searchString

• **searchString**? : *undefined | string*

Defined in types.ts:356
