[php-sayhey-chat-kit-client](../globals.md) › [ReactedUserDTO](reacteduserdto.md)

# Interface: ReactedUserDTO

## Hierarchy

* **ReactedUserDTO**

## Index

### Properties

* [createdAt](reacteduserdto.md#markdown-header-createdat)
* [messageId](reacteduserdto.md#markdown-header-messageid)
* [sequence](reacteduserdto.md#markdown-header-sequence)
* [type](reacteduserdto.md#markdown-header-type)
* [user](reacteduserdto.md#markdown-header-user)
* [userId](reacteduserdto.md#markdown-header-userid)

## Properties

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:1209

___

###  messageId

• **messageId**: *string*

Defined in types.ts:1210

___

###  sequence

• **sequence**: *number*

Defined in types.ts:1208

___

###  type

• **type**: *[ReactionType](../enums/reactiontype.md)*

Defined in types.ts:1207

___

###  user

• **user**: *[UserDTO](userdto.md)*

Defined in types.ts:1212

___

###  userId

• **userId**: *string*

Defined in types.ts:1211
