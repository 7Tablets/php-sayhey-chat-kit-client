[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationGroupMemberAdded](publisher.event.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberAdded**

## Index

### Properties

* [event](publisher.event.conversationgroupmemberadded.md#markdown-header-event)
* [payload](publisher.event.conversationgroupmemberadded.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationGroupMemberAdded](../enums/publisher.eventtype.md#markdown-header-conversationgroupmemberadded)*

*Overrides void*

Defined in types.ts:647

___

###  payload

• **payload**: *[ConversationGroupMemberAdded](publisher.payload.conversationgroupmemberadded.md)*

*Overrides void*

Defined in types.ts:648
