[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationCreated](publisher.event.conversationcreated.md)

# Interface: ConversationCreated

## Hierarchy

* EventMessage

  ↳ **ConversationCreated**

## Index

### Properties

* [event](publisher.event.conversationcreated.md#markdown-header-event)
* [payload](publisher.event.conversationcreated.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationCreated](../enums/publisher.eventtype.md#markdown-header-conversationcreated)*

*Overrides void*

Defined in types.ts:587

___

###  payload

• **payload**: *[ConversationCreated](publisher.payload.conversationcreated.md)*

*Overrides void*

Defined in types.ts:588
