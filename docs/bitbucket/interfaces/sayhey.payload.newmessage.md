[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [NewMessage](sayhey.payload.newmessage.md)

# Interface: NewMessage

## Hierarchy

* **NewMessage**

## Index

### Properties

* [clientRefId](sayhey.payload.newmessage.md#markdown-header-clientrefid)
* [message](sayhey.payload.newmessage.md#markdown-header-message)

## Properties

###  clientRefId

• **clientRefId**: *string | null*

Defined in types.ts:726

___

###  message

• **message**: *[MessageDomainEntity](messagedomainentity.md)*

Defined in types.ts:725
