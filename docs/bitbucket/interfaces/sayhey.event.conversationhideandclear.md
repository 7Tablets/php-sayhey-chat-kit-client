[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationHideAndClear](sayhey.event.conversationhideandclear.md)

# Interface: ConversationHideAndClear

## Hierarchy

* **ConversationHideAndClear**

## Index

### Properties

* [event](sayhey.event.conversationhideandclear.md#markdown-header-event)
* [payload](sayhey.event.conversationhideandclear.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationHideAndClear*

Defined in types.ts:874

___

###  payload

• **payload**: *[ConversationHideAndClear](publisher.payload.conversationhideandclear.md)*

Defined in types.ts:875
