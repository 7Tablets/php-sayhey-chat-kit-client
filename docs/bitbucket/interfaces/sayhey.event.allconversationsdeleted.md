[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [AllConversationsDeleted](sayhey.event.allconversationsdeleted.md)

# Interface: AllConversationsDeleted

## Hierarchy

* **AllConversationsDeleted**

## Index

### Properties

* [event](sayhey.event.allconversationsdeleted.md#markdown-header-event)
* [payload](sayhey.event.allconversationsdeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationHideAndClearAll*

Defined in types.ts:839

___

###  payload

• **payload**: *[ConversationHideAndClearAll](publisher.payload.conversationhideandclearall.md)*

Defined in types.ts:840
