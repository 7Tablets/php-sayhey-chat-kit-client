[php-sayhey-chat-kit-client](../globals.md) › [IndividualConversationDTO](individualconversationdto.md)

# Interface: IndividualConversationDTO

## Hierarchy

  ↳ [BaseConversationDTO](baseconversationdto.md)

  ↳ **IndividualConversationDTO**

## Index

### Properties

* [badge](individualconversationdto.md#markdown-header-badge)
* [createdAt](individualconversationdto.md#markdown-header-createdat)
* [deletedFromConversationAt](individualconversationdto.md#markdown-header-deletedfromconversationat)
* [endBefore](individualconversationdto.md#markdown-header-endbefore)
* [id](individualconversationdto.md#markdown-header-id)
* [isOwner](individualconversationdto.md#markdown-header-isowner)
* [lastMessage](individualconversationdto.md#markdown-header-lastmessage)
* [lastReadMessageId](individualconversationdto.md#markdown-header-lastreadmessageid)
* [muteUntil](individualconversationdto.md#markdown-header-muteuntil)
* [pinnedAt](individualconversationdto.md#markdown-header-pinnedat)
* [serverCreated](individualconversationdto.md#markdown-header-servercreated)
* [serverManaged](individualconversationdto.md#markdown-header-servermanaged)
* [startAfter](individualconversationdto.md#markdown-header-startafter)
* [type](individualconversationdto.md#markdown-header-type)
* [updatedAt](individualconversationdto.md#markdown-header-updatedat)
* [user](individualconversationdto.md#markdown-header-user)
* [visible](individualconversationdto.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[badge](baseconversationdto.md#markdown-header-badge)*

Defined in types.ts:229

___

###  createdAt

• **createdAt**: *string*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[createdAt](baseconversationdto.md#markdown-header-createdat)*

Defined in types.ts:233

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[deletedFromConversationAt](baseconversationdto.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:232

___

###  endBefore

• **endBefore**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[endBefore](baseconversationdto.md#markdown-header-endbefore)*

Defined in types.ts:231

___

###  id

• **id**: *string*

*Inherited from [BaseConversation](baseconversation.md).[id](baseconversation.md#markdown-header-id)*

Defined in types.ts:195

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isOwner](baseconversation.md#markdown-header-isowner)*

Defined in types.ts:198

___

###  lastMessage

• **lastMessage**: *[MessageDTO](messagedto.md) | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[lastMessage](baseconversationdto.md#markdown-header-lastmessage)*

Defined in types.ts:235

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)*

Defined in types.ts:196

___

###  muteUntil

• **muteUntil**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[muteUntil](baseconversationdto.md#markdown-header-muteuntil)*

Defined in types.ts:227

___

###  pinnedAt

• **pinnedAt**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[pinnedAt](baseconversationdto.md#markdown-header-pinnedat)*

Defined in types.ts:228

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverCreated](baseconversation.md#markdown-header-servercreated)*

Defined in types.ts:200

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverManaged](baseconversation.md#markdown-header-servermanaged)*

Defined in types.ts:201

___

###  startAfter

• **startAfter**: *string | null*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[startAfter](baseconversationdto.md#markdown-header-startafter)*

Defined in types.ts:230

___

###  type

• **type**: *[Individual](../enums/conversationtype.md#markdown-header-individual)*

*Overrides [BaseConversation](baseconversation.md).[type](baseconversation.md#markdown-header-type)*

Defined in types.ts:239

___

###  updatedAt

• **updatedAt**: *string*

*Inherited from [BaseConversationDTO](baseconversationdto.md).[updatedAt](baseconversationdto.md#markdown-header-updatedat)*

Defined in types.ts:234

___

###  user

• **user**: *[UserConversationInfoDTO](userconversationinfodto.md)*

Defined in types.ts:240

___

###  visible

• **visible**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[visible](baseconversation.md#markdown-header-visible)*

Defined in types.ts:199
