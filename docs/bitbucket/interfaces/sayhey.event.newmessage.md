[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [NewMessage](sayhey.event.newmessage.md)

# Interface: NewMessage

## Hierarchy

* **NewMessage**

## Index

### Properties

* [event](sayhey.event.newmessage.md#markdown-header-event)
* [payload](sayhey.event.newmessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.NewMessage*

Defined in types.ts:849

___

###  payload

• **payload**: *[NewMessage](publisher.payload.newmessage.md)*

Defined in types.ts:850
