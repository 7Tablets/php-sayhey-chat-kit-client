[php-sayhey-chat-kit-client](../globals.md) › [DefaultGroupImageUrlSet](defaultgroupimageurlset.md)

# Interface: DefaultGroupImageUrlSet

## Hierarchy

* **DefaultGroupImageUrlSet**

## Index

### Properties

* [filename](defaultgroupimageurlset.md#markdown-header-filename)
* [original](defaultgroupimageurlset.md#markdown-header-original)
* [thumbnail](defaultgroupimageurlset.md#markdown-header-thumbnail)

## Properties

###  filename

• **filename**: *string*

Defined in types.ts:1197

___

###  original

• **original**: *string*

Defined in types.ts:1198

___

###  thumbnail

• **thumbnail**: *string*

Defined in types.ts:1199
