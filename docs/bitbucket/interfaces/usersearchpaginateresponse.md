[php-sayhey-chat-kit-client](../globals.md) › [UserSearchPaginateResponse](usersearchpaginateresponse.md)

# Interface: UserSearchPaginateResponse

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **UserSearchPaginateResponse**

## Index

### Properties

* [completed](usersearchpaginateresponse.md#markdown-header-completed)
* [includeRefIdInSearch](usersearchpaginateresponse.md#markdown-header-includerefidinsearch)
* [limit](usersearchpaginateresponse.md#markdown-header-limit)
* [nextOffset](usersearchpaginateresponse.md#markdown-header-nextoffset)
* [offset](usersearchpaginateresponse.md#markdown-header-offset)
* [search](usersearchpaginateresponse.md#markdown-header-search)
* [totalCount](usersearchpaginateresponse.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  includeRefIdInSearch

• **includeRefIdInSearch**: *boolean*

Defined in types.ts:1203

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
