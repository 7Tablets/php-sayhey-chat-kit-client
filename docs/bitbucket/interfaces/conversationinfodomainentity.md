[php-sayhey-chat-kit-client](../globals.md) › [ConversationInfoDomainEntity](conversationinfodomainentity.md)

# Interface: ConversationInfoDomainEntity

## Hierarchy

* **ConversationInfoDomainEntity**

  ↳ [UserConversationInfoDomainEntity](userconversationinfodomainentity.md)

## Index

### Properties

* [name](conversationinfodomainentity.md#markdown-header-name)
* [picture](conversationinfodomainentity.md#markdown-header-picture)

## Properties

###  name

• **name**: *string*

Defined in types.ts:211

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:210
