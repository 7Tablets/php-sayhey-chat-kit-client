[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupMemberRemoved](publisher.payload.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* **ConversationGroupMemberRemoved**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupmemberremoved.md#markdown-header-conversationid)
* [deletedAt](publisher.payload.conversationgroupmemberremoved.md#markdown-header-deletedat)
* [endBefore](publisher.payload.conversationgroupmemberremoved.md#markdown-header-endbefore)
* [userIds](publisher.payload.conversationgroupmemberremoved.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:532

___

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:534

___

###  endBefore

• **endBefore**: *Date*

Defined in types.ts:535

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:533
