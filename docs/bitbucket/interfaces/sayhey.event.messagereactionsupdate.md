[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [MessageReactionsUpdate](sayhey.event.messagereactionsupdate.md)

# Interface: MessageReactionsUpdate

## Hierarchy

* EventMessage

  ↳ **MessageReactionsUpdate**

## Index

### Properties

* [event](sayhey.event.messagereactionsupdate.md#markdown-header-event)
* [payload](sayhey.event.messagereactionsupdate.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.MessageReactionsUpdate*

*Overrides void*

Defined in types.ts:919

___

###  payload

• **payload**: *[MessageReactionsUpdate](publisher.payload.messagereactionsupdate.md)*

*Overrides void*

Defined in types.ts:920
