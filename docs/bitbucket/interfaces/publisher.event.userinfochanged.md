[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserInfoChanged](publisher.event.userinfochanged.md)

# Interface: UserInfoChanged

## Hierarchy

* EventMessage

  ↳ **UserInfoChanged**

## Index

### Properties

* [event](publisher.event.userinfochanged.md#markdown-header-event)
* [payload](publisher.event.userinfochanged.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserInfoChanged](../enums/publisher.eventtype.md#markdown-header-userinfochanged)*

*Overrides void*

Defined in types.ts:627

___

###  payload

• **payload**: *[UserInfoChanged](publisher.payload.userinfochanged.md)*

*Overrides void*

Defined in types.ts:628
