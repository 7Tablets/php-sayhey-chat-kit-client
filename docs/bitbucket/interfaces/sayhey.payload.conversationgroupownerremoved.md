[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupOwnerRemoved](sayhey.payload.conversationgroupownerremoved.md)

# Interface: ConversationGroupOwnerRemoved

## Hierarchy

* **ConversationGroupOwnerRemoved**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupownerremoved.md#markdown-header-conversationid)
* [isOwner](sayhey.payload.conversationgroupownerremoved.md#markdown-header-isowner)
* [userId](sayhey.payload.conversationgroupownerremoved.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:770

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:772

___

###  userId

• **userId**: *string*

Defined in types.ts:771
