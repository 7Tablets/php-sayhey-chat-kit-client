[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationPinned](sayhey.event.conversationpinned.md)

# Interface: ConversationPinned

## Hierarchy

* **ConversationPinned**

## Index

### Properties

* [event](sayhey.event.conversationpinned.md#markdown-header-event)
* [payload](sayhey.event.conversationpinned.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationPinned*

Defined in types.ts:859

___

###  payload

• **payload**: *[ConversationPinned](publisher.payload.conversationpinned.md)*

Defined in types.ts:860
