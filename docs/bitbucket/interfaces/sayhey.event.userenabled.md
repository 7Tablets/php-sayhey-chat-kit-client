[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserEnabled](sayhey.event.userenabled.md)

# Interface: UserEnabled

## Hierarchy

* EventMessage

  ↳ **UserEnabled**

## Index

### Properties

* [event](sayhey.event.userenabled.md#markdown-header-event)
* [payload](sayhey.event.userenabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.UserEnabled*

*Overrides void*

Defined in types.ts:929

___

###  payload

• **payload**: *[UserEnabled](publisher.payload.userenabled.md)*

*Overrides void*

Defined in types.ts:930
