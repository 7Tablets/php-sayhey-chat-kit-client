[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [ConversationUnmuted](publisher.event.conversationunmuted.md)

# Interface: ConversationUnmuted

## Hierarchy

* EventMessage

  ↳ **ConversationUnmuted**

## Index

### Properties

* [event](publisher.event.conversationunmuted.md#markdown-header-event)
* [payload](publisher.event.conversationunmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[ConversationUnmuted](../enums/publisher.eventtype.md#markdown-header-conversationunmuted)*

*Overrides void*

Defined in types.ts:592

___

###  payload

• **payload**: *[ConversationUnmuted](publisher.payload.conversationunmuted.md)*

*Overrides void*

Defined in types.ts:593
