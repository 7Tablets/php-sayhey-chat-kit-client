[php-sayhey-chat-kit-client](../globals.md) › [ConversableUsersBatchMeta](conversableusersbatchmeta.md)

# Interface: ConversableUsersBatchMeta

DTOs

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **ConversableUsersBatchMeta**

  ↳ [ConversableUsersBatchDTO](conversableusersbatchdto.md)

  ↳ [ConversableUsersBatchResult](conversableusersbatchresult.md)

## Index

### Properties

* [completed](conversableusersbatchmeta.md#markdown-header-completed)
* [limit](conversableusersbatchmeta.md#markdown-header-limit)
* [nextOffset](conversableusersbatchmeta.md#markdown-header-nextoffset)
* [offset](conversableusersbatchmeta.md#markdown-header-offset)
* [search](conversableusersbatchmeta.md#markdown-header-search)
* [totalCount](conversableusersbatchmeta.md#markdown-header-totalcount)
* [userId](conversableusersbatchmeta.md#markdown-header-userid)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17

___

###  userId

• **userId**: *string*

Defined in types.ts:344
