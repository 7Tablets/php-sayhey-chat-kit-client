[php-sayhey-chat-kit-client](../globals.md) › [UserSelfDomainEntity](userselfdomainentity.md)

# Interface: UserSelfDomainEntity

## Hierarchy

  ↳ [UserDomainEntity](userdomainentity.md)

  ↳ **UserSelfDomainEntity**

## Index

### Properties

* [deletedAt](userselfdomainentity.md#markdown-header-deletedat)
* [disabled](userselfdomainentity.md#markdown-header-disabled)
* [email](userselfdomainentity.md#markdown-header-email)
* [firstName](userselfdomainentity.md#markdown-header-firstname)
* [fullName](userselfdomainentity.md#markdown-header-fullname)
* [id](userselfdomainentity.md#markdown-header-id)
* [lastName](userselfdomainentity.md#markdown-header-lastname)
* [muteAllUntil](userselfdomainentity.md#markdown-header-mutealluntil)
* [picture](userselfdomainentity.md#markdown-header-picture)
* [refId](userselfdomainentity.md#markdown-header-refid)
* [username](userselfdomainentity.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserDomainEntity](userdomainentity.md).[deletedAt](userdomainentity.md#markdown-header-deletedat)*

Defined in types.ts:308

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  fullName

• **fullName**: *string*

*Inherited from [UserDomainEntity](userdomainentity.md).[fullName](userdomainentity.md#markdown-header-fullname)*

Defined in types.ts:305

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  muteAllUntil

• **muteAllUntil**: *Date | null*

Defined in types.ts:1193

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

*Inherited from [UserDomainEntity](userdomainentity.md).[picture](userdomainentity.md#markdown-header-picture)*

Defined in types.ts:307

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295

___

###  username

• **username**: *string*

*Inherited from [UserDomainEntity](userdomainentity.md).[username](userdomainentity.md#markdown-header-username)*

Defined in types.ts:306
