[php-sayhey-chat-kit-client](../globals.md) › [UserOfConversationDTO](userofconversationdto.md)

# Interface: UserOfConversationDTO

## Hierarchy

  ↳ [UserDTO](userdto.md)

* [AdditionalUserOfConversation](additionaluserofconversation.md)

  ↳ **UserOfConversationDTO**

## Index

### Properties

* [deletedAt](userofconversationdto.md#markdown-header-deletedat)
* [deletedFromConversationAt](userofconversationdto.md#markdown-header-deletedfromconversationat)
* [disabled](userofconversationdto.md#markdown-header-disabled)
* [email](userofconversationdto.md#markdown-header-email)
* [firstName](userofconversationdto.md#markdown-header-firstname)
* [id](userofconversationdto.md#markdown-header-id)
* [isOwner](userofconversationdto.md#markdown-header-isowner)
* [lastName](userofconversationdto.md#markdown-header-lastname)
* [picture](userofconversationdto.md#markdown-header-picture)
* [refId](userofconversationdto.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *string | null*

*Inherited from [UserDTO](userdto.md).[deletedAt](userdto.md#markdown-header-deletedat)*

Defined in types.ts:301

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

*Inherited from [AdditionalUserOfConversation](additionaluserofconversation.md).[deletedFromConversationAt](additionaluserofconversation.md#markdown-header-deletedfromconversationat)*

Defined in types.ts:381

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [AdditionalUserOfConversation](additionaluserofconversation.md).[isOwner](additionaluserofconversation.md#markdown-header-isowner)*

Defined in types.ts:382

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [UserDTO](userdto.md).[picture](userdto.md#markdown-header-picture)*

Defined in types.ts:300

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295
