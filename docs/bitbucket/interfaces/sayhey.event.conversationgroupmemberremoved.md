[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupMemberRemoved](sayhey.event.conversationgroupmemberremoved.md)

# Interface: ConversationGroupMemberRemoved

## Hierarchy

* EventMessage

  ↳ **ConversationGroupMemberRemoved**

## Index

### Properties

* [event](sayhey.event.conversationgroupmemberremoved.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupmemberremoved.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupMemberRemoved*

*Overrides void*

Defined in types.ts:899

___

###  payload

• **payload**: *[ConversationGroupMemberRemoved](publisher.payload.conversationgroupmemberremoved.md)*

*Overrides void*

Defined in types.ts:900
