[php-sayhey-chat-kit-client](../globals.md) › [BaseConversationDomainEntity](baseconversationdomainentity.md)

# Interface: BaseConversationDomainEntity

## Hierarchy

* object

  ↳ **BaseConversationDomainEntity**

  ↳ [GroupConversationDomainEntity](groupconversationdomainentity.md)

  ↳ [IndividualConversationDomainEntity](individualconversationdomainentity.md)

## Index

### Properties

* [badge](baseconversationdomainentity.md#markdown-header-badge)
* [createdAt](baseconversationdomainentity.md#markdown-header-createdat)
* [deletedFromConversationAt](baseconversationdomainentity.md#markdown-header-deletedfromconversationat)
* [endBefore](baseconversationdomainentity.md#markdown-header-endbefore)
* [lastMessage](baseconversationdomainentity.md#markdown-header-lastmessage)
* [muteUntil](baseconversationdomainentity.md#markdown-header-muteuntil)
* [pinnedAt](baseconversationdomainentity.md#markdown-header-pinnedat)
* [startAfter](baseconversationdomainentity.md#markdown-header-startafter)
* [updatedAt](baseconversationdomainentity.md#markdown-header-updatedat)

## Properties

###  badge

• **badge**: *number*

Defined in types.ts:253

___

###  createdAt

• **createdAt**: *Date*

Defined in types.ts:257

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:256

___

###  endBefore

• **endBefore**: *Date | null*

Defined in types.ts:255

___

###  lastMessage

• **lastMessage**: *[MessageDomainEntity](messagedomainentity.md) | null*

Defined in types.ts:259

___

###  muteUntil

• **muteUntil**: *Date | null*

Defined in types.ts:251

___

###  pinnedAt

• **pinnedAt**: *Date | null*

Defined in types.ts:252

___

###  startAfter

• **startAfter**: *Date | null*

Defined in types.ts:254

___

###  updatedAt

• **updatedAt**: *Date*

Defined in types.ts:258
