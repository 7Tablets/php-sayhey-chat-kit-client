[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserInfoChanged](publisher.payload.userinfochanged.md)

# Interface: UserInfoChanged

## Hierarchy

* **UserInfoChanged**

## Index

### Properties

* [userEmail](publisher.payload.userinfochanged.md#markdown-header-useremail)
* [userFirstName](publisher.payload.userinfochanged.md#markdown-header-userfirstname)
* [userId](publisher.payload.userinfochanged.md#markdown-header-userid)
* [userLastName](publisher.payload.userinfochanged.md#markdown-header-userlastname)
* [userMuteAllUntil](publisher.payload.userinfochanged.md#markdown-header-usermutealluntil)
* [userPicture](publisher.payload.userinfochanged.md#markdown-header-userpicture)
* [userRefId](publisher.payload.userinfochanged.md#markdown-header-userrefid)

## Properties

###  userEmail

• **userEmail**: *string*

Defined in types.ts:504

___

###  userFirstName

• **userFirstName**: *string*

Defined in types.ts:506

___

###  userId

• **userId**: *string*

Defined in types.ts:503

___

###  userLastName

• **userLastName**: *string*

Defined in types.ts:507

___

###  userMuteAllUntil

• **userMuteAllUntil**: *Date | null*

Defined in types.ts:509

___

###  userPicture

• **userPicture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:505

___

###  userRefId

• **userRefId**: *string | null*

Defined in types.ts:508
