[php-sayhey-chat-kit-client](../globals.md) › [MessageDTO](messagedto.md)

# Interface: MessageDTO

## Hierarchy

* [BaseMessage](basemessage.md)

  ↳ **MessageDTO**

## Index

### Properties

* [conversationId](messagedto.md#markdown-header-conversationid)
* [createdAt](messagedto.md#markdown-header-createdat)
* [deletedAt](messagedto.md#markdown-header-deletedat)
* [file](messagedto.md#markdown-header-file)
* [id](messagedto.md#markdown-header-id)
* [reactions](messagedto.md#markdown-header-reactions)
* [refUrl](messagedto.md#markdown-header-refurl)
* [sender](messagedto.md#markdown-header-sender)
* [sequence](messagedto.md#markdown-header-sequence)
* [systemMessage](messagedto.md#markdown-header-systemmessage)
* [text](messagedto.md#markdown-header-text)
* [type](messagedto.md#markdown-header-type)

## Properties

###  conversationId

• **conversationId**: *string*

*Inherited from [BaseMessage](basemessage.md).[conversationId](basemessage.md#markdown-header-conversationid)*

Defined in types.ts:156

___

###  createdAt

• **createdAt**: *string*

Defined in types.ts:165

___

###  deletedAt

• **deletedAt**: *string | null*

Defined in types.ts:169

___

###  file

• **file**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:167

___

###  id

• **id**: *string*

*Inherited from [BaseMessage](basemessage.md).[id](basemessage.md#markdown-header-id)*

Defined in types.ts:155

___

###  reactions

• **reactions**: *[ReactionsObject](../classes/reactionsobject.md)*

*Inherited from [BaseMessage](basemessage.md).[reactions](basemessage.md#markdown-header-reactions)*

Defined in types.ts:161

___

###  refUrl

• **refUrl**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[refUrl](basemessage.md#markdown-header-refurl)*

Defined in types.ts:159

___

###  sender

• **sender**: *[UserDTO](userdto.md) | null*

Defined in types.ts:166

___

###  sequence

• **sequence**: *number*

*Inherited from [BaseMessage](basemessage.md).[sequence](basemessage.md#markdown-header-sequence)*

Defined in types.ts:160

___

###  systemMessage

• **systemMessage**: *[SystemMessageDTO](systemmessagedto.md) | null*

Defined in types.ts:168

___

###  text

• **text**: *string | null*

*Inherited from [BaseMessage](basemessage.md).[text](basemessage.md#markdown-header-text)*

Defined in types.ts:158

___

###  type

• **type**: *[MessageType](../enums/messagetype.md)*

*Inherited from [BaseMessage](basemessage.md).[type](basemessage.md#markdown-header-type)*

Defined in types.ts:157
