[php-sayhey-chat-kit-client](../globals.md) › [UserDomainEntity](userdomainentity.md)

# Interface: UserDomainEntity

## Hierarchy

* [BaseUser](baseuser.md)

  ↳ **UserDomainEntity**

  ↳ [ConversationUserDomainEntity](conversationuserdomainentity.md)

  ↳ [UserSelfDomainEntity](userselfdomainentity.md)

## Index

### Properties

* [deletedAt](userdomainentity.md#markdown-header-deletedat)
* [disabled](userdomainentity.md#markdown-header-disabled)
* [email](userdomainentity.md#markdown-header-email)
* [firstName](userdomainentity.md#markdown-header-firstname)
* [fullName](userdomainentity.md#markdown-header-fullname)
* [id](userdomainentity.md#markdown-header-id)
* [lastName](userdomainentity.md#markdown-header-lastname)
* [picture](userdomainentity.md#markdown-header-picture)
* [refId](userdomainentity.md#markdown-header-refid)
* [username](userdomainentity.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

Defined in types.ts:308

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  fullName

• **fullName**: *string*

Defined in types.ts:305

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

Defined in types.ts:307

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295

___

###  username

• **username**: *string*

Defined in types.ts:306
