[php-sayhey-chat-kit-client](../globals.md) › [ConversationUsersBasicDTO](conversationusersbasicdto.md)

# Interface: ConversationUsersBasicDTO

## Hierarchy

* **ConversationUsersBasicDTO**

## Index

### Properties

* [deletedFromConversationAt](conversationusersbasicdto.md#markdown-header-deletedfromconversationat)
* [id](conversationusersbasicdto.md#markdown-header-id)
* [isOwner](conversationusersbasicdto.md#markdown-header-isowner)
* [userDeletedAt](conversationusersbasicdto.md#markdown-header-userdeletedat)
* [userDisabled](conversationusersbasicdto.md#markdown-header-userdisabled)

## Properties

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:419

___

###  id

• **id**: *string*

Defined in types.ts:418

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:417

___

###  userDeletedAt

• **userDeletedAt**: *Date | null*

Defined in types.ts:421

___

###  userDisabled

• **userDisabled**: *boolean*

Defined in types.ts:420
