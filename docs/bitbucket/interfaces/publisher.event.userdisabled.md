[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserDisabled](publisher.event.userdisabled.md)

# Interface: UserDisabled

## Hierarchy

* EventMessage

  ↳ **UserDisabled**

## Index

### Properties

* [event](publisher.event.userdisabled.md#markdown-header-event)
* [payload](publisher.event.userdisabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserDisabled](../enums/publisher.eventtype.md#markdown-header-userdisabled)*

*Overrides void*

Defined in types.ts:667

___

###  payload

• **payload**: *[UserDisabled](publisher.payload.userdisabled.md)*

*Overrides void*

Defined in types.ts:668
