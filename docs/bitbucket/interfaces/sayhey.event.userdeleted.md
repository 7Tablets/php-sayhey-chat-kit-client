[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [UserDeleted](sayhey.event.userdeleted.md)

# Interface: UserDeleted

## Hierarchy

* EventMessage

  ↳ **UserDeleted**

## Index

### Properties

* [event](sayhey.event.userdeleted.md#markdown-header-event)
* [payload](sayhey.event.userdeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserDeleted](../enums/publisher.eventtype.md#markdown-header-userdeleted)*

*Overrides void*

Defined in types.ts:939

___

###  payload

• **payload**: *[UserDeleted](publisher.payload.userdeleted.md)*

*Overrides void*

Defined in types.ts:940
