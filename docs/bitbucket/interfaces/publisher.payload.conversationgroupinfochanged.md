[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupInfoChanged](publisher.payload.conversationgroupinfochanged.md)

# Interface: ConversationGroupInfoChanged

## Hierarchy

* **ConversationGroupInfoChanged**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupinfochanged.md#markdown-header-conversationid)
* [groupName](publisher.payload.conversationgroupinfochanged.md#markdown-header-groupname)
* [groupPicture](publisher.payload.conversationgroupinfochanged.md#markdown-header-grouppicture)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:497

___

###  groupName

• **groupName**: *string*

Defined in types.ts:498

___

###  groupPicture

• **groupPicture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:499
