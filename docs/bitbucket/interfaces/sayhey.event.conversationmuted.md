[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationMuted](sayhey.event.conversationmuted.md)

# Interface: ConversationMuted

## Hierarchy

* **ConversationMuted**

## Index

### Properties

* [event](sayhey.event.conversationmuted.md#markdown-header-event)
* [payload](sayhey.event.conversationmuted.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationMuted*

Defined in types.ts:854

___

###  payload

• **payload**: *[ConversationMuted](publisher.payload.conversationmuted.md)*

Defined in types.ts:855
