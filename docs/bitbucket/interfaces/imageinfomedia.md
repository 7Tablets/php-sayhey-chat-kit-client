[php-sayhey-chat-kit-client](../globals.md) › [ImageInfoMedia](imageinfomedia.md)

# Interface: ImageInfoMedia

## Hierarchy

* **ImageInfoMedia**

## Index

### Properties

* [height](imageinfomedia.md#markdown-header-height)
* [subImagesInfo](imageinfomedia.md#markdown-header-subimagesinfo)
* [width](imageinfomedia.md#markdown-header-width)

## Properties

###  height

• **height**: *number*

Defined in types.ts:66

___

###  subImagesInfo

• **subImagesInfo**: *[SubImageMinimal](subimageminimal.md)[]*

Defined in types.ts:67

___

###  width

• **width**: *number*

Defined in types.ts:65
