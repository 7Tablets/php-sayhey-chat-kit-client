[php-sayhey-chat-kit-client](../globals.md) › [BaseConversationDTO](baseconversationdto.md)

# Interface: BaseConversationDTO

## Hierarchy

* [BaseConversation](baseconversation.md)

  ↳ **BaseConversationDTO**

  ↳ [IndividualConversationDTO](individualconversationdto.md)

  ↳ [GroupConversationDTO](groupconversationdto.md)

## Index

### Properties

* [badge](baseconversationdto.md#markdown-header-badge)
* [createdAt](baseconversationdto.md#markdown-header-createdat)
* [deletedFromConversationAt](baseconversationdto.md#markdown-header-deletedfromconversationat)
* [endBefore](baseconversationdto.md#markdown-header-endbefore)
* [id](baseconversationdto.md#markdown-header-id)
* [isOwner](baseconversationdto.md#markdown-header-isowner)
* [lastMessage](baseconversationdto.md#markdown-header-lastmessage)
* [lastReadMessageId](baseconversationdto.md#markdown-header-lastreadmessageid)
* [muteUntil](baseconversationdto.md#markdown-header-muteuntil)
* [pinnedAt](baseconversationdto.md#markdown-header-pinnedat)
* [serverCreated](baseconversationdto.md#markdown-header-servercreated)
* [serverManaged](baseconversationdto.md#markdown-header-servermanaged)
* [startAfter](baseconversationdto.md#markdown-header-startafter)
* [type](baseconversationdto.md#markdown-header-type)
* [updatedAt](baseconversationdto.md#markdown-header-updatedat)
* [visible](baseconversationdto.md#markdown-header-visible)

## Properties

###  badge

• **badge**: *number | null*

Defined in types.ts:229

___

###  createdAt

• **createdAt**: *string*

Defined in types.ts:233

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *string | null*

Defined in types.ts:232

___

###  endBefore

• **endBefore**: *string | null*

Defined in types.ts:231

___

###  id

• **id**: *string*

*Inherited from [BaseConversation](baseconversation.md).[id](baseconversation.md#markdown-header-id)*

Defined in types.ts:195

___

###  isOwner

• **isOwner**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[isOwner](baseconversation.md#markdown-header-isowner)*

Defined in types.ts:198

___

###  lastMessage

• **lastMessage**: *[MessageDTO](messagedto.md) | null*

Defined in types.ts:235

___

###  lastReadMessageId

• **lastReadMessageId**: *string | null*

*Inherited from [BaseConversation](baseconversation.md).[lastReadMessageId](baseconversation.md#markdown-header-lastreadmessageid)*

Defined in types.ts:196

___

###  muteUntil

• **muteUntil**: *string | null*

Defined in types.ts:227

___

###  pinnedAt

• **pinnedAt**: *string | null*

Defined in types.ts:228

___

###  serverCreated

• **serverCreated**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverCreated](baseconversation.md#markdown-header-servercreated)*

Defined in types.ts:200

___

###  serverManaged

• **serverManaged**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[serverManaged](baseconversation.md#markdown-header-servermanaged)*

Defined in types.ts:201

___

###  startAfter

• **startAfter**: *string | null*

Defined in types.ts:230

___

###  type

• **type**: *[Group](../enums/conversationtype.md#markdown-header-group) | [Individual](../enums/conversationtype.md#markdown-header-individual)*

*Inherited from [BaseConversation](baseconversation.md).[type](baseconversation.md#markdown-header-type)*

Defined in types.ts:197

___

###  updatedAt

• **updatedAt**: *string*

Defined in types.ts:234

___

###  visible

• **visible**: *boolean*

*Inherited from [BaseConversation](baseconversation.md).[visible](baseconversation.md#markdown-header-visible)*

Defined in types.ts:199
