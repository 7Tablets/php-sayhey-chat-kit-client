[php-sayhey-chat-kit-client](../globals.md) › [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

# Interface: ConversationUsersBatchMeta

## Hierarchy

  ↳ [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

  ↳ **ConversationUsersBatchMeta**

  ↳ [ConversationUsersBatchDTO](conversationusersbatchdto.md)

## Index

### Properties

* [completed](conversationusersbatchmeta.md#markdown-header-completed)
* [conversationId](conversationusersbatchmeta.md#markdown-header-conversationid)
* [limit](conversationusersbatchmeta.md#markdown-header-limit)
* [nextOffset](conversationusersbatchmeta.md#markdown-header-nextoffset)
* [offset](conversationusersbatchmeta.md#markdown-header-offset)
* [search](conversationusersbatchmeta.md#markdown-header-search)
* [totalCount](conversationusersbatchmeta.md#markdown-header-totalcount)
* [type](conversationusersbatchmeta.md#markdown-header-type)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  conversationId

• **conversationId**: *string*

*Inherited from [ConversationConversableUsersBatchDTO](conversationconversableusersbatchdto.md).[conversationId](conversationconversableusersbatchdto.md#markdown-header-conversationid)*

Defined in types.ts:395

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17

___

###  type

• **type**: *[ConversationUserQueryType](../enums/conversationuserquerytype.md)*

Defined in types.ts:399
