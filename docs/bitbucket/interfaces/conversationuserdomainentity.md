[php-sayhey-chat-kit-client](../globals.md) › [ConversationUserDomainEntity](conversationuserdomainentity.md)

# Interface: ConversationUserDomainEntity

## Hierarchy

  ↳ [UserDomainEntity](userdomainentity.md)

  ↳ **ConversationUserDomainEntity**

## Index

### Properties

* [deletedAt](conversationuserdomainentity.md#markdown-header-deletedat)
* [deletedFromConversationAt](conversationuserdomainentity.md#markdown-header-deletedfromconversationat)
* [disabled](conversationuserdomainentity.md#markdown-header-disabled)
* [email](conversationuserdomainentity.md#markdown-header-email)
* [firstName](conversationuserdomainentity.md#markdown-header-firstname)
* [fullName](conversationuserdomainentity.md#markdown-header-fullname)
* [id](conversationuserdomainentity.md#markdown-header-id)
* [isOwner](conversationuserdomainentity.md#markdown-header-isowner)
* [lastName](conversationuserdomainentity.md#markdown-header-lastname)
* [picture](conversationuserdomainentity.md#markdown-header-picture)
* [refId](conversationuserdomainentity.md#markdown-header-refid)
* [username](conversationuserdomainentity.md#markdown-header-username)

## Properties

###  deletedAt

• **deletedAt**: *Date | null*

*Inherited from [UserDomainEntity](userdomainentity.md).[deletedAt](userdomainentity.md#markdown-header-deletedat)*

Defined in types.ts:308

___

###  deletedFromConversationAt

• **deletedFromConversationAt**: *Date | null*

Defined in types.ts:408

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  fullName

• **fullName**: *string*

*Inherited from [UserDomainEntity](userdomainentity.md).[fullName](userdomainentity.md#markdown-header-fullname)*

Defined in types.ts:305

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:409

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  picture

• **picture**: *[MediaDomainEntity](mediadomainentity.md) | null*

*Inherited from [UserDomainEntity](userdomainentity.md).[picture](userdomainentity.md#markdown-header-picture)*

Defined in types.ts:307

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295

___

###  username

• **username**: *string*

*Inherited from [UserDomainEntity](userdomainentity.md).[username](userdomainentity.md#markdown-header-username)*

Defined in types.ts:306
