[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [ConversationGroupOwnerAdded](sayhey.event.conversationgroupowneradded.md)

# Interface: ConversationGroupOwnerAdded

## Hierarchy

* **ConversationGroupOwnerAdded**

## Index

### Properties

* [event](sayhey.event.conversationgroupowneradded.md#markdown-header-event)
* [payload](sayhey.event.conversationgroupowneradded.md#markdown-header-payload)

## Properties

###  event

• **event**: *SayHey.EventType.ConversationGroupOwnerAdded*

Defined in types.ts:894

___

###  payload

• **payload**: *[ConversationGroupOwnerAdded](publisher.payload.conversationgroupowneradded.md)*

Defined in types.ts:895
