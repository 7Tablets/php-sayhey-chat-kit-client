[php-sayhey-chat-kit-client](../globals.md) › [ConversationConversableUsersBatchMeta](conversationconversableusersbatchmeta.md)

# Interface: ConversationConversableUsersBatchMeta

## Hierarchy

* [SearchPaginateResponse](searchpaginateresponse.md)

  ↳ **ConversationConversableUsersBatchMeta**

  ↳ [ConversationConversableUsersBatchDTO](conversationconversableusersbatchdto.md)

  ↳ [ConversationConversableUsersBatch](conversationconversableusersbatch.md)

  ↳ [ConversationUsersBatchMeta](conversationusersbatchmeta.md)

  ↳ [ConversationUsersBatch](conversationusersbatch.md)

## Index

### Properties

* [completed](conversationconversableusersbatchmeta.md#markdown-header-completed)
* [conversationId](conversationconversableusersbatchmeta.md#markdown-header-conversationid)
* [limit](conversationconversableusersbatchmeta.md#markdown-header-limit)
* [nextOffset](conversationconversableusersbatchmeta.md#markdown-header-nextoffset)
* [offset](conversationconversableusersbatchmeta.md#markdown-header-offset)
* [search](conversationconversableusersbatchmeta.md#markdown-header-search)
* [totalCount](conversationconversableusersbatchmeta.md#markdown-header-totalcount)

## Properties

###  completed

• **completed**: *boolean*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[completed](searchpaginateresponse.md#markdown-header-completed)*

Defined in types.ts:21

___

###  conversationId

• **conversationId**: *string*

Defined in types.ts:395

___

###  limit

• **limit**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[limit](searchpaginateresponse.md#markdown-header-limit)*

Defined in types.ts:20

___

###  nextOffset

• **nextOffset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[nextOffset](searchpaginateresponse.md#markdown-header-nextoffset)*

Defined in types.ts:19

___

###  offset

• **offset**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[offset](searchpaginateresponse.md#markdown-header-offset)*

Defined in types.ts:18

___

###  search

• **search**: *string | null*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[search](searchpaginateresponse.md#markdown-header-search)*

Defined in types.ts:16

___

###  totalCount

• **totalCount**: *number*

*Inherited from [SearchPaginateResponse](searchpaginateresponse.md).[totalCount](searchpaginateresponse.md#markdown-header-totalcount)*

Defined in types.ts:17
