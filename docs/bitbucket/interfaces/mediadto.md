[php-sayhey-chat-kit-client](../globals.md) › [MediaDTO](mediadto.md)

# Interface: MediaDTO

## Hierarchy

* [BaseMedia](basemedia.md)

  ↳ **MediaDTO**

## Index

### Properties

* [encoding](mediadto.md#markdown-header-encoding)
* [extension](mediadto.md#markdown-header-extension)
* [fileSize](mediadto.md#markdown-header-filesize)
* [fileType](mediadto.md#markdown-header-filetype)
* [filename](mediadto.md#markdown-header-filename)
* [imageInfo](mediadto.md#markdown-header-imageinfo)
* [mimeType](mediadto.md#markdown-header-mimetype)
* [urlPath](mediadto.md#markdown-header-urlpath)
* [videoInfo](mediadto.md#markdown-header-videoinfo)

## Properties

###  encoding

• **encoding**: *string*

*Inherited from [BaseMedia](basemedia.md).[encoding](basemedia.md#markdown-header-encoding)*

Defined in types.ts:82

___

###  extension

• **extension**: *string*

*Inherited from [BaseMedia](basemedia.md).[extension](basemedia.md#markdown-header-extension)*

Defined in types.ts:81

___

###  fileSize

• **fileSize**: *number*

*Inherited from [BaseMedia](basemedia.md).[fileSize](basemedia.md#markdown-header-filesize)*

Defined in types.ts:84

___

###  fileType

• **fileType**: *[FileType](../enums/filetype.md)*

*Inherited from [BaseMedia](basemedia.md).[fileType](basemedia.md#markdown-header-filetype)*

Defined in types.ts:78

___

###  filename

• **filename**: *string*

*Inherited from [BaseMedia](basemedia.md).[filename](basemedia.md#markdown-header-filename)*

Defined in types.ts:83

___

###  imageInfo

• **imageInfo**: *[ImageInfoMedia](imageinfomedia.md) | null*

*Inherited from [BaseMedia](basemedia.md).[imageInfo](basemedia.md#markdown-header-imageinfo)*

Defined in types.ts:85

___

###  mimeType

• **mimeType**: *string*

*Inherited from [BaseMedia](basemedia.md).[mimeType](basemedia.md#markdown-header-mimetype)*

Defined in types.ts:80

___

###  urlPath

• **urlPath**: *string*

*Inherited from [BaseMedia](basemedia.md).[urlPath](basemedia.md#markdown-header-urlpath)*

Defined in types.ts:79

___

###  videoInfo

• **videoInfo**: *object | null*

Defined in types.ts:89
