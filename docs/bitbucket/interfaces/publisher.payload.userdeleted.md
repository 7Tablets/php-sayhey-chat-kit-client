[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [UserDeleted](publisher.payload.userdeleted.md)

# Interface: UserDeleted

## Hierarchy

* **UserDeleted**

## Index

### Properties

* [deletedAt](publisher.payload.userdeleted.md#markdown-header-deletedat)
* [userId](publisher.payload.userdeleted.md#markdown-header-userid)

## Properties

###  deletedAt

• **deletedAt**: *Date*

Defined in types.ts:576

___

###  userId

• **userId**: *string*

Defined in types.ts:575
