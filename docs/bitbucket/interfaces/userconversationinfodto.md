[php-sayhey-chat-kit-client](../globals.md) › [UserConversationInfoDTO](userconversationinfodto.md)

# Interface: UserConversationInfoDTO

## Hierarchy

* [ConversationInfoDTO](conversationinfodto.md)

  ↳ **UserConversationInfoDTO**

## Index

### Properties

* [email](userconversationinfodto.md#markdown-header-email)
* [id](userconversationinfodto.md#markdown-header-id)
* [name](userconversationinfodto.md#markdown-header-name)
* [picture](userconversationinfodto.md#markdown-header-picture)
* [refId](userconversationinfodto.md#markdown-header-refid)

## Properties

###  email

• **email**: *string*

Defined in types.ts:216

___

###  id

• **id**: *string*

Defined in types.ts:215

___

###  name

• **name**: *string*

*Inherited from [ConversationInfoDTO](conversationinfodto.md).[name](conversationinfodto.md#markdown-header-name)*

Defined in types.ts:206

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

*Inherited from [ConversationInfoDTO](conversationinfodto.md).[picture](conversationinfodto.md#markdown-header-picture)*

Defined in types.ts:205

___

###  refId

• **refId**: *string | null*

Defined in types.ts:217
