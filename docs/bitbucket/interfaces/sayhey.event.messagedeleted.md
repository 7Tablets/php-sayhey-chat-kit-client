[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Event](../modules/sayhey.event.md) › [MessageDeleted](sayhey.event.messagedeleted.md)

# Interface: MessageDeleted

## Hierarchy

* EventMessage

  ↳ **MessageDeleted**

## Index

### Properties

* [event](sayhey.event.messagedeleted.md#markdown-header-event)
* [payload](sayhey.event.messagedeleted.md#markdown-header-payload)

## Properties

###  event

• **event**: *[MessageDeleted](../enums/publisher.eventtype.md#markdown-header-messagedeleted)*

*Overrides void*

Defined in types.ts:934

___

###  payload

• **payload**: *[MessageDeleted](publisher.payload.messagedeleted.md)*

*Overrides void*

Defined in types.ts:935
