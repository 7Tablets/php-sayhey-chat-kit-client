[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [Payload](../modules/sayhey.payload.md) › [ConversationGroupMemberAdded](sayhey.payload.conversationgroupmemberadded.md)

# Interface: ConversationGroupMemberAdded

## Hierarchy

* **ConversationGroupMemberAdded**

## Index

### Properties

* [conversationId](sayhey.payload.conversationgroupmemberadded.md#markdown-header-conversationid)
* [deletedAt](sayhey.payload.conversationgroupmemberadded.md#markdown-header-deletedat)
* [endBefore](sayhey.payload.conversationgroupmemberadded.md#markdown-header-endbefore)
* [userIds](sayhey.payload.conversationgroupmemberadded.md#markdown-header-userids)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:789

___

###  deletedAt

• **deletedAt**: *null*

Defined in types.ts:791

___

###  endBefore

• **endBefore**: *null*

Defined in types.ts:792

___

###  userIds

• **userIds**: *string[]*

Defined in types.ts:790
