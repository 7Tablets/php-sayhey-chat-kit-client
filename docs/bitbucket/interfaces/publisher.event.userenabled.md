[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserEnabled](publisher.event.userenabled.md)

# Interface: UserEnabled

## Hierarchy

* EventMessage

  ↳ **UserEnabled**

## Index

### Properties

* [event](publisher.event.userenabled.md#markdown-header-event)
* [payload](publisher.event.userenabled.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserEnabled](../enums/publisher.eventtype.md#markdown-header-userenabled)*

*Overrides void*

Defined in types.ts:672

___

###  payload

• **payload**: *[UserEnabled](publisher.payload.userenabled.md)*

*Overrides void*

Defined in types.ts:673
