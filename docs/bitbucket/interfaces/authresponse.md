[php-sayhey-chat-kit-client](../globals.md) › [AuthResponse](authresponse.md)

# Interface: AuthResponse

## Hierarchy

* **AuthResponse**

## Index

### Properties

* [accessToken](authresponse.md#markdown-header-accesstoken)
* [publisherToken](authresponse.md#markdown-header-publishertoken)
* [refreshToken](authresponse.md#markdown-header-refreshtoken)
* [userId](authresponse.md#markdown-header-userid)

## Properties

###  accessToken

• **accessToken**: *string*

Defined in types.ts:313

___

###  publisherToken

• **publisherToken**: *string*

Defined in types.ts:314

___

###  refreshToken

• **refreshToken**: *string*

Defined in types.ts:315

___

###  userId

• **userId**: *string*

Defined in types.ts:312
