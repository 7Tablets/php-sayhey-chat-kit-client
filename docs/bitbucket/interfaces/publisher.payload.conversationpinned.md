[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationPinned](publisher.payload.conversationpinned.md)

# Interface: ConversationPinned

## Hierarchy

* **ConversationPinned**

## Index

### Properties

* [conversationId](publisher.payload.conversationpinned.md#markdown-header-conversationid)
* [conversationPinDto](publisher.payload.conversationpinned.md#markdown-header-conversationpindto)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:475

___

###  conversationPinDto

• **conversationPinDto**: *object*

Defined in types.ts:476

#### Type declaration:

* **pinnedAt**: *string*
