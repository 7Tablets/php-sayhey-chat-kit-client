[php-sayhey-chat-kit-client](../globals.md) › [UserDTO](userdto.md)

# Interface: UserDTO

## Hierarchy

* [BaseUser](baseuser.md)

  ↳ **UserDTO**

  ↳ [UserOfConversationDTO](userofconversationdto.md)

  ↳ [UserSelfDTO](userselfdto.md)

## Index

### Properties

* [deletedAt](userdto.md#markdown-header-deletedat)
* [disabled](userdto.md#markdown-header-disabled)
* [email](userdto.md#markdown-header-email)
* [firstName](userdto.md#markdown-header-firstname)
* [id](userdto.md#markdown-header-id)
* [lastName](userdto.md#markdown-header-lastname)
* [picture](userdto.md#markdown-header-picture)
* [refId](userdto.md#markdown-header-refid)

## Properties

###  deletedAt

• **deletedAt**: *string | null*

Defined in types.ts:301

___

###  disabled

• **disabled**: *boolean*

*Inherited from [BaseUser](baseuser.md).[disabled](baseuser.md#markdown-header-disabled)*

Defined in types.ts:296

___

###  email

• **email**: *string*

*Inherited from [BaseUser](baseuser.md).[email](baseuser.md#markdown-header-email)*

Defined in types.ts:294

___

###  firstName

• **firstName**: *string*

*Inherited from [BaseUser](baseuser.md).[firstName](baseuser.md#markdown-header-firstname)*

Defined in types.ts:292

___

###  id

• **id**: *string*

*Inherited from [BaseUser](baseuser.md).[id](baseuser.md#markdown-header-id)*

Defined in types.ts:291

___

###  lastName

• **lastName**: *string*

*Inherited from [BaseUser](baseuser.md).[lastName](baseuser.md#markdown-header-lastname)*

Defined in types.ts:293

___

###  picture

• **picture**: *[MediaDTO](mediadto.md) | null*

Defined in types.ts:300

___

###  refId

• **refId**: *string | null*

*Inherited from [BaseUser](baseuser.md).[refId](baseuser.md#markdown-header-refid)*

Defined in types.ts:295
