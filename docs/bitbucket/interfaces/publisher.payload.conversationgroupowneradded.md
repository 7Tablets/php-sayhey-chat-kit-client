[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Payload](../modules/publisher.payload.md) › [ConversationGroupOwnerAdded](publisher.payload.conversationgroupowneradded.md)

# Interface: ConversationGroupOwnerAdded

## Hierarchy

* **ConversationGroupOwnerAdded**

## Index

### Properties

* [conversationId](publisher.payload.conversationgroupowneradded.md#markdown-header-conversationid)
* [isOwner](publisher.payload.conversationgroupowneradded.md#markdown-header-isowner)
* [userId](publisher.payload.conversationgroupowneradded.md#markdown-header-userid)

## Properties

###  conversationId

• **conversationId**: *string*

Defined in types.ts:519

___

###  isOwner

• **isOwner**: *boolean*

Defined in types.ts:521

___

###  userId

• **userId**: *string*

Defined in types.ts:520
