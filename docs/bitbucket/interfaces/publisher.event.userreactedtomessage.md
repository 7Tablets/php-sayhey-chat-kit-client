[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [Event](../modules/publisher.event.md) › [UserReactedToMessage](publisher.event.userreactedtomessage.md)

# Interface: UserReactedToMessage

## Hierarchy

* EventMessage

  ↳ **UserReactedToMessage**

## Index

### Properties

* [event](publisher.event.userreactedtomessage.md#markdown-header-event)
* [payload](publisher.event.userreactedtomessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *[UserReactedToMessage](../enums/publisher.eventtype.md#markdown-header-userreactedtomessage)*

*Overrides void*

Defined in types.ts:652

___

###  payload

• **payload**: *[UserReactedToMessage](publisher.payload.userreactedtomessage.md)*

*Overrides void*

Defined in types.ts:653
