[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [UserErrorType](sayhey.chatkiterror.errortype.usererrortype.md)

# Enumeration: UserErrorType

## Index

### Enumeration members

* [AlreadyDisabled](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadydisabled)
* [AlreadyEnabled](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadyenabled)
* [AlreadyMuted](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-alreadymuted)
* [NotOnMute](sayhey.chatkiterror.errortype.usererrortype.md#markdown-header-notonmute)

## Enumeration members

###  AlreadyDisabled

• **AlreadyDisabled**: = "UserErrorTypeAlreadyDisabled"

Defined in types.ts:1057

___

###  AlreadyEnabled

• **AlreadyEnabled**: = "UserErrorTypeAlreadyEnabled"

Defined in types.ts:1056

___

###  AlreadyMuted

• **AlreadyMuted**: = "UserErrorTypeAlreadyMuted"

Defined in types.ts:1054

___

###  NotOnMute

• **NotOnMute**: = "UserErrorTypeNotOnMute"

Defined in types.ts:1055
