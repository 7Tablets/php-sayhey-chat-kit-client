[php-sayhey-chat-kit-client](../globals.md) › [ConversationUserQueryType](conversationuserquerytype.md)

# Enumeration: ConversationUserQueryType

## Index

### Enumeration members

* [All](conversationuserquerytype.md#markdown-header-all)
* [Current](conversationuserquerytype.md#markdown-header-current)
* [Removed](conversationuserquerytype.md#markdown-header-removed)

## Enumeration members

###  All

• **All**: = "all"

Defined in types.ts:363

___

###  Current

• **Current**: = "current"

Defined in types.ts:364

___

###  Removed

• **Removed**: = "removed"

Defined in types.ts:365
