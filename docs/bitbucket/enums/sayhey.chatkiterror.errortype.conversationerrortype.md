[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [ConversationErrorType](sayhey.chatkiterror.errortype.conversationerrortype.md)

# Enumeration: ConversationErrorType

## Index

### Enumeration members

* [AlreadyAnOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyanowner)
* [AlreadyExists](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyexists)
* [AlreadyMuted](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadymuted)
* [AlreadyPinned](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadypinned)
* [AlreadyRemovedUsers](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-alreadyremovedusers)
* [CannotAddExistingUsers](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotaddexistingusers)
* [CannotChangeAccessToSelf](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotchangeaccesstoself)
* [CannotChatWithSelf](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotchatwithself)
* [CannotConverseWithUser](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotconversewithuser)
* [CannotRemoveOwners](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-cannotremoveowners)
* [InvalidConversationUserType](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-invalidconversationusertype)
* [InvalidType](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-invalidtype)
* [MustBeOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-mustbeowner)
* [NoValidUsersToAdd](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-novaliduserstoadd)
* [NoValidUsersToRemove](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-novaliduserstoremove)
* [NotAnOwner](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notanowner)
* [NotFound](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfound)
* [NotFoundForApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfoundforapp)
* [NotFoundForUser](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notfoundforuser)
* [NotOnMute](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notonmute)
* [NotPartOfApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notpartofapp)
* [NotPinned](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-notpinned)
* [UsersNotInApp](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-usersnotinapp)
* [UsersNotInConversation](sayhey.chatkiterror.errortype.conversationerrortype.md#markdown-header-usersnotinconversation)

## Enumeration members

###  AlreadyAnOwner

• **AlreadyAnOwner**: = "ConversationErrorTypeAlreadyAnOwner"

Defined in types.ts:1063

___

###  AlreadyExists

• **AlreadyExists**: = "ConversationErrorTypeAlreadyExists"

Defined in types.ts:1070

___

###  AlreadyMuted

• **AlreadyMuted**: = "ConversationErrorTypeAlreadyMuted"

Defined in types.ts:1072

___

###  AlreadyPinned

• **AlreadyPinned**: = "ConversationErrorTypeAlreadyPinned"

Defined in types.ts:1074

___

###  AlreadyRemovedUsers

• **AlreadyRemovedUsers**: = "ConversationErrorTypeAlreadyRemovedUsers"

Defined in types.ts:1081

___

###  CannotAddExistingUsers

• **CannotAddExistingUsers**: = "ConversationErrorTypeCannotAddExistingUsers"

Defined in types.ts:1080

___

###  CannotChangeAccessToSelf

• **CannotChangeAccessToSelf**: = "ConversationErrorTypeCannotChangeAccessToSelf"

Defined in types.ts:1079

___

###  CannotChatWithSelf

• **CannotChatWithSelf**: = "ConversationErrorTypeCannotChatWithSelf"

Defined in types.ts:1071

___

###  CannotConverseWithUser

• **CannotConverseWithUser**: = "ConversationErrorTypeCannotConverseWithUser"

Defined in types.ts:1061

___

###  CannotRemoveOwners

• **CannotRemoveOwners**: = "ConversationErrorTypeCannotRemoveOwners"

Defined in types.ts:1083

___

###  InvalidConversationUserType

• **InvalidConversationUserType**: = "ConversationErrorTypeInvalidConversationUserType"

Defined in types.ts:1066

___

###  InvalidType

• **InvalidType**: = "ConversationErrorTypeInvalidType"

Defined in types.ts:1065

___

###  MustBeOwner

• **MustBeOwner**: = "ConversationErrorTypeMustBeOwner"

Defined in types.ts:1067

___

###  NoValidUsersToAdd

• **NoValidUsersToAdd**: = "ConversationErrorTypeNoValidUsersToAdd"

Defined in types.ts:1077

___

###  NoValidUsersToRemove

• **NoValidUsersToRemove**: = "ConversationErrorTypeNoValidUsersToRemove"

Defined in types.ts:1078

___

###  NotAnOwner

• **NotAnOwner**: = "ConversationErrorTypeNotAnOwner"

Defined in types.ts:1062

___

###  NotFound

• **NotFound**: = "ConversationErrorTypeNotFound"

Defined in types.ts:1060

___

###  NotFoundForApp

• **NotFoundForApp**: = "ConversationErrorTypeNotFoundForApp"

Defined in types.ts:1064

___

###  NotFoundForUser

• **NotFoundForUser**: = "ConversationErrorTypeNotFoundForUser"

Defined in types.ts:1069

___

###  NotOnMute

• **NotOnMute**: = "ConversationErrorTypeNotOnMute"

Defined in types.ts:1073

___

###  NotPartOfApp

• **NotPartOfApp**: = "ConversationErrorTypeNotPartOfApp"

Defined in types.ts:1068

___

###  NotPinned

• **NotPinned**: = "ConversationErrorTypeNotPinned"

Defined in types.ts:1075

___

###  UsersNotInApp

• **UsersNotInApp**: = "ConversationErrorTypeUsersNotInApp"

Defined in types.ts:1076

___

###  UsersNotInConversation

• **UsersNotInConversation**: = "ConversationErrorTypeUsersNotInConversation"

Defined in types.ts:1082
