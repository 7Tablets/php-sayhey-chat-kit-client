[php-sayhey-chat-kit-client](../globals.md) › [Publisher](../modules/publisher.md) › [EventType](publisher.eventtype.md)

# Enumeration: EventType

## Index

### Enumeration members

* [ConversationCreated](publisher.eventtype.md#markdown-header-conversationcreated)
* [ConversationGroupInfoChanged](publisher.eventtype.md#markdown-header-conversationgroupinfochanged)
* [ConversationGroupMemberAdded](publisher.eventtype.md#markdown-header-conversationgroupmemberadded)
* [ConversationGroupMemberRemoved](publisher.eventtype.md#markdown-header-conversationgroupmemberremoved)
* [ConversationGroupOwnerAdded](publisher.eventtype.md#markdown-header-conversationgroupowneradded)
* [ConversationGroupOwnerRemoved](publisher.eventtype.md#markdown-header-conversationgroupownerremoved)
* [ConversationHideAndClear](publisher.eventtype.md#markdown-header-conversationhideandclear)
* [ConversationHideAndClearAll](publisher.eventtype.md#markdown-header-conversationhideandclearall)
* [ConversationMuted](publisher.eventtype.md#markdown-header-conversationmuted)
* [ConversationPinned](publisher.eventtype.md#markdown-header-conversationpinned)
* [ConversationUnmuted](publisher.eventtype.md#markdown-header-conversationunmuted)
* [ConversationUnpinned](publisher.eventtype.md#markdown-header-conversationunpinned)
* [MessageDeleted](publisher.eventtype.md#markdown-header-messagedeleted)
* [MessageReactionsUpdate](publisher.eventtype.md#markdown-header-messagereactionsupdate)
* [NewMessage](publisher.eventtype.md#markdown-header-newmessage)
* [UserDeleted](publisher.eventtype.md#markdown-header-userdeleted)
* [UserDisabled](publisher.eventtype.md#markdown-header-userdisabled)
* [UserEnabled](publisher.eventtype.md#markdown-header-userenabled)
* [UserInfoChanged](publisher.eventtype.md#markdown-header-userinfochanged)
* [UserReactedToMessage](publisher.eventtype.md#markdown-header-userreactedtomessage)
* [UserUnReactedToMessage](publisher.eventtype.md#markdown-header-userunreactedtomessage)

## Enumeration members

###  ConversationCreated

• **ConversationCreated**: = "CONVERSATION_CREATED"

Defined in types.ts:427

___

###  ConversationGroupInfoChanged

• **ConversationGroupInfoChanged**: = "CONVERSATION_GROUP_INFO_CHANGED"

Defined in types.ts:434

___

###  ConversationGroupMemberAdded

• **ConversationGroupMemberAdded**: = "CONVERSATION_GROUP_MEMBER_ADDED"

Defined in types.ts:438

___

###  ConversationGroupMemberRemoved

• **ConversationGroupMemberRemoved**: = "CONVERSATION_GROUP_MEMBER_REMOVED"

Defined in types.ts:439

___

###  ConversationGroupOwnerAdded

• **ConversationGroupOwnerAdded**: = "CONVERSATION_GROUP_OWNER_ADDED"

Defined in types.ts:436

___

###  ConversationGroupOwnerRemoved

• **ConversationGroupOwnerRemoved**: = "CONVERSATION_GROUP_OWNER_REMOVED"

Defined in types.ts:437

___

###  ConversationHideAndClear

• **ConversationHideAndClear**: = "CONVERSATION_HIDE_AND_CLEAR"

Defined in types.ts:432

___

###  ConversationHideAndClearAll

• **ConversationHideAndClearAll**: = "CONVERSATION_HIDE_AND_CLEAR_ALL"

Defined in types.ts:433

___

###  ConversationMuted

• **ConversationMuted**: = "CONVERSATION_MUTED"

Defined in types.ts:428

___

###  ConversationPinned

• **ConversationPinned**: = "CONVERSATION_PINNED"

Defined in types.ts:430

___

###  ConversationUnmuted

• **ConversationUnmuted**: = "CONVERSATION_UNMUTED"

Defined in types.ts:429

___

###  ConversationUnpinned

• **ConversationUnpinned**: = "CONVERSATION_UNPINNED"

Defined in types.ts:431

___

###  MessageDeleted

• **MessageDeleted**: = "MESSAGE_DELETED"

Defined in types.ts:445

___

###  MessageReactionsUpdate

• **MessageReactionsUpdate**: = "MESSAGE_REACTIONS_UPDATE"

Defined in types.ts:442

___

###  NewMessage

• **NewMessage**: = "NEW_MESSAGE"

Defined in types.ts:426

___

###  UserDeleted

• **UserDeleted**: = "USER_DELETED"

Defined in types.ts:446

___

###  UserDisabled

• **UserDisabled**: = "USER_DISABLED"

Defined in types.ts:443

___

###  UserEnabled

• **UserEnabled**: = "USER_ENABLED"

Defined in types.ts:444

___

###  UserInfoChanged

• **UserInfoChanged**: = "USER_INFO_CHANGED"

Defined in types.ts:435

___

###  UserReactedToMessage

• **UserReactedToMessage**: = "USER_REACTED_TO_MESSAGE"

Defined in types.ts:440

___

###  UserUnReactedToMessage

• **UserUnReactedToMessage**: = "USER_UNREACTED_TO_MESSAGE"

Defined in types.ts:441
