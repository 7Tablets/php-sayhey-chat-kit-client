[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [FileErrorType](sayhey.chatkiterror.errortype.fileerrortype.md)

# Enumeration: FileErrorType

## Index

### Enumeration members

* [FileNotForMessage](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-filenotformessage)
* [FileSizeLimitExceeded](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-filesizelimitexceeded)
* [ImageNotForConversation](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-imagenotforconversation)
* [ImageNotForUser](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-imagenotforuser)
* [IncorrectFileType](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-incorrectfiletype)
* [InvalidFieldNameForFile](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidfieldnameforfile)
* [InvalidFileName](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidfilename)
* [InvalidMetadata](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-invalidmetadata)
* [NoFileSentType](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-nofilesenttype)
* [NoThumbnailFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-nothumbnailfound)
* [NotForApp](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notforapp)
* [NotFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notfound)
* [NotSent](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-notsent)
* [SizeLimitExceeded](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-sizelimitexceeded)
* [SizeNotFound](sayhey.chatkiterror.errortype.fileerrortype.md#markdown-header-sizenotfound)

## Enumeration members

###  FileNotForMessage

• **FileNotForMessage**: = "FileErrorTypeFileNotForMessage"

Defined in types.ts:1027

___

###  FileSizeLimitExceeded

• **FileSizeLimitExceeded**: = "FileErrorTypeFileSizeLimitExceeded"

Defined in types.ts:1020

___

###  ImageNotForConversation

• **ImageNotForConversation**: = "FileErrorTypeImageNotForConversation"

Defined in types.ts:1025

___

###  ImageNotForUser

• **ImageNotForUser**: = "FileErrorTypeImageNotForUser"

Defined in types.ts:1026

___

###  IncorrectFileType

• **IncorrectFileType**: = "FileErrorTypeIncorrectFileType"

Defined in types.ts:1030

___

###  InvalidFieldNameForFile

• **InvalidFieldNameForFile**: = "FileErrorTypeInvalidFieldNameForFile"

Defined in types.ts:1018

___

###  InvalidFileName

• **InvalidFileName**: = "FileErrorTypeInvalidFileName"

Defined in types.ts:1021

___

###  InvalidMetadata

• **InvalidMetadata**: = "FileErrorTypeInvalidMetadata"

Defined in types.ts:1019

___

###  NoFileSentType

• **NoFileSentType**: = "FileErrorTypeNoFileSentType"

Defined in types.ts:1017

___

###  NoThumbnailFound

• **NoThumbnailFound**: = "FileErrorTypeNoThumbnailFound"

Defined in types.ts:1024

___

###  NotForApp

• **NotForApp**: = "FileErrorTypeNotForApp"

Defined in types.ts:1029

___

###  NotFound

• **NotFound**: = "FileErrorTypeNotFound"

Defined in types.ts:1023

___

###  NotSent

• **NotSent**: = "FileErrorTypeNotSent"

Defined in types.ts:1022

___

###  SizeLimitExceeded

• **SizeLimitExceeded**: = "FileErrorTypeSizeLimitExceeded"

Defined in types.ts:1016

___

###  SizeNotFound

• **SizeNotFound**: = "FileErrorTypeSizeNotFound"

Defined in types.ts:1028
