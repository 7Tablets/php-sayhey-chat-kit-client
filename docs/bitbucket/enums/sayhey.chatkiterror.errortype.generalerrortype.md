[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [GeneralErrorType](sayhey.chatkiterror.errortype.generalerrortype.md)

# Enumeration: GeneralErrorType

## Index

### Enumeration members

* [LimitNotValid](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-limitnotvalid)
* [MissingQueryParams](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-missingqueryparams)
* [OffsetNotValid](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-offsetnotvalid)
* [RouteNotFound](sayhey.chatkiterror.errortype.generalerrortype.md#markdown-header-routenotfound)

## Enumeration members

###  LimitNotValid

• **LimitNotValid**: = "GeneralErrorTypeLimitNotValid"

Defined in types.ts:1039

___

###  MissingQueryParams

• **MissingQueryParams**: = "GeneralErrorTypeMissingQueryParams"

Defined in types.ts:1037

___

###  OffsetNotValid

• **OffsetNotValid**: = "GeneralErrorTypeOffsetNotValid"

Defined in types.ts:1040

___

###  RouteNotFound

• **RouteNotFound**: = "GeneralErrorTypeRouteNotFound"

Defined in types.ts:1038
