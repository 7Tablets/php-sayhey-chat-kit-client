[php-sayhey-chat-kit-client](../globals.md) › [HighLightType](highlighttype.md)

# Enumeration: HighLightType

## Index

### Enumeration members

* [Email](highlighttype.md#markdown-header-email)
* [Phone](highlighttype.md#markdown-header-phone)
* [Text](highlighttype.md#markdown-header-text)
* [Url](highlighttype.md#markdown-header-url)
* [UserId](highlighttype.md#markdown-header-userid)

## Enumeration members

###  Email

• **Email**: = "email"

Defined in types.ts:115

___

###  Phone

• **Phone**: = "phone"

Defined in types.ts:116

___

###  Text

• **Text**: = "text"

Defined in types.ts:114

___

###  Url

• **Url**: = "url"

Defined in types.ts:117

___

###  UserId

• **UserId**: = "userId"

Defined in types.ts:118
