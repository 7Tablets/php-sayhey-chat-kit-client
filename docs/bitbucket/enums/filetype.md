[php-sayhey-chat-kit-client](../globals.md) › [FileType](filetype.md)

# Enumeration: FileType

## Index

### Enumeration members

* [Audio](filetype.md#markdown-header-audio)
* [Image](filetype.md#markdown-header-image)
* [Other](filetype.md#markdown-header-other)
* [Video](filetype.md#markdown-header-video)

## Enumeration members

###  Audio

• **Audio**: = "audio"

Defined in types.ts:73

___

###  Image

• **Image**: = "image"

Defined in types.ts:72

___

###  Other

• **Other**: = "other"

Defined in types.ts:74

___

###  Video

• **Video**: = "video"

Defined in types.ts:71
