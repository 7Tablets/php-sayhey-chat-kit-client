[php-sayhey-chat-kit-client](../globals.md) › [ReactionType](reactiontype.md)

# Enumeration: ReactionType

Reaction Types

## Index

### Enumeration members

* [Fist](reactiontype.md#markdown-header-fist)
* [Heart](reactiontype.md#markdown-header-heart)
* [Thumb](reactiontype.md#markdown-header-thumb)

## Enumeration members

###  Fist

• **Fist**: = "Fist"

Defined in types.ts:31

___

###  Heart

• **Heart**: = "Heart"

Defined in types.ts:30

___

###  Thumb

• **Thumb**: = "Thumb"

Defined in types.ts:29
