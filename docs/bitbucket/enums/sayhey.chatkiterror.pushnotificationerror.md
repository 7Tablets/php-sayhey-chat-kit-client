[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [PushNotificationError](sayhey.chatkiterror.pushnotificationerror.md)

# Enumeration: PushNotificationError

## Index

### Enumeration members

* [AlreadyRegisteredForUser](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-alreadyregisteredforuser)
* [InvalidPushParams](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-invalidpushparams)
* [NoTokensToPush](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-notokenstopush)
* [NotFound](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-notfound)
* [OnlyStringData](sayhey.chatkiterror.pushnotificationerror.md#markdown-header-onlystringdata)

## Enumeration members

###  AlreadyRegisteredForUser

• **AlreadyRegisteredForUser**: = "DeviceTokenErrorTypeAlreadyRegisteredForUser"

Defined in types.ts:1089

___

###  InvalidPushParams

• **InvalidPushParams**: = "DeviceTokenErrorTypeInvalidPushParams"

Defined in types.ts:1088

___

###  NoTokensToPush

• **NoTokensToPush**: = "DeviceTokenErrorTypeNoTokensToPush"

Defined in types.ts:1091

___

###  NotFound

• **NotFound**: = "DeviceTokenErrorTypeNotFound"

Defined in types.ts:1090

___

###  OnlyStringData

• **OnlyStringData**: = "DeviceTokenErrorTypeOnlyStringData"

Defined in types.ts:1092
