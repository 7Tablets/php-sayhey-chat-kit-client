[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [AuthErrorType](sayhey.chatkiterror.errortype.autherrortype.md)

# Enumeration: AuthErrorType

## Index

### Enumeration members

* [AccountAlreadyExists](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-accountalreadyexists)
* [AppHasNoNotificationSetup](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-apphasnonotificationsetup)
* [HashComparison](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-hashcomparison)
* [HashGeneration](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-hashgeneration)
* [IncorrectPassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-incorrectpassword)
* [InvalidCredentials](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidcredentials)
* [InvalidKeyErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidkeyerrortype)
* [InvalidPassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidpassword)
* [InvalidToken](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidtoken)
* [InvalidUserType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidusertype)
* [InvalidWebhookKey](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-invalidwebhookkey)
* [MissingAuthHeaderField](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-missingauthheaderfield)
* [MissingParamsErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-missingparamserrortype)
* [NoAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-noaccesstouser)
* [NoAppAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-noappaccesstouser)
* [NoSamePassword](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-nosamepassword)
* [NotFoundErrorType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-notfounderrortype)
* [NotPartOfApp](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-notpartofapp)
* [RefreshTokenAlreadyExchanged](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-refreshtokenalreadyexchanged)
* [RefreshTokenNotFound](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-refreshtokennotfound)
* [ServerNoAccessToUser](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-servernoaccesstouser)
* [TokenExpiredType](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-tokenexpiredtype)
* [TokenNotStarted](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-tokennotstarted)
* [UserDisabled](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-userdisabled)
* [UserNotFound](sayhey.chatkiterror.errortype.autherrortype.md#markdown-header-usernotfound)

## Enumeration members

###  AccountAlreadyExists

• **AccountAlreadyExists**: = "AuthErrorTypeAccountAlreadyExists"

Defined in types.ts:1004

___

###  AppHasNoNotificationSetup

• **AppHasNoNotificationSetup**: = "AuthErrorTypeAppHasNoNotificationSetup"

Defined in types.ts:995

___

###  HashComparison

• **HashComparison**: = "AuthErrorTypeHashComparison"

Defined in types.ts:1010

___

###  HashGeneration

• **HashGeneration**: = "AuthErrorTypeHashGeneration"

Defined in types.ts:1009

___

###  IncorrectPassword

• **IncorrectPassword**: = "AuthErrorTypeIncorrectPassword"

Defined in types.ts:1000

___

###  InvalidCredentials

• **InvalidCredentials**: = "AuthErrorTypeInvalidCredentials"

Defined in types.ts:1008

___

###  InvalidKeyErrorType

• **InvalidKeyErrorType**: = "AuthErrorTypeInvalidKey"

Defined in types.ts:989

___

###  InvalidPassword

• **InvalidPassword**: = "AuthErrorTypeInvalidPassword"

Defined in types.ts:993

___

###  InvalidToken

• **InvalidToken**: = "AuthErrorTypeInvalidToken"

Defined in types.ts:1006

___

###  InvalidUserType

• **InvalidUserType**: = "AuthErrorTypeInvalidUserType"

Defined in types.ts:990

___

###  InvalidWebhookKey

• **InvalidWebhookKey**: = "AuthErrorTypeInvalidWebhookKey"

Defined in types.ts:997

___

###  MissingAuthHeaderField

• **MissingAuthHeaderField**: = "AuthErrorTypeMissingAuthHeaderField"

Defined in types.ts:1007

___

###  MissingParamsErrorType

• **MissingParamsErrorType**: = "AuthErrorTypeMissingParams"

Defined in types.ts:992

___

###  NoAccessToUser

• **NoAccessToUser**: = "AuthErrorTypeNoAccessToUser"

Defined in types.ts:998

___

###  NoAppAccessToUser

• **NoAppAccessToUser**: = "AuthErrorTypeNoAppAccessToUser"

Defined in types.ts:1002

___

###  NoSamePassword

• **NoSamePassword**: = "AuthErrorTypeNoSamePassword"

Defined in types.ts:1001

___

###  NotFoundErrorType

• **NotFoundErrorType**: = "AuthErrorTypeNotFound"

Defined in types.ts:991

___

###  NotPartOfApp

• **NotPartOfApp**: = "AuthErrorTypeNotPartOfApp"

Defined in types.ts:996

___

###  RefreshTokenAlreadyExchanged

• **RefreshTokenAlreadyExchanged**: = "AuthErrorTypeRefreshTokenAlreadyExchanged"

Defined in types.ts:1011

___

###  RefreshTokenNotFound

• **RefreshTokenNotFound**: = "AuthErrorTypeRefreshTokenNotFound"

Defined in types.ts:1012

___

###  ServerNoAccessToUser

• **ServerNoAccessToUser**: = "AuthErrorTypeServerNoAccessToUser"

Defined in types.ts:999

___

###  TokenExpiredType

• **TokenExpiredType**: = "AuthErrorTypeTokenExpired"

Defined in types.ts:994

___

###  TokenNotStarted

• **TokenNotStarted**: = "AuthErrorTypeTokenNotStarted"

Defined in types.ts:1003

___

###  UserDisabled

• **UserDisabled**: = "AuthErrorTypeUserDisabled"

Defined in types.ts:1013

___

###  UserNotFound

• **UserNotFound**: = "AuthErrorTypeUserNotFound"

Defined in types.ts:1005
