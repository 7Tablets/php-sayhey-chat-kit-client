[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [MessageErrorType](sayhey.chatkiterror.errortype.messageerrortype.md)

# Enumeration: MessageErrorType

## Index

### Enumeration members

* [CannotDeleteSystemMessage](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-cannotdeletesystemmessage)
* [InvalidDate](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-invaliddate)
* [MessageAlreadyRead](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-messagealreadyread)
* [NotFound](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-notfound)
* [NotFoundForUser](sayhey.chatkiterror.errortype.messageerrortype.md#markdown-header-notfoundforuser)

## Enumeration members

###  CannotDeleteSystemMessage

• **CannotDeleteSystemMessage**: = "MessageErrorTypeCannotDeleteSystemMessage"

Defined in types.ts:1047

___

###  InvalidDate

• **InvalidDate**: = "MessageErrorTypeInvalidDate"

Defined in types.ts:1043

___

###  MessageAlreadyRead

• **MessageAlreadyRead**: = "MessageErrorTypeMessageAlreadyRead"

Defined in types.ts:1046

___

###  NotFound

• **NotFound**: = "MessageErrorTypeNotFound"

Defined in types.ts:1044

___

###  NotFoundForUser

• **NotFoundForUser**: = "MessageErrorTypeNotFoundForUser"

Defined in types.ts:1045
