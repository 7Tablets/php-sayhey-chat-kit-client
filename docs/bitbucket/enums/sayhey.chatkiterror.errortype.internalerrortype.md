[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [ErrorType](../modules/sayhey.chatkiterror.errortype.md) › [InternalErrorType](sayhey.chatkiterror.errortype.internalerrortype.md)

# Enumeration: InternalErrorType

## Index

### Enumeration members

* [AuthMissing](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-authmissing)
* [BadData](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-baddata)
* [MissingParameter](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-missingparameter)
* [Uninitialized](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-uninitialized)
* [Unknown](sayhey.chatkiterror.errortype.internalerrortype.md#markdown-header-unknown)

## Enumeration members

###  AuthMissing

• **AuthMissing**: = "InternalAuthMissing"

Defined in types.ts:972

___

###  BadData

• **BadData**: = "InternalBadData"

Defined in types.ts:973

___

###  MissingParameter

• **MissingParameter**: = "InternalMissingParameter"

Defined in types.ts:974

___

###  Uninitialized

• **Uninitialized**: = "InternalUninitialized"

Defined in types.ts:971

___

###  Unknown

• **Unknown**: = "InternalUnknown"

Defined in types.ts:970
