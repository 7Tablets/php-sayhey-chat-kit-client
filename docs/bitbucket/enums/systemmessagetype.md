[php-sayhey-chat-kit-client](../globals.md) › [SystemMessageType](systemmessagetype.md)

# Enumeration: SystemMessageType

## Index

### Enumeration members

* [GroupCreated](systemmessagetype.md#markdown-header-groupcreated)
* [GroupDisabled](systemmessagetype.md#markdown-header-groupdisabled)
* [GroupNameChanged](systemmessagetype.md#markdown-header-groupnamechanged)
* [GroupPictureChanged](systemmessagetype.md#markdown-header-grouppicturechanged)
* [IndividualChatCreated](systemmessagetype.md#markdown-header-individualchatcreated)
* [MemberAdded](systemmessagetype.md#markdown-header-memberadded)
* [MemberRemoved](systemmessagetype.md#markdown-header-memberremoved)
* [OwnerAdded](systemmessagetype.md#markdown-header-owneradded)
* [OwnerRemoved](systemmessagetype.md#markdown-header-ownerremoved)

## Enumeration members

###  GroupCreated

• **GroupCreated**: = "GroupCreated"

Defined in types.ts:129

___

###  GroupDisabled

• **GroupDisabled**: = "GroupDisabled"

Defined in types.ts:128

___

###  GroupNameChanged

• **GroupNameChanged**: = "GroupNameChanged"

Defined in types.ts:127

___

###  GroupPictureChanged

• **GroupPictureChanged**: = "GroupPictureChanged"

Defined in types.ts:126

___

###  IndividualChatCreated

• **IndividualChatCreated**: = "IndividualChatCreated"

Defined in types.ts:130

___

###  MemberAdded

• **MemberAdded**: = "MemberAdded"

Defined in types.ts:123

___

###  MemberRemoved

• **MemberRemoved**: = "MemberRemoved"

Defined in types.ts:122

___

###  OwnerAdded

• **OwnerAdded**: = "OwnerAdded"

Defined in types.ts:125

___

###  OwnerRemoved

• **OwnerRemoved**: = "OwnerRemoved"

Defined in types.ts:124
