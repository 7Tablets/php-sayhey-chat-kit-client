[php-sayhey-chat-kit-client](../globals.md) › [ImageSizes](imagesizes.md)

# Enumeration: ImageSizes

Message Types

## Index

### Enumeration members

* [Large](imagesizes.md#markdown-header-large)
* [Medium](imagesizes.md#markdown-header-medium)
* [Small](imagesizes.md#markdown-header-small)
* [Tiny](imagesizes.md#markdown-header-tiny)
* [XLarge](imagesizes.md#markdown-header-xlarge)

## Enumeration members

###  Large

• **Large**: = "large"

Defined in types.ts:53

___

###  Medium

• **Medium**: = "medium"

Defined in types.ts:52

___

###  Small

• **Small**: = "small"

Defined in types.ts:51

___

###  Tiny

• **Tiny**: = "tiny"

Defined in types.ts:50

___

###  XLarge

• **XLarge**: = "xlarge"

Defined in types.ts:54
