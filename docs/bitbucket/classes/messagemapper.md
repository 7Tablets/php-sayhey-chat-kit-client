[php-sayhey-chat-kit-client](../globals.md) › [MessageMapper](messagemapper.md)

# Class: MessageMapper

## Hierarchy

* **MessageMapper**

## Index

### Methods

* [toDomainEntities](messagemapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](messagemapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntities

▸ **toDomainEntities**(`messages`: [MessageDTO](../interfaces/messagedto.md)[]): *[MessageDomainEntity](../interfaces/messagedomainentity.md)[]*

Defined in mapper.ts:125

**Parameters:**

Name | Type |
------ | ------ |
`messages` | [MessageDTO](../interfaces/messagedto.md)[] |

**Returns:** *[MessageDomainEntity](../interfaces/messagedomainentity.md)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`message`: [MessageDTO](../interfaces/messagedto.md)): *[MessageDomainEntity](../interfaces/messagedomainentity.md)*

Defined in mapper.ts:90

**Parameters:**

Name | Type |
------ | ------ |
`message` | [MessageDTO](../interfaces/messagedto.md) |

**Returns:** *[MessageDomainEntity](../interfaces/messagedomainentity.md)*
