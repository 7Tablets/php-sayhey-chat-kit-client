[php-sayhey-chat-kit-client](../globals.md) › [UserMapper](usermapper.md)

# Class: UserMapper

## Hierarchy

* **UserMapper**

## Index

### Methods

* [toConversableUserDomainEntities](usermapper.md#markdown-header-static-toconversableuserdomainentities)
* [toConversableUserDomainEntity](usermapper.md#markdown-header-static-toconversableuserdomainentity)
* [toDomainEntities](usermapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](usermapper.md#markdown-header-static-todomainentity)
* [toSelfDomainEntity](usermapper.md#markdown-header-static-toselfdomainentity)

## Methods

### `Static` toConversableUserDomainEntities

▸ **toConversableUserDomainEntities**(`users`: [UserOfConversationDTO](../interfaces/userofconversationdto.md)[]): *[ConversationUserDomainEntity](../interfaces/conversationuserdomainentity.md)[]*

Defined in mapper.ts:180

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserOfConversationDTO](../interfaces/userofconversationdto.md)[] |

**Returns:** *[ConversationUserDomainEntity](../interfaces/conversationuserdomainentity.md)[]*

___

### `Static` toConversableUserDomainEntity

▸ **toConversableUserDomainEntity**(`user`: [UserOfConversationDTO](../interfaces/userofconversationdto.md)): *[ConversationUserDomainEntity](../interfaces/conversationuserdomainentity.md)*

Defined in mapper.ts:147

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserOfConversationDTO](../interfaces/userofconversationdto.md) |

**Returns:** *[ConversationUserDomainEntity](../interfaces/conversationuserdomainentity.md)*

___

### `Static` toDomainEntities

▸ **toDomainEntities**(`users`: [UserDTO](../interfaces/userdto.md)[]): *[UserDomainEntity](../interfaces/userdomainentity.md)[]*

Defined in mapper.ts:176

**Parameters:**

Name | Type |
------ | ------ |
`users` | [UserDTO](../interfaces/userdto.md)[] |

**Returns:** *[UserDomainEntity](../interfaces/userdomainentity.md)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`user`: [UserDTO](../interfaces/userdto.md)): *[UserDomainEntity](../interfaces/userdomainentity.md)*

Defined in mapper.ts:131

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserDTO](../interfaces/userdto.md) |

**Returns:** *[UserDomainEntity](../interfaces/userdomainentity.md)*

___

### `Static` toSelfDomainEntity

▸ **toSelfDomainEntity**(`user`: [UserSelfDTO](../interfaces/userselfdto.md)): *[UserSelfDomainEntity](../interfaces/userselfdomainentity.md)*

Defined in mapper.ts:185

**Parameters:**

Name | Type |
------ | ------ |
`user` | [UserSelfDTO](../interfaces/userselfdto.md) |

**Returns:** *[UserSelfDomainEntity](../interfaces/userselfdomainentity.md)*
