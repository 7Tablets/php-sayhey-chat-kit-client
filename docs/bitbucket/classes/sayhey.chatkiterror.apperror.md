[php-sayhey-chat-kit-client](../globals.md) › [SayHey](../modules/sayhey.md) › [ChatKitError](../modules/sayhey.chatkiterror.md) › [AppError](sayhey.chatkiterror.apperror.md)

# Class: AppError

## Hierarchy

* **AppError**

## Index

### Constructors

* [constructor](sayhey.chatkiterror.apperror.md#markdown-header-constructor)

### Properties

* [authFailed](sayhey.chatkiterror.apperror.md#markdown-header-optional-authfailed)
* [error](sayhey.chatkiterror.apperror.md#markdown-header-optional-error)
* [exchange](sayhey.chatkiterror.apperror.md#markdown-header-optional-exchange)
* [message](sayhey.chatkiterror.apperror.md#markdown-header-optional-message)
* [statusCode](sayhey.chatkiterror.apperror.md#markdown-header-optional-statuscode)
* [type](sayhey.chatkiterror.apperror.md#markdown-header-type)

### Methods

* [setErrorActions](sayhey.chatkiterror.apperror.md#markdown-header-seterroractions)
* [setMessage](sayhey.chatkiterror.apperror.md#markdown-header-setmessage)
* [setStatusCode](sayhey.chatkiterror.apperror.md#markdown-header-setstatuscode)
* [setType](sayhey.chatkiterror.apperror.md#markdown-header-settype)

## Constructors

###  constructor

\+ **new AppError**(`err?`: string | [Error](../modules/sayhey.md#markdown-header-error), `type?`: [Type](../modules/sayhey.chatkiterror.md#markdown-header-type)): *[AppError](sayhey.chatkiterror.apperror.md)*

Defined in types.ts:1103

**Parameters:**

Name | Type |
------ | ------ |
`err?` | string &#124; [Error](../modules/sayhey.md#markdown-header-error) |
`type?` | [Type](../modules/sayhey.chatkiterror.md#markdown-header-type) |

**Returns:** *[AppError](sayhey.chatkiterror.apperror.md)*

## Properties

### `Optional` authFailed

• **authFailed**? : *undefined | false | true*

Defined in types.ts:1102

___

### `Optional` error

• **error**? : *[Error](../modules/sayhey.md#markdown-header-error)*

Defined in types.ts:1100

___

### `Optional` exchange

• **exchange**? : *undefined | false | true*

Defined in types.ts:1103

___

### `Optional` message

• **message**? : *undefined | string*

Defined in types.ts:1101

___

### `Optional` statusCode

• **statusCode**? : *undefined | number*

Defined in types.ts:1099

___

###  type

• **type**: *[Type](../modules/sayhey.chatkiterror.md#markdown-header-type)* = ErrorType.InternalErrorType.Unknown

Defined in types.ts:1098

## Methods

###  setErrorActions

▸ **setErrorActions**(`params`: object): *void*

Defined in types.ts:1129

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`authFailed?` | undefined &#124; false &#124; true |
`exchange?` | undefined &#124; false &#124; true |

**Returns:** *void*

___

###  setMessage

▸ **setMessage**(`msg`: string): *void*

Defined in types.ts:1125

**Parameters:**

Name | Type |
------ | ------ |
`msg` | string |

**Returns:** *void*

___

###  setStatusCode

▸ **setStatusCode**(`statusCode?`: undefined | number): *void*

Defined in types.ts:1117

**Parameters:**

Name | Type |
------ | ------ |
`statusCode?` | undefined &#124; number |

**Returns:** *void*

___

###  setType

▸ **setType**(`type`: [Type](../modules/sayhey.chatkiterror.md#markdown-header-type)): *void*

Defined in types.ts:1121

**Parameters:**

Name | Type |
------ | ------ |
`type` | [Type](../modules/sayhey.chatkiterror.md#markdown-header-type) |

**Returns:** *void*
