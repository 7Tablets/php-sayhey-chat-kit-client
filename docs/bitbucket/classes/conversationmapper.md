[php-sayhey-chat-kit-client](../globals.md) › [ConversationMapper](conversationmapper.md)

# Class: ConversationMapper

## Hierarchy

* **ConversationMapper**

## Index

### Methods

* [toDomainEntities](conversationmapper.md#markdown-header-static-todomainentities)
* [toDomainEntity](conversationmapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntities

▸ **toDomainEntities**(`conversations`: [ConversationDTO](../globals.md#markdown-header-conversationdto)[]): *[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[]*

Defined in mapper.ts:84

**Parameters:**

Name | Type |
------ | ------ |
`conversations` | [ConversationDTO](../globals.md#markdown-header-conversationdto)[] |

**Returns:** *[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[]*

___

### `Static` toDomainEntity

▸ **toDomainEntity**(`conversation`: [ConversationDTO](../globals.md#markdown-header-conversationdto)): *[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)*

Defined in mapper.ts:23

**Parameters:**

Name | Type |
------ | ------ |
`conversation` | [ConversationDTO](../globals.md#markdown-header-conversationdto) |

**Returns:** *[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)*
