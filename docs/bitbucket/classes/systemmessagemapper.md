[php-sayhey-chat-kit-client](../globals.md) › [SystemMessageMapper](systemmessagemapper.md)

# Class: SystemMessageMapper

## Hierarchy

* **SystemMessageMapper**

## Index

### Methods

* [toDomainEntity](systemmessagemapper.md#markdown-header-static-todomainentity)

## Methods

### `Static` toDomainEntity

▸ **toDomainEntity**(`systemMessage`: [SystemMessageDTO](../interfaces/systemmessagedto.md)): *[SystemMessageDomainEntity](../interfaces/systemmessagedomainentity.md)*

Defined in mapper.ts:246

**Parameters:**

Name | Type |
------ | ------ |
`systemMessage` | [SystemMessageDTO](../interfaces/systemmessagedto.md) |

**Returns:** *[SystemMessageDomainEntity](../interfaces/systemmessagedomainentity.md)*
