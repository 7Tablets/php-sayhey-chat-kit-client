[php-sayhey-chat-kit-client](../globals.md) › [ChatKit](chatkit.md)

# Class: ChatKit

## Hierarchy

* **ChatKit**

## Index

### Constructors

* [constructor](chatkit.md#markdown-header-private-constructor)

### Properties

* [_initialized](chatkit.md#markdown-header-static-private-_initialized)
* [_subscribed](chatkit.md#markdown-header-static-private-_subscribed)
* [accessToken](chatkit.md#markdown-header-static-accesstoken)
* [apiKey](chatkit.md#markdown-header-static-apikey)
* [apiVersion](chatkit.md#markdown-header-static-apiversion)
* [appId](chatkit.md#markdown-header-static-appid)
* [authDetailChangeListener](chatkit.md#markdown-header-static-authdetailchangelistener)
* [disableAutoRefreshToken](chatkit.md#markdown-header-static-private-optional-disableautorefreshtoken)
* [disableUserDeletedHandler](chatkit.md#markdown-header-static-private-optional-disableuserdeletedhandler)
* [disableUserDisabledHandler](chatkit.md#markdown-header-static-private-optional-disableuserdisabledhandler)
* [eventListener](chatkit.md#markdown-header-static-eventlistener)
* [exchangeTokenPromise](chatkit.md#markdown-header-static-private-exchangetokenpromise)
* [httpClient](chatkit.md#markdown-header-static-httpclient)
* [instanceIdPushNotification](chatkit.md#markdown-header-static-instanceidpushnotification)
* [isSendingReadReceipt](chatkit.md#markdown-header-static-private-issendingreadreceipt)
* [isUserAuthenticated](chatkit.md#markdown-header-static-isuserauthenticated)
* [loginId](chatkit.md#markdown-header-static-private-loginid)
* [publisherToken](chatkit.md#markdown-header-static-publishertoken)
* [readMessageMap](chatkit.md#markdown-header-static-private-readmessagemap)
* [readMessageSequenceMap](chatkit.md#markdown-header-static-readmessagesequencemap)
* [refreshToken](chatkit.md#markdown-header-static-refreshtoken)
* [serverUrl](chatkit.md#markdown-header-static-serverurl)
* [sessionExpiredListener](chatkit.md#markdown-header-static-optional-sessionexpiredlistener)
* [timerHandle](chatkit.md#markdown-header-static-private-timerhandle)

### Methods

* [addListeners](chatkit.md#markdown-header-static-addlisteners)
* [addUsersToConversation](chatkit.md#markdown-header-static-adduserstoconversation)
* [changePassword](chatkit.md#markdown-header-static-changepassword)
* [checkForExistingConversation](chatkit.md#markdown-header-static-checkforexistingconversation)
* [createGroupConversation](chatkit.md#markdown-header-static-creategroupconversation)
* [createIndividualConversation](chatkit.md#markdown-header-static-createindividualconversation)
* [deleteAllConversations](chatkit.md#markdown-header-static-deleteallconversations)
* [deleteConversation](chatkit.md#markdown-header-static-deleteconversation)
* [disconnectPublisher](chatkit.md#markdown-header-static-disconnectpublisher)
* [exchangeToken](chatkit.md#markdown-header-static-exchangetoken)
* [exchangeTokenOnce](chatkit.md#markdown-header-static-exchangetokenonce)
* [getConversationBasicUsers](chatkit.md#markdown-header-static-getconversationbasicusers)
* [getConversationDetails](chatkit.md#markdown-header-static-getconversationdetails)
* [getConversationReachableUsers](chatkit.md#markdown-header-static-getconversationreachableusers)
* [getConversationUsers](chatkit.md#markdown-header-static-getconversationusers)
* [getConversations](chatkit.md#markdown-header-static-getconversations)
* [getGalleryMessages](chatkit.md#markdown-header-static-getgallerymessages)
* [getGroupDefaultImageSet](chatkit.md#markdown-header-static-getgroupdefaultimageset)
* [getMediaDirectUrl](chatkit.md#markdown-header-static-getmediadirecturl)
* [getMediaSourceDetails](chatkit.md#markdown-header-static-getmediasourcedetails)
* [getMessages](chatkit.md#markdown-header-static-getmessages)
* [getReachableUsers](chatkit.md#markdown-header-static-getreachableusers)
* [getReactedUsers](chatkit.md#markdown-header-static-getreactedusers)
* [getUserConversationMessagesReactions](chatkit.md#markdown-header-static-getuserconversationmessagesreactions)
* [getUserConversationReactions](chatkit.md#markdown-header-static-getuserconversationreactions)
* [getUserDetails](chatkit.md#markdown-header-static-getuserdetails)
* [guard](chatkit.md#markdown-header-static-private-guard)
* [guardWithAuth](chatkit.md#markdown-header-static-private-guardwithauth)
* [initialize](chatkit.md#markdown-header-static-initialize)
* [isSocketConnected](chatkit.md#markdown-header-static-issocketconnected)
* [makeConversationOwner](chatkit.md#markdown-header-static-makeconversationowner)
* [markMessageRead](chatkit.md#markdown-header-static-private-markmessageread)
* [muteAllNotifications](chatkit.md#markdown-header-static-muteallnotifications)
* [muteConversation](chatkit.md#markdown-header-static-muteconversation)
* [onMessage](chatkit.md#markdown-header-static-private-onmessage)
* [pinConversation](chatkit.md#markdown-header-static-pinconversation)
* [readMessage](chatkit.md#markdown-header-static-readmessage)
* [reconnectPublisher](chatkit.md#markdown-header-static-reconnectpublisher)
* [registerForPushNotification](chatkit.md#markdown-header-static-registerforpushnotification)
* [removeConversationOwner](chatkit.md#markdown-header-static-removeconversationowner)
* [removeMessageReaction](chatkit.md#markdown-header-static-removemessagereaction)
* [removeUsersFromConversation](chatkit.md#markdown-header-static-removeusersfromconversation)
* [sendMessage](chatkit.md#markdown-header-static-sendmessage)
* [setMessageReaction](chatkit.md#markdown-header-static-setmessagereaction)
* [signIn](chatkit.md#markdown-header-static-signin)
* [signInWithToken](chatkit.md#markdown-header-static-signinwithtoken)
* [signOut](chatkit.md#markdown-header-static-signout)
* [signUp](chatkit.md#markdown-header-static-signup)
* [unmuteAllNotifications](chatkit.md#markdown-header-static-unmuteallnotifications)
* [unmuteConversation](chatkit.md#markdown-header-static-unmuteconversation)
* [unpinConversation](chatkit.md#markdown-header-static-unpinconversation)
* [unregisterForPushNotification](chatkit.md#markdown-header-static-unregisterforpushnotification)
* [updateAuthDetails](chatkit.md#markdown-header-static-private-updateauthdetails)
* [updateGroupImage](chatkit.md#markdown-header-static-updategroupimage)
* [updateGroupInfo](chatkit.md#markdown-header-static-updategroupinfo)
* [updateUserProfileImage](chatkit.md#markdown-header-static-updateuserprofileimage)
* [updateUserProfileInfo](chatkit.md#markdown-header-static-updateuserprofileinfo)
* [uploadFile](chatkit.md#markdown-header-static-private-uploadfile)
* [uploadImage](chatkit.md#markdown-header-static-uploadimage)

## Constructors

### `Private` constructor

\+ **new ChatKit**(): *[ChatKit](chatkit.md)*

Defined in sayhey-chat-kit-client.ts:96

**Returns:** *[ChatKit](chatkit.md)*

## Properties

### `Static` `Private` _initialized

▪ **_initialized**: *boolean* = false

Defined in sayhey-chat-kit-client.ts:74

___

### `Static` `Private` _subscribed

▪ **_subscribed**: *boolean* = false

Defined in sayhey-chat-kit-client.ts:76

___

### `Static` accessToken

▪ **accessToken**: *string*

Defined in sayhey-chat-kit-client.ts:50

___

### `Static` apiKey

▪ **apiKey**: *string*

Defined in sayhey-chat-kit-client.ts:64

___

### `Static` apiVersion

▪ **apiVersion**: *string* = API_VERSION

Defined in sayhey-chat-kit-client.ts:58

___

### `Static` appId

▪ **appId**: *string*

Defined in sayhey-chat-kit-client.ts:62

___

### `Static` authDetailChangeListener

▪ **authDetailChangeListener**: *function*

Defined in sayhey-chat-kit-client.ts:68

#### Type declaration:

▸ (`res`: [AuthResponse](../interfaces/authresponse.md)): *Promise‹void› | void*

**Parameters:**

Name | Type |
------ | ------ |
`res` | [AuthResponse](../interfaces/authresponse.md) |

___

### `Static` `Private` `Optional` disableAutoRefreshToken

▪ **disableAutoRefreshToken**? : *undefined | false | true*

Defined in sayhey-chat-kit-client.ts:90

___

### `Static` `Private` `Optional` disableUserDeletedHandler

▪ **disableUserDeletedHandler**? : *undefined | false | true*

Defined in sayhey-chat-kit-client.ts:94

___

### `Static` `Private` `Optional` disableUserDisabledHandler

▪ **disableUserDisabledHandler**? : *undefined | false | true*

Defined in sayhey-chat-kit-client.ts:92

___

### `Static` eventListener

▪ **eventListener**: *function*

Defined in sayhey-chat-kit-client.ts:70

#### Type declaration:

▸ (`event`: [Event](../modules/sayhey.event.md)): *void*

**Parameters:**

Name | Type |
------ | ------ |
`event` | [Event](../modules/sayhey.event.md) |

___

### `Static` `Private` exchangeTokenPromise

▪ **exchangeTokenPromise**: *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›› | null* = null

Defined in sayhey-chat-kit-client.ts:88

___

### `Static` httpClient

▪ **httpClient**: *AxiosInstance*

Defined in sayhey-chat-kit-client.ts:60

___

### `Static` instanceIdPushNotification

▪ **instanceIdPushNotification**: *string* = ""

Defined in sayhey-chat-kit-client.ts:56

___

### `Static` `Private` isSendingReadReceipt

▪ **isSendingReadReceipt**: *boolean* = false

Defined in sayhey-chat-kit-client.ts:84

___

### `Static` isUserAuthenticated

▪ **isUserAuthenticated**: *boolean* = false

Defined in sayhey-chat-kit-client.ts:78

___

### `Static` `Private` loginId

▪ **loginId**: *number* = 0

Defined in sayhey-chat-kit-client.ts:96

___

### `Static` publisherToken

▪ **publisherToken**: *string*

Defined in sayhey-chat-kit-client.ts:52

___

### `Static` `Private` readMessageMap

▪ **readMessageMap**: *object*

Defined in sayhey-chat-kit-client.ts:80

#### Type declaration:

* \[ **conversationId**: *string*\]: string

___

### `Static` readMessageSequenceMap

▪ **readMessageSequenceMap**: *object*

Defined in sayhey-chat-kit-client.ts:82

#### Type declaration:

* \[ **conversationId**: *string*\]: number

___

### `Static` refreshToken

▪ **refreshToken**: *string*

Defined in sayhey-chat-kit-client.ts:54

___

### `Static` serverUrl

▪ **serverUrl**: *string*

Defined in sayhey-chat-kit-client.ts:66

___

### `Static` `Optional` sessionExpiredListener

▪ **sessionExpiredListener**? : *undefined | function*

Defined in sayhey-chat-kit-client.ts:72

___

### `Static` `Private` timerHandle

▪ **timerHandle**: *any* = null

Defined in sayhey-chat-kit-client.ts:86

## Methods

### `Static` addListeners

▸ **addListeners**(`listeners`: object): *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

Defined in sayhey-chat-kit-client.ts:394

**Parameters:**

▪ **listeners**: *object*

Name | Type |
------ | ------ |
`onAuthDetailChange` | function |
`onEvent` | function |
`onSessionExpired?` | undefined &#124; function |
`onSocketConnect?` | undefined &#124; function |
`onSocketDisconnect?` | undefined &#124; function |
`onSocketError?` | undefined &#124; function |
`onSocketReconnect?` | undefined &#124; function |

**Returns:** *Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)›*

___

### `Static` addUsersToConversation

▸ **addUsersToConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1029

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` changePassword

▸ **changePassword**(`currentPassword`: string, `newPassword`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:562

**Parameters:**

Name | Type |
------ | ------ |
`currentPassword` | string |
`newPassword` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` checkForExistingConversation

▸ **checkForExistingConversation**(`withUserId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:743

**Parameters:**

Name | Type |
------ | ------ |
`withUserId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` createGroupConversation

▸ **createGroupConversation**(`params`: object): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:778

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`file` | [FileUpload](../globals.md#markdown-header-fileupload) |
`groupName` | string |
`userIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` createIndividualConversation

▸ **createIndividualConversation**(`withUserId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:759

**Parameters:**

Name | Type |
------ | ------ |
`withUserId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` deleteAllConversations

▸ **deleteAllConversations**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:886

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` deleteConversation

▸ **deleteConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:870

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` disconnectPublisher

▸ **disconnectPublisher**(): *Promise‹void›*

Defined in sayhey-chat-kit-client.ts:432

**Returns:** *Promise‹void›*

___

### `Static` exchangeToken

▸ **exchangeToken**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:533

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` exchangeTokenOnce

▸ **exchangeTokenOnce**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:141

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getConversationBasicUsers

▸ **getConversationBasicUsers**(`params`: object): *Promise‹Result‹[ConversationUsersBasicDTO](../interfaces/conversationusersbasicdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1009

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`type?` | "all" &#124; "current" &#124; "default" |

**Returns:** *Promise‹Result‹[ConversationUsersBasicDTO](../interfaces/conversationusersbasicdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getConversationDetails

▸ **getConversationDetails**(`conversationId`: string): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:726

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getConversationReachableUsers

▸ **getConversationReachableUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:979

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getConversationUsers

▸ **getConversationUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversationUsersBatch](../interfaces/conversationusersbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:946

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |
`type` | undefined &#124; [All](../enums/conversationuserquerytype.md#markdown-header-all) &#124; [Current](../enums/conversationuserquerytype.md#markdown-header-current) &#124; [Removed](../enums/conversationuserquerytype.md#markdown-header-removed) |

**Returns:** *Promise‹Result‹[ConversationUsersBatch](../interfaces/conversationusersbatch.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getConversations

▸ **getConversations**(): *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:710

Conversation APIs

**Returns:** *Promise‹Result‹[ConversationDomainEntity](../globals.md#markdown-header-conversationdomainentity)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getGalleryMessages

▸ **getGalleryMessages**(`conversationId`: string, `beforeSequence?`: undefined | number, `limit?`: undefined | number): *Promise‹Result‹[GalleryPayload](../interfaces/gallerypayload.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1448

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`beforeSequence?` | undefined &#124; number |
`limit?` | undefined &#124; number |

**Returns:** *Promise‹Result‹[GalleryPayload](../interfaces/gallerypayload.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getGroupDefaultImageSet

▸ **getGroupDefaultImageSet**(): *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1147

**Returns:** *Promise‹Result‹[DefaultGroupImageUrlSet](../interfaces/defaultgroupimageurlset.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getMediaDirectUrl

▸ **getMediaDirectUrl**(`params`: object | object): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:240

**Parameters:**

Name | Type |
------ | ------ |
`params` | object &#124; object |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getMediaSourceDetails

▸ **getMediaSourceDetails**(`urlPath`: string): *object*

Defined in sayhey-chat-kit-client.ts:228

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`urlPath` | string | url path returned from SayHey  |

**Returns:** *object*

* **method**: *string* = "GET"

* **uri**: *string* = `${ChatKit.serverUrl}${urlPath}`

* ### **headers**: *object*

  * **Accept**: *string* = "image/*, video/*, audio/*"

  * **Authorization**: *string* = `Bearer ${ChatKit.accessToken}`

  * **__computed**: *string* = ChatKit.apiKey

___

### `Static` getMessages

▸ **getMessages**(`conversationId`: string, `pivotSequence?`: undefined | number, `after?`: undefined | false | true): *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1166

Message APIs

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`pivotSequence?` | undefined &#124; number |
`after?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getReachableUsers

▸ **getReachableUsers**(`__namedParameters`: object): *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:587

User APIs

**Parameters:**

▪`Default value`  **__namedParameters**: *object*= {}

Name | Type |
------ | ------ |
`includeRefIdInSearch` | undefined &#124; false &#124; true |
`limit` | undefined &#124; number |
`nextCursor` | undefined &#124; number |
`searchString` | undefined &#124; string |

**Returns:** *Promise‹Result‹[ConversableUsersBatchResult](../interfaces/conversableusersbatchresult.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getReactedUsers

▸ **getReactedUsers**(`__namedParameters`: object): *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1602

**Parameters:**

▪ **__namedParameters**: *object*

Name | Type |
------ | ------ |
`afterSequence` | null &#124; number |
`limit` | number |
`messageId` | string |
`type` | undefined &#124; [Thumb](../enums/reactiontype.md#markdown-header-thumb) &#124; [Heart](../enums/reactiontype.md#markdown-header-heart) &#124; [Fist](../enums/reactiontype.md#markdown-header-fist) |

**Returns:** *Promise‹Result‹[ReactedUserDTO](../interfaces/reacteduserdto.md)[], [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getUserConversationMessagesReactions

▸ **getUserConversationMessagesReactions**(`conversationId`: string, `messageIds`: string[]): *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1128

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`messageIds` | string[] |

**Returns:** *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getUserConversationReactions

▸ **getUserConversationReactions**(`conversationId`: string): *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1113

**`deprecated`** The method should not be used

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹[ConversationReactionsDomainEntity](../globals.md#markdown-header-conversationreactionsdomainentity), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` getUserDetails

▸ **getUserDetails**(): *Promise‹Result‹[UserSelfDomainEntity](../interfaces/userselfdomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:664

**Returns:** *Promise‹Result‹[UserSelfDomainEntity](../interfaces/userselfdomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` guard

▸ **guard**<**T**>(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:108

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be executed only if ChatKit is initialized and subscribed to

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` guardWithAuth

▸ **guardWithAuth**<**T**>(`cb`: function): *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:159

**Type parameters:**

▪ **T**

**Parameters:**

▪ **cb**: *function*

Callback to be called only when user is authenticated and will try to refresh token if access token expired

▸ (...`args`: any[]): *Promise‹T | [Error](../modules/sayhey.md#markdown-header-error)›*

**Parameters:**

Name | Type |
------ | ------ |
`...args` | any[] |

**Returns:** *Promise‹Result‹T, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` initialize

▸ **initialize**(`params`: object): *void*

Defined in sayhey-chat-kit-client.ts:338

Setup methods

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`disableAutoRefreshToken?` | undefined &#124; false &#124; true |
`disableUserDeletedHandler?` | undefined &#124; false &#124; true |
`disableUserDisabledHandler?` | undefined &#124; false &#124; true |
`messageUpdateInterval?` | undefined &#124; number |
`publisherApiKey` | string |
`publisherAppId` | string |
`publisherUrl` | string |
`sayheyApiKey` | string |
`sayheyAppId` | string |
`sayheyUrl` | string |

**Returns:** *void*

___

### `Static` isSocketConnected

▸ **isSocketConnected**(): *boolean*

Defined in sayhey-chat-kit-client.ts:151

**Returns:** *boolean*

___

### `Static` makeConversationOwner

▸ **makeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1076

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` markMessageRead

▸ **markMessageRead**(`messageId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1545

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` muteAllNotifications

▸ **muteAllNotifications**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:678

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` muteConversation

▸ **muteConversation**(`conversationId`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:806

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` onMessage

▸ **onMessage**(`data`: [Event](../modules/publisher.event.md)): *void*

Defined in sayhey-chat-kit-client.ts:1224

**Parameters:**

Name | Type |
------ | ------ |
`data` | [Event](../modules/publisher.event.md) |

**Returns:** *void*

___

### `Static` pinConversation

▸ **pinConversation**(`conversationId`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:838

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` readMessage

▸ **readMessage**(`params`: object): *void*

Defined in sayhey-chat-kit-client.ts:1513

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`conversationId` | string |
`immediateUpdate?` | undefined &#124; false &#124; true |
`messageId` | string |
`sequence` | number |

**Returns:** *void*

___

### `Static` reconnectPublisher

▸ **reconnectPublisher**(): *Promise‹void›*

Defined in sayhey-chat-kit-client.ts:436

**Returns:** *Promise‹void›*

___

### `Static` registerForPushNotification

▸ **registerForPushNotification**(`instanceId`: string, `token`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1559

Push Notification APIs

**Parameters:**

Name | Type |
------ | ------ |
`instanceId` | string |
`token` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` removeConversationOwner

▸ **removeConversationOwner**(`conversationId`: string, `userId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1093

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` removeMessageReaction

▸ **removeMessageReaction**(`messageId`: string, `type`: [ReactionType](../enums/reactiontype.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1494

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |
`type` | [ReactionType](../enums/reactiontype.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` removeUsersFromConversation

▸ **removeUsersFromConversation**(`conversationId`: string, `userIds`: string[], `force?`: undefined | false | true): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1053

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`userIds` | string[] |
`force?` | undefined &#124; false &#124; true |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` sendMessage

▸ **sendMessage**(`params`: [SendMessageParams](../globals.md#markdown-header-sendmessageparams)): *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1343

**Parameters:**

Name | Type |
------ | ------ |
`params` | [SendMessageParams](../globals.md#markdown-header-sendmessageparams) |

**Returns:** *Promise‹Result‹[MessageDomainEntity](../interfaces/messagedomainentity.md), [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` setMessageReaction

▸ **setMessageReaction**(`messageId`: string, `type`: [ReactionType](../enums/reactiontype.md)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1475

**Parameters:**

Name | Type |
------ | ------ |
`messageId` | string |
`type` | [ReactionType](../enums/reactiontype.md) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` signIn

▸ **signIn**(`email`: string, `password`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:450

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`email` | string | user email for SayHey |
`password` | string | user password for SayHey   |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` signInWithToken

▸ **signInWithToken**(`accessToken`: string, `publisherToken`: string, `refreshToken`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:474

**Parameters:**

Name | Type |
------ | ------ |
`accessToken` | string |
`publisherToken` | string |
`refreshToken` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` signOut

▸ **signOut**(): *Promise‹void›*

Defined in sayhey-chat-kit-client.ts:515

**Returns:** *Promise‹void›*

___

### `Static` signUp

▸ **signUp**(`email`: string, `password`: string, `firstName`: string, `lastName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:487

**Parameters:**

Name | Type |
------ | ------ |
`email` | string |
`password` | string |
`firstName` | string |
`lastName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` unmuteAllNotifications

▸ **unmuteAllNotifications**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:692

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` unmuteConversation

▸ **unmuteConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:822

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` unpinConversation

▸ **unpinConversation**(`conversationId`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:854

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` unregisterForPushNotification

▸ **unregisterForPushNotification**(): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:1584

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` updateAuthDetails

▸ **updateAuthDetails**(`accessToken`: string, `publisherToken`: string, `refreshToken`: string): *Promise‹void›*

Defined in sayhey-chat-kit-client.ts:192

**Parameters:**

Name | Type | Description |
------ | ------ | ------ |
`accessToken` | string | access token returned from SayHey that will be used to verify user with SayHey |
`publisherToken` | string | publisher token returned from SayHey that will be used to verify Publisher access |
`refreshToken` | string | token returned from SayHey that will be used to refresh access token when it expired  |

**Returns:** *Promise‹void›*

___

### `Static` updateGroupImage

▸ **updateGroupImage**(`conversationId`: string, `file`: [FileUpload](../globals.md#markdown-header-fileupload)): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:900

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`file` | [FileUpload](../globals.md#markdown-header-fileupload) |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` updateGroupInfo

▸ **updateGroupInfo**(`conversationId`: string, `groupName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:925

**Parameters:**

Name | Type |
------ | ------ |
`conversationId` | string |
`groupName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` updateUserProfileImage

▸ **updateUserProfileImage**(`fileUri`: string, `fileName`: string): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:621

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string |
`fileName` | string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` updateUserProfileInfo

▸ **updateUserProfileInfo**(`options`: object): *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:645

**Parameters:**

▪ **options**: *object*

Name | Type |
------ | ------ |
`email?` | undefined &#124; string |
`firstName?` | undefined &#124; string |
`lastName?` | undefined &#124; string |

**Returns:** *Promise‹Result‹boolean, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` `Private` uploadFile

▸ **uploadFile**(`fileUri`: string | Blob, `fileName`: string, `progress?`: undefined | function, `thumbnailId?`: undefined | string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:269

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |
`progress?` | undefined &#124; function |
`thumbnailId?` | undefined &#124; string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

___

### `Static` uploadImage

▸ **uploadImage**(`fileUri`: string | Blob, `fileName`: string): *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*

Defined in sayhey-chat-kit-client.ts:317

**Parameters:**

Name | Type |
------ | ------ |
`fileUri` | string &#124; Blob |
`fileName` | string |

**Returns:** *Promise‹Result‹string, [Error](../modules/sayhey.md#markdown-header-error)››*
