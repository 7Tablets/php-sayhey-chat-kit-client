import { ParsedLine } from './types';
export declare const isSingleEmoji: (text: string) => false | RegExpMatchArray | null;
export declare const textHighlightRegex: RegExp;
export declare const isUrl: (url: string) => boolean;
export declare const isPhone: (phone: string) => boolean;
export declare const isEmail: (email: string) => boolean;
export declare const isUser: (userId: string) => boolean;
export declare const parseText: (text: string | null) => {
    parsedText: ParsedLine[];
    previewUrl: string | null;
} | null;
export declare const mimeTypes: {
    [extension: string]: string;
};
