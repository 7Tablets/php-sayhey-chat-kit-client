import { ApiKeyName } from './constants';
import { AxiosInstance } from 'axios';
import { ConversationDomainEntity, MessageDomainEntity, AuthResponse, SayHey, GalleryPayload, ConversableUsersBatchResult, ReachableUsersQuery, ConversationUserQuery, ConversationReachableUsersQuery, SendMessageParams, ConversationUsersBatch, ReactionType, ConversationReactionsDomainEntity, UserSelfDomainEntity, DefaultGroupImageUrlSet, FileUpload, ConversationUsersBasicDTO, ReactedUserDTO } from './types';
import { Result } from 'neverthrow';
export { isSingleEmoji, parseText } from './helper';
export * from './types';
export default class ChatKit {
    static accessToken: string;
    static publisherToken: string;
    static refreshToken: string;
    static instanceIdPushNotification: string;
    static readonly apiVersion: string;
    static httpClient: AxiosInstance;
    static appId: string;
    static apiKey: string;
    static serverUrl: string;
    static authDetailChangeListener: (res: AuthResponse) => Promise<void> | void;
    static eventListener: (event: SayHey.Event) => void;
    static sessionExpiredListener?: () => void;
    private static _initialized;
    private static _subscribed;
    static isUserAuthenticated: boolean;
    private static readMessageMap;
    static readMessageSequenceMap: {
        [conversationId: string]: number;
    };
    private static isSendingReadReceipt;
    private static timerHandle;
    private static exchangeTokenPromise;
    private static disableAutoRefreshToken?;
    private static disableUserDisabledHandler?;
    private static disableUserDeletedHandler?;
    private static loginId;
    private constructor();
    /**
     * Helper methods
     */
    /**
     *
     * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
     */
    private static guard;
    static exchangeTokenOnce(): Promise<Result<boolean, SayHey.Error>>;
    static isSocketConnected(): boolean;
    /**
     *
     * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
     */
    private static guardWithAuth;
    /**
     *
     * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
     * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
     * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
     */
    private static updateAuthDetails;
    /**
     *
     * @param urlPath - url path returned from SayHey
     */
    static getMediaSourceDetails(urlPath: string): {
        uri: string;
        method: string;
        headers: {
            "x-sayhey-client-api-key": string;
            Authorization: string;
            Accept: string;
        };
    };
    static getMediaDirectUrl(params: {
        type: 'image';
        urlPath: string;
        size: 'tiny' | 'small' | 'medium' | 'large' | 'xlarge';
    } | {
        type: 'other';
        urlPath: string;
    }): Promise<Result<string, SayHey.Error>>;
    private static uploadFile;
    static uploadImage(fileUri: string | Blob, fileName: string): Promise<Result<string, SayHey.Error>>;
    /**
     * Setup methods
     */
    static initialize(params: {
        sayheyAppId: string;
        sayheyApiKey: string;
        publisherAppId: string;
        publisherApiKey: string;
        sayheyUrl: string;
        publisherUrl: string;
        messageUpdateInterval?: number;
        disableAutoRefreshToken?: boolean;
        disableUserDisabledHandler?: boolean;
        disableUserDeletedHandler?: boolean;
    }): void;
    static addListeners: (listeners: {
        onAuthDetailChange: (res: AuthResponse) => void | Promise<void>;
        onEvent: (event: SayHey.Event) => void;
        onSocketConnect?: (() => void) | undefined;
        onSocketDisconnect?: ((reason: string) => void) | undefined;
        onSocketError?: ((error: Error) => void) | undefined;
        onSocketReconnect?: ((attemptNumber: number) => void) | undefined;
        onSessionExpired?: (() => void) | undefined;
    }) => Result<boolean, SayHey.Error>;
    static disconnectPublisher(): Promise<void>;
    static reconnectPublisher(): Promise<void>;
    /**
     * Token APIs
     */
    /**
     *
     * @param email - user email for SayHey
     * @param password - user password for SayHey
     *
     */
    static signIn(email: string, password: string): Promise<Result<boolean, SayHey.Error>>;
    static signInWithToken(accessToken: string, publisherToken: string, refreshToken: string): Promise<Result<boolean, SayHey.Error>>;
    static signUp(email: string, password: string, firstName: string, lastName: string): Promise<Result<boolean, SayHey.Error>>;
    static signOut(): Promise<void>;
    static exchangeToken(): Promise<Result<boolean, SayHey.Error>>;
    static changePassword(currentPassword: string, newPassword: string): Promise<Result<boolean, SayHey.Error>>;
    /**
     * User APIs
     */
    static getReachableUsers({ searchString, limit, nextCursor, includeRefIdInSearch }?: ReachableUsersQuery): Promise<Result<ConversableUsersBatchResult, SayHey.Error>>;
    static updateUserProfileImage(fileUri: string, fileName: string): Promise<Result<boolean, SayHey.Error>>;
    static updateUserProfileInfo(options: {
        firstName?: string;
        lastName?: string;
        email?: string;
    }): Promise<Result<boolean, SayHey.Error>>;
    static getUserDetails(): Promise<Result<UserSelfDomainEntity, SayHey.Error>>;
    static muteAllNotifications(): Promise<Result<boolean, SayHey.Error>>;
    static unmuteAllNotifications(): Promise<Result<boolean, SayHey.Error>>;
    /**
     * Conversation APIs
     */
    static getConversations(): Promise<Result<ConversationDomainEntity[], SayHey.Error>>;
    static getConversationDetails(conversationId: string): Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    static checkForExistingConversation(withUserId: string): Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    static createIndividualConversation(withUserId: string): Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    static createGroupConversation(params: {
        groupName: string;
        file: FileUpload;
        userIds: string[];
    }): Promise<Result<ConversationDomainEntity, SayHey.Error>>;
    static muteConversation(conversationId: string): Promise<Result<string, SayHey.Error>>;
    static unmuteConversation(conversationId: string): Promise<Result<boolean, SayHey.Error>>;
    static pinConversation(conversationId: string): Promise<Result<string, SayHey.Error>>;
    static unpinConversation(conversationId: string): Promise<Result<boolean, SayHey.Error>>;
    static deleteConversation(conversationId: string): Promise<Result<boolean, SayHey.Error>>;
    static deleteAllConversations(): Promise<Result<boolean, SayHey.Error>>;
    static updateGroupImage(conversationId: string, file: FileUpload): Promise<Result<boolean, SayHey.Error>>;
    static updateGroupInfo(conversationId: string, groupName: string): Promise<Result<boolean, SayHey.Error>>;
    static getConversationUsers({ conversationId, searchString, limit, nextCursor, type, includeRefIdInSearch }: ConversationUserQuery): Promise<Result<ConversationUsersBatch, SayHey.Error>>;
    static getConversationReachableUsers({ conversationId, searchString, limit, nextCursor, includeRefIdInSearch }: ConversationReachableUsersQuery): Promise<Result<ConversableUsersBatchResult, SayHey.Error>>;
    static getConversationBasicUsers(params: {
        conversationId: string;
        type?: 'all' | 'current' | 'default';
    }): Promise<Result<ConversationUsersBasicDTO[], SayHey.Error>>;
    static addUsersToConversation(conversationId: string, userIds: string[], force?: boolean): Promise<Result<boolean, SayHey.Error>>;
    static removeUsersFromConversation(conversationId: string, userIds: string[], force?: boolean): Promise<Result<boolean, SayHey.Error>>;
    static makeConversationOwner(conversationId: string, userId: string): Promise<Result<boolean, SayHey.Error>>;
    static removeConversationOwner(conversationId: string, userId: string): Promise<Result<boolean, SayHey.Error>>;
    /**
     * @deprecated The method should not be used
     */
    static getUserConversationReactions(conversationId: string): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>>;
    static getUserConversationMessagesReactions(conversationId: string, messageIds: string[]): Promise<Result<ConversationReactionsDomainEntity, SayHey.Error>>;
    static getGroupDefaultImageSet(): Promise<Result<DefaultGroupImageUrlSet[], SayHey.Error>>;
    /**
     * Message APIs
     */
    static getMessages(conversationId: string, pivotSequence?: number, after?: boolean): Promise<Result<MessageDomainEntity[], SayHey.Error>>;
    private static onMessage;
    static sendMessage(params: SendMessageParams): Promise<Result<MessageDomainEntity, SayHey.Error>>;
    static getGalleryMessages(conversationId: string, beforeSequence?: number, limit?: number): Promise<Result<GalleryPayload, SayHey.Error>>;
    static setMessageReaction(messageId: string, type: ReactionType): Promise<Result<boolean, SayHey.Error>>;
    static removeMessageReaction(messageId: string, type: ReactionType): Promise<Result<boolean, SayHey.Error>>;
    static readMessage(params: {
        conversationId: string;
        messageId: string;
        sequence: number;
        immediateUpdate?: boolean;
    }): void;
    private static markMessageRead;
    /** Push Notification APIs */
    static registerForPushNotification(instanceId: string, token: string): Promise<Result<boolean, SayHey.Error>>;
    static unregisterForPushNotification(): Promise<Result<boolean, SayHey.Error>>;
    static getReactedUsers({ messageId, type, limit, afterSequence }: {
        messageId: string;
        type?: ReactionType;
        limit: number;
        afterSequence: number | null;
    }): Promise<Result<ReactedUserDTO[], SayHey.Error>>;
}
