import { ConversationDTO, ConversationDomainEntity, MessageDTO, MessageDomainEntity, UserDTO, UserDomainEntity, MediaDTO, MediaDomainEntity, SystemMessageDTO, SystemMessageDomainEntity, ConversationUserDomainEntity, UserOfConversationDTO, ConversationReactionsDomainEntity, ConversationReactionsDTO, UserSelfDTO, UserSelfDomainEntity } from './types';
export declare class ConversationMapper {
    static toDomainEntity(conversation: ConversationDTO): ConversationDomainEntity;
    static toDomainEntities(conversations: ConversationDTO[]): ConversationDomainEntity[];
}
export declare class MessageMapper {
    static toDomainEntity(message: MessageDTO): MessageDomainEntity;
    static toDomainEntities(messages: MessageDTO[]): MessageDomainEntity[];
}
export declare class UserMapper {
    static toDomainEntity(user: UserDTO): UserDomainEntity;
    static toConversableUserDomainEntity(user: UserOfConversationDTO): ConversationUserDomainEntity;
    static toDomainEntities(users: UserDTO[]): UserDomainEntity[];
    static toConversableUserDomainEntities(users: UserOfConversationDTO[]): ConversationUserDomainEntity[];
    static toSelfDomainEntity(user: UserSelfDTO): UserSelfDomainEntity;
}
export declare class MediaMapper {
    static toDomainEntity(media: MediaDTO): MediaDomainEntity;
}
export declare class SystemMessageMapper {
    static toDomainEntity(systemMessage: SystemMessageDTO): SystemMessageDomainEntity;
}
export declare class ReactionMapper {
    static toDomianEntity(reactions: ConversationReactionsDTO[]): ConversationReactionsDomainEntity;
}
