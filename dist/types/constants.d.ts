import { HttpMethod } from './types';
export declare const API_VERSION = "v1";
export declare const ApiKeyName = "x-sayhey-client-api-key";
export declare const MESSAGE_READ_UPDATE_INTERVAL = 5000;
export declare const Routes: {
    SignIn: {
        Endpoint: string;
        Method: HttpMethod;
    };
    SignUp: {
        Endpoint: string;
        Method: HttpMethod;
    };
    ChangePassword: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetReachableUsers: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetExistingConversationWithUser: {
        Endpoint: (userId: string) => string;
        Method: HttpMethod;
    };
    GetConversations: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversationDetails: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    CreateIndividualConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    CreateGroupConversation: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetMessages: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    SendMessage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    MuteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UnmuteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    PinConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UnpinConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    DeleteAllConversations: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UploadFile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateGroupImage: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UpdateGroupInfo: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    UpdateUserProfileImage: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UpdateUserProfileInfo: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetConversationUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetConversationReachableUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    AddUsersToConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RemoveUsersFromConversation: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    MakeConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    RemoveConversationOwner: {
        Endpoint: (conversationId: string, userId: string) => string;
        Method: HttpMethod;
    };
    GetUserConversationReactions: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    GetUserConversationMessagesReactions: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    SetMessageReaction: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    RemoveMessageReaction: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    MuteAllNotifications: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UnmuteAllNotifications: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetUserProfile: {
        Endpoint: string;
        Method: HttpMethod;
    };
    GetGroupDefaultImageSet: {
        Endpoint: string;
        Method: HttpMethod;
    };
    MarkMessageAsRead: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
    GetConversationBasicUsers: {
        Endpoint: (conversationId: string) => string;
        Method: HttpMethod;
    };
    RegisterPushNotification: {
        Endpoint: string;
        Method: HttpMethod;
    };
    UnregisterPushNotification: {
        Endpoint: (instanceId: string) => string;
        Method: HttpMethod;
    };
    GetReactedUsers: {
        Endpoint: (messageId: string) => string;
        Method: HttpMethod;
    };
};
