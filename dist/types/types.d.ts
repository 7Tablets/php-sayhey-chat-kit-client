/**
 * Http Types
 */
import { EventMessage } from 'php-sayhey-publisher-kit-client';
export declare enum HttpMethod {
    Post = "post",
    Get = "get",
    Delete = "delete",
    Patch = "patch",
    Put = "put"
}
export interface SearchPaginateResponse {
    search: string | null;
    totalCount: number;
    offset: number;
    nextOffset: number;
    limit: number;
    completed: boolean;
}
/**
 * Reaction Types
 */
export declare enum ReactionType {
    Thumb = "Thumb",
    Heart = "Heart",
    Fist = "Fist"
}
export interface Reaction {
    type: ReactionType;
    count: number;
    reacted: boolean;
}
export declare class ReactionsObject {
    [ReactionType.Fist]: number;
    [ReactionType.Heart]: number;
    [ReactionType.Thumb]: number;
}
/**
 * Message Types
 */
export declare enum ImageSizes {
    Tiny = "tiny",
    Small = "small",
    Medium = "medium",
    Large = "large",
    XLarge = "xlarge"
}
export interface SubImageMinimal {
    imageSize: ImageSizes;
    width: number;
    height: number;
    postfix: string;
}
export interface ImageInfoMedia {
    width: number;
    height: number;
    subImagesInfo: SubImageMinimal[];
}
export declare enum FileType {
    Video = "video",
    Image = "image",
    Audio = "audio",
    Other = "other"
}
interface BaseMedia {
    fileType: FileType;
    urlPath: string;
    mimeType: string;
    extension: string;
    encoding: string;
    filename: string;
    fileSize: number;
    imageInfo: ImageInfoMedia | null;
}
export interface MediaDTO extends BaseMedia {
    videoInfo: {
        duration: number;
        thumbnail: MediaDTO | null;
    } | null;
}
export interface MediaDomainEntity extends BaseMedia {
    videoInfo: {
        duration: number;
        thumbnail: MediaDomainEntity | null;
    } | null;
}
export declare enum MessageType {
    System = "system",
    Text = "text",
    Image = "image",
    Video = "video",
    Document = "document",
    Gif = "gif",
    Emoji = "emoji",
    Audio = "audio"
}
export declare enum HighLightType {
    Text = "text",
    Email = "email",
    Phone = "phone",
    Url = "url",
    UserId = "userId"
}
export declare enum SystemMessageType {
    MemberRemoved = "MemberRemoved",
    MemberAdded = "MemberAdded",
    OwnerRemoved = "OwnerRemoved",
    OwnerAdded = "OwnerAdded",
    GroupPictureChanged = "GroupPictureChanged",
    GroupNameChanged = "GroupNameChanged",
    GroupDisabled = "GroupDisabled",
    GroupCreated = "GroupCreated",
    IndividualChatCreated = "IndividualChatCreated"
}
export interface SystemMessageBase {
    systemMessageType: SystemMessageType;
    id: string;
    refValue: string | null;
    actorCount: number;
    initiator: UserSimpleInfo | null;
    actors: UserSimpleInfo[];
    text?: string | null;
}
export interface SystemMessageDTO extends SystemMessageBase {
}
export interface SystemMessageDomainEntity extends SystemMessageBase {
}
export interface UserSimpleInfo {
    firstName: string;
    lastName: string;
    id: string;
    refId: string | null;
}
interface BaseMessage {
    id: string;
    conversationId: string;
    type: MessageType;
    text: string | null;
    refUrl: string | null;
    sequence: number;
    reactions: ReactionsObject;
}
export interface MessageDTO extends BaseMessage {
    createdAt: string;
    sender: UserDTO | null;
    file: MediaDTO | null;
    systemMessage: SystemMessageDTO | null;
    deletedAt: string | null;
}
export declare type ParsedLine = {
    type: HighLightType;
    value: string;
}[];
export interface MessageDomainEntity extends BaseMessage {
    sender: UserDomainEntity | null;
    file: MediaDomainEntity | null;
    createdAt: Date;
    deletedAt: Date | null;
    sent: boolean;
    parsedText: ParsedLine[] | null;
    previewUrl?: string | null;
    systemMessage: SystemMessageDomainEntity | null;
}
/**
 * Conversation Types
 */
export declare enum ConversationType {
    Group = "group",
    Individual = "individual"
}
interface BaseConversation {
    id: string;
    lastReadMessageId: string | null;
    type: ConversationType.Group | ConversationType.Individual;
    isOwner: boolean;
    visible: boolean;
    serverCreated: boolean;
    serverManaged: boolean;
}
interface ConversationInfoDTO {
    picture: MediaDTO | null;
    name: string;
}
interface ConversationInfoDomainEntity {
    picture: MediaDomainEntity | null;
    name: string;
}
interface UserConversationInfoDTO extends ConversationInfoDTO {
    id: string;
    email: string;
    refId: string | null;
}
interface UserConversationInfoDomainEntity extends ConversationInfoDomainEntity {
    id: string;
    email: string;
    refId: string | null;
}
export interface BaseConversationDTO extends BaseConversation {
    muteUntil: string | null;
    pinnedAt: string | null;
    badge: number | null;
    startAfter: string | null;
    endBefore: string | null;
    deletedFromConversationAt: string | null;
    createdAt: string;
    updatedAt: string;
    lastMessage: MessageDTO | null;
}
export interface IndividualConversationDTO extends BaseConversationDTO {
    type: ConversationType.Individual;
    user: UserConversationInfoDTO;
}
export interface GroupConversationDTO extends BaseConversationDTO {
    type: ConversationType.Group;
    group: ConversationInfoDTO;
}
export declare type ConversationDTO = IndividualConversationDTO | GroupConversationDTO;
export interface BaseConversationDomainEntity extends Omit<BaseConversation, 'user' | 'group'> {
    muteUntil: Date | null;
    pinnedAt: Date | null;
    badge: number;
    startAfter: Date | null;
    endBefore: Date | null;
    deletedFromConversationAt: Date | null;
    createdAt: Date;
    updatedAt: Date;
    lastMessage: MessageDomainEntity | null;
}
export interface GroupConversationDomainEntity extends BaseConversationDomainEntity {
    type: ConversationType.Group;
    group: ConversationInfoDomainEntity;
}
export interface IndividualConversationDomainEntity extends BaseConversationDomainEntity {
    type: ConversationType.Individual;
    user: UserConversationInfoDomainEntity;
}
export declare type ConversationDomainEntity = GroupConversationDomainEntity | IndividualConversationDomainEntity;
/**
 * Auth Types
 */
export interface AuthDTO {
    accessToken: string;
    publisherToken: string;
    refreshToken: string;
}
/**
 * User Types
 */
interface BaseUser {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    refId: string | null;
    disabled: boolean;
}
export interface UserDTO extends BaseUser {
    picture: MediaDTO | null;
    deletedAt: string | null;
}
export interface UserDomainEntity extends BaseUser {
    fullName: string;
    username: string;
    picture: MediaDomainEntity | null;
    deletedAt: Date | null;
}
export interface AuthResponse {
    userId: string;
    accessToken: string;
    publisherToken: string;
    refreshToken: string;
}
export declare type FileUpload = {
    path: string | Blob;
    fileName: string;
};
export declare type FileWithText = {
    uri: string | Blob;
    thumbnail?: {
        name: string;
        uri: string | Blob;
    };
    type: MessageType;
    text?: string;
    fileName: string;
};
export interface GalleryPayload {
    nextCursor: number;
    messageList: MessageDomainEntity[];
}
/**
 * DTOs
 */
interface ConversableUsersBatchMeta extends SearchPaginateResponse {
    userId: string;
}
export interface ConversableUsersBatchDTO extends ConversableUsersBatchMeta {
    results: UserDTO[];
}
export interface ConversableUsersBatchResult extends ConversableUsersBatchMeta {
    results: UserDomainEntity[];
}
export interface ReachableUsersQuery {
    searchString?: string;
    limit?: number;
    nextCursor?: number;
    includeRefIdInSearch?: boolean;
}
export declare enum ConversationUserQueryType {
    All = "all",
    Current = "current",
    Removed = "removed"
}
export interface ConversationReachableUsersQuery {
    conversationId: string;
    searchString?: string;
    limit?: number;
    nextCursor?: number;
    includeRefIdInSearch?: boolean;
}
export interface ConversationUserQuery extends ConversationReachableUsersQuery {
    type?: ConversationUserQueryType;
}
interface AdditionalUserOfConversation {
    deletedFromConversationAt: Date | null;
    isOwner: boolean;
}
export interface ConversationConversableUsersBatchDTO extends ConversationConversableUsersBatchMeta {
    results: UserDTO[];
}
export interface ConversationConversableUsersBatch extends ConversationConversableUsersBatchMeta {
    results: UserDomainEntity[];
}
interface ConversationConversableUsersBatchMeta extends SearchPaginateResponse {
    conversationId: string;
}
interface ConversationUsersBatchMeta extends ConversationConversableUsersBatchMeta {
    type: ConversationUserQueryType;
}
export interface UserOfConversationDTO extends UserDTO, AdditionalUserOfConversation {
}
export interface ConversationUsersBatchDTO extends ConversationUsersBatchMeta {
    results: UserOfConversationDTO[];
}
export interface ConversationUserDomainEntity extends UserDomainEntity {
    deletedFromConversationAt: Date | null;
    isOwner: boolean;
}
export interface ConversationUsersBatch extends ConversationConversableUsersBatchMeta {
    results: ConversationUserDomainEntity[];
}
export interface ConversationUsersBasicDTO {
    isOwner: boolean;
    id: string;
    deletedFromConversationAt: Date | null;
    userDisabled: boolean;
    userDeletedAt: Date | null;
}
export declare namespace Publisher {
    export enum EventType {
        NewMessage = "NEW_MESSAGE",
        ConversationCreated = "CONVERSATION_CREATED",
        ConversationMuted = "CONVERSATION_MUTED",
        ConversationUnmuted = "CONVERSATION_UNMUTED",
        ConversationPinned = "CONVERSATION_PINNED",
        ConversationUnpinned = "CONVERSATION_UNPINNED",
        ConversationHideAndClear = "CONVERSATION_HIDE_AND_CLEAR",
        ConversationHideAndClearAll = "CONVERSATION_HIDE_AND_CLEAR_ALL",
        ConversationGroupInfoChanged = "CONVERSATION_GROUP_INFO_CHANGED",
        UserInfoChanged = "USER_INFO_CHANGED",
        ConversationGroupOwnerAdded = "CONVERSATION_GROUP_OWNER_ADDED",
        ConversationGroupOwnerRemoved = "CONVERSATION_GROUP_OWNER_REMOVED",
        ConversationGroupMemberAdded = "CONVERSATION_GROUP_MEMBER_ADDED",
        ConversationGroupMemberRemoved = "CONVERSATION_GROUP_MEMBER_REMOVED",
        UserReactedToMessage = "USER_REACTED_TO_MESSAGE",
        UserUnReactedToMessage = "USER_UNREACTED_TO_MESSAGE",
        MessageReactionsUpdate = "MESSAGE_REACTIONS_UPDATE",
        UserDisabled = "USER_DISABLED",
        UserEnabled = "USER_ENABLED",
        MessageDeleted = "MESSAGE_DELETED",
        UserDeleted = "USER_DELETED"
    }
    namespace Payload {
        interface NewMessage {
            message: MessageDTO;
            clientRefId: string | null;
        }
        interface ConversationCreated {
            conversationId: string;
        }
        interface ConversationUnmuted {
            conversationId: string;
        }
        interface ConversationUnpinned {
            conversationId: string;
        }
        interface ConversationMuted {
            conversationId: string;
            conversationMuteDto: {
                muteUntil: string;
            };
        }
        interface ConversationPinned {
            conversationId: string;
            conversationPinDto: {
                pinnedAt: string;
            };
        }
        interface ConversationHideAndClear {
            conversationId: string;
            conversationUserHideClearDto: {
                startAfter: string;
                visible: boolean;
            };
        }
        interface ConversationHideAndClearAll {
            conversationUserHideClearDto: {
                startAfter: string;
                visible: boolean;
            };
        }
        interface ConversationGroupInfoChanged {
            conversationId: string;
            groupName: string;
            groupPicture: MediaDTO | null;
        }
        interface UserInfoChanged {
            userId: string;
            userEmail: string;
            userPicture: MediaDTO | null;
            userFirstName: string;
            userLastName: string;
            userRefId: string | null;
            userMuteAllUntil: Date | null;
        }
        interface ConversationGroupOwnerRemoved {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupOwnerAdded {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupMemberAdded {
            conversationId: string;
            userIds: string[];
            deletedAt: null;
            endBefore: null;
        }
        interface ConversationGroupMemberRemoved {
            conversationId: string;
            userIds: string[];
            deletedAt: Date;
            endBefore: Date;
        }
        interface UserReactedToMessage {
            conversationId: string;
            messageId: string;
            type: ReactionType;
            userId: string;
        }
        interface UserUnReactedToMessage {
            conversationId: string;
            messageId: string;
            userId: string;
            type?: ReactionType;
        }
        interface MessageReactionsUpdate {
            conversationId: string;
            messageId: string;
            messageReactionsObject: ReactionsObject;
        }
        interface UserDisabled {
            userId: string;
            disabled: true;
        }
        interface UserEnabled {
            userId: string;
            disabled: false;
        }
        interface MessageDeleted {
            conversationId: string;
            messageId: string;
            deletedAt: Date;
        }
        interface UserDeleted {
            userId: string;
            deletedAt: Date;
        }
    }
    export namespace Event {
        interface NewMessage extends EventMessage {
            event: EventType.NewMessage;
            payload: Payload.NewMessage;
        }
        interface ConversationCreated extends EventMessage {
            event: EventType.ConversationCreated;
            payload: Payload.ConversationCreated;
        }
        interface ConversationUnmuted extends EventMessage {
            event: EventType.ConversationUnmuted;
            payload: Payload.ConversationUnmuted;
        }
        interface ConversationUnpinned extends EventMessage {
            event: EventType.ConversationUnpinned;
            payload: Payload.ConversationUnpinned;
        }
        interface ConversationMuted extends EventMessage {
            event: EventType.ConversationMuted;
            payload: Payload.ConversationMuted;
        }
        interface ConversationPinned extends EventMessage {
            event: EventType.ConversationPinned;
            payload: Payload.ConversationPinned;
        }
        interface ConversationHideAndClear extends EventMessage {
            event: EventType.ConversationHideAndClear;
            payload: Payload.ConversationHideAndClear;
        }
        interface ConversationHideAndClearAll extends EventMessage {
            event: EventType.ConversationHideAndClearAll;
            payload: Payload.ConversationHideAndClearAll;
        }
        interface ConversationGroupInfoChanged extends EventMessage {
            event: EventType.ConversationGroupInfoChanged;
            payload: Payload.ConversationGroupInfoChanged;
        }
        interface UserInfoChanged extends EventMessage {
            event: EventType.UserInfoChanged;
            payload: Payload.UserInfoChanged;
        }
        interface ConversationGroupOwnerAdded extends EventMessage {
            event: EventType.ConversationGroupOwnerAdded;
            payload: Payload.ConversationGroupOwnerAdded;
        }
        interface ConversationGroupOwnerRemoved extends EventMessage {
            event: EventType.ConversationGroupOwnerRemoved;
            payload: Payload.ConversationGroupOwnerRemoved;
        }
        interface ConversationGroupMemberRemoved extends EventMessage {
            event: EventType.ConversationGroupMemberRemoved;
            payload: Payload.ConversationGroupMemberRemoved;
        }
        interface ConversationGroupMemberAdded extends EventMessage {
            event: EventType.ConversationGroupMemberAdded;
            payload: Payload.ConversationGroupMemberAdded;
        }
        interface UserReactedToMessage extends EventMessage {
            event: EventType.UserReactedToMessage;
            payload: Payload.UserReactedToMessage;
        }
        interface UserUnReactedToMessage extends EventMessage {
            event: EventType.UserUnReactedToMessage;
            payload: Payload.UserUnReactedToMessage;
        }
        interface MessageReactionsUpdate extends EventMessage {
            event: EventType.MessageReactionsUpdate;
            payload: Payload.MessageReactionsUpdate;
        }
        interface UserDisabled extends EventMessage {
            event: EventType.UserDisabled;
            payload: Payload.UserDisabled;
        }
        interface UserEnabled extends EventMessage {
            event: EventType.UserEnabled;
            payload: Payload.UserEnabled;
        }
        interface MessageDeleted extends EventMessage {
            event: EventType.MessageDeleted;
            payload: Payload.MessageDeleted;
        }
        interface UserDeleted extends EventMessage {
            event: EventType.UserDeleted;
            payload: Payload.UserDeleted;
        }
        export type AllEvents = NewMessage | ConversationCreated | ConversationMuted | ConversationPinned | ConversationUnmuted | ConversationUnpinned | ConversationHideAndClear | ConversationHideAndClearAll | ConversationGroupInfoChanged | UserInfoChanged | ConversationGroupOwnerAdded | ConversationGroupOwnerRemoved | ConversationGroupMemberAdded | ConversationGroupMemberRemoved | UserReactedToMessage | UserUnReactedToMessage | MessageReactionsUpdate | UserDisabled | UserEnabled | MessageDeleted | UserDeleted;
        export {};
    }
    export import Event = Event.AllEvents;
    export {};
}
export declare namespace SayHey {
    namespace Payload {
        interface ConversationHideAndClearAll {
            startAfter: Date;
            visible: boolean;
        }
        interface ConversationCreated {
            conversationId: string;
        }
        interface NewMessage {
            message: MessageDomainEntity;
            clientRefId: string | null;
        }
        interface ConversationMuted {
            conversationId: string;
            muteUntil: Date;
        }
        interface ConversationPinned {
            conversationId: string;
            pinnedAt: Date;
        }
        interface ConversationUnmuted {
            conversationId: string;
        }
        interface ConversationUnpinned {
            conversationId: string;
        }
        interface ConversationHideAndClear {
            conversationId: string;
            startAfter: Date;
            visible: boolean;
        }
        interface ConversationGroupInfoChanged {
            conversationId: string;
            groupName: string;
            groupPicture: MediaDomainEntity | null;
        }
        interface UserInfoChanged {
            userId: string;
            userEmail: string;
            userPicture: MediaDomainEntity | null;
            userFirstName: string;
            userLastName: string;
            userRefId: string | null;
            userMuteAllUntil: Date | null;
        }
        interface ConversationGroupOwnerRemoved {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupOwnerAdded {
            conversationId: string;
            userId: string;
            isOwner: boolean;
        }
        interface ConversationGroupMemberRemoved {
            conversationId: string;
            userIds: string[];
            deletedAt: Date;
            endBefore: Date;
        }
        interface ConversationGroupMemberAdded {
            conversationId: string;
            userIds: string[];
            deletedAt: null;
            endBefore: null;
        }
        interface UserReactedToMessage {
            conversationId: string;
            messageId: string;
            type: ReactionType;
            userId: string;
        }
        interface MessageReactionsUpdate {
            conversationId: string;
            messageId: string;
            messageReactionsObject: ReactionsObject;
        }
        interface UserUnReactedToMessage {
            conversationId: string;
            messageId: string;
            userId: string;
            type?: ReactionType;
        }
        interface UserDisabled {
            userId: string;
            disabled: boolean;
        }
        interface UserEnabled {
            userId: string;
            disabled: boolean;
        }
        interface MessageDeleted {
            conversationId: string;
            messageId: string;
            deletedAt: Date;
        }
        interface UserDeleted {
            userId: string;
            deletedAt: Date;
        }
    }
    export namespace Event {
        interface AllConversationsDeleted {
            event: SayHey.EventType.ConversationHideAndClearAll;
            payload: Payload.ConversationHideAndClearAll;
        }
        interface ConversationCreated {
            event: SayHey.EventType.ConversationCreated;
            payload: Payload.ConversationCreated;
        }
        interface NewMessage {
            event: SayHey.EventType.NewMessage;
            payload: Payload.NewMessage;
        }
        interface ConversationMuted {
            event: SayHey.EventType.ConversationMuted;
            payload: Payload.ConversationMuted;
        }
        interface ConversationPinned {
            event: SayHey.EventType.ConversationPinned;
            payload: Payload.ConversationPinned;
        }
        interface ConversationUnmuted {
            event: SayHey.EventType.ConversationUnmuted;
            payload: Payload.ConversationUnmuted;
        }
        interface ConversationUnpinned {
            event: SayHey.EventType.ConversationUnpinned;
            payload: Payload.ConversationUnpinned;
        }
        interface ConversationHideAndClear {
            event: SayHey.EventType.ConversationHideAndClear;
            payload: Payload.ConversationHideAndClear;
        }
        interface ConversationGroupInfoChanged {
            event: SayHey.EventType.ConversationGroupInfoChanged;
            payload: Payload.ConversationGroupInfoChanged;
        }
        interface UserInfoChanged {
            event: SayHey.EventType.UserInfoChanged;
            payload: Payload.UserInfoChanged;
        }
        interface ConversationGroupOwnerRemoved {
            event: SayHey.EventType.ConversationGroupOwnerRemoved;
            payload: Payload.ConversationGroupOwnerRemoved;
        }
        interface ConversationGroupOwnerAdded {
            event: SayHey.EventType.ConversationGroupOwnerAdded;
            payload: Payload.ConversationGroupOwnerAdded;
        }
        interface ConversationGroupMemberRemoved extends EventMessage {
            event: SayHey.EventType.ConversationGroupMemberRemoved;
            payload: Payload.ConversationGroupMemberRemoved;
        }
        interface ConversationGroupMemberAdded extends EventMessage {
            event: SayHey.EventType.ConversationGroupMemberAdded;
            payload: Payload.ConversationGroupMemberAdded;
        }
        interface UserReactedToMessage extends EventMessage {
            event: SayHey.EventType.UserReactedToMessage;
            payload: Payload.UserReactedToMessage;
        }
        interface UserUnReactedToMessage extends EventMessage {
            event: SayHey.EventType.UserUnReactedToMessage;
            payload: Payload.UserUnReactedToMessage;
        }
        interface MessageReactionsUpdate extends EventMessage {
            event: SayHey.EventType.MessageReactionsUpdate;
            payload: Payload.MessageReactionsUpdate;
        }
        interface UserDisabled extends EventMessage {
            event: SayHey.EventType.UserDisabled;
            payload: Payload.UserDisabled;
        }
        interface UserEnabled extends EventMessage {
            event: SayHey.EventType.UserEnabled;
            payload: Payload.UserEnabled;
        }
        interface MessageDeleted extends EventMessage {
            event: EventType.MessageDeleted;
            payload: Payload.MessageDeleted;
        }
        interface UserDeleted extends EventMessage {
            event: EventType.UserDeleted;
            payload: Payload.UserDeleted;
        }
        export type AllEvents = AllConversationsDeleted | ConversationCreated | NewMessage | ConversationMuted | ConversationUnmuted | ConversationUnpinned | ConversationPinned | ConversationHideAndClear | ConversationGroupInfoChanged | UserInfoChanged | ConversationGroupOwnerAdded | ConversationGroupOwnerRemoved | ConversationGroupMemberRemoved | ConversationGroupMemberAdded | UserReactedToMessage | UserUnReactedToMessage | MessageReactionsUpdate | UserDisabled | UserEnabled | MessageDeleted | UserDeleted;
        export {};
    }
    export namespace ChatKitError {
        namespace ErrorType {
            enum InternalErrorType {
                Unknown = "InternalUnknown",
                Uninitialized = "InternalUninitialized",
                AuthMissing = "InternalAuthMissing",
                BadData = "InternalBadData",
                MissingParameter = "InternalMissingParameter"
            }
            type Type = InternalErrorType | AuthErrorType | FileErrorType | ServerErrorType | GeneralErrorType | MessageErrorType | UserErrorType | ConversationErrorType | DateErrorType;
            enum AuthErrorType {
                InvalidKeyErrorType = "AuthErrorTypeInvalidKey",
                InvalidUserType = "AuthErrorTypeInvalidUserType",
                NotFoundErrorType = "AuthErrorTypeNotFound",
                MissingParamsErrorType = "AuthErrorTypeMissingParams",
                InvalidPassword = "AuthErrorTypeInvalidPassword",
                TokenExpiredType = "AuthErrorTypeTokenExpired",
                AppHasNoNotificationSetup = "AuthErrorTypeAppHasNoNotificationSetup",
                NotPartOfApp = "AuthErrorTypeNotPartOfApp",
                InvalidWebhookKey = "AuthErrorTypeInvalidWebhookKey",
                NoAccessToUser = "AuthErrorTypeNoAccessToUser",
                ServerNoAccessToUser = "AuthErrorTypeServerNoAccessToUser",
                IncorrectPassword = "AuthErrorTypeIncorrectPassword",
                NoSamePassword = "AuthErrorTypeNoSamePassword",
                NoAppAccessToUser = "AuthErrorTypeNoAppAccessToUser",
                TokenNotStarted = "AuthErrorTypeTokenNotStarted",
                AccountAlreadyExists = "AuthErrorTypeAccountAlreadyExists",
                UserNotFound = "AuthErrorTypeUserNotFound",
                InvalidToken = "AuthErrorTypeInvalidToken",
                MissingAuthHeaderField = "AuthErrorTypeMissingAuthHeaderField",
                InvalidCredentials = "AuthErrorTypeInvalidCredentials",
                HashGeneration = "AuthErrorTypeHashGeneration",
                HashComparison = "AuthErrorTypeHashComparison",
                RefreshTokenAlreadyExchanged = "AuthErrorTypeRefreshTokenAlreadyExchanged",
                RefreshTokenNotFound = "AuthErrorTypeRefreshTokenNotFound",
                UserDisabled = "AuthErrorTypeUserDisabled"
            }
            enum FileErrorType {
                SizeLimitExceeded = "FileErrorTypeSizeLimitExceeded",
                NoFileSentType = "FileErrorTypeNoFileSentType",
                InvalidFieldNameForFile = "FileErrorTypeInvalidFieldNameForFile",
                InvalidMetadata = "FileErrorTypeInvalidMetadata",
                FileSizeLimitExceeded = "FileErrorTypeFileSizeLimitExceeded",
                InvalidFileName = "FileErrorTypeInvalidFileName",
                NotSent = "FileErrorTypeNotSent",
                NotFound = "FileErrorTypeNotFound",
                NoThumbnailFound = "FileErrorTypeNoThumbnailFound",
                ImageNotForConversation = "FileErrorTypeImageNotForConversation",
                ImageNotForUser = "FileErrorTypeImageNotForUser",
                FileNotForMessage = "FileErrorTypeFileNotForMessage",
                SizeNotFound = "FileErrorTypeSizeNotFound",
                NotForApp = "FileErrorTypeNotForApp",
                IncorrectFileType = "FileErrorTypeIncorrectFileType"
            }
            enum ServerErrorType {
                UnknownError = "UnknownError",
                InternalServerError = "InternalServerError"
            }
            enum GeneralErrorType {
                MissingQueryParams = "GeneralErrorTypeMissingQueryParams",
                RouteNotFound = "GeneralErrorTypeRouteNotFound",
                LimitNotValid = "GeneralErrorTypeLimitNotValid",
                OffsetNotValid = "GeneralErrorTypeOffsetNotValid"
            }
            enum MessageErrorType {
                InvalidDate = "MessageErrorTypeInvalidDate",
                NotFound = "MessageErrorTypeNotFound",
                NotFoundForUser = "MessageErrorTypeNotFoundForUser",
                MessageAlreadyRead = "MessageErrorTypeMessageAlreadyRead",
                CannotDeleteSystemMessage = "MessageErrorTypeCannotDeleteSystemMessage"
            }
            enum DateErrorType {
                InvalidFromDateError = "DateErrorTypeInvalidFromDateError",
                InvalidToDateError = "DateErrorTypeInvalidToDateError"
            }
            enum UserErrorType {
                AlreadyMuted = "UserErrorTypeAlreadyMuted",
                NotOnMute = "UserErrorTypeNotOnMute",
                AlreadyEnabled = "UserErrorTypeAlreadyEnabled",
                AlreadyDisabled = "UserErrorTypeAlreadyDisabled"
            }
            enum ConversationErrorType {
                NotFound = "ConversationErrorTypeNotFound",
                CannotConverseWithUser = "ConversationErrorTypeCannotConverseWithUser",
                NotAnOwner = "ConversationErrorTypeNotAnOwner",
                AlreadyAnOwner = "ConversationErrorTypeAlreadyAnOwner",
                NotFoundForApp = "ConversationErrorTypeNotFoundForApp",
                InvalidType = "ConversationErrorTypeInvalidType",
                InvalidConversationUserType = "ConversationErrorTypeInvalidConversationUserType",
                MustBeOwner = "ConversationErrorTypeMustBeOwner",
                NotPartOfApp = "ConversationErrorTypeNotPartOfApp",
                NotFoundForUser = "ConversationErrorTypeNotFoundForUser",
                AlreadyExists = "ConversationErrorTypeAlreadyExists",
                CannotChatWithSelf = "ConversationErrorTypeCannotChatWithSelf",
                AlreadyMuted = "ConversationErrorTypeAlreadyMuted",
                NotOnMute = "ConversationErrorTypeNotOnMute",
                AlreadyPinned = "ConversationErrorTypeAlreadyPinned",
                NotPinned = "ConversationErrorTypeNotPinned",
                UsersNotInApp = "ConversationErrorTypeUsersNotInApp",
                NoValidUsersToAdd = "ConversationErrorTypeNoValidUsersToAdd",
                NoValidUsersToRemove = "ConversationErrorTypeNoValidUsersToRemove",
                CannotChangeAccessToSelf = "ConversationErrorTypeCannotChangeAccessToSelf",
                CannotAddExistingUsers = "ConversationErrorTypeCannotAddExistingUsers",
                AlreadyRemovedUsers = "ConversationErrorTypeAlreadyRemovedUsers",
                UsersNotInConversation = "ConversationErrorTypeUsersNotInConversation",
                CannotRemoveOwners = "ConversationErrorTypeCannotRemoveOwners"
            }
        }
        enum PushNotificationError {
            InvalidPushParams = "DeviceTokenErrorTypeInvalidPushParams",
            AlreadyRegisteredForUser = "DeviceTokenErrorTypeAlreadyRegisteredForUser",
            NotFound = "DeviceTokenErrorTypeNotFound",
            NoTokensToPush = "DeviceTokenErrorTypeNoTokensToPush",
            OnlyStringData = "DeviceTokenErrorTypeOnlyStringData"
        }
        type Type = ErrorType.Type | PushNotificationError;
        class AppError {
            type: Type;
            statusCode?: number;
            error?: Error;
            message?: string;
            authFailed?: boolean;
            exchange?: boolean;
            constructor(err?: string | Error, type?: Type);
            setStatusCode: (statusCode?: number | undefined) => void;
            setType: (type: Type) => void;
            setMessage: (msg: string) => void;
            setErrorActions: (params: {
                authFailed?: boolean | undefined;
                exchange?: boolean | undefined;
            }) => void;
        }
    }
    export import EventType = Publisher.EventType;
    export import ErrorType = ChatKitError.ErrorType;
    export import Event = Event.AllEvents;
    export import Error = ChatKitError.AppError;
    export {};
}
interface SendMessageParamsBase {
    conversationId: string;
    clientRefId: string;
}
interface SendTextMessageParams extends SendMessageParamsBase {
    type: MessageType.Text;
    text: string;
}
interface SendEmojiMessageParams extends SendMessageParamsBase {
    type: MessageType.Emoji;
    text: string;
}
interface SendGifMessageParams extends SendMessageParamsBase {
    type: MessageType.Gif;
    text?: string;
    url: string;
}
interface SendFileMessageParams extends SendMessageParamsBase {
    type: MessageType.Image | MessageType.Audio | MessageType.Video | MessageType.Document;
    file: FileWithText;
    progress?: (percentage: number) => void;
}
export declare type SendMessageParams = SendTextMessageParams | SendEmojiMessageParams | SendGifMessageParams | SendFileMessageParams;
export interface ConversationReactionsDTO {
    messageId: string;
    type: ReactionType | ReactionType[];
}
export declare type ConversationReactionsDomainEntity = Partial<{
    [messageId: string]: ReactionType[];
}>;
export interface UserSelfDTO extends UserDTO {
    muteAllUntil: string | null;
}
export interface UserSelfDomainEntity extends UserDomainEntity {
    muteAllUntil: Date | null;
}
export interface DefaultGroupImageUrlSet {
    filename: string;
    original: string;
    thumbnail: string;
}
export interface UserSearchPaginateResponse extends SearchPaginateResponse {
    includeRefIdInSearch: boolean;
}
export interface ReactedUserDTO {
    type: ReactionType;
    sequence: number;
    createdAt: Date;
    messageId: string;
    userId: string;
    user: UserDTO;
}
export {};
