"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var types_1 = require("./types");
var helper_1 = require("./helper");
var ConversationMapper = /** @class */ (function () {
    function ConversationMapper() {
    }
    ConversationMapper.toDomainEntity = function (conversation) {
        var id = conversation.id, lastReadMessageId = conversation.lastReadMessageId, lastMessage = conversation.lastMessage, muteUntil = conversation.muteUntil, pinnedAt = conversation.pinnedAt, badge = conversation.badge, type = conversation.type, createdAt = conversation.createdAt, updatedAt = conversation.updatedAt, isOwner = conversation.isOwner, startAfter = conversation.startAfter, visible = conversation.visible, endBefore = conversation.endBefore, deletedFromConversationAt = conversation.deletedFromConversationAt, serverCreated = conversation.serverCreated, serverManaged = conversation.serverManaged;
        var common = {
            id: id,
            lastReadMessageId: lastReadMessageId,
            lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
            muteUntil: muteUntil ? new Date(muteUntil) : null,
            pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
            createdAt: new Date(createdAt),
            updatedAt: new Date(updatedAt),
            startAfter: startAfter ? new Date(startAfter) : null,
            endBefore: endBefore ? new Date(endBefore) : null,
            deletedFromConversationAt: deletedFromConversationAt
                ? new Date(deletedFromConversationAt)
                : null,
            badge: badge ? badge : 0,
            type: type,
            isOwner: isOwner,
            visible: visible,
            serverCreated: serverCreated,
            serverManaged: serverManaged
        };
        if (conversation.type === types_1.ConversationType.Group) {
            var group = conversation.group;
            return __assign(__assign({}, common), { type: types_1.ConversationType.Group, group: __assign(__assign({}, group), { picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null }) });
        }
        var user = conversation.user;
        return __assign(__assign({}, common), { type: types_1.ConversationType.Individual, user: __assign(__assign({}, user), { picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null }) });
    };
    ConversationMapper.toDomainEntities = function (conversations) {
        return conversations.map(function (conversation) { return ConversationMapper.toDomainEntity(conversation); });
    };
    return ConversationMapper;
}());
exports.ConversationMapper = ConversationMapper;
var MessageMapper = /** @class */ (function () {
    function MessageMapper() {
    }
    MessageMapper.toDomainEntity = function (message) {
        var id = message.id, conversationId = message.conversationId, sender = message.sender, type = message.type, text = message.text, sequence = message.sequence, systemMessage = message.systemMessage, file = message.file, createdAt = message.createdAt, reactions = message.reactions, refUrl = message.refUrl, deletedAt = message.deletedAt;
        var parsedResult = helper_1.parseText(text);
        return {
            id: id,
            conversationId: conversationId,
            sender: sender,
            type: type,
            text: text,
            deletedAt: deletedAt ? new Date(deletedAt) : null,
            sequence: sequence,
            systemMessage: systemMessage ? SystemMessageMapper.toDomainEntity(systemMessage) : null,
            createdAt: new Date(createdAt),
            reactions: reactions,
            sent: true,
            parsedText: parsedResult ? parsedResult.parsedText : null,
            file: file ? MediaMapper.toDomainEntity(file) : null,
            refUrl: refUrl,
            previewUrl: parsedResult === null || parsedResult === void 0 ? void 0 : parsedResult.previewUrl
        };
    };
    MessageMapper.toDomainEntities = function (messages) {
        return messages.map(function (value) { return MessageMapper.toDomainEntity(value); });
    };
    return MessageMapper;
}());
exports.MessageMapper = MessageMapper;
var UserMapper = /** @class */ (function () {
    function UserMapper() {
    }
    UserMapper.toDomainEntity = function (user) {
        var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
        return {
            id: id,
            firstName: firstName,
            lastName: lastName,
            fullName: firstName + " " + lastName,
            email: email,
            refId: refId,
            username: email,
            disabled: disabled,
            deletedAt: deletedAt ? new Date(deletedAt) : null,
            picture: picture ? MediaMapper.toDomainEntity(picture) : null
        };
    };
    UserMapper.toConversableUserDomainEntity = function (user) {
        var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, isOwner = user.isOwner, deletedFromConversationAt = user.deletedFromConversationAt, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
        return {
            id: id,
            firstName: firstName,
            lastName: lastName,
            username: email,
            fullName: firstName + " " + lastName,
            email: email,
            picture: picture ? MediaMapper.toDomainEntity(picture) : null,
            isOwner: isOwner,
            deletedFromConversationAt: deletedFromConversationAt,
            refId: refId,
            disabled: disabled,
            deletedAt: deletedAt ? new Date(deletedAt) : null
        };
    };
    UserMapper.toDomainEntities = function (users) {
        return users.map(UserMapper.toDomainEntity);
    };
    UserMapper.toConversableUserDomainEntities = function (users) {
        return users.map(UserMapper.toConversableUserDomainEntity);
    };
    UserMapper.toSelfDomainEntity = function (user) {
        var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, muteAllUntil = user.muteAllUntil, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
        return {
            id: id,
            firstName: firstName,
            lastName: lastName,
            username: email,
            fullName: firstName + " " + lastName,
            email: email,
            refId: refId,
            picture: picture ? MediaMapper.toDomainEntity(picture) : null,
            muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null,
            disabled: disabled,
            deletedAt: deletedAt ? new Date(deletedAt) : null
        };
    };
    return UserMapper;
}());
exports.UserMapper = UserMapper;
var MediaMapper = /** @class */ (function () {
    function MediaMapper() {
    }
    MediaMapper.toDomainEntity = function (media) {
        var urlPath = media.urlPath, fileSize = media.fileSize, fileType = media.fileType, extension = media.extension, filename = media.filename, mimeType = media.mimeType, encoding = media.encoding, imageInfo = media.imageInfo, videoInfo = media.videoInfo;
        return {
            urlPath: urlPath,
            fileSize: fileSize,
            fileType: fileType,
            extension: extension,
            filename: filename,
            mimeType: mimeType,
            encoding: encoding,
            imageInfo: imageInfo,
            videoInfo: !videoInfo
                ? null
                : {
                    duration: videoInfo.duration,
                    thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
                }
        };
    };
    return MediaMapper;
}());
exports.MediaMapper = MediaMapper;
var SystemMessageMapper = /** @class */ (function () {
    function SystemMessageMapper() {
    }
    SystemMessageMapper.toDomainEntity = function (systemMessage) {
        var systemMessageType = systemMessage.systemMessageType, id = systemMessage.id, refValue = systemMessage.refValue, actorCount = systemMessage.actorCount, initiator = systemMessage.initiator, actors = systemMessage.actors, text = systemMessage.text;
        return {
            systemMessageType: systemMessageType,
            id: id,
            refValue: refValue,
            actorCount: actorCount,
            initiator: initiator,
            actors: actors,
            text: text
        };
    };
    return SystemMessageMapper;
}());
exports.SystemMessageMapper = SystemMessageMapper;
var ReactionMapper = /** @class */ (function () {
    function ReactionMapper() {
    }
    ReactionMapper.toDomianEntity = function (reactions) {
        var conversationReactions = {};
        reactions.forEach(function (item) {
            var messageId = item.messageId;
            var reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type];
            if (!conversationReactions[messageId]) {
                conversationReactions[messageId] = [];
            }
            var currentReactionTypes = conversationReactions[messageId] || [];
            var reactionSet = new Set(__spreadArrays(currentReactionTypes, reactionTypesArray));
            conversationReactions[messageId] = Array.from(reactionSet);
        });
        return conversationReactions;
    };
    return ReactionMapper;
}());
exports.ReactionMapper = ReactionMapper;
//# sourceMappingURL=mapper.js.map