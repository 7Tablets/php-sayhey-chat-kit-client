"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var constants_1 = require("./constants");
var axios_1 = require("./axios");
var axios_2 = require("axios");
var types_1 = require("./types");
var neverthrow_1 = require("neverthrow");
var mapper_1 = require("./mapper");
var jwt_decode_1 = require("jwt-decode");
var php_sayhey_publisher_kit_client_1 = require("php-sayhey-publisher-kit-client");
var helper_1 = require("./helper");
var helper_2 = require("./helper");
exports.isSingleEmoji = helper_2.isSingleEmoji;
exports.parseText = helper_2.parseText;
__export(require("./types"));
var ChatKit = /** @class */ (function () {
    function ChatKit() {
    }
    /**
     * Helper methods
     */
    /**
     *
     * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
     */
    ChatKit.guard = function (cb) {
        return __awaiter(this, void 0, void 0, function () {
            var res, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!ChatKit._initialized) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        if (!ChatKit._subscribed) {
                            return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('ChatKit events have not been subscribed to!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, cb()];
                    case 2:
                        res = _a.sent();
                        if (res instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(res)];
                        }
                        return [2 /*return*/, neverthrow_1.ok(res)];
                    case 3:
                        e_1 = _a.sent();
                        if (e_1 instanceof types_1.SayHey.Error) {
                            return [2 /*return*/, neverthrow_1.err(e_1)];
                        }
                        return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Unknown error', types_1.SayHey.ErrorType.InternalErrorType.Unknown))];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ChatKit.exchangeTokenOnce = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (ChatKit.exchangeTokenPromise) {
                            return [2 /*return*/, ChatKit.exchangeTokenPromise];
                        }
                        ChatKit.exchangeTokenPromise = ChatKit.exchangeToken();
                        return [4 /*yield*/, ChatKit.exchangeTokenPromise];
                    case 1:
                        response = _a.sent();
                        ChatKit.exchangeTokenPromise = null;
                        return [2 /*return*/, response];
                }
            });
        });
    };
    ChatKit.isSocketConnected = function () {
        return php_sayhey_publisher_kit_client_1.default.isConnected();
    };
    /**
     *
     * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
     */
    ChatKit.guardWithAuth = function (cb) {
        return __awaiter(this, void 0, void 0, function () {
            var currentLoginId, res, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        currentLoginId = ChatKit.loginId;
                        return [4 /*yield*/, ChatKit.guard(cb)];
                    case 1:
                        res = _a.sent();
                        if (!res.isErr()) return [3 /*break*/, 4];
                        if (!(!ChatKit.disableAutoRefreshToken &&
                            res.error.type === types_1.SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 4];
                        if (!(currentLoginId === ChatKit.loginId)) return [3 /*break*/, 3];
                        return [4 /*yield*/, ChatKit.exchangeTokenOnce()];
                    case 2:
                        response = _a.sent();
                        if (response.isOk()) {
                            return [2 /*return*/, ChatKit.guard(cb)];
                        }
                        ChatKit.signOut();
                        return [3 /*break*/, 4];
                    case 3: 
                    // If token has been exchanged, use the new credentials to make the request again
                    return [2 /*return*/, ChatKit.guard(cb)];
                    case 4: return [2 /*return*/, res];
                }
            });
        });
    };
    /**
     *
     * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
     * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
     * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
     */
    ChatKit.updateAuthDetails = function (accessToken, publisherToken, refreshToken) {
        return __awaiter(this, void 0, void 0, function () {
            var id;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        ChatKit.accessToken = accessToken;
                        ChatKit.publisherToken = publisherToken;
                        ChatKit.refreshToken = refreshToken;
                        id = accessToken ? jwt_decode_1.default(accessToken).id : '';
                        if (publisherToken && accessToken && refreshToken) {
                            ChatKit.loginId += 1;
                            ChatKit.httpClient = axios_1.default.createWithAuth({
                                apiKey: ChatKit.apiKey,
                                baseURL: ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/apps/" + ChatKit.appId,
                                accessToken: ChatKit.accessToken
                            });
                            php_sayhey_publisher_kit_client_1.default.updateAccessParams({
                                token: publisherToken,
                                userId: id
                            });
                            if (!php_sayhey_publisher_kit_client_1.default.isConnected()) {
                                php_sayhey_publisher_kit_client_1.default.connect();
                            }
                        }
                        return [4 /*yield*/, ChatKit.authDetailChangeListener({
                                userId: id,
                                accessToken: accessToken,
                                publisherToken: publisherToken,
                                refreshToken: refreshToken
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
     *
     * @param urlPath - url path returned from SayHey
     */
    ChatKit.getMediaSourceDetails = function (urlPath) {
        var _a;
        return {
            uri: "" + ChatKit.serverUrl + urlPath,
            method: 'GET',
            headers: (_a = {},
                _a[constants_1.ApiKeyName] = ChatKit.apiKey,
                _a.Authorization = "Bearer " + ChatKit.accessToken,
                _a.Accept = 'image/*, video/*, audio/*',
                _a)
        };
    };
    ChatKit.getMediaDirectUrl = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            function getMediaDirectUrl() {
                return __awaiter(this, void 0, void 0, function () {
                    var queryString, data;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                queryString = {
                                    noRedirect: true
                                };
                                if (params.type === 'image') {
                                    queryString.size = params.size;
                                }
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: "" + ChatKit.serverUrl + params.urlPath,
                                        method: 'GET',
                                        params: queryString
                                    })];
                            case 1:
                                data = (_a.sent()).data;
                                return [2 /*return*/, data.url];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(getMediaDirectUrl)];
            });
        });
    };
    ChatKit.uploadFile = function (fileUri, fileName, progress, thumbnailId) {
        return __awaiter(this, void 0, void 0, function () {
            function uploadFile() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, formData, extension, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                formData = new FormData();
                                if (fileUri instanceof Blob) {
                                    formData.append('file', fileUri, fileName);
                                    if (thumbnailId) {
                                        formData.append('thumbnailId', thumbnailId);
                                    }
                                }
                                else {
                                    extension = fileName.split('.').pop();
                                    if (extension) {
                                        formData.append('file', {
                                            uri: fileUri,
                                            name: fileName,
                                            type: helper_1.mimeTypes[extension.toLowerCase()]
                                        });
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        return [2 /*return*/, new types_1.SayHey.Error('Invalid file')];
                                    }
                                }
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: formData,
                                        onUploadProgress: progress
                                            ? function (event) {
                                                var loaded = event.loaded, total = event.total;
                                                progress((loaded / total) * 0.95);
                                            }
                                            : undefined
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                progress === null || progress === void 0 ? void 0 : progress(1);
                                return [2 /*return*/, data.id];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(uploadFile)];
            });
        });
    };
    ChatKit.uploadImage = function (fileUri, fileName) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            var extension, isSupportedImage;
            return __generator(this, function (_b) {
                extension = (_a = fileName
                    .split('.')
                    .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                isSupportedImage = helper_1.mimeTypes[extension || ''].split('/')[0] === 'image';
                if (isSupportedImage) {
                    return [2 /*return*/, ChatKit.uploadFile(fileUri, fileName)];
                }
                return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Please select a valid image!', types_1.SayHey.ErrorType.InternalErrorType.BadData))];
            });
        });
    };
    /**
     * Setup methods
     */
    ChatKit.initialize = function (params) {
        var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, publisherApiKey = params.publisherApiKey, publisherAppId = params.publisherAppId, sayheyUrl = params.sayheyUrl, publisherUrl = params.publisherUrl, messageUpdateInterval = params.messageUpdateInterval, disableAutoRefreshToken = params.disableAutoRefreshToken, disableUserDeletedHandler = params.disableUserDeletedHandler, disableUserDisabledHandler = params.disableUserDisabledHandler;
        ChatKit.serverUrl = sayheyUrl;
        var baseURL = ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/apps/" + sayheyAppId;
        ChatKit.apiKey = sayheyApiKey;
        ChatKit.appId = sayheyAppId;
        ChatKit.disableAutoRefreshToken = disableAutoRefreshToken;
        ChatKit.disableUserDisabledHandler = disableUserDisabledHandler;
        ChatKit.disableUserDeletedHandler = disableUserDeletedHandler;
        ChatKit.httpClient = axios_1.default.createWithoutAuth({
            apiKey: ChatKit.apiKey,
            baseURL: baseURL
        });
        php_sayhey_publisher_kit_client_1.default.initialize({
            appId: publisherAppId,
            clientApiKey: publisherApiKey,
            publisherUrl: publisherUrl
        });
        if (ChatKit.timerHandle !== null) {
            clearInterval(ChatKit.timerHandle);
        }
        ChatKit.timerHandle = setInterval(function () {
            ChatKit.isSendingReadReceipt = true;
            var copy = __assign({}, ChatKit.readMessageMap);
            ChatKit.readMessageMap = {};
            ChatKit.isSendingReadReceipt = false;
            for (var conversationId in copy) {
                var messageId = copy[conversationId];
                ChatKit.markMessageRead(messageId);
            }
        }, messageUpdateInterval || constants_1.MESSAGE_READ_UPDATE_INTERVAL);
        ChatKit._initialized = true;
    };
    ChatKit.disconnectPublisher = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                php_sayhey_publisher_kit_client_1.default.disconnect();
                return [2 /*return*/];
            });
        });
    };
    ChatKit.reconnectPublisher = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                php_sayhey_publisher_kit_client_1.default.connect();
                return [2 /*return*/];
            });
        });
    };
    /**
     * Token APIs
     */
    /**
     *
     * @param email - user email for SayHey
     * @param password - user password for SayHey
     *
     */
    ChatKit.signIn = function (email, password) {
        return __awaiter(this, void 0, void 0, function () {
            function signInCallback() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 2:
                                _b.sent();
                                ChatKit.isUserAuthenticated = true;
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guard(signInCallback)];
            });
        });
    };
    ChatKit.signInWithToken = function (accessToken, publisherToken, refreshToken) {
        return __awaiter(this, void 0, void 0, function () {
            function signInWithTokenCallback() {
                return __awaiter(this, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0: return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 1:
                                _a.sent();
                                ChatKit.isUserAuthenticated = true;
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guard(signInWithTokenCallback)];
            });
        });
    };
    ChatKit.signUp = function (email, password, firstName, lastName) {
        return __awaiter(this, void 0, void 0, function () {
            function signUp() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            email: email,
                                            password: password,
                                            firstName: firstName,
                                            lastName: lastName
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 2:
                                _b.sent();
                                ChatKit.isUserAuthenticated = true;
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guard(signUp)];
            });
        });
    };
    ChatKit.signOut = function () {
        return __awaiter(this, void 0, void 0, function () {
            var unregisteringPushNotification;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        php_sayhey_publisher_kit_client_1.default.disconnect();
                        ChatKit.isUserAuthenticated = false;
                        ChatKit.loginId = 0;
                        unregisteringPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                            var result;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, ChatKit.unregisterForPushNotification()];
                                    case 1:
                                        result = _a.sent();
                                        if (result.isOk() ||
                                            (result.isErr() && result.error.statusCode && result.error.statusCode < 500)) {
                                            return [2 /*return*/];
                                        }
                                        setTimeout(unregisteringPushNotification, 3000);
                                        return [2 /*return*/];
                                }
                            });
                        }); };
                        return [4 /*yield*/, unregisteringPushNotification()];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChatKit.exchangeToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            function exchangeToken() {
                var _a, _b;
                return __awaiter(this, void 0, void 0, function () {
                    var data, refreshToken, accessToken, publisherToken, e_2;
                    return __generator(this, function (_c) {
                        switch (_c.label) {
                            case 0:
                                _c.trys.push([0, 6, , 8]);
                                return [4 /*yield*/, axios_2.default({
                                        method: types_1.HttpMethod.Post,
                                        url: ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/tokens/exchange",
                                        data: {
                                            refreshToken: ChatKit.refreshToken
                                        }
                                    })];
                            case 1:
                                data = (_c.sent()).data;
                                refreshToken = data.refreshToken, accessToken = data.accessToken, publisherToken = data.publisherToken;
                                if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                            case 2:
                                _c.sent();
                                ChatKit.isUserAuthenticated = true;
                                return [3 /*break*/, 5];
                            case 3: return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                            case 4:
                                _c.sent();
                                (_a = ChatKit.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(ChatKit);
                                ChatKit.isUserAuthenticated = false;
                                _c.label = 5;
                            case 5: return [3 /*break*/, 8];
                            case 6:
                                e_2 = _c.sent();
                                return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                            case 7:
                                _c.sent();
                                (_b = ChatKit.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(ChatKit);
                                ChatKit.isUserAuthenticated = false;
                                return [3 /*break*/, 8];
                            case 8: return [2 /*return*/, ChatKit.isUserAuthenticated];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guard(exchangeToken)];
            });
        });
    };
    ChatKit.changePassword = function (currentPassword, newPassword) {
        return __awaiter(this, void 0, void 0, function () {
            function changePassword() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.ChangePassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            currentPassword: currentPassword,
                                            newPassword: newPassword
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(changePassword)];
            });
        });
    };
    /**
     * User APIs
     */
    ChatKit.getReachableUsers = function (_a) {
        var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
        return __awaiter(this, void 0, void 0, function () {
            function getReachableUsers() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, params, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint;
                                params = {
                                    search: searchString || '',
                                    includeRefIdInSearch: includeRefIdInSearch || false
                                };
                                if (limit) {
                                    params.limit = limit;
                                }
                                if (nextCursor) {
                                    params.offset = nextCursor;
                                }
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: url,
                                        method: Method,
                                        params: params
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toDomainEntities(data.results) })];
                        }
                    });
                });
            }
            return __generator(this, function (_c) {
                return [2 /*return*/, ChatKit.guardWithAuth(getReachableUsers)];
            });
        });
    };
    ChatKit.updateUserProfileImage = function (fileUri, fileName) {
        return __awaiter(this, void 0, void 0, function () {
            function updateUserProfileImage() {
                return __awaiter(this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, ChatKit.uploadFile(fileUri, fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = constants_1.Routes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            profileImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(updateUserProfileImage)];
            });
        });
    };
    ChatKit.updateUserProfileInfo = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            function updateUserProfileInfo() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UpdateUserProfileInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: options
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(updateUserProfileInfo)];
            });
        });
    };
    ChatKit.getUserDetails = function () {
        return __awaiter(this, void 0, void 0, function () {
            function getUserDetails() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetUserProfile, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.UserMapper.toSelfDomainEntity(data)];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(getUserDetails)];
            });
        });
    };
    ChatKit.muteAllNotifications = function () {
        return __awaiter(this, void 0, void 0, function () {
            function muteAllNotifications() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.MuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(muteAllNotifications)];
            });
        });
    };
    ChatKit.unmuteAllNotifications = function () {
        return __awaiter(this, void 0, void 0, function () {
            function unmuteAllNotifications() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UnmuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(unmuteAllNotifications)];
            });
        });
    };
    /**
     * Conversation APIs
     */
    ChatKit.getConversations = function () {
        return __awaiter(this, void 0, void 0, function () {
            function getConversations() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntities(data)];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(getConversations)];
            });
        });
    };
    ChatKit.getConversationDetails = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function getConversationDetails() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(getConversationDetails)];
            });
        });
    };
    ChatKit.checkForExistingConversation = function (withUserId) {
        return __awaiter(this, void 0, void 0, function () {
            function checkForExistingConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetExistingConversationWithUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(withUserId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(checkForExistingConversation)];
            });
        });
    };
    ChatKit.createIndividualConversation = function (withUserId) {
        return __awaiter(this, void 0, void 0, function () {
            function createIndividualConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.CreateIndividualConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            memberId: withUserId
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(createIndividualConversation)];
            });
        });
    };
    ChatKit.createGroupConversation = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            function createGroupConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, file, groupName, userIds, fileIdResponse, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                file = params.file, groupName = params.groupName, userIds = params.userIds;
                                return [4 /*yield*/, ChatKit.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            groupName: groupName,
                                            groupImageId: fileIdResponse.value,
                                            memberIds: userIds
                                        }
                                    })];
                            case 2:
                                data = (_b.sent()).data;
                                return [2 /*return*/, mapper_1.ConversationMapper.toDomainEntity(data)];
                            case 3: return [2 /*return*/, fileIdResponse.error];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(createGroupConversation)];
            });
        });
    };
    ChatKit.muteConversation = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function muteConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.MuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.muteUntil];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(muteConversation)];
            });
        });
    };
    ChatKit.unmuteConversation = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function unmuteConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UnmuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(unmuteConversation)];
            });
        });
    };
    ChatKit.pinConversation = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function pinConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.PinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data.pinnedAt];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(pinConversation)];
            });
        });
    };
    ChatKit.unpinConversation = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function unpinConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UnpinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(unpinConversation)];
            });
        });
    };
    ChatKit.deleteConversation = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            function deleteConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.DeleteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(deleteConversation)];
            });
        });
    };
    ChatKit.deleteAllConversations = function () {
        return __awaiter(this, void 0, void 0, function () {
            function deleteAllConversations() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.DeleteAllConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(deleteAllConversations)];
            });
        });
    };
    ChatKit.updateGroupImage = function (conversationId, file) {
        return __awaiter(this, void 0, void 0, function () {
            function updateGroupImage() {
                return __awaiter(this, void 0, void 0, function () {
                    var fileIdResponse, _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0: return [4 /*yield*/, ChatKit.uploadFile(file.path, file.fileName)];
                            case 1:
                                fileIdResponse = _b.sent();
                                if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                _a = constants_1.Routes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupImageId: fileIdResponse.value
                                        }
                                    })];
                            case 2:
                                _b.sent();
                                return [2 /*return*/, true];
                            case 3: return [2 /*return*/, false];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(updateGroupImage)];
            });
        });
    };
    ChatKit.updateGroupInfo = function (conversationId, groupName) {
        return __awaiter(this, void 0, void 0, function () {
            function updateGroupInfo() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            groupName: groupName
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(updateGroupInfo)];
            });
        });
    };
    ChatKit.getConversationUsers = function (_a) {
        var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
        return __awaiter(this, void 0, void 0, function () {
            function getConversationUsers() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        params: {
                                            type: type || types_1.ConversationUserQueryType.Current,
                                            offset: nextCursor,
                                            limit: limit,
                                            search: searchString,
                                            includeRefIdInSearch: includeRefIdInSearch || false
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toConversableUserDomainEntities(data.results) })];
                        }
                    });
                });
            }
            return __generator(this, function (_b) {
                return [2 /*return*/, ChatKit.guardWithAuth(getConversationUsers)];
            });
        });
    };
    ChatKit.getConversationReachableUsers = function (_a) {
        var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, includeRefIdInSearch = _a.includeRefIdInSearch;
        return __awaiter(this, void 0, void 0, function () {
            function getConversationReachableUsers() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, url, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetConversationReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                url = Endpoint(conversationId);
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: url,
                                        method: Method,
                                        params: {
                                            limit: limit,
                                            search: searchString,
                                            offset: nextCursor,
                                            includeRefIdInSearch: includeRefIdInSearch || false
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, __assign(__assign({}, data), { results: mapper_1.UserMapper.toDomainEntities(data.results) })];
                        }
                    });
                });
            }
            return __generator(this, function (_b) {
                return [2 /*return*/, ChatKit.guardWithAuth(getConversationReachableUsers)];
            });
        });
    };
    ChatKit.getConversationBasicUsers = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var conversationId, _a, type;
            return __generator(this, function (_b) {
                conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a;
                return [2 /*return*/, ChatKit.guard(function getConversationUsers() {
                        return __awaiter(this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = constants_1.Routes.GetConversationBasicUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    type: type
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data];
                                }
                            });
                        });
                    })];
            });
        });
    };
    ChatKit.addUsersToConversation = function (conversationId, userIds, force) {
        return __awaiter(this, void 0, void 0, function () {
            function addUsersToConversation() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: {
                                            memberIds: userIds
                                        },
                                        params: {
                                            force: force
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(addUsersToConversation)];
            });
        });
    };
    ChatKit.removeUsersFromConversation = function (conversationId, userIds, force) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.makeConversationOwner = function (conversationId, userId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.removeConversationOwner = function (conversationId, userId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId, userId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     * @deprecated The method should not be used
     */
    ChatKit.getUserConversationReactions = function (conversationId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.GetUserConversationReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.getUserConversationMessagesReactions = function (conversationId, messageIds) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.GetUserConversationMessagesReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                messageIds: messageIds
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.ReactionMapper.toDomianEntity(data)];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.getGroupDefaultImageSet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    }); })];
            });
        });
    };
    /**
     * Message APIs
     */
    ChatKit.getMessages = function (conversationId, pivotSequence, after) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, lastMessageSequenceInBatch, messageListIds, messageList, data, incomingMessageList, _i, incomingMessageList_1, m;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    if (!!after) return [3 /*break*/, 2];
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                beforeSequence: pivotSequence,
                                                limit: 20
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toDomainEntities(data)];
                                case 2:
                                    if (!pivotSequence) {
                                        return [2 /*return*/, []];
                                    }
                                    lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER;
                                    messageListIds = new Set();
                                    messageList = [];
                                    _b.label = 3;
                                case 3:
                                    if (!(lastMessageSequenceInBatch > pivotSequence + 1)) return [3 /*break*/, 5];
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                limit: 20,
                                                beforeSequence: lastMessageSequenceInBatch
                                            }
                                        })];
                                case 4:
                                    data = (_b.sent()).data;
                                    incomingMessageList = data;
                                    if (incomingMessageList.length === 0) {
                                        return [3 /*break*/, 5];
                                    }
                                    for (_i = 0, incomingMessageList_1 = incomingMessageList; _i < incomingMessageList_1.length; _i++) {
                                        m = incomingMessageList_1[_i];
                                        lastMessageSequenceInBatch = m.sequence;
                                        if (lastMessageSequenceInBatch <= pivotSequence) {
                                            break;
                                        }
                                        if (!messageListIds.has(m.id)) {
                                            messageListIds.add(m.id);
                                            messageList.push(m);
                                        }
                                    }
                                    return [3 /*break*/, 3];
                                case 5: return [2 /*return*/, mapper_1.MessageMapper.toDomainEntities(messageList)];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.onMessage = function (data) {
        switch (data.event) {
            case types_1.Publisher.EventType.NewMessage: {
                ChatKit.eventListener({
                    event: data.event,
                    payload: {
                        message: mapper_1.MessageMapper.toDomainEntity(data.payload.message),
                        clientRefId: data.payload.clientRefId
                    }
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationMuted: {
                var _a = data.payload, conversationId = _a.conversationId, muteUntil = _a.conversationMuteDto.muteUntil;
                ChatKit.eventListener({
                    event: data.event,
                    payload: {
                        conversationId: conversationId,
                        muteUntil: new Date(muteUntil)
                    }
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationPinned: {
                var _b = data.payload, conversationId = _b.conversationId, pinnedAt = _b.conversationPinDto.pinnedAt;
                ChatKit.eventListener({
                    event: data.event,
                    payload: {
                        conversationId: conversationId,
                        pinnedAt: new Date(pinnedAt)
                    }
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationHideAndClear: {
                var _c = data.payload, conversationId = _c.conversationId, _d = _c.conversationUserHideClearDto, startAfter = _d.startAfter, visible = _d.visible;
                ChatKit.eventListener({
                    event: data.event,
                    payload: {
                        conversationId: conversationId,
                        visible: visible,
                        startAfter: new Date(startAfter)
                    }
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationHideAndClearAll: {
                var _e = data.payload.conversationUserHideClearDto, startAfter = _e.startAfter, visible = _e.visible;
                ChatKit.eventListener({
                    event: data.event,
                    payload: {
                        visible: visible,
                        startAfter: new Date(startAfter)
                    }
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationGroupInfoChanged: {
                ChatKit.eventListener({
                    event: data.event,
                    payload: __assign(__assign({}, data.payload), { groupPicture: data.payload.groupPicture
                            ? mapper_1.MediaMapper.toDomainEntity(data.payload.groupPicture)
                            : null })
                });
                break;
            }
            case types_1.Publisher.EventType.ConversationCreated:
            case types_1.Publisher.EventType.ConversationUnpinned:
            case types_1.Publisher.EventType.ConversationUnmuted:
            case types_1.Publisher.EventType.UserInfoChanged:
            case types_1.Publisher.EventType.ConversationGroupOwnerAdded:
            case types_1.Publisher.EventType.ConversationGroupOwnerRemoved:
            case types_1.Publisher.EventType.ConversationGroupMemberAdded:
            case types_1.Publisher.EventType.ConversationGroupMemberRemoved:
            case types_1.Publisher.EventType.UserReactedToMessage:
            case types_1.Publisher.EventType.UserUnReactedToMessage:
            case types_1.Publisher.EventType.MessageReactionsUpdate:
            case types_1.Publisher.EventType.MessageDeleted:
            case types_1.Publisher.EventType.UserEnabled: {
                ChatKit.eventListener(data);
                break;
            }
            case types_1.Publisher.EventType.UserDisabled: {
                ChatKit.eventListener(data);
                if (!ChatKit.disableUserDisabledHandler) {
                    ChatKit.signOut();
                }
                break;
            }
            case types_1.Publisher.EventType.UserDeleted: {
                ChatKit.eventListener(data);
                if (!ChatKit.disableUserDeletedHandler) {
                    ChatKit.signOut();
                }
                break;
            }
        }
    };
    ChatKit.sendMessage = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var conversationId, clientRefId, _a, Endpoint, Method, payload, _b, file, progress_1, subProgress, fileIdResponse, file, progress_2, subProgress, uri, fileName, thumbnail, thumbnailId, thumbnailIdResponse, videoUploadProgress, fileIdResponse, data;
                        var _c, _d, _e;
                        return __generator(this, function (_f) {
                            switch (_f.label) {
                                case 0:
                                    conversationId = params.conversationId, clientRefId = params.clientRefId;
                                    _a = constants_1.Routes.SendMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    _b = params.type;
                                    switch (_b) {
                                        case types_1.MessageType.Text: return [3 /*break*/, 1];
                                        case types_1.MessageType.Emoji: return [3 /*break*/, 1];
                                        case types_1.MessageType.Gif: return [3 /*break*/, 2];
                                        case types_1.MessageType.Audio: return [3 /*break*/, 3];
                                        case types_1.MessageType.Image: return [3 /*break*/, 3];
                                        case types_1.MessageType.Document: return [3 /*break*/, 3];
                                        case types_1.MessageType.Video: return [3 /*break*/, 5];
                                    }
                                    return [3 /*break*/, 9];
                                case 1:
                                    {
                                        payload = {
                                            type: params.type,
                                            text: params.text.trim()
                                        };
                                        return [3 /*break*/, 9];
                                    }
                                    _f.label = 2;
                                case 2:
                                    {
                                        if (!helper_1.isUrl(params.url)) {
                                            return [2 /*return*/, new types_1.SayHey.Error('Invalid URL!', types_1.SayHey.ErrorType.InternalErrorType.MissingParameter)];
                                        }
                                        payload = {
                                            type: types_1.MessageType.Gif,
                                            refUrl: params.url,
                                            text: (_c = params.text) === null || _c === void 0 ? void 0 : _c.trim()
                                        };
                                        return [3 /*break*/, 9];
                                    }
                                    _f.label = 3;
                                case 3:
                                    file = params.file, progress_1 = params.progress;
                                    subProgress = progress_1
                                        ? function (percentage) {
                                            progress_1(percentage * 0.9);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, ChatKit.uploadFile(file.uri, file.fileName, subProgress)];
                                case 4:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_d = file.text) === null || _d === void 0 ? void 0 : _d.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    return [3 /*break*/, 9];
                                case 5:
                                    file = params.file, progress_2 = params.progress;
                                    subProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.4);
                                        }
                                        : undefined;
                                    uri = file.uri, fileName = file.fileName, thumbnail = file.thumbnail;
                                    thumbnailId = '';
                                    if (!thumbnail) return [3 /*break*/, 7];
                                    return [4 /*yield*/, ChatKit.uploadFile(thumbnail.uri, thumbnail.name, subProgress)];
                                case 6:
                                    thumbnailIdResponse = _f.sent();
                                    if (thumbnailIdResponse.isErr()) {
                                        return [2 /*return*/, thumbnailIdResponse.error];
                                    }
                                    thumbnailId = thumbnailIdResponse.value;
                                    _f.label = 7;
                                case 7:
                                    videoUploadProgress = progress_2
                                        ? function (percentage) {
                                            progress_2(percentage * 0.6 + 0.4);
                                        }
                                        : undefined;
                                    return [4 /*yield*/, ChatKit.uploadFile(uri, fileName, videoUploadProgress, thumbnailId)];
                                case 8:
                                    fileIdResponse = _f.sent();
                                    if (fileIdResponse.isErr()) {
                                        return [2 /*return*/, fileIdResponse.error];
                                    }
                                    payload = {
                                        type: file.type,
                                        text: (_e = file.text) === null || _e === void 0 ? void 0 : _e.trim(),
                                        fileId: fileIdResponse.value
                                    };
                                    _f.label = 9;
                                case 9: return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(conversationId),
                                        method: Method,
                                        data: payload,
                                        params: {
                                            clientRefId: clientRefId
                                        }
                                    })];
                                case 10:
                                    data = (_f.sent()).data;
                                    return [2 /*return*/, mapper_1.MessageMapper.toDomainEntity(data)];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.getGalleryMessages = function (conversationId, beforeSequence, limit) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, messages, mediaList;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                galleryOnly: true,
                                                limit: limit,
                                                beforeSequence: beforeSequence
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    messages = mapper_1.MessageMapper.toDomainEntities(data);
                                    mediaList = messages.map(function (message) { return message.file; });
                                    return [2 /*return*/, {
                                            nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
                                            messageList: messages
                                        }];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.setMessageReaction = function (messageId, type) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.SetMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.removeMessageReaction = function (messageId, type) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.RemoveMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            data: {
                                                type: type
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    ChatKit.readMessage = function (params) {
        var conversationId = params.conversationId, messageId = params.messageId, sequence = params.sequence, immediateUpdate = params.immediateUpdate;
        var mark = function () {
            if (Number.isInteger(ChatKit.readMessageSequenceMap[conversationId]) &&
                ChatKit.readMessageSequenceMap[conversationId] >= sequence) {
                return;
            }
            if (immediateUpdate) {
                ChatKit.markMessageRead(messageId).then(function (res) {
                    if (res.isErr()) {
                        ChatKit.readMessageMap[conversationId] = messageId;
                    }
                });
            }
            else {
                ChatKit.readMessageMap[conversationId] = messageId;
            }
            ChatKit.readMessageSequenceMap[conversationId] = sequence;
        };
        if (!ChatKit.isSendingReadReceipt) {
            mark();
        }
        else {
            setTimeout(mark, 500);
        }
    };
    ChatKit.markMessageRead = function (messageId) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = constants_1.Routes.MarkMessageAsRead, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    }); })];
            });
        });
    };
    /** Push Notification APIs */
    ChatKit.registerForPushNotification = function (instanceId, token) {
        return __awaiter(this, void 0, void 0, function () {
            function registerForPushNotification() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.RegisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                ChatKit.instanceIdPushNotification = instanceId;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint,
                                        method: Method,
                                        data: {
                                            instanceId: instanceId,
                                            token: token
                                        }
                                    })];
                            case 1:
                                _b.sent();
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                if (!instanceId || !token) {
                    return [2 /*return*/, neverthrow_1.err(new types_1.SayHey.Error('Invalid instance id or token string!'))];
                }
                return [2 /*return*/, ChatKit.guardWithAuth(registerForPushNotification)];
            });
        });
    };
    ChatKit.unregisterForPushNotification = function () {
        return __awaiter(this, void 0, void 0, function () {
            function unregisterForPushNotification() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.UnregisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(ChatKit.instanceIdPushNotification),
                                        method: Method
                                    })];
                            case 1:
                                _b.sent();
                                ChatKit.instanceIdPushNotification = '';
                                return [2 /*return*/, true];
                        }
                    });
                });
            }
            return __generator(this, function (_a) {
                if (!ChatKit.instanceIdPushNotification) {
                    return [2 /*return*/, neverthrow_1.ok(true)];
                }
                return [2 /*return*/, ChatKit.guard(unregisterForPushNotification)];
            });
        });
    };
    ChatKit.getReactedUsers = function (_a) {
        var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
        return __awaiter(this, void 0, void 0, function () {
            function getReactedUsers() {
                return __awaiter(this, void 0, void 0, function () {
                    var _a, Endpoint, Method, data;
                    return __generator(this, function (_b) {
                        switch (_b.label) {
                            case 0:
                                _a = constants_1.Routes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                return [4 /*yield*/, ChatKit.httpClient({
                                        url: Endpoint(messageId),
                                        method: Method,
                                        params: {
                                            limit: limit,
                                            afterSequence: afterSequence,
                                            type: type || ''
                                        }
                                    })];
                            case 1:
                                data = (_b.sent()).data;
                                return [2 /*return*/, data];
                        }
                    });
                });
            }
            return __generator(this, function (_b) {
                return [2 /*return*/, ChatKit.guardWithAuth(getReactedUsers)];
            });
        });
    };
    ChatKit.instanceIdPushNotification = '';
    ChatKit.apiVersion = constants_1.API_VERSION;
    ChatKit._initialized = false;
    ChatKit._subscribed = false;
    ChatKit.isUserAuthenticated = false;
    ChatKit.readMessageMap = {};
    ChatKit.readMessageSequenceMap = {};
    ChatKit.isSendingReadReceipt = false;
    ChatKit.timerHandle = null;
    ChatKit.exchangeTokenPromise = null;
    ChatKit.loginId = 0;
    ChatKit.addListeners = function (listeners) {
        if (ChatKit._initialized) {
            var onSocketConnect = listeners.onSocketConnect, onSocketDisconnect = listeners.onSocketDisconnect, onSocketError = listeners.onSocketError, onSocketReconnect = listeners.onSocketReconnect, onAuthDetailChange = listeners.onAuthDetailChange, onEvent = listeners.onEvent, onSessionExpired = listeners.onSessionExpired;
            ChatKit._subscribed = true;
            ChatKit.authDetailChangeListener = onAuthDetailChange;
            ChatKit.eventListener = onEvent;
            ChatKit.sessionExpiredListener = onSessionExpired;
            php_sayhey_publisher_kit_client_1.default.onMessage(ChatKit.onMessage);
            onSocketConnect && php_sayhey_publisher_kit_client_1.default.onConnect(onSocketConnect);
            onSocketDisconnect && php_sayhey_publisher_kit_client_1.default.onDisconnect(onSocketDisconnect);
            onSocketError && php_sayhey_publisher_kit_client_1.default.onError(onSocketError);
            onSocketReconnect && php_sayhey_publisher_kit_client_1.default.onReconnect(onSocketReconnect);
            return neverthrow_1.ok(true);
        }
        return neverthrow_1.err(new types_1.SayHey.Error('ChatKit has not been initialized!', types_1.SayHey.ErrorType.InternalErrorType.Uninitialized));
    };
    return ChatKit;
}());
exports.default = ChatKit;
//# sourceMappingURL=sayhey-chat-kit-client.js.map