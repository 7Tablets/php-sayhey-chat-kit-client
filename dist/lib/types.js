"use strict";
/**
 * Http Types
 */
var _a, _b, _c;
Object.defineProperty(exports, "__esModule", { value: true });
var HttpMethod;
(function (HttpMethod) {
    HttpMethod["Post"] = "post";
    HttpMethod["Get"] = "get";
    HttpMethod["Delete"] = "delete";
    HttpMethod["Patch"] = "patch";
    HttpMethod["Put"] = "put";
})(HttpMethod = exports.HttpMethod || (exports.HttpMethod = {}));
/**
 * Reaction Types
 */
var ReactionType;
(function (ReactionType) {
    ReactionType["Thumb"] = "Thumb";
    ReactionType["Heart"] = "Heart";
    ReactionType["Fist"] = "Fist";
})(ReactionType = exports.ReactionType || (exports.ReactionType = {}));
var ReactionsObject = /** @class */ (function () {
    function ReactionsObject() {
        this[_a] = 0;
        this[_b] = 0;
        this[_c] = 0;
    }
    return ReactionsObject;
}());
exports.ReactionsObject = ReactionsObject;
_a = ReactionType.Fist, _b = ReactionType.Heart, _c = ReactionType.Thumb;
/**
 * Message Types
 */
var ImageSizes;
(function (ImageSizes) {
    ImageSizes["Tiny"] = "tiny";
    ImageSizes["Small"] = "small";
    ImageSizes["Medium"] = "medium";
    ImageSizes["Large"] = "large";
    ImageSizes["XLarge"] = "xlarge";
})(ImageSizes = exports.ImageSizes || (exports.ImageSizes = {}));
var FileType;
(function (FileType) {
    FileType["Video"] = "video";
    FileType["Image"] = "image";
    FileType["Audio"] = "audio";
    FileType["Other"] = "other";
})(FileType = exports.FileType || (exports.FileType = {}));
var MessageType;
(function (MessageType) {
    MessageType["System"] = "system";
    MessageType["Text"] = "text";
    MessageType["Image"] = "image";
    MessageType["Video"] = "video";
    MessageType["Document"] = "document";
    MessageType["Gif"] = "gif";
    MessageType["Emoji"] = "emoji";
    MessageType["Audio"] = "audio";
})(MessageType = exports.MessageType || (exports.MessageType = {}));
var HighLightType;
(function (HighLightType) {
    HighLightType["Text"] = "text";
    HighLightType["Email"] = "email";
    HighLightType["Phone"] = "phone";
    HighLightType["Url"] = "url";
    HighLightType["UserId"] = "userId";
})(HighLightType = exports.HighLightType || (exports.HighLightType = {}));
var SystemMessageType;
(function (SystemMessageType) {
    SystemMessageType["MemberRemoved"] = "MemberRemoved";
    SystemMessageType["MemberAdded"] = "MemberAdded";
    SystemMessageType["OwnerRemoved"] = "OwnerRemoved";
    SystemMessageType["OwnerAdded"] = "OwnerAdded";
    SystemMessageType["GroupPictureChanged"] = "GroupPictureChanged";
    SystemMessageType["GroupNameChanged"] = "GroupNameChanged";
    SystemMessageType["GroupDisabled"] = "GroupDisabled";
    SystemMessageType["GroupCreated"] = "GroupCreated";
    SystemMessageType["IndividualChatCreated"] = "IndividualChatCreated";
})(SystemMessageType = exports.SystemMessageType || (exports.SystemMessageType = {}));
/**
 * Conversation Types
 */
var ConversationType;
(function (ConversationType) {
    ConversationType["Group"] = "group";
    ConversationType["Individual"] = "individual";
})(ConversationType = exports.ConversationType || (exports.ConversationType = {}));
var ConversationUserQueryType;
(function (ConversationUserQueryType) {
    ConversationUserQueryType["All"] = "all";
    ConversationUserQueryType["Current"] = "current";
    ConversationUserQueryType["Removed"] = "removed";
})(ConversationUserQueryType = exports.ConversationUserQueryType || (exports.ConversationUserQueryType = {}));
var Publisher;
(function (Publisher) {
    var EventType;
    (function (EventType) {
        EventType["NewMessage"] = "NEW_MESSAGE";
        EventType["ConversationCreated"] = "CONVERSATION_CREATED";
        EventType["ConversationMuted"] = "CONVERSATION_MUTED";
        EventType["ConversationUnmuted"] = "CONVERSATION_UNMUTED";
        EventType["ConversationPinned"] = "CONVERSATION_PINNED";
        EventType["ConversationUnpinned"] = "CONVERSATION_UNPINNED";
        EventType["ConversationHideAndClear"] = "CONVERSATION_HIDE_AND_CLEAR";
        EventType["ConversationHideAndClearAll"] = "CONVERSATION_HIDE_AND_CLEAR_ALL";
        EventType["ConversationGroupInfoChanged"] = "CONVERSATION_GROUP_INFO_CHANGED";
        EventType["UserInfoChanged"] = "USER_INFO_CHANGED";
        EventType["ConversationGroupOwnerAdded"] = "CONVERSATION_GROUP_OWNER_ADDED";
        EventType["ConversationGroupOwnerRemoved"] = "CONVERSATION_GROUP_OWNER_REMOVED";
        EventType["ConversationGroupMemberAdded"] = "CONVERSATION_GROUP_MEMBER_ADDED";
        EventType["ConversationGroupMemberRemoved"] = "CONVERSATION_GROUP_MEMBER_REMOVED";
        EventType["UserReactedToMessage"] = "USER_REACTED_TO_MESSAGE";
        EventType["UserUnReactedToMessage"] = "USER_UNREACTED_TO_MESSAGE";
        EventType["MessageReactionsUpdate"] = "MESSAGE_REACTIONS_UPDATE";
        EventType["UserDisabled"] = "USER_DISABLED";
        EventType["UserEnabled"] = "USER_ENABLED";
        EventType["MessageDeleted"] = "MESSAGE_DELETED";
        EventType["UserDeleted"] = "USER_DELETED";
    })(EventType = Publisher.EventType || (Publisher.EventType = {}));
})(Publisher = exports.Publisher || (exports.Publisher = {}));
var SayHey;
(function (SayHey) {
    var ChatKitError;
    (function (ChatKitError) {
        var ErrorType;
        (function (ErrorType) {
            var InternalErrorType;
            (function (InternalErrorType) {
                InternalErrorType["Unknown"] = "InternalUnknown";
                InternalErrorType["Uninitialized"] = "InternalUninitialized";
                InternalErrorType["AuthMissing"] = "InternalAuthMissing";
                InternalErrorType["BadData"] = "InternalBadData";
                InternalErrorType["MissingParameter"] = "InternalMissingParameter";
            })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
            var AuthErrorType;
            (function (AuthErrorType) {
                AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
                AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
                AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
                AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
                AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
                AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
                AuthErrorType["AppHasNoNotificationSetup"] = "AuthErrorTypeAppHasNoNotificationSetup";
                AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
                AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
                AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
                AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
                AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
                AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
                AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
                AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
                AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
                AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
                AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
                AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
                AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
                AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
                AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
                AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
                AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
                AuthErrorType["UserDisabled"] = "AuthErrorTypeUserDisabled";
            })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
            var FileErrorType;
            (function (FileErrorType) {
                FileErrorType["SizeLimitExceeded"] = "FileErrorTypeSizeLimitExceeded";
                FileErrorType["NoFileSentType"] = "FileErrorTypeNoFileSentType";
                FileErrorType["InvalidFieldNameForFile"] = "FileErrorTypeInvalidFieldNameForFile";
                FileErrorType["InvalidMetadata"] = "FileErrorTypeInvalidMetadata";
                FileErrorType["FileSizeLimitExceeded"] = "FileErrorTypeFileSizeLimitExceeded";
                FileErrorType["InvalidFileName"] = "FileErrorTypeInvalidFileName";
                FileErrorType["NotSent"] = "FileErrorTypeNotSent";
                FileErrorType["NotFound"] = "FileErrorTypeNotFound";
                FileErrorType["NoThumbnailFound"] = "FileErrorTypeNoThumbnailFound";
                FileErrorType["ImageNotForConversation"] = "FileErrorTypeImageNotForConversation";
                FileErrorType["ImageNotForUser"] = "FileErrorTypeImageNotForUser";
                FileErrorType["FileNotForMessage"] = "FileErrorTypeFileNotForMessage";
                FileErrorType["SizeNotFound"] = "FileErrorTypeSizeNotFound";
                FileErrorType["NotForApp"] = "FileErrorTypeNotForApp";
                FileErrorType["IncorrectFileType"] = "FileErrorTypeIncorrectFileType";
            })(FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {}));
            var ServerErrorType;
            (function (ServerErrorType) {
                ServerErrorType["UnknownError"] = "UnknownError";
                ServerErrorType["InternalServerError"] = "InternalServerError";
            })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
            var GeneralErrorType;
            (function (GeneralErrorType) {
                GeneralErrorType["MissingQueryParams"] = "GeneralErrorTypeMissingQueryParams";
                GeneralErrorType["RouteNotFound"] = "GeneralErrorTypeRouteNotFound";
                GeneralErrorType["LimitNotValid"] = "GeneralErrorTypeLimitNotValid";
                GeneralErrorType["OffsetNotValid"] = "GeneralErrorTypeOffsetNotValid";
            })(GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {}));
            var MessageErrorType;
            (function (MessageErrorType) {
                MessageErrorType["InvalidDate"] = "MessageErrorTypeInvalidDate";
                MessageErrorType["NotFound"] = "MessageErrorTypeNotFound";
                MessageErrorType["NotFoundForUser"] = "MessageErrorTypeNotFoundForUser";
                MessageErrorType["MessageAlreadyRead"] = "MessageErrorTypeMessageAlreadyRead";
                MessageErrorType["CannotDeleteSystemMessage"] = "MessageErrorTypeCannotDeleteSystemMessage";
            })(MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {}));
            var DateErrorType;
            (function (DateErrorType) {
                DateErrorType["InvalidFromDateError"] = "DateErrorTypeInvalidFromDateError";
                DateErrorType["InvalidToDateError"] = "DateErrorTypeInvalidToDateError";
            })(DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {}));
            var UserErrorType;
            (function (UserErrorType) {
                UserErrorType["AlreadyMuted"] = "UserErrorTypeAlreadyMuted";
                UserErrorType["NotOnMute"] = "UserErrorTypeNotOnMute";
                UserErrorType["AlreadyEnabled"] = "UserErrorTypeAlreadyEnabled";
                UserErrorType["AlreadyDisabled"] = "UserErrorTypeAlreadyDisabled";
            })(UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {}));
            var ConversationErrorType;
            (function (ConversationErrorType) {
                ConversationErrorType["NotFound"] = "ConversationErrorTypeNotFound";
                ConversationErrorType["CannotConverseWithUser"] = "ConversationErrorTypeCannotConverseWithUser";
                ConversationErrorType["NotAnOwner"] = "ConversationErrorTypeNotAnOwner";
                ConversationErrorType["AlreadyAnOwner"] = "ConversationErrorTypeAlreadyAnOwner";
                ConversationErrorType["NotFoundForApp"] = "ConversationErrorTypeNotFoundForApp";
                ConversationErrorType["InvalidType"] = "ConversationErrorTypeInvalidType";
                ConversationErrorType["InvalidConversationUserType"] = "ConversationErrorTypeInvalidConversationUserType";
                ConversationErrorType["MustBeOwner"] = "ConversationErrorTypeMustBeOwner";
                ConversationErrorType["NotPartOfApp"] = "ConversationErrorTypeNotPartOfApp";
                ConversationErrorType["NotFoundForUser"] = "ConversationErrorTypeNotFoundForUser";
                ConversationErrorType["AlreadyExists"] = "ConversationErrorTypeAlreadyExists";
                ConversationErrorType["CannotChatWithSelf"] = "ConversationErrorTypeCannotChatWithSelf";
                ConversationErrorType["AlreadyMuted"] = "ConversationErrorTypeAlreadyMuted";
                ConversationErrorType["NotOnMute"] = "ConversationErrorTypeNotOnMute";
                ConversationErrorType["AlreadyPinned"] = "ConversationErrorTypeAlreadyPinned";
                ConversationErrorType["NotPinned"] = "ConversationErrorTypeNotPinned";
                ConversationErrorType["UsersNotInApp"] = "ConversationErrorTypeUsersNotInApp";
                ConversationErrorType["NoValidUsersToAdd"] = "ConversationErrorTypeNoValidUsersToAdd";
                ConversationErrorType["NoValidUsersToRemove"] = "ConversationErrorTypeNoValidUsersToRemove";
                ConversationErrorType["CannotChangeAccessToSelf"] = "ConversationErrorTypeCannotChangeAccessToSelf";
                ConversationErrorType["CannotAddExistingUsers"] = "ConversationErrorTypeCannotAddExistingUsers";
                ConversationErrorType["AlreadyRemovedUsers"] = "ConversationErrorTypeAlreadyRemovedUsers";
                ConversationErrorType["UsersNotInConversation"] = "ConversationErrorTypeUsersNotInConversation";
                ConversationErrorType["CannotRemoveOwners"] = "ConversationErrorTypeCannotRemoveOwners";
            })(ConversationErrorType = ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}));
        })(ErrorType = ChatKitError.ErrorType || (ChatKitError.ErrorType = {}));
        var PushNotificationError;
        (function (PushNotificationError) {
            PushNotificationError["InvalidPushParams"] = "DeviceTokenErrorTypeInvalidPushParams";
            PushNotificationError["AlreadyRegisteredForUser"] = "DeviceTokenErrorTypeAlreadyRegisteredForUser";
            PushNotificationError["NotFound"] = "DeviceTokenErrorTypeNotFound";
            PushNotificationError["NoTokensToPush"] = "DeviceTokenErrorTypeNoTokensToPush";
            PushNotificationError["OnlyStringData"] = "DeviceTokenErrorTypeOnlyStringData";
        })(PushNotificationError = ChatKitError.PushNotificationError || (ChatKitError.PushNotificationError = {}));
        var AppError = /** @class */ (function () {
            function AppError(err, type) {
                var _this = this;
                this.type = ErrorType.InternalErrorType.Unknown;
                this.setStatusCode = function (statusCode) {
                    _this.statusCode = statusCode;
                };
                this.setType = function (type) {
                    _this.type = type;
                };
                this.setMessage = function (msg) {
                    _this.message = msg;
                };
                this.setErrorActions = function (params) {
                    var authFailed = params.authFailed, exchange = params.exchange;
                    _this.exchange = exchange;
                    _this.authFailed = authFailed;
                };
                if (type) {
                    this.type = type;
                }
                if (err instanceof SayHey.Error) {
                    this.error = err;
                    this.message = err.message;
                }
                else {
                    this.message = err;
                }
            }
            return AppError;
        }());
        ChatKitError.AppError = AppError;
    })(ChatKitError = SayHey.ChatKitError || (SayHey.ChatKitError = {}));
    SayHey.EventType = Publisher.EventType;
    SayHey.ErrorType = ChatKitError.ErrorType;
    SayHey.Error = ChatKitError.AppError;
})(SayHey = exports.SayHey || (exports.SayHey = {}));
//# sourceMappingURL=types.js.map