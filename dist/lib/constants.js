"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var types_1 = require("./types");
exports.API_VERSION = 'v1';
exports.ApiKeyName = 'x-sayhey-client-api-key';
exports.MESSAGE_READ_UPDATE_INTERVAL = 5000;
exports.Routes = {
    SignIn: {
        Endpoint: '/users/sign-in',
        Method: types_1.HttpMethod.Post
    },
    SignUp: {
        Endpoint: '/users/sign-up',
        Method: types_1.HttpMethod.Post
    },
    ChangePassword: {
        Endpoint: '/users/change-password',
        Method: types_1.HttpMethod.Post
    },
    GetReachableUsers: {
        Endpoint: '/users/conversable',
        Method: types_1.HttpMethod.Get
    },
    GetExistingConversationWithUser: {
        Endpoint: function (userId) { return "/users/" + userId + "/conversations/individual"; },
        Method: types_1.HttpMethod.Get
    },
    GetConversations: {
        Endpoint: '/conversations',
        Method: types_1.HttpMethod.Get
    },
    GetConversationDetails: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId; },
        Method: types_1.HttpMethod.Get
    },
    CreateIndividualConversation: {
        Endpoint: '/conversations/individual',
        Method: types_1.HttpMethod.Post
    },
    CreateGroupConversation: {
        Endpoint: '/conversations/group',
        Method: types_1.HttpMethod.Post
    },
    GetMessages: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: types_1.HttpMethod.Get
    },
    SendMessage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
        Method: types_1.HttpMethod.Post
    },
    MuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: types_1.HttpMethod.Put
    },
    UnmuteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
        Method: types_1.HttpMethod.Delete
    },
    PinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: types_1.HttpMethod.Put
    },
    UnpinConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
        Method: types_1.HttpMethod.Delete
    },
    DeleteConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/hide-clear"; },
        Method: types_1.HttpMethod.Put
    },
    DeleteAllConversations: {
        Endpoint: "/conversations/hide-clear",
        Method: types_1.HttpMethod.Put
    },
    UploadFile: {
        Endpoint: '/files',
        Method: types_1.HttpMethod.Post
    },
    UpdateGroupImage: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
        Method: types_1.HttpMethod.Put
    },
    UpdateGroupInfo: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-info"; },
        Method: types_1.HttpMethod.Put
    },
    UpdateUserProfileImage: {
        Endpoint: '/users/profile-image',
        Method: types_1.HttpMethod.Put
    },
    UpdateUserProfileInfo: {
        Endpoint: '/users/profile-info',
        Method: types_1.HttpMethod.Put
    },
    GetConversationUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Get
    },
    GetConversationReachableUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/conversable-users"; },
        Method: types_1.HttpMethod.Get
    },
    AddUsersToConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
        Method: types_1.HttpMethod.Post
    },
    RemoveUsersFromConversation: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-users"; },
        Method: types_1.HttpMethod.Post
    },
    MakeConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Put
    },
    RemoveConversationOwner: {
        Endpoint: function (conversationId, userId) {
            return "/conversations/" + conversationId + "/owners/" + userId;
        },
        Method: types_1.HttpMethod.Delete
    },
    GetUserConversationReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/reactions"; },
        Method: types_1.HttpMethod.Get
    },
    GetUserConversationMessagesReactions: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/get-messages-reactions"; },
        Method: types_1.HttpMethod.Post
    },
    SetMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/add"; },
        Method: types_1.HttpMethod.Put
    },
    RemoveMessageReaction: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/remove"; },
        Method: types_1.HttpMethod.Put
    },
    MuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: types_1.HttpMethod.Put
    },
    UnmuteAllNotifications: {
        Endpoint: '/users/mute-all',
        Method: types_1.HttpMethod.Delete
    },
    GetUserProfile: {
        Endpoint: '/users/profile-info',
        Method: types_1.HttpMethod.Get
    },
    GetGroupDefaultImageSet: {
        Endpoint: '/default-group-images',
        Method: types_1.HttpMethod.Get
    },
    MarkMessageAsRead: {
        Endpoint: function (messageId) { return "/read-messages/" + messageId; },
        Method: types_1.HttpMethod.Put
    },
    GetConversationBasicUsers: {
        Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/basic-users"; },
        Method: types_1.HttpMethod.Get
    },
    RegisterPushNotification: {
        Endpoint: '/device-tokens',
        Method: types_1.HttpMethod.Post
    },
    UnregisterPushNotification: {
        Endpoint: function (instanceId) { return "/device-tokens/" + instanceId; },
        Method: types_1.HttpMethod.Delete
    },
    GetReactedUsers: {
        Endpoint: function (messageId) { return "/messages/" + messageId + "/reacted-users"; },
        Method: types_1.HttpMethod.Get
    }
};
//# sourceMappingURL=constants.js.map