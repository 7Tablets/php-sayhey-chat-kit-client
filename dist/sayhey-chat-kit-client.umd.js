(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('axios'), require('neverthrow'), require('stringz'), require('jwt-decode'), require('php-sayhey-publisher-kit-client')) :
    typeof define === 'function' && define.amd ? define(['exports', 'axios', 'neverthrow', 'stringz', 'jwt-decode', 'php-sayhey-publisher-kit-client'], factory) :
    (factory((global.ChatKitClient = {}),global.axios,global.neverthrow,global.stringz,global.decode,global.PublisherKitClient));
}(this, (function (exports,axios,neverthrow,stringz,decode,PublisherKitClient) { 'use strict';

    axios = axios && axios.hasOwnProperty('default') ? axios['default'] : axios;
    decode = decode && decode.hasOwnProperty('default') ? decode['default'] : decode;
    PublisherKitClient = PublisherKitClient && PublisherKitClient.hasOwnProperty('default') ? PublisherKitClient['default'] : PublisherKitClient;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    }

    /**
     * Http Types
     */
    var _a, _b, _c;
    (function (HttpMethod) {
        HttpMethod["Post"] = "post";
        HttpMethod["Get"] = "get";
        HttpMethod["Delete"] = "delete";
        HttpMethod["Patch"] = "patch";
        HttpMethod["Put"] = "put";
    })(exports.HttpMethod || (exports.HttpMethod = {}));
    (function (ReactionType) {
        ReactionType["Thumb"] = "Thumb";
        ReactionType["Heart"] = "Heart";
        ReactionType["Fist"] = "Fist";
    })(exports.ReactionType || (exports.ReactionType = {}));
    var ReactionsObject = /** @class */ (function () {
        function ReactionsObject() {
            this[_a] = 0;
            this[_b] = 0;
            this[_c] = 0;
        }
        return ReactionsObject;
    }());
    _a = exports.ReactionType.Fist, _b = exports.ReactionType.Heart, _c = exports.ReactionType.Thumb;
    (function (ImageSizes) {
        ImageSizes["Tiny"] = "tiny";
        ImageSizes["Small"] = "small";
        ImageSizes["Medium"] = "medium";
        ImageSizes["Large"] = "large";
        ImageSizes["XLarge"] = "xlarge";
    })(exports.ImageSizes || (exports.ImageSizes = {}));
    (function (FileType) {
        FileType["Video"] = "video";
        FileType["Image"] = "image";
        FileType["Audio"] = "audio";
        FileType["Other"] = "other";
    })(exports.FileType || (exports.FileType = {}));
    (function (MessageType) {
        MessageType["System"] = "system";
        MessageType["Text"] = "text";
        MessageType["Image"] = "image";
        MessageType["Video"] = "video";
        MessageType["Document"] = "document";
        MessageType["Gif"] = "gif";
        MessageType["Emoji"] = "emoji";
        MessageType["Audio"] = "audio";
    })(exports.MessageType || (exports.MessageType = {}));
    (function (HighLightType) {
        HighLightType["Text"] = "text";
        HighLightType["Email"] = "email";
        HighLightType["Phone"] = "phone";
        HighLightType["Url"] = "url";
        HighLightType["UserId"] = "userId";
    })(exports.HighLightType || (exports.HighLightType = {}));
    (function (SystemMessageType) {
        SystemMessageType["MemberRemoved"] = "MemberRemoved";
        SystemMessageType["MemberAdded"] = "MemberAdded";
        SystemMessageType["OwnerRemoved"] = "OwnerRemoved";
        SystemMessageType["OwnerAdded"] = "OwnerAdded";
        SystemMessageType["GroupPictureChanged"] = "GroupPictureChanged";
        SystemMessageType["GroupNameChanged"] = "GroupNameChanged";
        SystemMessageType["GroupDisabled"] = "GroupDisabled";
        SystemMessageType["GroupCreated"] = "GroupCreated";
        SystemMessageType["IndividualChatCreated"] = "IndividualChatCreated";
    })(exports.SystemMessageType || (exports.SystemMessageType = {}));
    (function (ConversationType) {
        ConversationType["Group"] = "group";
        ConversationType["Individual"] = "individual";
    })(exports.ConversationType || (exports.ConversationType = {}));
    (function (ConversationUserQueryType) {
        ConversationUserQueryType["All"] = "all";
        ConversationUserQueryType["Current"] = "current";
        ConversationUserQueryType["Removed"] = "removed";
    })(exports.ConversationUserQueryType || (exports.ConversationUserQueryType = {}));
    (function (Publisher) {
        var EventType;
        (function (EventType) {
            EventType["NewMessage"] = "NEW_MESSAGE";
            EventType["ConversationCreated"] = "CONVERSATION_CREATED";
            EventType["ConversationMuted"] = "CONVERSATION_MUTED";
            EventType["ConversationUnmuted"] = "CONVERSATION_UNMUTED";
            EventType["ConversationPinned"] = "CONVERSATION_PINNED";
            EventType["ConversationUnpinned"] = "CONVERSATION_UNPINNED";
            EventType["ConversationHideAndClear"] = "CONVERSATION_HIDE_AND_CLEAR";
            EventType["ConversationHideAndClearAll"] = "CONVERSATION_HIDE_AND_CLEAR_ALL";
            EventType["ConversationGroupInfoChanged"] = "CONVERSATION_GROUP_INFO_CHANGED";
            EventType["UserInfoChanged"] = "USER_INFO_CHANGED";
            EventType["ConversationGroupOwnerAdded"] = "CONVERSATION_GROUP_OWNER_ADDED";
            EventType["ConversationGroupOwnerRemoved"] = "CONVERSATION_GROUP_OWNER_REMOVED";
            EventType["ConversationGroupMemberAdded"] = "CONVERSATION_GROUP_MEMBER_ADDED";
            EventType["ConversationGroupMemberRemoved"] = "CONVERSATION_GROUP_MEMBER_REMOVED";
            EventType["UserReactedToMessage"] = "USER_REACTED_TO_MESSAGE";
            EventType["UserUnReactedToMessage"] = "USER_UNREACTED_TO_MESSAGE";
            EventType["MessageReactionsUpdate"] = "MESSAGE_REACTIONS_UPDATE";
            EventType["UserDisabled"] = "USER_DISABLED";
            EventType["UserEnabled"] = "USER_ENABLED";
            EventType["MessageDeleted"] = "MESSAGE_DELETED";
            EventType["UserDeleted"] = "USER_DELETED";
        })(EventType = Publisher.EventType || (Publisher.EventType = {}));
    })(exports.Publisher || (exports.Publisher = {}));
    (function (SayHey) {
        var ChatKitError;
        (function (ChatKitError) {
            var ErrorType;
            (function (ErrorType) {
                var InternalErrorType;
                (function (InternalErrorType) {
                    InternalErrorType["Unknown"] = "InternalUnknown";
                    InternalErrorType["Uninitialized"] = "InternalUninitialized";
                    InternalErrorType["AuthMissing"] = "InternalAuthMissing";
                    InternalErrorType["BadData"] = "InternalBadData";
                    InternalErrorType["MissingParameter"] = "InternalMissingParameter";
                })(InternalErrorType = ErrorType.InternalErrorType || (ErrorType.InternalErrorType = {}));
                var AuthErrorType;
                (function (AuthErrorType) {
                    AuthErrorType["InvalidKeyErrorType"] = "AuthErrorTypeInvalidKey";
                    AuthErrorType["InvalidUserType"] = "AuthErrorTypeInvalidUserType";
                    AuthErrorType["NotFoundErrorType"] = "AuthErrorTypeNotFound";
                    AuthErrorType["MissingParamsErrorType"] = "AuthErrorTypeMissingParams";
                    AuthErrorType["InvalidPassword"] = "AuthErrorTypeInvalidPassword";
                    AuthErrorType["TokenExpiredType"] = "AuthErrorTypeTokenExpired";
                    AuthErrorType["AppHasNoNotificationSetup"] = "AuthErrorTypeAppHasNoNotificationSetup";
                    AuthErrorType["NotPartOfApp"] = "AuthErrorTypeNotPartOfApp";
                    AuthErrorType["InvalidWebhookKey"] = "AuthErrorTypeInvalidWebhookKey";
                    AuthErrorType["NoAccessToUser"] = "AuthErrorTypeNoAccessToUser";
                    AuthErrorType["ServerNoAccessToUser"] = "AuthErrorTypeServerNoAccessToUser";
                    AuthErrorType["IncorrectPassword"] = "AuthErrorTypeIncorrectPassword";
                    AuthErrorType["NoSamePassword"] = "AuthErrorTypeNoSamePassword";
                    AuthErrorType["NoAppAccessToUser"] = "AuthErrorTypeNoAppAccessToUser";
                    AuthErrorType["TokenNotStarted"] = "AuthErrorTypeTokenNotStarted";
                    AuthErrorType["AccountAlreadyExists"] = "AuthErrorTypeAccountAlreadyExists";
                    AuthErrorType["UserNotFound"] = "AuthErrorTypeUserNotFound";
                    AuthErrorType["InvalidToken"] = "AuthErrorTypeInvalidToken";
                    AuthErrorType["MissingAuthHeaderField"] = "AuthErrorTypeMissingAuthHeaderField";
                    AuthErrorType["InvalidCredentials"] = "AuthErrorTypeInvalidCredentials";
                    AuthErrorType["HashGeneration"] = "AuthErrorTypeHashGeneration";
                    AuthErrorType["HashComparison"] = "AuthErrorTypeHashComparison";
                    AuthErrorType["RefreshTokenAlreadyExchanged"] = "AuthErrorTypeRefreshTokenAlreadyExchanged";
                    AuthErrorType["RefreshTokenNotFound"] = "AuthErrorTypeRefreshTokenNotFound";
                    AuthErrorType["UserDisabled"] = "AuthErrorTypeUserDisabled";
                })(AuthErrorType = ErrorType.AuthErrorType || (ErrorType.AuthErrorType = {}));
                var FileErrorType;
                (function (FileErrorType) {
                    FileErrorType["SizeLimitExceeded"] = "FileErrorTypeSizeLimitExceeded";
                    FileErrorType["NoFileSentType"] = "FileErrorTypeNoFileSentType";
                    FileErrorType["InvalidFieldNameForFile"] = "FileErrorTypeInvalidFieldNameForFile";
                    FileErrorType["InvalidMetadata"] = "FileErrorTypeInvalidMetadata";
                    FileErrorType["FileSizeLimitExceeded"] = "FileErrorTypeFileSizeLimitExceeded";
                    FileErrorType["InvalidFileName"] = "FileErrorTypeInvalidFileName";
                    FileErrorType["NotSent"] = "FileErrorTypeNotSent";
                    FileErrorType["NotFound"] = "FileErrorTypeNotFound";
                    FileErrorType["NoThumbnailFound"] = "FileErrorTypeNoThumbnailFound";
                    FileErrorType["ImageNotForConversation"] = "FileErrorTypeImageNotForConversation";
                    FileErrorType["ImageNotForUser"] = "FileErrorTypeImageNotForUser";
                    FileErrorType["FileNotForMessage"] = "FileErrorTypeFileNotForMessage";
                    FileErrorType["SizeNotFound"] = "FileErrorTypeSizeNotFound";
                    FileErrorType["NotForApp"] = "FileErrorTypeNotForApp";
                    FileErrorType["IncorrectFileType"] = "FileErrorTypeIncorrectFileType";
                })(FileErrorType = ErrorType.FileErrorType || (ErrorType.FileErrorType = {}));
                var ServerErrorType;
                (function (ServerErrorType) {
                    ServerErrorType["UnknownError"] = "UnknownError";
                    ServerErrorType["InternalServerError"] = "InternalServerError";
                })(ServerErrorType = ErrorType.ServerErrorType || (ErrorType.ServerErrorType = {}));
                var GeneralErrorType;
                (function (GeneralErrorType) {
                    GeneralErrorType["MissingQueryParams"] = "GeneralErrorTypeMissingQueryParams";
                    GeneralErrorType["RouteNotFound"] = "GeneralErrorTypeRouteNotFound";
                    GeneralErrorType["LimitNotValid"] = "GeneralErrorTypeLimitNotValid";
                    GeneralErrorType["OffsetNotValid"] = "GeneralErrorTypeOffsetNotValid";
                })(GeneralErrorType = ErrorType.GeneralErrorType || (ErrorType.GeneralErrorType = {}));
                var MessageErrorType;
                (function (MessageErrorType) {
                    MessageErrorType["InvalidDate"] = "MessageErrorTypeInvalidDate";
                    MessageErrorType["NotFound"] = "MessageErrorTypeNotFound";
                    MessageErrorType["NotFoundForUser"] = "MessageErrorTypeNotFoundForUser";
                    MessageErrorType["MessageAlreadyRead"] = "MessageErrorTypeMessageAlreadyRead";
                    MessageErrorType["CannotDeleteSystemMessage"] = "MessageErrorTypeCannotDeleteSystemMessage";
                })(MessageErrorType = ErrorType.MessageErrorType || (ErrorType.MessageErrorType = {}));
                var DateErrorType;
                (function (DateErrorType) {
                    DateErrorType["InvalidFromDateError"] = "DateErrorTypeInvalidFromDateError";
                    DateErrorType["InvalidToDateError"] = "DateErrorTypeInvalidToDateError";
                })(DateErrorType = ErrorType.DateErrorType || (ErrorType.DateErrorType = {}));
                var UserErrorType;
                (function (UserErrorType) {
                    UserErrorType["AlreadyMuted"] = "UserErrorTypeAlreadyMuted";
                    UserErrorType["NotOnMute"] = "UserErrorTypeNotOnMute";
                    UserErrorType["AlreadyEnabled"] = "UserErrorTypeAlreadyEnabled";
                    UserErrorType["AlreadyDisabled"] = "UserErrorTypeAlreadyDisabled";
                })(UserErrorType = ErrorType.UserErrorType || (ErrorType.UserErrorType = {}));
                var ConversationErrorType;
                (function (ConversationErrorType) {
                    ConversationErrorType["NotFound"] = "ConversationErrorTypeNotFound";
                    ConversationErrorType["CannotConverseWithUser"] = "ConversationErrorTypeCannotConverseWithUser";
                    ConversationErrorType["NotAnOwner"] = "ConversationErrorTypeNotAnOwner";
                    ConversationErrorType["AlreadyAnOwner"] = "ConversationErrorTypeAlreadyAnOwner";
                    ConversationErrorType["NotFoundForApp"] = "ConversationErrorTypeNotFoundForApp";
                    ConversationErrorType["InvalidType"] = "ConversationErrorTypeInvalidType";
                    ConversationErrorType["InvalidConversationUserType"] = "ConversationErrorTypeInvalidConversationUserType";
                    ConversationErrorType["MustBeOwner"] = "ConversationErrorTypeMustBeOwner";
                    ConversationErrorType["NotPartOfApp"] = "ConversationErrorTypeNotPartOfApp";
                    ConversationErrorType["NotFoundForUser"] = "ConversationErrorTypeNotFoundForUser";
                    ConversationErrorType["AlreadyExists"] = "ConversationErrorTypeAlreadyExists";
                    ConversationErrorType["CannotChatWithSelf"] = "ConversationErrorTypeCannotChatWithSelf";
                    ConversationErrorType["AlreadyMuted"] = "ConversationErrorTypeAlreadyMuted";
                    ConversationErrorType["NotOnMute"] = "ConversationErrorTypeNotOnMute";
                    ConversationErrorType["AlreadyPinned"] = "ConversationErrorTypeAlreadyPinned";
                    ConversationErrorType["NotPinned"] = "ConversationErrorTypeNotPinned";
                    ConversationErrorType["UsersNotInApp"] = "ConversationErrorTypeUsersNotInApp";
                    ConversationErrorType["NoValidUsersToAdd"] = "ConversationErrorTypeNoValidUsersToAdd";
                    ConversationErrorType["NoValidUsersToRemove"] = "ConversationErrorTypeNoValidUsersToRemove";
                    ConversationErrorType["CannotChangeAccessToSelf"] = "ConversationErrorTypeCannotChangeAccessToSelf";
                    ConversationErrorType["CannotAddExistingUsers"] = "ConversationErrorTypeCannotAddExistingUsers";
                    ConversationErrorType["AlreadyRemovedUsers"] = "ConversationErrorTypeAlreadyRemovedUsers";
                    ConversationErrorType["UsersNotInConversation"] = "ConversationErrorTypeUsersNotInConversation";
                    ConversationErrorType["CannotRemoveOwners"] = "ConversationErrorTypeCannotRemoveOwners";
                })(ConversationErrorType = ErrorType.ConversationErrorType || (ErrorType.ConversationErrorType = {}));
            })(ErrorType = ChatKitError.ErrorType || (ChatKitError.ErrorType = {}));
            var PushNotificationError;
            (function (PushNotificationError) {
                PushNotificationError["InvalidPushParams"] = "DeviceTokenErrorTypeInvalidPushParams";
                PushNotificationError["AlreadyRegisteredForUser"] = "DeviceTokenErrorTypeAlreadyRegisteredForUser";
                PushNotificationError["NotFound"] = "DeviceTokenErrorTypeNotFound";
                PushNotificationError["NoTokensToPush"] = "DeviceTokenErrorTypeNoTokensToPush";
                PushNotificationError["OnlyStringData"] = "DeviceTokenErrorTypeOnlyStringData";
            })(PushNotificationError = ChatKitError.PushNotificationError || (ChatKitError.PushNotificationError = {}));
            var AppError = /** @class */ (function () {
                function AppError(err, type) {
                    var _this = this;
                    this.type = ErrorType.InternalErrorType.Unknown;
                    this.setStatusCode = function (statusCode) {
                        _this.statusCode = statusCode;
                    };
                    this.setType = function (type) {
                        _this.type = type;
                    };
                    this.setMessage = function (msg) {
                        _this.message = msg;
                    };
                    this.setErrorActions = function (params) {
                        var authFailed = params.authFailed, exchange = params.exchange;
                        _this.exchange = exchange;
                        _this.authFailed = authFailed;
                    };
                    if (type) {
                        this.type = type;
                    }
                    if (err instanceof SayHey.Error) {
                        this.error = err;
                        this.message = err.message;
                    }
                    else {
                        this.message = err;
                    }
                }
                return AppError;
            }());
            ChatKitError.AppError = AppError;
        })(ChatKitError = SayHey.ChatKitError || (SayHey.ChatKitError = {}));
        SayHey.EventType = exports.Publisher.EventType;
        SayHey.ErrorType = ChatKitError.ErrorType;
        SayHey.Error = ChatKitError.AppError;
    })(exports.SayHey || (exports.SayHey = {}));

    var API_VERSION = 'v1';
    var ApiKeyName = 'x-sayhey-client-api-key';
    var MESSAGE_READ_UPDATE_INTERVAL = 5000;
    var Routes = {
        SignIn: {
            Endpoint: '/users/sign-in',
            Method: exports.HttpMethod.Post
        },
        SignUp: {
            Endpoint: '/users/sign-up',
            Method: exports.HttpMethod.Post
        },
        ChangePassword: {
            Endpoint: '/users/change-password',
            Method: exports.HttpMethod.Post
        },
        GetReachableUsers: {
            Endpoint: '/users/conversable',
            Method: exports.HttpMethod.Get
        },
        GetExistingConversationWithUser: {
            Endpoint: function (userId) { return "/users/" + userId + "/conversations/individual"; },
            Method: exports.HttpMethod.Get
        },
        GetConversations: {
            Endpoint: '/conversations',
            Method: exports.HttpMethod.Get
        },
        GetConversationDetails: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId; },
            Method: exports.HttpMethod.Get
        },
        CreateIndividualConversation: {
            Endpoint: '/conversations/individual',
            Method: exports.HttpMethod.Post
        },
        CreateGroupConversation: {
            Endpoint: '/conversations/group',
            Method: exports.HttpMethod.Post
        },
        GetMessages: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
            Method: exports.HttpMethod.Get
        },
        SendMessage: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/messages"; },
            Method: exports.HttpMethod.Post
        },
        MuteConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
            Method: exports.HttpMethod.Put
        },
        UnmuteConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/mute"; },
            Method: exports.HttpMethod.Delete
        },
        PinConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
            Method: exports.HttpMethod.Put
        },
        UnpinConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/pin"; },
            Method: exports.HttpMethod.Delete
        },
        DeleteConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/hide-clear"; },
            Method: exports.HttpMethod.Put
        },
        DeleteAllConversations: {
            Endpoint: "/conversations/hide-clear",
            Method: exports.HttpMethod.Put
        },
        UploadFile: {
            Endpoint: '/files',
            Method: exports.HttpMethod.Post
        },
        UpdateGroupImage: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-image"; },
            Method: exports.HttpMethod.Put
        },
        UpdateGroupInfo: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/group-info"; },
            Method: exports.HttpMethod.Put
        },
        UpdateUserProfileImage: {
            Endpoint: '/users/profile-image',
            Method: exports.HttpMethod.Put
        },
        UpdateUserProfileInfo: {
            Endpoint: '/users/profile-info',
            Method: exports.HttpMethod.Put
        },
        GetConversationUsers: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
            Method: exports.HttpMethod.Get
        },
        GetConversationReachableUsers: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/conversable-users"; },
            Method: exports.HttpMethod.Get
        },
        AddUsersToConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/users"; },
            Method: exports.HttpMethod.Post
        },
        RemoveUsersFromConversation: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/delete-users"; },
            Method: exports.HttpMethod.Post
        },
        MakeConversationOwner: {
            Endpoint: function (conversationId, userId) {
                return "/conversations/" + conversationId + "/owners/" + userId;
            },
            Method: exports.HttpMethod.Put
        },
        RemoveConversationOwner: {
            Endpoint: function (conversationId, userId) {
                return "/conversations/" + conversationId + "/owners/" + userId;
            },
            Method: exports.HttpMethod.Delete
        },
        GetUserConversationReactions: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/reactions"; },
            Method: exports.HttpMethod.Get
        },
        GetUserConversationMessagesReactions: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/get-messages-reactions"; },
            Method: exports.HttpMethod.Post
        },
        SetMessageReaction: {
            Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/add"; },
            Method: exports.HttpMethod.Put
        },
        RemoveMessageReaction: {
            Endpoint: function (messageId) { return "/messages/" + messageId + "/reactions/remove"; },
            Method: exports.HttpMethod.Put
        },
        MuteAllNotifications: {
            Endpoint: '/users/mute-all',
            Method: exports.HttpMethod.Put
        },
        UnmuteAllNotifications: {
            Endpoint: '/users/mute-all',
            Method: exports.HttpMethod.Delete
        },
        GetUserProfile: {
            Endpoint: '/users/profile-info',
            Method: exports.HttpMethod.Get
        },
        GetGroupDefaultImageSet: {
            Endpoint: '/default-group-images',
            Method: exports.HttpMethod.Get
        },
        MarkMessageAsRead: {
            Endpoint: function (messageId) { return "/read-messages/" + messageId; },
            Method: exports.HttpMethod.Put
        },
        GetConversationBasicUsers: {
            Endpoint: function (conversationId) { return "/conversations/" + conversationId + "/basic-users"; },
            Method: exports.HttpMethod.Get
        },
        RegisterPushNotification: {
            Endpoint: '/device-tokens',
            Method: exports.HttpMethod.Post
        },
        UnregisterPushNotification: {
            Endpoint: function (instanceId) { return "/device-tokens/" + instanceId; },
            Method: exports.HttpMethod.Delete
        },
        GetReactedUsers: {
            Endpoint: function (messageId) { return "/messages/" + messageId + "/reacted-users"; },
            Method: exports.HttpMethod.Get
        }
    };

    function checkApiKey(config) {
        if (!config.headers[ApiKeyName]) {
            throw new exports.SayHey.Error('Missing api key in request header', exports.SayHey.ErrorType.InternalErrorType.AuthMissing);
        }
        return config;
    }
    function checkaccessToken(config) {
        if (!config.headers.Authorization) {
            throw new exports.SayHey.Error('Missing auth token in request header', exports.SayHey.ErrorType.InternalErrorType.AuthMissing);
        }
        return config;
    }
    function requestFailed(error) {
        var response = error.response;
        var err = new exports.SayHey.Error(response === null || response === void 0 ? void 0 : response.data.message, response === null || response === void 0 ? void 0 : response.data.type);
        err.setStatusCode(response === null || response === void 0 ? void 0 : response.status);
        err.setErrorActions((response === null || response === void 0 ? void 0 : response.data.actions) || {});
        throw err;
    }
    var httpClient = {
        createWithoutAuth: function (config) {
            var _a;
            var baseURL = config.baseURL, apiKey = config.apiKey;
            var instance = axios.create({
                baseURL: baseURL,
                headers: (_a = {},
                    _a[ApiKeyName] = apiKey,
                    _a)
            });
            instance.interceptors.request.use(checkApiKey);
            instance.interceptors.response.use(undefined, requestFailed);
            return instance;
        },
        createWithAuth: function (config) {
            var _a;
            var baseURL = config.baseURL, accessToken = config.accessToken, apiKey = config.apiKey;
            var instance = axios.create({
                baseURL: baseURL,
                headers: (_a = {},
                    _a[ApiKeyName] = apiKey,
                    _a.Authorization = "Bearer " + accessToken,
                    _a)
            });
            instance.interceptors.request.use(checkApiKey);
            instance.interceptors.request.use(checkaccessToken);
            instance.interceptors.response.use(undefined, requestFailed);
            return instance;
        }
    };

    var emojiRegex = /\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]/;
    var isSingleEmoji = function (text) { return stringz.length(text) === 1 && text.match(emojiRegex); };
    var textHighlightRegex = /(\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\})|((([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*)|(((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$))|(\S+@\S+\.\S+)/gi;
    var urlRegex = /(([hH][tT]{2}[pP][sS]?:\/\/|www\.)[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b)([-a-zA-Z0-9@:%_\+.~#?&\/=]*[-a-zA-Z0-9@:%_\+~#?&\/=])*/;
    // New phone regex (?:^|\s{1})((\+?1\s?)?((?:\([2-9]{1}\d{2}\))|(?:[2-9]{1}\d{2}))[\s\-]?(\d{3})[\s\-]?(\d{4}))(?:\s|$)
    var phoneRegex = /((^|\s{1}))((\+?1\s?)?((\([2-9]{1}\d{2}\))|([2-9]{1}\d{2}))[\s\-]?\d{3}[\s\-]?\d{4})(\s|$)/;
    var userIdRegex = /\{\{([0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12})\}\}/i;
    var isUrl = function (url) { return !!urlRegex.test(url); };
    var isPhone = function (phone) { return !!phoneRegex.test(phone); };
    var isUser = function (userId) { return !!userIdRegex.test(userId); };
    var parseText = function (text) {
        if (!text) {
            return null;
        }
        var result = [];
        var trimedText = text.trim();
        var url = null;
        trimedText.split('\n').forEach(function (line, index) {
            result.push([]);
            if (line.trim()) {
                var lastIndex = 0;
                var match = void 0;
                while ((match = textHighlightRegex.exec(line))) {
                    if (isUrl(match[0])) {
                        if (match.index > lastIndex) {
                            var currentText = line.substring(lastIndex, match.index);
                            if (currentText.length > 1) {
                                result[index].push({ type: exports.HighLightType.Text, value: currentText });
                            }
                        }
                        if (!url) {
                            url = match[0].replace(match[4], match[4].toLowerCase());
                        }
                        result[index].push({ type: exports.HighLightType.Url, value: match[0] });
                        lastIndex = textHighlightRegex.lastIndex;
                    }
                    else if (isUser(match[0])) {
                        if (match.index > lastIndex) {
                            var currentText = line.substring(lastIndex, match.index);
                            if (currentText.length > 1) {
                                result[index].push({ type: exports.HighLightType.Text, value: currentText });
                            }
                        }
                        result[index].push({ type: exports.HighLightType.UserId, value: match[2] });
                        lastIndex = textHighlightRegex.lastIndex;
                    }
                    else if (isPhone(match[0])) {
                        var startIndex = match[9] ? match.index + 1 : match.index;
                        if (startIndex > lastIndex) {
                            var currentText = line.substring(lastIndex, startIndex);
                            if (currentText.length >= 1) {
                                result[index].push({ type: exports.HighLightType.Text, value: currentText });
                            }
                        }
                        result[index].push({ type: exports.HighLightType.Phone, value: match[10] });
                        lastIndex = match[15] ? textHighlightRegex.lastIndex - 1 : textHighlightRegex.lastIndex;
                    }
                    else {
                        if (match.index > lastIndex) {
                            var currentText = line.substring(lastIndex, match.index);
                            if (currentText.length > 1) {
                                result[index].push({ type: exports.HighLightType.Text, value: currentText });
                            }
                        }
                        result[index].push({ type: exports.HighLightType.Email, value: match[0] });
                        lastIndex = textHighlightRegex.lastIndex;
                    }
                }
                if (lastIndex <= line.length - 1) {
                    result[index].push({ type: exports.HighLightType.Text, value: line.substring(lastIndex) });
                }
            }
            else {
                result[index].push({ type: exports.HighLightType.Text, value: '\n' });
            }
        });
        return {
            parsedText: result,
            previewUrl: url
        };
    };
    var mimeTypes = {
        '3gp': 'video/3gpp',
        a: 'application/octet-stream',
        ai: 'application/postscript',
        aif: 'audio/x-aiff',
        aiff: 'audio/x-aiff',
        asc: 'application/pgp-signature',
        asf: 'video/x-ms-asf',
        asm: 'text/x-asm',
        asx: 'video/x-ms-asf',
        atom: 'application/atom+xml',
        au: 'audio/basic',
        avi: 'video/x-msvideo',
        bat: 'application/x-msdownload',
        bin: 'application/octet-stream',
        bmp: 'image/bmp',
        bz2: 'application/x-bzip2',
        c: 'text/x-csrc',
        cab: 'application/vnd.ms-cab-compressed',
        can: 'application/candor',
        cc: 'text/x-c++src',
        chm: 'application/vnd.ms-htmlhelp',
        class: 'application/octet-stream',
        com: 'application/x-msdownload',
        conf: 'text/plain',
        cpp: 'text/x-c',
        crt: 'application/x-x509-ca-cert',
        css: 'text/css',
        csv: 'text/csv',
        cxx: 'text/x-c',
        deb: 'application/x-debian-package',
        der: 'application/x-x509-ca-cert',
        diff: 'text/x-diff',
        djv: 'image/vnd.djvu',
        djvu: 'image/vnd.djvu',
        dll: 'application/x-msdownload',
        dmg: 'application/octet-stream',
        doc: 'application/msword',
        dot: 'application/msword',
        dtd: 'application/xml-dtd',
        dvi: 'application/x-dvi',
        ear: 'application/java-archive',
        eml: 'message/rfc822',
        eps: 'application/postscript',
        exe: 'application/x-msdownload',
        f: 'text/x-fortran',
        f77: 'text/x-fortran',
        f90: 'text/x-fortran',
        flv: 'video/x-flv',
        for: 'text/x-fortran',
        gem: 'application/octet-stream',
        gemspec: 'text/x-script.ruby',
        gif: 'image/gif',
        gyp: 'text/x-script.python',
        gypi: 'text/x-script.python',
        gz: 'application/x-gzip',
        h: 'text/x-chdr',
        hh: 'text/x-c++hdr',
        heic: 'image/heic',
        heif: 'image/heif',
        hevc: 'video/mp4',
        htm: 'text/html',
        html: 'text/html',
        ico: 'image/vnd.microsoft.icon',
        ics: 'text/calendar',
        ifb: 'text/calendar',
        iso: 'application/octet-stream',
        jpeg: 'image/jpeg',
        jpg: 'image/jpeg',
        js: 'application/javascript',
        json: 'application/json',
        less: 'text/css',
        log: 'text/plain',
        lua: 'text/x-script.lua',
        luac: 'application/x-bytecode.lua',
        m3u: 'audio/x-mpegurl',
        m4v: 'video/mp4',
        man: 'text/troff',
        manifest: 'text/cache-manifest',
        markdown: 'text/x-markdown',
        mathml: 'application/mathml+xml',
        mbox: 'application/mbox',
        mdoc: 'text/troff',
        md: 'text/x-markdown',
        me: 'text/troff',
        mid: 'audio/midi',
        midi: 'audio/midi',
        mime: 'message/rfc822',
        mml: 'application/mathml+xml',
        mng: 'video/x-mng',
        mov: 'video/quicktime',
        mp3: 'audio/mpeg',
        mp4: 'video/mp4',
        mp4v: 'video/mp4',
        mpeg: 'video/mpeg',
        mpg: 'video/mpeg',
        ms: 'text/troff',
        msi: 'application/x-msdownload',
        odp: 'application/vnd.oasis.opendocument.presentation',
        ods: 'application/vnd.oasis.opendocument.spreadsheet',
        odt: 'application/vnd.oasis.opendocument.text',
        ogg: 'application/ogg',
        p: 'text/x-pascal',
        pas: 'text/x-pascal',
        pbm: 'image/x-portable-bitmap',
        pdf: 'application/pdf',
        pem: 'application/x-x509-ca-cert',
        pgm: 'image/x-portable-graymap',
        pgp: 'application/pgp-encrypted',
        pkg: 'application/octet-stream',
        png: 'image/png',
        pnm: 'image/x-portable-anymap',
        ppm: 'image/x-portable-pixmap',
        pps: 'application/vnd.ms-powerpoint',
        ppt: 'application/vnd.ms-powerpoint',
        ps: 'application/postscript',
        psd: 'image/vnd.adobe.photoshop',
        qt: 'video/quicktime',
        ra: 'audio/x-pn-realaudio',
        ram: 'audio/x-pn-realaudio',
        rar: 'application/x-rar-compressed',
        rdf: 'application/rdf+xml',
        roff: 'text/troff',
        rss: 'application/rss+xml',
        rtf: 'application/rtf',
        s: 'text/x-asm',
        sgm: 'text/sgml',
        sgml: 'text/sgml',
        sh: 'application/x-sh',
        sig: 'application/pgp-signature',
        snd: 'audio/basic',
        so: 'application/octet-stream',
        svg: 'image/svg+xml',
        svgz: 'image/svg+xml',
        swf: 'application/x-shockwave-flash',
        t: 'text/troff',
        tar: 'application/x-tar',
        tbz: 'application/x-bzip-compressed-tar',
        tci: 'application/x-topcloud',
        tcl: 'application/x-tcl',
        tex: 'application/x-tex',
        texi: 'application/x-texinfo',
        texinfo: 'application/x-texinfo',
        text: 'text/plain',
        tif: 'image/tiff',
        tiff: 'image/tiff',
        torrent: 'application/x-bittorrent',
        tr: 'text/troff',
        ttf: 'application/x-font-ttf',
        txt: 'text/plain',
        vcf: 'text/x-vcard',
        vcs: 'text/x-vcalendar',
        war: 'application/java-archive',
        wav: 'audio/x-wav',
        webm: 'video/webm',
        wma: 'audio/x-ms-wma',
        wmv: 'video/x-ms-wmv',
        wmx: 'video/x-ms-wmx',
        wrl: 'model/vrml',
        wsdl: 'application/wsdl+xml',
        xbm: 'image/x-xbitmap',
        xhtml: 'application/xhtml+xml',
        xls: 'application/vnd.ms-excel',
        xml: 'application/xml',
        zip: 'application/zip'
    };

    var ConversationMapper = /** @class */ (function () {
        function ConversationMapper() {
        }
        ConversationMapper.toDomainEntity = function (conversation) {
            var id = conversation.id, lastReadMessageId = conversation.lastReadMessageId, lastMessage = conversation.lastMessage, muteUntil = conversation.muteUntil, pinnedAt = conversation.pinnedAt, badge = conversation.badge, type = conversation.type, createdAt = conversation.createdAt, updatedAt = conversation.updatedAt, isOwner = conversation.isOwner, startAfter = conversation.startAfter, visible = conversation.visible, endBefore = conversation.endBefore, deletedFromConversationAt = conversation.deletedFromConversationAt, serverCreated = conversation.serverCreated, serverManaged = conversation.serverManaged;
            var common = {
                id: id,
                lastReadMessageId: lastReadMessageId,
                lastMessage: lastMessage ? MessageMapper.toDomainEntity(lastMessage) : null,
                muteUntil: muteUntil ? new Date(muteUntil) : null,
                pinnedAt: pinnedAt ? new Date(pinnedAt) : null,
                createdAt: new Date(createdAt),
                updatedAt: new Date(updatedAt),
                startAfter: startAfter ? new Date(startAfter) : null,
                endBefore: endBefore ? new Date(endBefore) : null,
                deletedFromConversationAt: deletedFromConversationAt
                    ? new Date(deletedFromConversationAt)
                    : null,
                badge: badge ? badge : 0,
                type: type,
                isOwner: isOwner,
                visible: visible,
                serverCreated: serverCreated,
                serverManaged: serverManaged
            };
            if (conversation.type === exports.ConversationType.Group) {
                var group = conversation.group;
                return __assign(__assign({}, common), { type: exports.ConversationType.Group, group: __assign(__assign({}, group), { picture: group.picture ? MediaMapper.toDomainEntity(group.picture) : null }) });
            }
            var user = conversation.user;
            return __assign(__assign({}, common), { type: exports.ConversationType.Individual, user: __assign(__assign({}, user), { picture: user.picture ? MediaMapper.toDomainEntity(user.picture) : null }) });
        };
        ConversationMapper.toDomainEntities = function (conversations) {
            return conversations.map(function (conversation) { return ConversationMapper.toDomainEntity(conversation); });
        };
        return ConversationMapper;
    }());
    var MessageMapper = /** @class */ (function () {
        function MessageMapper() {
        }
        MessageMapper.toDomainEntity = function (message) {
            var id = message.id, conversationId = message.conversationId, sender = message.sender, type = message.type, text = message.text, sequence = message.sequence, systemMessage = message.systemMessage, file = message.file, createdAt = message.createdAt, reactions = message.reactions, refUrl = message.refUrl, deletedAt = message.deletedAt;
            var parsedResult = parseText(text);
            return {
                id: id,
                conversationId: conversationId,
                sender: sender,
                type: type,
                text: text,
                deletedAt: deletedAt ? new Date(deletedAt) : null,
                sequence: sequence,
                systemMessage: systemMessage ? SystemMessageMapper.toDomainEntity(systemMessage) : null,
                createdAt: new Date(createdAt),
                reactions: reactions,
                sent: true,
                parsedText: parsedResult ? parsedResult.parsedText : null,
                file: file ? MediaMapper.toDomainEntity(file) : null,
                refUrl: refUrl,
                previewUrl: parsedResult === null || parsedResult === void 0 ? void 0 : parsedResult.previewUrl
            };
        };
        MessageMapper.toDomainEntities = function (messages) {
            return messages.map(function (value) { return MessageMapper.toDomainEntity(value); });
        };
        return MessageMapper;
    }());
    var UserMapper = /** @class */ (function () {
        function UserMapper() {
        }
        UserMapper.toDomainEntity = function (user) {
            var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
            return {
                id: id,
                firstName: firstName,
                lastName: lastName,
                fullName: firstName + " " + lastName,
                email: email,
                refId: refId,
                username: email,
                disabled: disabled,
                deletedAt: deletedAt ? new Date(deletedAt) : null,
                picture: picture ? MediaMapper.toDomainEntity(picture) : null
            };
        };
        UserMapper.toConversableUserDomainEntity = function (user) {
            var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, isOwner = user.isOwner, deletedFromConversationAt = user.deletedFromConversationAt, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
            return {
                id: id,
                firstName: firstName,
                lastName: lastName,
                username: email,
                fullName: firstName + " " + lastName,
                email: email,
                picture: picture ? MediaMapper.toDomainEntity(picture) : null,
                isOwner: isOwner,
                deletedFromConversationAt: deletedFromConversationAt,
                refId: refId,
                disabled: disabled,
                deletedAt: deletedAt ? new Date(deletedAt) : null
            };
        };
        UserMapper.toDomainEntities = function (users) {
            return users.map(UserMapper.toDomainEntity);
        };
        UserMapper.toConversableUserDomainEntities = function (users) {
            return users.map(UserMapper.toConversableUserDomainEntity);
        };
        UserMapper.toSelfDomainEntity = function (user) {
            var id = user.id, firstName = user.firstName, lastName = user.lastName, picture = user.picture, email = user.email, muteAllUntil = user.muteAllUntil, refId = user.refId, disabled = user.disabled, deletedAt = user.deletedAt;
            return {
                id: id,
                firstName: firstName,
                lastName: lastName,
                username: email,
                fullName: firstName + " " + lastName,
                email: email,
                refId: refId,
                picture: picture ? MediaMapper.toDomainEntity(picture) : null,
                muteAllUntil: muteAllUntil ? new Date(muteAllUntil) : null,
                disabled: disabled,
                deletedAt: deletedAt ? new Date(deletedAt) : null
            };
        };
        return UserMapper;
    }());
    var MediaMapper = /** @class */ (function () {
        function MediaMapper() {
        }
        MediaMapper.toDomainEntity = function (media) {
            var urlPath = media.urlPath, fileSize = media.fileSize, fileType = media.fileType, extension = media.extension, filename = media.filename, mimeType = media.mimeType, encoding = media.encoding, imageInfo = media.imageInfo, videoInfo = media.videoInfo;
            return {
                urlPath: urlPath,
                fileSize: fileSize,
                fileType: fileType,
                extension: extension,
                filename: filename,
                mimeType: mimeType,
                encoding: encoding,
                imageInfo: imageInfo,
                videoInfo: !videoInfo
                    ? null
                    : {
                        duration: videoInfo.duration,
                        thumbnail: videoInfo.thumbnail ? MediaMapper.toDomainEntity(videoInfo.thumbnail) : null
                    }
            };
        };
        return MediaMapper;
    }());
    var SystemMessageMapper = /** @class */ (function () {
        function SystemMessageMapper() {
        }
        SystemMessageMapper.toDomainEntity = function (systemMessage) {
            var systemMessageType = systemMessage.systemMessageType, id = systemMessage.id, refValue = systemMessage.refValue, actorCount = systemMessage.actorCount, initiator = systemMessage.initiator, actors = systemMessage.actors, text = systemMessage.text;
            return {
                systemMessageType: systemMessageType,
                id: id,
                refValue: refValue,
                actorCount: actorCount,
                initiator: initiator,
                actors: actors,
                text: text
            };
        };
        return SystemMessageMapper;
    }());
    var ReactionMapper = /** @class */ (function () {
        function ReactionMapper() {
        }
        ReactionMapper.toDomianEntity = function (reactions) {
            var conversationReactions = {};
            reactions.forEach(function (item) {
                var messageId = item.messageId;
                var reactionTypesArray = Array.isArray(item.type) ? item.type : [item.type];
                if (!conversationReactions[messageId]) {
                    conversationReactions[messageId] = [];
                }
                var currentReactionTypes = conversationReactions[messageId] || [];
                var reactionSet = new Set(__spreadArrays(currentReactionTypes, reactionTypesArray));
                conversationReactions[messageId] = Array.from(reactionSet);
            });
            return conversationReactions;
        };
        return ReactionMapper;
    }());

    var ChatKit = /** @class */ (function () {
        function ChatKit() {
        }
        /**
         * Helper methods
         */
        /**
         *
         * @param cb - Callback to be executed only if ChatKit is initialized and subscribed to
         */
        ChatKit.guard = function (cb) {
            return __awaiter(this, void 0, void 0, function () {
                var res, e_1;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (!ChatKit._initialized) {
                                return [2 /*return*/, neverthrow.err(new exports.SayHey.Error('ChatKit has not been initialized!', exports.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                            }
                            if (!ChatKit._subscribed) {
                                return [2 /*return*/, neverthrow.err(new exports.SayHey.Error('ChatKit events have not been subscribed to!', exports.SayHey.ErrorType.InternalErrorType.Uninitialized))];
                            }
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, cb()];
                        case 2:
                            res = _a.sent();
                            if (res instanceof exports.SayHey.Error) {
                                return [2 /*return*/, neverthrow.err(res)];
                            }
                            return [2 /*return*/, neverthrow.ok(res)];
                        case 3:
                            e_1 = _a.sent();
                            if (e_1 instanceof exports.SayHey.Error) {
                                return [2 /*return*/, neverthrow.err(e_1)];
                            }
                            return [2 /*return*/, neverthrow.err(new exports.SayHey.Error('Unknown error', exports.SayHey.ErrorType.InternalErrorType.Unknown))];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        };
        ChatKit.exchangeTokenOnce = function () {
            return __awaiter(this, void 0, void 0, function () {
                var response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            if (ChatKit.exchangeTokenPromise) {
                                return [2 /*return*/, ChatKit.exchangeTokenPromise];
                            }
                            ChatKit.exchangeTokenPromise = ChatKit.exchangeToken();
                            return [4 /*yield*/, ChatKit.exchangeTokenPromise];
                        case 1:
                            response = _a.sent();
                            ChatKit.exchangeTokenPromise = null;
                            return [2 /*return*/, response];
                    }
                });
            });
        };
        ChatKit.isSocketConnected = function () {
            return PublisherKitClient.isConnected();
        };
        /**
         *
         * @param cb - Callback to be called only when user is authenticated and will try to refresh token if access token expired
         */
        ChatKit.guardWithAuth = function (cb) {
            return __awaiter(this, void 0, void 0, function () {
                var currentLoginId, res, response;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            currentLoginId = ChatKit.loginId;
                            return [4 /*yield*/, ChatKit.guard(cb)];
                        case 1:
                            res = _a.sent();
                            if (!res.isErr()) return [3 /*break*/, 4];
                            if (!(!ChatKit.disableAutoRefreshToken &&
                                res.error.type === exports.SayHey.ErrorType.AuthErrorType.TokenExpiredType)) return [3 /*break*/, 4];
                            if (!(currentLoginId === ChatKit.loginId)) return [3 /*break*/, 3];
                            return [4 /*yield*/, ChatKit.exchangeTokenOnce()];
                        case 2:
                            response = _a.sent();
                            if (response.isOk()) {
                                return [2 /*return*/, ChatKit.guard(cb)];
                            }
                            ChatKit.signOut();
                            return [3 /*break*/, 4];
                        case 3: 
                        // If token has been exchanged, use the new credentials to make the request again
                        return [2 /*return*/, ChatKit.guard(cb)];
                        case 4: return [2 /*return*/, res];
                    }
                });
            });
        };
        /**
         *
         * @param accessToken - access token returned from SayHey that will be used to verify user with SayHey
         * @param publisherToken - publisher token returned from SayHey that will be used to verify Publisher access
         * @param refreshToken - token returned from SayHey that will be used to refresh access token when it expired
         */
        ChatKit.updateAuthDetails = function (accessToken, publisherToken, refreshToken) {
            return __awaiter(this, void 0, void 0, function () {
                var id;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            ChatKit.accessToken = accessToken;
                            ChatKit.publisherToken = publisherToken;
                            ChatKit.refreshToken = refreshToken;
                            id = accessToken ? decode(accessToken).id : '';
                            if (publisherToken && accessToken && refreshToken) {
                                ChatKit.loginId += 1;
                                ChatKit.httpClient = httpClient.createWithAuth({
                                    apiKey: ChatKit.apiKey,
                                    baseURL: ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/apps/" + ChatKit.appId,
                                    accessToken: ChatKit.accessToken
                                });
                                PublisherKitClient.updateAccessParams({
                                    token: publisherToken,
                                    userId: id
                                });
                                if (!PublisherKitClient.isConnected()) {
                                    PublisherKitClient.connect();
                                }
                            }
                            return [4 /*yield*/, ChatKit.authDetailChangeListener({
                                    userId: id,
                                    accessToken: accessToken,
                                    publisherToken: publisherToken,
                                    refreshToken: refreshToken
                                })];
                        case 1:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        /**
         *
         * @param urlPath - url path returned from SayHey
         */
        ChatKit.getMediaSourceDetails = function (urlPath) {
            var _a;
            return {
                uri: "" + ChatKit.serverUrl + urlPath,
                method: 'GET',
                headers: (_a = {},
                    _a[ApiKeyName] = ChatKit.apiKey,
                    _a.Authorization = "Bearer " + ChatKit.accessToken,
                    _a.Accept = 'image/*, video/*, audio/*',
                    _a)
            };
        };
        ChatKit.getMediaDirectUrl = function (params) {
            return __awaiter(this, void 0, void 0, function () {
                function getMediaDirectUrl() {
                    return __awaiter(this, void 0, void 0, function () {
                        var queryString, data;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    queryString = {
                                        noRedirect: true
                                    };
                                    if (params.type === 'image') {
                                        queryString.size = params.size;
                                    }
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: "" + ChatKit.serverUrl + params.urlPath,
                                            method: 'GET',
                                            params: queryString
                                        })];
                                case 1:
                                    data = (_a.sent()).data;
                                    return [2 /*return*/, data.url];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getMediaDirectUrl)];
                });
            });
        };
        ChatKit.uploadFile = function (fileUri, fileName, progress, thumbnailId) {
            return __awaiter(this, void 0, void 0, function () {
                function uploadFile() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, formData, extension, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UploadFile, Endpoint = _a.Endpoint, Method = _a.Method;
                                    formData = new FormData();
                                    if (fileUri instanceof Blob) {
                                        formData.append('file', fileUri, fileName);
                                        if (thumbnailId) {
                                            formData.append('thumbnailId', thumbnailId);
                                        }
                                    }
                                    else {
                                        extension = fileName.split('.').pop();
                                        if (extension) {
                                            formData.append('file', {
                                                uri: fileUri,
                                                name: fileName,
                                                type: mimeTypes[extension.toLowerCase()]
                                            });
                                            if (thumbnailId) {
                                                formData.append('thumbnailId', thumbnailId);
                                            }
                                        }
                                        else {
                                            return [2 /*return*/, new exports.SayHey.Error('Invalid file')];
                                        }
                                    }
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: formData,
                                            onUploadProgress: progress
                                                ? function (event) {
                                                    var loaded = event.loaded, total = event.total;
                                                    progress((loaded / total) * 0.95);
                                                }
                                                : undefined
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    progress === null || progress === void 0 ? void 0 : progress(1);
                                    return [2 /*return*/, data.id];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(uploadFile)];
                });
            });
        };
        ChatKit.uploadImage = function (fileUri, fileName) {
            var _a;
            return __awaiter(this, void 0, void 0, function () {
                var extension, isSupportedImage;
                return __generator(this, function (_b) {
                    extension = (_a = fileName
                        .split('.')
                        .pop()) === null || _a === void 0 ? void 0 : _a.toLowerCase();
                    isSupportedImage = mimeTypes[extension || ''].split('/')[0] === 'image';
                    if (isSupportedImage) {
                        return [2 /*return*/, ChatKit.uploadFile(fileUri, fileName)];
                    }
                    return [2 /*return*/, neverthrow.err(new exports.SayHey.Error('Please select a valid image!', exports.SayHey.ErrorType.InternalErrorType.BadData))];
                });
            });
        };
        /**
         * Setup methods
         */
        ChatKit.initialize = function (params) {
            var sayheyAppId = params.sayheyAppId, sayheyApiKey = params.sayheyApiKey, publisherApiKey = params.publisherApiKey, publisherAppId = params.publisherAppId, sayheyUrl = params.sayheyUrl, publisherUrl = params.publisherUrl, messageUpdateInterval = params.messageUpdateInterval, disableAutoRefreshToken = params.disableAutoRefreshToken, disableUserDeletedHandler = params.disableUserDeletedHandler, disableUserDisabledHandler = params.disableUserDisabledHandler;
            ChatKit.serverUrl = sayheyUrl;
            var baseURL = ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/apps/" + sayheyAppId;
            ChatKit.apiKey = sayheyApiKey;
            ChatKit.appId = sayheyAppId;
            ChatKit.disableAutoRefreshToken = disableAutoRefreshToken;
            ChatKit.disableUserDisabledHandler = disableUserDisabledHandler;
            ChatKit.disableUserDeletedHandler = disableUserDeletedHandler;
            ChatKit.httpClient = httpClient.createWithoutAuth({
                apiKey: ChatKit.apiKey,
                baseURL: baseURL
            });
            PublisherKitClient.initialize({
                appId: publisherAppId,
                clientApiKey: publisherApiKey,
                publisherUrl: publisherUrl
            });
            if (ChatKit.timerHandle !== null) {
                clearInterval(ChatKit.timerHandle);
            }
            ChatKit.timerHandle = setInterval(function () {
                ChatKit.isSendingReadReceipt = true;
                var copy = __assign({}, ChatKit.readMessageMap);
                ChatKit.readMessageMap = {};
                ChatKit.isSendingReadReceipt = false;
                for (var conversationId in copy) {
                    var messageId = copy[conversationId];
                    ChatKit.markMessageRead(messageId);
                }
            }, messageUpdateInterval || MESSAGE_READ_UPDATE_INTERVAL);
            ChatKit._initialized = true;
        };
        ChatKit.disconnectPublisher = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    PublisherKitClient.disconnect();
                    return [2 /*return*/];
                });
            });
        };
        ChatKit.reconnectPublisher = function () {
            return __awaiter(this, void 0, void 0, function () {
                return __generator(this, function (_a) {
                    PublisherKitClient.connect();
                    return [2 /*return*/];
                });
            });
        };
        /**
         * Token APIs
         */
        /**
         *
         * @param email - user email for SayHey
         * @param password - user password for SayHey
         *
         */
        ChatKit.signIn = function (email, password) {
            return __awaiter(this, void 0, void 0, function () {
                function signInCallback() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.SignIn, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                email: email,
                                                password: password
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    ChatKit.isUserAuthenticated = true;
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guard(signInCallback)];
                });
            });
        };
        ChatKit.signInWithToken = function (accessToken, publisherToken, refreshToken) {
            return __awaiter(this, void 0, void 0, function () {
                function signInWithTokenCallback() {
                    return __awaiter(this, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 1:
                                    _a.sent();
                                    ChatKit.isUserAuthenticated = true;
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guard(signInWithTokenCallback)];
                });
            });
        };
        ChatKit.signUp = function (email, password, firstName, lastName) {
            return __awaiter(this, void 0, void 0, function () {
                function signUp() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data, accessToken, refreshToken, publisherToken;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.SignUp, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                email: email,
                                                password: password,
                                                firstName: firstName,
                                                lastName: lastName
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    accessToken = data.accessToken, refreshToken = data.refreshToken, publisherToken = data.publisherToken;
                                    return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _b.sent();
                                    ChatKit.isUserAuthenticated = true;
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guard(signUp)];
                });
            });
        };
        ChatKit.signOut = function () {
            return __awaiter(this, void 0, void 0, function () {
                var unregisteringPushNotification;
                var _this = this;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            PublisherKitClient.disconnect();
                            ChatKit.isUserAuthenticated = false;
                            ChatKit.loginId = 0;
                            unregisteringPushNotification = function () { return __awaiter(_this, void 0, void 0, function () {
                                var result;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, ChatKit.unregisterForPushNotification()];
                                        case 1:
                                            result = _a.sent();
                                            if (result.isOk() ||
                                                (result.isErr() && result.error.statusCode && result.error.statusCode < 500)) {
                                                return [2 /*return*/];
                                            }
                                            setTimeout(unregisteringPushNotification, 3000);
                                            return [2 /*return*/];
                                    }
                                });
                            }); };
                            return [4 /*yield*/, unregisteringPushNotification()];
                        case 1:
                            _a.sent();
                            return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
        ChatKit.exchangeToken = function () {
            return __awaiter(this, void 0, void 0, function () {
                function exchangeToken() {
                    var _a, _b;
                    return __awaiter(this, void 0, void 0, function () {
                        var data, refreshToken, accessToken, publisherToken, e_2;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    _c.trys.push([0, 6, , 8]);
                                    return [4 /*yield*/, axios({
                                            method: exports.HttpMethod.Post,
                                            url: ChatKit.serverUrl + "/" + ChatKit.apiVersion + "/tokens/exchange",
                                            data: {
                                                refreshToken: ChatKit.refreshToken
                                            }
                                        })];
                                case 1:
                                    data = (_c.sent()).data;
                                    refreshToken = data.refreshToken, accessToken = data.accessToken, publisherToken = data.publisherToken;
                                    if (!(refreshToken && accessToken)) return [3 /*break*/, 3];
                                    return [4 /*yield*/, ChatKit.updateAuthDetails(accessToken, publisherToken, refreshToken)];
                                case 2:
                                    _c.sent();
                                    ChatKit.isUserAuthenticated = true;
                                    return [3 /*break*/, 5];
                                case 3: return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                                case 4:
                                    _c.sent();
                                    (_a = ChatKit.sessionExpiredListener) === null || _a === void 0 ? void 0 : _a.call(ChatKit);
                                    ChatKit.isUserAuthenticated = false;
                                    _c.label = 5;
                                case 5: return [3 /*break*/, 8];
                                case 6:
                                    e_2 = _c.sent();
                                    return [4 /*yield*/, ChatKit.updateAuthDetails('', '', '')];
                                case 7:
                                    _c.sent();
                                    (_b = ChatKit.sessionExpiredListener) === null || _b === void 0 ? void 0 : _b.call(ChatKit);
                                    ChatKit.isUserAuthenticated = false;
                                    return [3 /*break*/, 8];
                                case 8: return [2 /*return*/, ChatKit.isUserAuthenticated];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guard(exchangeToken)];
                });
            });
        };
        ChatKit.changePassword = function (currentPassword, newPassword) {
            return __awaiter(this, void 0, void 0, function () {
                function changePassword() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.ChangePassword, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                currentPassword: currentPassword,
                                                newPassword: newPassword
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(changePassword)];
                });
            });
        };
        /**
         * User APIs
         */
        ChatKit.getReachableUsers = function (_a) {
            var _b = _a === void 0 ? {} : _a, searchString = _b.searchString, limit = _b.limit, nextCursor = _b.nextCursor, includeRefIdInSearch = _b.includeRefIdInSearch;
            return __awaiter(this, void 0, void 0, function () {
                function getReachableUsers() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, params, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint;
                                    params = {
                                        search: searchString || '',
                                        includeRefIdInSearch: includeRefIdInSearch || false
                                    };
                                    if (limit) {
                                        params.limit = limit;
                                    }
                                    if (nextCursor) {
                                        params.offset = nextCursor;
                                    }
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: url,
                                            method: Method,
                                            params: params
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    });
                }
                return __generator(this, function (_c) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getReachableUsers)];
                });
            });
        };
        ChatKit.updateUserProfileImage = function (fileUri, fileName) {
            return __awaiter(this, void 0, void 0, function () {
                function updateUserProfileImage() {
                    return __awaiter(this, void 0, void 0, function () {
                        var fileIdResponse, _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0: return [4 /*yield*/, ChatKit.uploadFile(fileUri, fileName)];
                                case 1:
                                    fileIdResponse = _b.sent();
                                    if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                    _a = Routes.UpdateUserProfileImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                profileImageId: fileIdResponse.value
                                            }
                                        })];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                                case 3: return [2 /*return*/, false];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(updateUserProfileImage)];
                });
            });
        };
        ChatKit.updateUserProfileInfo = function (options) {
            return __awaiter(this, void 0, void 0, function () {
                function updateUserProfileInfo() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UpdateUserProfileInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: options
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(updateUserProfileInfo)];
                });
            });
        };
        ChatKit.getUserDetails = function () {
            return __awaiter(this, void 0, void 0, function () {
                function getUserDetails() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetUserProfile, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, UserMapper.toSelfDomainEntity(data)];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getUserDetails)];
                });
            });
        };
        ChatKit.muteAllNotifications = function () {
            return __awaiter(this, void 0, void 0, function () {
                function muteAllNotifications() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.MuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(muteAllNotifications)];
                });
            });
        };
        ChatKit.unmuteAllNotifications = function () {
            return __awaiter(this, void 0, void 0, function () {
                function unmuteAllNotifications() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UnmuteAllNotifications, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(unmuteAllNotifications)];
                });
            });
        };
        /**
         * Conversation APIs
         */
        ChatKit.getConversations = function () {
            return __awaiter(this, void 0, void 0, function () {
                function getConversations() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ConversationMapper.toDomainEntities(data)];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getConversations)];
                });
            });
        };
        ChatKit.getConversationDetails = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function getConversationDetails() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetConversationDetails, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getConversationDetails)];
                });
            });
        };
        ChatKit.checkForExistingConversation = function (withUserId) {
            return __awaiter(this, void 0, void 0, function () {
                function checkForExistingConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetExistingConversationWithUser, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(withUserId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(checkForExistingConversation)];
                });
            });
        };
        ChatKit.createIndividualConversation = function (withUserId) {
            return __awaiter(this, void 0, void 0, function () {
                function createIndividualConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.CreateIndividualConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                memberId: withUserId
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(createIndividualConversation)];
                });
            });
        };
        ChatKit.createGroupConversation = function (params) {
            return __awaiter(this, void 0, void 0, function () {
                function createGroupConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, file, groupName, userIds, fileIdResponse, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.CreateGroupConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    file = params.file, groupName = params.groupName, userIds = params.userIds;
                                    return [4 /*yield*/, ChatKit.uploadFile(file.path, file.fileName)];
                                case 1:
                                    fileIdResponse = _b.sent();
                                    if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                groupName: groupName,
                                                groupImageId: fileIdResponse.value,
                                                memberIds: userIds
                                            }
                                        })];
                                case 2:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, ConversationMapper.toDomainEntity(data)];
                                case 3: return [2 /*return*/, fileIdResponse.error];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(createGroupConversation)];
                });
            });
        };
        ChatKit.muteConversation = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function muteConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.MuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data.muteUntil];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(muteConversation)];
                });
            });
        };
        ChatKit.unmuteConversation = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function unmuteConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UnmuteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(unmuteConversation)];
                });
            });
        };
        ChatKit.pinConversation = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function pinConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.PinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data.pinnedAt];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(pinConversation)];
                });
            });
        };
        ChatKit.unpinConversation = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function unpinConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UnpinConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(unpinConversation)];
                });
            });
        };
        ChatKit.deleteConversation = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                function deleteConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.DeleteConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(deleteConversation)];
                });
            });
        };
        ChatKit.deleteAllConversations = function () {
            return __awaiter(this, void 0, void 0, function () {
                function deleteAllConversations() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.DeleteAllConversations, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(deleteAllConversations)];
                });
            });
        };
        ChatKit.updateGroupImage = function (conversationId, file) {
            return __awaiter(this, void 0, void 0, function () {
                function updateGroupImage() {
                    return __awaiter(this, void 0, void 0, function () {
                        var fileIdResponse, _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0: return [4 /*yield*/, ChatKit.uploadFile(file.path, file.fileName)];
                                case 1:
                                    fileIdResponse = _b.sent();
                                    if (!fileIdResponse.isOk()) return [3 /*break*/, 3];
                                    _a = Routes.UpdateGroupImage, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                groupImageId: fileIdResponse.value
                                            }
                                        })];
                                case 2:
                                    _b.sent();
                                    return [2 /*return*/, true];
                                case 3: return [2 /*return*/, false];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(updateGroupImage)];
                });
            });
        };
        ChatKit.updateGroupInfo = function (conversationId, groupName) {
            return __awaiter(this, void 0, void 0, function () {
                function updateGroupInfo() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UpdateGroupInfo, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                groupName: groupName
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(updateGroupInfo)];
                });
            });
        };
        ChatKit.getConversationUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, type = _a.type, includeRefIdInSearch = _a.includeRefIdInSearch;
            return __awaiter(this, void 0, void 0, function () {
                function getConversationUsers() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetConversationUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            params: {
                                                type: type || exports.ConversationUserQueryType.Current,
                                                offset: nextCursor,
                                                limit: limit,
                                                search: searchString,
                                                includeRefIdInSearch: includeRefIdInSearch || false
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toConversableUserDomainEntities(data.results) })];
                            }
                        });
                    });
                }
                return __generator(this, function (_b) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getConversationUsers)];
                });
            });
        };
        ChatKit.getConversationReachableUsers = function (_a) {
            var conversationId = _a.conversationId, searchString = _a.searchString, limit = _a.limit, nextCursor = _a.nextCursor, includeRefIdInSearch = _a.includeRefIdInSearch;
            return __awaiter(this, void 0, void 0, function () {
                function getConversationReachableUsers() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, url, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetConversationReachableUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    url = Endpoint(conversationId);
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: url,
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                search: searchString,
                                                offset: nextCursor,
                                                includeRefIdInSearch: includeRefIdInSearch || false
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, __assign(__assign({}, data), { results: UserMapper.toDomainEntities(data.results) })];
                            }
                        });
                    });
                }
                return __generator(this, function (_b) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getConversationReachableUsers)];
                });
            });
        };
        ChatKit.getConversationBasicUsers = function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var conversationId, _a, type;
                return __generator(this, function (_b) {
                    conversationId = params.conversationId, _a = params.type, type = _a === void 0 ? 'current' : _a;
                    return [2 /*return*/, ChatKit.guard(function getConversationUsers() {
                            return __awaiter(this, void 0, void 0, function () {
                                var _a, Endpoint, Method, data;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            _a = Routes.GetConversationBasicUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                            return [4 /*yield*/, ChatKit.httpClient({
                                                    url: Endpoint(conversationId),
                                                    method: Method,
                                                    params: {
                                                        type: type
                                                    }
                                                })];
                                        case 1:
                                            data = (_b.sent()).data;
                                            return [2 /*return*/, data];
                                    }
                                });
                            });
                        })];
                });
            });
        };
        ChatKit.addUsersToConversation = function (conversationId, userIds, force) {
            return __awaiter(this, void 0, void 0, function () {
                function addUsersToConversation() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.AddUsersToConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: {
                                                memberIds: userIds
                                            },
                                            params: {
                                                force: force
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(addUsersToConversation)];
                });
            });
        };
        ChatKit.removeUsersFromConversation = function (conversationId, userIds, force) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.RemoveUsersFromConversation, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    memberIds: userIds
                                                },
                                                params: {
                                                    force: force
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.makeConversationOwner = function (conversationId, userId) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.MakeConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId, userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.removeConversationOwner = function (conversationId, userId) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.RemoveConversationOwner, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId, userId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        /**
         * @deprecated The method should not be used
         */
        ChatKit.getUserConversationReactions = function (conversationId) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.GetUserConversationReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, ReactionMapper.toDomianEntity(data)];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.getUserConversationMessagesReactions = function (conversationId, messageIds) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.GetUserConversationMessagesReactions, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                data: {
                                                    messageIds: messageIds
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, ReactionMapper.toDomianEntity(data)];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.getGroupDefaultImageSet = function () {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.GetGroupDefaultImageSet, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint,
                                                method: Method
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, data];
                                }
                            });
                        }); })];
                });
            });
        };
        /**
         * Message APIs
         */
        ChatKit.getMessages = function (conversationId, pivotSequence, after) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data, lastMessageSequenceInBatch, messageListIds, messageList, data, incomingMessageList, _i, incomingMessageList_1, m;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                        if (!!after) return [3 /*break*/, 2];
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    beforeSequence: pivotSequence,
                                                    limit: 20
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        return [2 /*return*/, MessageMapper.toDomainEntities(data)];
                                    case 2:
                                        if (!pivotSequence) {
                                            return [2 /*return*/, []];
                                        }
                                        lastMessageSequenceInBatch = Number.MAX_SAFE_INTEGER;
                                        messageListIds = new Set();
                                        messageList = [];
                                        _b.label = 3;
                                    case 3:
                                        if (!(lastMessageSequenceInBatch > pivotSequence + 1)) return [3 /*break*/, 5];
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    limit: 20,
                                                    beforeSequence: lastMessageSequenceInBatch
                                                }
                                            })];
                                    case 4:
                                        data = (_b.sent()).data;
                                        incomingMessageList = data;
                                        if (incomingMessageList.length === 0) {
                                            return [3 /*break*/, 5];
                                        }
                                        for (_i = 0, incomingMessageList_1 = incomingMessageList; _i < incomingMessageList_1.length; _i++) {
                                            m = incomingMessageList_1[_i];
                                            lastMessageSequenceInBatch = m.sequence;
                                            if (lastMessageSequenceInBatch <= pivotSequence) {
                                                break;
                                            }
                                            if (!messageListIds.has(m.id)) {
                                                messageListIds.add(m.id);
                                                messageList.push(m);
                                            }
                                        }
                                        return [3 /*break*/, 3];
                                    case 5: return [2 /*return*/, MessageMapper.toDomainEntities(messageList)];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.onMessage = function (data) {
            switch (data.event) {
                case exports.Publisher.EventType.NewMessage: {
                    ChatKit.eventListener({
                        event: data.event,
                        payload: {
                            message: MessageMapper.toDomainEntity(data.payload.message),
                            clientRefId: data.payload.clientRefId
                        }
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationMuted: {
                    var _a = data.payload, conversationId = _a.conversationId, muteUntil = _a.conversationMuteDto.muteUntil;
                    ChatKit.eventListener({
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            muteUntil: new Date(muteUntil)
                        }
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationPinned: {
                    var _b = data.payload, conversationId = _b.conversationId, pinnedAt = _b.conversationPinDto.pinnedAt;
                    ChatKit.eventListener({
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            pinnedAt: new Date(pinnedAt)
                        }
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationHideAndClear: {
                    var _c = data.payload, conversationId = _c.conversationId, _d = _c.conversationUserHideClearDto, startAfter = _d.startAfter, visible = _d.visible;
                    ChatKit.eventListener({
                        event: data.event,
                        payload: {
                            conversationId: conversationId,
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationHideAndClearAll: {
                    var _e = data.payload.conversationUserHideClearDto, startAfter = _e.startAfter, visible = _e.visible;
                    ChatKit.eventListener({
                        event: data.event,
                        payload: {
                            visible: visible,
                            startAfter: new Date(startAfter)
                        }
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationGroupInfoChanged: {
                    ChatKit.eventListener({
                        event: data.event,
                        payload: __assign(__assign({}, data.payload), { groupPicture: data.payload.groupPicture
                                ? MediaMapper.toDomainEntity(data.payload.groupPicture)
                                : null })
                    });
                    break;
                }
                case exports.Publisher.EventType.ConversationCreated:
                case exports.Publisher.EventType.ConversationUnpinned:
                case exports.Publisher.EventType.ConversationUnmuted:
                case exports.Publisher.EventType.UserInfoChanged:
                case exports.Publisher.EventType.ConversationGroupOwnerAdded:
                case exports.Publisher.EventType.ConversationGroupOwnerRemoved:
                case exports.Publisher.EventType.ConversationGroupMemberAdded:
                case exports.Publisher.EventType.ConversationGroupMemberRemoved:
                case exports.Publisher.EventType.UserReactedToMessage:
                case exports.Publisher.EventType.UserUnReactedToMessage:
                case exports.Publisher.EventType.MessageReactionsUpdate:
                case exports.Publisher.EventType.MessageDeleted:
                case exports.Publisher.EventType.UserEnabled: {
                    ChatKit.eventListener(data);
                    break;
                }
                case exports.Publisher.EventType.UserDisabled: {
                    ChatKit.eventListener(data);
                    if (!ChatKit.disableUserDisabledHandler) {
                        ChatKit.signOut();
                    }
                    break;
                }
                case exports.Publisher.EventType.UserDeleted: {
                    ChatKit.eventListener(data);
                    if (!ChatKit.disableUserDeletedHandler) {
                        ChatKit.signOut();
                    }
                    break;
                }
            }
        };
        ChatKit.sendMessage = function (params) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var conversationId, clientRefId, _a, Endpoint, Method, payload, _b, file, progress_1, subProgress, fileIdResponse, file, progress_2, subProgress, uri, fileName, thumbnail, thumbnailId, thumbnailIdResponse, videoUploadProgress, fileIdResponse, data;
                            var _c, _d, _e;
                            return __generator(this, function (_f) {
                                switch (_f.label) {
                                    case 0:
                                        conversationId = params.conversationId, clientRefId = params.clientRefId;
                                        _a = Routes.SendMessage, Endpoint = _a.Endpoint, Method = _a.Method;
                                        _b = params.type;
                                        switch (_b) {
                                            case exports.MessageType.Text: return [3 /*break*/, 1];
                                            case exports.MessageType.Emoji: return [3 /*break*/, 1];
                                            case exports.MessageType.Gif: return [3 /*break*/, 2];
                                            case exports.MessageType.Audio: return [3 /*break*/, 3];
                                            case exports.MessageType.Image: return [3 /*break*/, 3];
                                            case exports.MessageType.Document: return [3 /*break*/, 3];
                                            case exports.MessageType.Video: return [3 /*break*/, 5];
                                        }
                                        return [3 /*break*/, 9];
                                    case 1:
                                        {
                                            payload = {
                                                type: params.type,
                                                text: params.text.trim()
                                            };
                                            return [3 /*break*/, 9];
                                        }
                                        _f.label = 2;
                                    case 2:
                                        {
                                            if (!isUrl(params.url)) {
                                                return [2 /*return*/, new exports.SayHey.Error('Invalid URL!', exports.SayHey.ErrorType.InternalErrorType.MissingParameter)];
                                            }
                                            payload = {
                                                type: exports.MessageType.Gif,
                                                refUrl: params.url,
                                                text: (_c = params.text) === null || _c === void 0 ? void 0 : _c.trim()
                                            };
                                            return [3 /*break*/, 9];
                                        }
                                        _f.label = 3;
                                    case 3:
                                        file = params.file, progress_1 = params.progress;
                                        subProgress = progress_1
                                            ? function (percentage) {
                                                progress_1(percentage * 0.9);
                                            }
                                            : undefined;
                                        return [4 /*yield*/, ChatKit.uploadFile(file.uri, file.fileName, subProgress)];
                                    case 4:
                                        fileIdResponse = _f.sent();
                                        if (fileIdResponse.isErr()) {
                                            return [2 /*return*/, fileIdResponse.error];
                                        }
                                        payload = {
                                            type: file.type,
                                            text: (_d = file.text) === null || _d === void 0 ? void 0 : _d.trim(),
                                            fileId: fileIdResponse.value
                                        };
                                        return [3 /*break*/, 9];
                                    case 5:
                                        file = params.file, progress_2 = params.progress;
                                        subProgress = progress_2
                                            ? function (percentage) {
                                                progress_2(percentage * 0.4);
                                            }
                                            : undefined;
                                        uri = file.uri, fileName = file.fileName, thumbnail = file.thumbnail;
                                        thumbnailId = '';
                                        if (!thumbnail) return [3 /*break*/, 7];
                                        return [4 /*yield*/, ChatKit.uploadFile(thumbnail.uri, thumbnail.name, subProgress)];
                                    case 6:
                                        thumbnailIdResponse = _f.sent();
                                        if (thumbnailIdResponse.isErr()) {
                                            return [2 /*return*/, thumbnailIdResponse.error];
                                        }
                                        thumbnailId = thumbnailIdResponse.value;
                                        _f.label = 7;
                                    case 7:
                                        videoUploadProgress = progress_2
                                            ? function (percentage) {
                                                progress_2(percentage * 0.6 + 0.4);
                                            }
                                            : undefined;
                                        return [4 /*yield*/, ChatKit.uploadFile(uri, fileName, videoUploadProgress, thumbnailId)];
                                    case 8:
                                        fileIdResponse = _f.sent();
                                        if (fileIdResponse.isErr()) {
                                            return [2 /*return*/, fileIdResponse.error];
                                        }
                                        payload = {
                                            type: file.type,
                                            text: (_e = file.text) === null || _e === void 0 ? void 0 : _e.trim(),
                                            fileId: fileIdResponse.value
                                        };
                                        _f.label = 9;
                                    case 9: return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(conversationId),
                                            method: Method,
                                            data: payload,
                                            params: {
                                                clientRefId: clientRefId
                                            }
                                        })];
                                    case 10:
                                        data = (_f.sent()).data;
                                        return [2 /*return*/, MessageMapper.toDomainEntity(data)];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.getGalleryMessages = function (conversationId, beforeSequence, limit) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method, data, messages, mediaList;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.GetMessages, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(conversationId),
                                                method: Method,
                                                params: {
                                                    galleryOnly: true,
                                                    limit: limit,
                                                    beforeSequence: beforeSequence
                                                }
                                            })];
                                    case 1:
                                        data = (_b.sent()).data;
                                        messages = MessageMapper.toDomainEntities(data);
                                        mediaList = messages.map(function (message) { return message.file; });
                                        return [2 /*return*/, {
                                                nextCursor: mediaList.length ? messages[messages.length - 1].sequence : -1,
                                                messageList: messages
                                            }];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.setMessageReaction = function (messageId, type) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.SetMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(messageId),
                                                method: Method,
                                                data: {
                                                    type: type
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.removeMessageReaction = function (messageId, type) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.RemoveMessageReaction, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(messageId),
                                                method: Method,
                                                data: {
                                                    type: type
                                                }
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        ChatKit.readMessage = function (params) {
            var conversationId = params.conversationId, messageId = params.messageId, sequence = params.sequence, immediateUpdate = params.immediateUpdate;
            var mark = function () {
                if (Number.isInteger(ChatKit.readMessageSequenceMap[conversationId]) &&
                    ChatKit.readMessageSequenceMap[conversationId] >= sequence) {
                    return;
                }
                if (immediateUpdate) {
                    ChatKit.markMessageRead(messageId).then(function (res) {
                        if (res.isErr()) {
                            ChatKit.readMessageMap[conversationId] = messageId;
                        }
                    });
                }
                else {
                    ChatKit.readMessageMap[conversationId] = messageId;
                }
                ChatKit.readMessageSequenceMap[conversationId] = sequence;
            };
            if (!ChatKit.isSendingReadReceipt) {
                mark();
            }
            else {
                setTimeout(mark, 500);
            }
        };
        ChatKit.markMessageRead = function (messageId) {
            return __awaiter(this, void 0, void 0, function () {
                var _this = this;
                return __generator(this, function (_a) {
                    return [2 /*return*/, ChatKit.guardWithAuth(function () { return __awaiter(_this, void 0, void 0, function () {
                            var _a, Endpoint, Method;
                            return __generator(this, function (_b) {
                                switch (_b.label) {
                                    case 0:
                                        _a = Routes.MarkMessageAsRead, Endpoint = _a.Endpoint, Method = _a.Method;
                                        return [4 /*yield*/, ChatKit.httpClient({
                                                url: Endpoint(messageId),
                                                method: Method
                                            })];
                                    case 1:
                                        _b.sent();
                                        return [2 /*return*/, true];
                                }
                            });
                        }); })];
                });
            });
        };
        /** Push Notification APIs */
        ChatKit.registerForPushNotification = function (instanceId, token) {
            return __awaiter(this, void 0, void 0, function () {
                function registerForPushNotification() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.RegisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                    ChatKit.instanceIdPushNotification = instanceId;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint,
                                            method: Method,
                                            data: {
                                                instanceId: instanceId,
                                                token: token
                                            }
                                        })];
                                case 1:
                                    _b.sent();
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    if (!instanceId || !token) {
                        return [2 /*return*/, neverthrow.err(new exports.SayHey.Error('Invalid instance id or token string!'))];
                    }
                    return [2 /*return*/, ChatKit.guardWithAuth(registerForPushNotification)];
                });
            });
        };
        ChatKit.unregisterForPushNotification = function () {
            return __awaiter(this, void 0, void 0, function () {
                function unregisterForPushNotification() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.UnregisterPushNotification, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(ChatKit.instanceIdPushNotification),
                                            method: Method
                                        })];
                                case 1:
                                    _b.sent();
                                    ChatKit.instanceIdPushNotification = '';
                                    return [2 /*return*/, true];
                            }
                        });
                    });
                }
                return __generator(this, function (_a) {
                    if (!ChatKit.instanceIdPushNotification) {
                        return [2 /*return*/, neverthrow.ok(true)];
                    }
                    return [2 /*return*/, ChatKit.guard(unregisterForPushNotification)];
                });
            });
        };
        ChatKit.getReactedUsers = function (_a) {
            var messageId = _a.messageId, type = _a.type, limit = _a.limit, afterSequence = _a.afterSequence;
            return __awaiter(this, void 0, void 0, function () {
                function getReactedUsers() {
                    return __awaiter(this, void 0, void 0, function () {
                        var _a, Endpoint, Method, data;
                        return __generator(this, function (_b) {
                            switch (_b.label) {
                                case 0:
                                    _a = Routes.GetReactedUsers, Endpoint = _a.Endpoint, Method = _a.Method;
                                    return [4 /*yield*/, ChatKit.httpClient({
                                            url: Endpoint(messageId),
                                            method: Method,
                                            params: {
                                                limit: limit,
                                                afterSequence: afterSequence,
                                                type: type || ''
                                            }
                                        })];
                                case 1:
                                    data = (_b.sent()).data;
                                    return [2 /*return*/, data];
                            }
                        });
                    });
                }
                return __generator(this, function (_b) {
                    return [2 /*return*/, ChatKit.guardWithAuth(getReactedUsers)];
                });
            });
        };
        ChatKit.instanceIdPushNotification = '';
        ChatKit.apiVersion = API_VERSION;
        ChatKit._initialized = false;
        ChatKit._subscribed = false;
        ChatKit.isUserAuthenticated = false;
        ChatKit.readMessageMap = {};
        ChatKit.readMessageSequenceMap = {};
        ChatKit.isSendingReadReceipt = false;
        ChatKit.timerHandle = null;
        ChatKit.exchangeTokenPromise = null;
        ChatKit.loginId = 0;
        ChatKit.addListeners = function (listeners) {
            if (ChatKit._initialized) {
                var onSocketConnect = listeners.onSocketConnect, onSocketDisconnect = listeners.onSocketDisconnect, onSocketError = listeners.onSocketError, onSocketReconnect = listeners.onSocketReconnect, onAuthDetailChange = listeners.onAuthDetailChange, onEvent = listeners.onEvent, onSessionExpired = listeners.onSessionExpired;
                ChatKit._subscribed = true;
                ChatKit.authDetailChangeListener = onAuthDetailChange;
                ChatKit.eventListener = onEvent;
                ChatKit.sessionExpiredListener = onSessionExpired;
                PublisherKitClient.onMessage(ChatKit.onMessage);
                onSocketConnect && PublisherKitClient.onConnect(onSocketConnect);
                onSocketDisconnect && PublisherKitClient.onDisconnect(onSocketDisconnect);
                onSocketError && PublisherKitClient.onError(onSocketError);
                onSocketReconnect && PublisherKitClient.onReconnect(onSocketReconnect);
                return neverthrow.ok(true);
            }
            return neverthrow.err(new exports.SayHey.Error('ChatKit has not been initialized!', exports.SayHey.ErrorType.InternalErrorType.Uninitialized));
        };
        return ChatKit;
    }());

    exports.default = ChatKit;
    exports.isSingleEmoji = isSingleEmoji;
    exports.parseText = parseText;
    exports.ReactionsObject = ReactionsObject;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=sayhey-chat-kit-client.umd.js.map
